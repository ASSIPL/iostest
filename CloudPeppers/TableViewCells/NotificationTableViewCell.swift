//
//  NotificationTableViewCell.swift
//  CloudPeppers
//
//  Created by Allvy on 03/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
@IBOutlet var deleteButton: UIButton!
    
    @IBOutlet weak var displayView: UIView!
    @IBOutlet weak var sideImageView: UIImageView!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var notificationLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
       
        displayView.layer.cornerRadius = 10.0
        displayView.layer.shadowColor = UIColor.gray.cgColor
        displayView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        displayView.layer.shadowRadius = 2.0
        displayView.layer.shadowOpacity = 0.7
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

