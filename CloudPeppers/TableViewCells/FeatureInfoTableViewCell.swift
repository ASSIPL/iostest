//
//  FeatureInfoTableViewCell.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 25/09/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class FeatureInfoTableViewCell: UITableViewCell {
    
    @IBOutlet var detailsFeature: UILabel!
    
    @IBOutlet var imageFeature: UIImageView!
    
    
    @IBOutlet var namefature: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
