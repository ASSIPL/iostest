//
//  OptionsTableViewCell.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 23/04/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class OptionsTableViewCell: UITableViewCell {

    
    @IBOutlet var OptionImage: UIImageView!
    @IBOutlet var optionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
