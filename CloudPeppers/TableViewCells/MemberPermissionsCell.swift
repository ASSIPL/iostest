//
//  MemberPermissionsCell.swift
//  CloudPeppers
//
//  Created by Allvy on 03/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class MemberPermissionsCell: UITableViewCell {

    
    
    @IBOutlet weak var permissionsLabel: UILabel!
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var askButton: UIButton!
    @IBOutlet weak var giveButton: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func askButtonTapped(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func giveButtonTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
}
