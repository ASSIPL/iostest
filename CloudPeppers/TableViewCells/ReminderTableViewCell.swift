//
//  ReminderTableViewCell.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 03/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class ReminderTableViewCell: UITableViewCell {

    
    @IBOutlet weak var reminderDescription: UILabel!
    @IBOutlet weak var reminderName: UILabel!
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var displayView: UIView!
    
    @IBOutlet weak var timeDisplayView: UIView!
    
    @IBOutlet var dateRemainder: UILabel!
    
    @IBOutlet var timeRemainder: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        displayView.dropShadow()
//        displayView.layer.cornerRadius = 25
//        displayView.clipsToBounds = true
      //  weeksDisViewCornerRadius(view: timeDisplayView)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

   
    func weeksDisViewCornerRadius(view:UIView)
    {
        view.layer.cornerRadius = 12
        view.clipsToBounds = true
        view.layer.borderWidth = 1
        view.layer.borderColor = #colorLiteral(red: 0.250289619, green: 0.7255241275, blue: 0.9223501086, alpha: 1)
    }
}
