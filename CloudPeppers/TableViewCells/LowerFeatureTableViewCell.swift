//
//  LowerFeatureTableViewCell.swift
//  CloudPeppers
//
//  Created by Allvy on 27/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class LowerFeatureTableViewCell: UITableViewCell {

    @IBOutlet weak var priceWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var featureLabel: UILabel!
    @IBOutlet weak var leftSideButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func infoButtonTapped(_ sender: Any) {
    }
    @IBAction func leftSideButtonTapped(_ sender: Any) {
    }
}
