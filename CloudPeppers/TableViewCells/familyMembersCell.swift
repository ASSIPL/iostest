//
//  familyMembersCell.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 24/04/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class familyMembersCell: UICollectionViewCell {
    
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var resendView: UIView!
    @IBOutlet weak var upperImage: UIImageView!
    @IBOutlet var shadowView: UIView!
    @IBOutlet var profilePick: UIImageView!
    @IBOutlet var profileName: UILabel!
    @IBOutlet var panicButton: UIButton!
    @IBOutlet var setRuluesButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        

      profilePick.clipsToBounds = true
        profilePick.layer.cornerRadius = profilePick.frame.size.width/2
       
        shadowView.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: 5.0, opacity: 0.35)
        // Initialization code
    }

    @IBAction func resentButtonTapped(_ sender: Any) {
    }
}
