//
//  CustomInfoWindow.swift
//  CloudPeppers
//
//  Created by APPLE on 4/27/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class CustomInfoWindow: UIView {
    @IBOutlet weak var infoWindowAddress: UILabel!
    
    @IBOutlet weak var getDirectionsLabel: UILabel!
    @IBOutlet weak var directionImageView: UIImageView!
    @IBOutlet weak var lineLabel: UILabel!
    @IBOutlet var memberName: UILabel!
    
    
    @IBOutlet weak var callAction: UIButton!
    
    
    @IBOutlet weak var messagebutton: UIButton!
    
    
    @IBOutlet var directionButton: UIButton!
    
    @IBOutlet weak var infoWindowTimeStamp: UILabel!
    

    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
