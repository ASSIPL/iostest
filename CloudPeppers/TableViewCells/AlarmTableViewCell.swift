//
//  AlarmTableViewCell.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 03/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class AlarmTableViewCell: UITableViewCell {

    
    @IBOutlet weak var weeksDisView: UIView!
    
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var displayView: UIView!
    
    @IBOutlet weak var alarmNameLabel: UILabel!
    
    @IBOutlet weak var alarmDescripLabel: UILabel!
    
      @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet var monday: UIButton!
    
    @IBOutlet var thuesday: UIButton!
    
    
    @IBOutlet var wednesday: UIButton!
    
    
    @IBOutlet var thursday: UIButton!
    
    @IBOutlet var friday: UIButton!
    
    @IBOutlet var saturday: UIButton!
    @IBOutlet var sunday: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      //  weeksDisViewCornerRadius(view: weeksDisView)
       // cornerRadiusforView(view: displayView)
        
        displayView.dropShadow()
      //  displayView.dropShadow1()
        // Initialization code
    }

   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func weeksDisViewCornerRadius(view:UIView)
    {
        view.layer.cornerRadius = 12
        view.clipsToBounds = true
        view.layer.borderWidth = 1
        view.layer.borderColor = #colorLiteral(red: 0.250289619, green: 0.7255241275, blue: 0.9223501086, alpha: 1)
    }
    func cornerRadiusforView(view:UIView)
    {
        view.layer.cornerRadius = 5
        view.clipsToBounds = true
    }
  

}
extension UIView {
    
    func dropShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.layer.cornerRadius = 20
        self.clipsToBounds = true
        
    }
    func dropShadow1() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
     
        
    }
}
