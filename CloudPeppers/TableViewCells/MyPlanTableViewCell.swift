//
//  MyPlanTableViewCell.swift
//  CloudPeppers
//
//  Created by Allvy on 26/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class MyPlanTableViewCell: UITableViewCell {

    
    @IBOutlet weak var featureNameLabel: UILabel!
    
    @IBOutlet weak var infoButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
