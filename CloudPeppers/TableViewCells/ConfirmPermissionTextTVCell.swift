//
//  ConfirmPermissionTextTVCell.swift
//  CloudPeppers
//
//  Created by Allvy on 03/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class ConfirmPermissionTextTVCell: UITableViewCell {

    
    @IBOutlet weak var upperTextLabel: UILabel!
    
    @IBOutlet weak var lowerTextLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
