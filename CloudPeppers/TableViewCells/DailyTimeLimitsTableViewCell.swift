//
//  DailyTimeLimitsTableViewCell.swift
//  CloudPeppers
//
//  Created by Allvy on 28/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class DailyTimeLimitsTableViewCell: UITableViewCell {

    @IBOutlet weak var dailyTimeLimitsCellView: UIView!
    
    
    @IBOutlet weak var displayTimeLabel: UILabel!
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet var monday: UIButton!
    
    @IBOutlet var thuesday: UIButton!
    
    
    @IBOutlet var wednesday: UIButton!
    
    
    @IBOutlet var thursday: UIButton!
    
    @IBOutlet var friday: UIButton!
    
    @IBOutlet var saturday: UIButton!
    @IBOutlet var sunday: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dailyTimeLimitsCellView.dropShadow()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    

}

