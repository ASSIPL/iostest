//
//  BasePlusLowerTableViewCell.swift
//  CloudPeppers
//
//  Created by Allvy on 20/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class BasePlusLowerTableViewCell: UITableViewCell {

    @IBOutlet weak var checkboxButton: UIButton!
    
    
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var featureLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
