//
//  PaymentMainTableViewCell.swift
//  CloudPeppers
//
//  Created by Allvy on 19/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class PaymentMainTableViewCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var serialNumberingLab: UILabel!
    @IBOutlet var tickMark: UIImageView!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var priceFeatureLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var lowerLabel: UILabel!
    @IBOutlet weak var upperLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
