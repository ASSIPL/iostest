//
//  BasePlusUpperTableViewCell.swift
//  CloudPeppers
//
//  Created by Allvy on 20/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class BasePlusUpperTableViewCell: UITableViewCell {

    
    @IBOutlet weak var featureNameLabel: UILabel!
    
    @IBOutlet weak var infoButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
