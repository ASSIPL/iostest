//
//  SettingsTableViewCell.swift
//  CloudPeppers
//
//  Created by Allvy on 30/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var displayView: UIView!
    
    @IBOutlet weak var disTitle: UILabel!
    @IBOutlet weak var disImage: UIImageView!
    override func awakeFromNib() {
        displayView.dropShadow()
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
