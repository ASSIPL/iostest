//
//  AppsandGamesCollectionCell.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 30/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class AppsandGamesCollectionCell: UICollectionViewCell {

    @IBOutlet var appImage: UIImageView!
    
    @IBOutlet var appName: UILabel!
    
    @IBOutlet var tickMark: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
