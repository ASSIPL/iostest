//
//  PermissionsGiveToYouCell.swift
//  CloudPeppers
//
//  Created by Allvy on 03/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class PermissionsGiveToYouCell: UITableViewCell {

    
    
    @IBOutlet weak var permissionsTextLabel: UILabel!
    @IBOutlet weak var PermissionsImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
