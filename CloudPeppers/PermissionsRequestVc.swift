//
//  PermissionsRequestVc.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 21/06/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase

class PermissionsRequestVc: UIViewController {
    
    @IBOutlet var barButton: UIBarButtonItem!
    
    @IBOutlet var upperView: UIView!
    
    
    @IBOutlet var lowerView: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = Constants.colour
        
        self.changeFontForViewController()
        
        self.title = "Permission Requests"
        upperView.dropShadow()
        lowerView.dropShadow()
        
        // navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        barButton.target = revealViewController()
        
        
        barButton.action = #selector(SWRevealViewController.revealToggle(_:))
        
        
        let tapTop = UITapGestureRecognizer(target: self, action: #selector(self.tapTop(_:)))
        
        upperView.addGestureRecognizer(tapTop)
        
        upperView.isUserInteractionEnabled = true
        
        
        let tapbuttom = UITapGestureRecognizer(target: self, action: #selector(self.tapbuttom(_:)))
        
        lowerView.addGestureRecognizer(tapbuttom)
        
        lowerView.isUserInteractionEnabled = true
        
        
        
        // function which is triggered when handleTap is called
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Analytics.logEvent("Permission_Request_Screen_Enter ", parameters:nil)

    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("Permission_Request_Screen_Exit", parameters:nil)

    }
    
    @objc func tapTop(_ sender: UITapGestureRecognizer) {
        Analytics.logEvent("Permission_Request_SenttoMembers_clicked ", parameters:nil)
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SentToMembersView") as! SentToMembersView
        
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func tapbuttom(_ sender: UITapGestureRecognizer) {
        
        Analytics.logEvent("Permission_Request_SenttoYou_clicked ", parameters:nil)
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "sentToYouView") as! sentToYouView
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
