//
//  ConfigurationViewController.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 25/04/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage


class ConfigurationViewController: UIViewController {
    var imageview : [UIImageView]?
    var imageviewsaved1 :  UIImageView?
    var imageviewsaved2 :  UIImageView?
    var imageviewsaved3 :  UIImageView?
    var imageviewsaved4 :  UIImageView?
    
    
    @IBOutlet weak var coolpadLinkImg: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HttpWrapper.gets(with: API.getOrganizationInfo, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatus.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    
                    if let data = try? JSONEncoder().encode(loginRespone.response) {
                        UserDefaults.standard.set(data, forKey: "organizationdetails")
                    }
                    print()
                    //     print(loginRespone.response)
                    
                    SharedData.data.organizationdetails  = loginRespone.response
                    
                    self.imageviewsaved1?.sd_setImage(with: URL(string: (SharedData.data.organizationdetails?.roles![0].imageUrl!)!),placeholderImage: UIImage(named: "newcoolpadlogo"))
                    self.imageviewsaved2?.sd_setImage(with: URL(string: (SharedData.data.organizationdetails?.roles![1].imageUrl!)!),placeholderImage: UIImage(named: "newcoolpadlogo"))
                    self.imageviewsaved3?.sd_setImage(with: URL(string: (SharedData.data.organizationdetails?.roles![3].imageUrl!)!),placeholderImage: UIImage(named: "newcoolpadlogo"))
                    //   self.imageviewsaved4?.sd_setImage(with: URL(string: (SharedData.data.organizationdetails?.roles![4].imageUrl!)!),placeholderImage: UIImage(named: "CpfamilyNew"))
                    
                    var datas = [Data]()
                    for each in (SharedData.data.organizationdetails?.roles)!
                    {
                        let url = URL(string: (each.imageUrl!))
                        let data = try? Data(contentsOf: url!)
                        datas.append(data!)
                        //                        SharedData.data.imagedata?.append(data!)
                        //
                        //                        SharedData.data.imagedata1 = data
                    }
                    print(datas.count)
                    SharedData.data.imagedata = datas
                    //     print(SharedData.data.imagedata?.count)
                    
                }
                
                _ = UIApplication.shared.delegate as! AppDelegate
                let id = "SplashscreenViewController"
                let vc = UIStoryboard.getViewController(storyboardName: "Main", storyboardId: id) as? SplashscreenViewController
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = vc
                    appDelegate.window?.makeKeyAndVisible()
                }
            } catch let jsonErr {
                
                print("Error serializing json:", jsonErr)
                
            }
            
        }){ (error) in
            
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
            print(error)
        }
        
        // Do any additional setup after loading the view.
    }
    
    
}
