//
//  DirectionsViewController.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 27/04/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import AVFoundation
import Speech
import SwiftyJSON


class DirectionsViewController: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate {
    let synth = AVSpeechSynthesizer()
      var speakerText: String?
    var myUtterance = AVSpeechUtterance(string: "")
      var distanceBetweenLocations: CLLocationDistance?
      fileprivate var locationManager: CLLocationManager!
    var marker = GMSMarker()
        var currentLocations:CLLocation?
    @IBOutlet var mapView: GMSMapView!
    var timer = Timer()
    var timer1 = Timer()
    var startLOC = CLLocation()
    var endLOC = CLLocation()
    var sourceLat = 0.0
    var memberId:Int?
    var sourceLong = 0.0
     var modeuser:String?
    var DestinationLat = 0.0
     var membersGeoList:[MembersGeoList]?
    var DestinationLong = 0.0
    var currentLocation: CLLocationCoordinate2D?
    var markerLocation:CLLocationCoordinate2D?
    
    
    
    
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var busButton: UIButton!
    @IBOutlet weak var bikeButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var navTitleLabel: UILabel!
    @IBOutlet weak var carButton: UIButton!
    
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var routeDesLabel: UILabel!
    @IBOutlet weak var appoxTimeLabel: UILabel!
    @IBOutlet weak var milesLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Get Directions"
        
        
        let imageDataDict:[String: Any] = ["isinChatView":"true"]
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "inDirectionView"), object: nil,userInfo: imageDataDict)
        
        mapView.isMyLocationEnabled = true
        
           print(memberId)
        
        print(endLOC.coordinate.latitude)
      
       setUI()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined,.restricted,.denied:
                print("notDetermined")
                
                AlertFun.openSettingApp(title: "", message: "Please enable location services", in: self)
                //ShowAlert(title: "Chomp", message: "Please select not more than 3", in: self)
                return
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                
            }
        } else {
            AlertFun.openSettingApp(title: "", message: "Please enable location services", in: self)
            
            return
            
        }
        
        setupLocationManager()
        
        updateCoordinates()
        
        
        
      //  timer1 = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(speaker), userInfo: nil, repeats: true)
        
        
        
        // mapView.delegate = self
      //  drawPath(startLocation: CLLocation(latitude: (currentLocation?.latitude)!, longitude: (currentLocation?.longitude)!), endLocation: CLLocation(latitude: (markerLocation?.latitude)!, longitude: (markerLocation?.longitude)!))
        // Do any additional setup after loading the view.
    }
    
    func setUI()
    {
        navTitleLabel.text = "Directions"
        self.navigationController?.isNavigationBarHidden = true
        dismissButton.changeButtonCornerRadius()
        carButton.setImage(UIImage(named: "carSelected"), for: .normal)
        bikeButton.setImage(UIImage(named: "bikeUnselected"), for: .normal)
        busButton.setImage(UIImage(named: "busUnselected"), for: .normal)
        
        
        bottomView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        bottomView.layer.shadowColor = UIColor.gray.cgColor
        bottomView.layer.shadowOpacity = 1
        bottomView.layer.shadowOffset = CGSize.zero
        bottomView.layer.shadowRadius = 5
        bottomView.layer.cornerRadius = 10
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
     
        
         self.navigationController?.isNavigationBarHidden = false
                    UserDefaults.standard.set("panicSOSfalse", forKey: "panicSOS")
                    UserDefaults.standard.set(nil, forKey: "panicSOSdata")
        
        let imageDataDict:[String: Any] = ["isinChatView":"false"]
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "inDirectionView"), object: nil,userInfo: imageDataDict)
        
        
        timer.invalidate()
     //   timer1.invalidate()
        
    }
    func drawPath(startLocation: CLLocation, endLocation: CLLocation)
    {
        let origin = "\(startLocation.coordinate.latitude),\(startLocation.coordinate.longitude)"
        let destination = "\(endLocation.coordinate.latitude),\(endLocation.coordinate.longitude)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(Constants.googlemapsKey)"
        
        print(url)
        
        HttpWrapper.post(with: url, parameters: nil, headers: nil, completionHandler: { (response) in
            print(response)
            let  routes = response["routes"] as?  NSArray
            //  print(routes)
            self.mapView.clear()
            // let route = routes?.first
            let legs = (routes as AnyObject).value(forKey:"legs") as? NSArray
            // print(legs)
            // let leg = legs?.first
            // print((routes![0] as AnyObject).legs[0].distance.text)
            //  let legss = routes?.firstObject?["legs"] as? NSDictionary
            
            for each in legs!
            {
                print(each)
                let distance = (each as AnyObject).value(forKey:"distance") as? NSArray
               
                
                
            
               
                
                for eachdistance in distance!
                {
                    
                    
                    self.appoxTimeLabel.text = (eachdistance as AnyObject).value(forKey:"text") as? String
                    //print((eachdistance as AnyObject).value(forKey:"text") as? String)
                    //  print((eachdistance as AnyObject).value(forKey:"text") as? String)
                }
                // let text = distance?["text"] as? String
                let duration = (each as AnyObject).value(forKey:"duration") as?  NSArray
                for eachduration in duration!
                {
                    
                   self.appoxTimeLabel.text = (eachduration as AnyObject).value(forKey:"text") as? String
                    //print((eachduration as AnyObject).value(forKey:"text") as? String)
                    // print((eachduration as AnyObject).value(forKey:"text") as? String)
                }
                
                let steps = (each as? NSObject)?.value(forKey:"steps") as? NSArray
                // print(steps)
                for eachh in steps!
                {
                    print(eachh)
                    //   print((eachh as AnyObject).value(forKey: "distance") as? [String:Any])
                }
                
            }
            
            // print(distanceText)
            OperationQueue.main.addOperation({
                for route in routes!
                {
                    // print(route)
                    let routeOverviewPolyline = (route as AnyObject).value(forKey:"overview_polyline") as? [String:Any]
                    //route["overview_polyline"]
                    // print(routeOverviewPolyline)
                    self.marker = GMSMarker()
                    self.marker.position = CLLocationCoordinate2DMake(endLocation.coordinate.latitude, endLocation.coordinate.longitude)
                    self.marker.icon = UIImage(named: "maplocationoran")
                    self.marker.map = self.mapView
                    let points = routeOverviewPolyline?["points"] as? String
                    //   print(points)
                    let path = GMSPath.init(fromEncodedPath: points!)
                    // print(path)
                    // let path = GMSPath.init(fromEncodedPath: points!)
                    let polyline = GMSPolyline.init(path: path)
                    polyline.isTappable = true
                    polyline.zIndex = 0
                    let bounds = GMSCoordinateBounds(path: path!)
                    //print(polyline)
                    let cameraUpdate = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 40, left: 15, bottom: 10, right: 15))
                    self.mapView.animate(with: cameraUpdate)
                    polyline.strokeWidth = 7
                    polyline.geodesic = true
                    
                    polyline.strokeColor =  UIColor(red: 0.0/255.0 , green: 182.0/255.0, blue: 255.0/255.0, alpha: 1)
                    polyline.map = self.mapView
                    
                }
                
            })
        }){ (error) in
            
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
        
    }
    
    
    
    func drawPath(startLocation: CLLocation, endLocation: CLLocation,mode:String)
    {
        let origin = "\(startLocation.coordinate.latitude),\(startLocation.coordinate.longitude)"
        let destination = "\(endLocation.coordinate.latitude),\(endLocation.coordinate.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&sensor=false&mode=\(mode)&alternatives=false&key=AIzaSyB7cJxxacb1JymyupzwHRLUGjcvetDr6Pw"
        // print(url)
        // let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving"
        HttpWrapper.post(with: url, parameters: nil, headers: nil, completionHandler: { (response) in
            // print(response)
            
            if(response != nil)
            {
                let parsedData = JSON(response)
                
                print("parsedData12554 : \(parsedData)")
                
                print("parsed : \(parsedData["status"])")
                
                if(parsedData["status"] == "OK")
                {

                    let routes = parsedData["routes"][0]

                    print("Routesss260 \(routes)")

                    let legs = routes["legs"][0]

                    // print(legs)

                    let distance = legs["distance"]
                    
                    
                      let end_address = legs["end_address"].string
                    
                    self.routeDesLabel.text = end_address
                    
                    
                    print(distance)

                    let duration = legs["duration"]
                     print(duration)

                    let disValue = distance["text"].string


                    let durValue = duration["text"].string
                    
                    self.appoxTimeLabel.text = durValue
                    
                    
               //     self.distance.text = "Duration \(durValue!) \nDistance \(disValue!) "

                    print(legs["steps"].count)

                    let steps = legs["steps"][0]

                    let html_instructions = steps["html_instructions"].string
                  //  self.startLocatioLabel.text = html_instructions?.html2String
                    self.speakerText = html_instructions?.html2String
                    //  print(html_instructions?.html2String)
//
//                                        if html_instructions?.html2String != nil
//                                        {
//                                            self.myUtterance.rate = AVSpeechUtteranceDefaultSpeechRate;
//                                            self.myUtterance.voice = AVSpeechSynthesisVoice(language: "en-US")
//                                            self.myUtterance = AVSpeechUtterance(string: (html_instructions?.html2String)!)
//                                            self.myUtterance.rate = 0.3
//                                            self.synth.speak(self.myUtterance)
//                                        }
//                    



                    //  let travel_mode = steps["travel_mode"].string

                    let maneuver = legs["steps"][1]["maneuver"].string
                    
                    print(maneuver)
               //     self.directionInstuction.text = maneuver
//
//                      print(travel_mode)
//                       print("disValue250 \(disValue)")
//
//                       print("durValue250 \(durValue)")
//
//
//                                        if(durValue != 0.0)
//                                        {
//                                            self.speed = disValue / durValue
//                                        }
//



                }
                
                
            }
            let  routes = response["routes"] as? NSArray
            //  print(routes)
            self.mapView.clear()
            
            
            
            
            
            
//            let marker = GMSMarker()
//            marker.position = CLLocationCoordinate2D(latitude: self.sourceLat, longitude: self.sourceLong)
//            marker.title = "Source"
//            marker.icon = UIImage(named: "navigationicon")
//            marker.map = self.mapView
//
            OperationQueue.main.addOperation({
                for route in routes!
                {
                    let routeOverviewPolyline = (route as AnyObject).value(forKey:"overview_polyline") as? [String:Any]
                    self.marker = GMSMarker()
                    self.marker.position = CLLocationCoordinate2DMake(endLocation.coordinate.latitude, endLocation.coordinate.longitude)
                    self.marker.icon = UIImage(named: "mapLocation")
                    self.marker.map = self.mapView
                  //  self.marker.title                    = "Kid"
                    let points = routeOverviewPolyline?["points"] as? String
                    // print(points)
                    let path =  GMSMutablePath.init(fromEncodedPath: points!)
                    // print(path)
                    
                    let polyline = GMSPolyline.init(path: path)
                    
                    let bounds = GMSCoordinateBounds(path: path!)
                    //print(polyline)
                    let cameraUpdate = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 40, left: 15, bottom: 10, right: 15))
                    self.mapView.animate(with: cameraUpdate)
                    polyline.strokeWidth = 7
                    polyline.geodesic = true
                    self.mapView.animate(toViewingAngle: 45)
                    
                    self.mapView.animate(toLocation: CLLocationCoordinate2D(latitude:(self.currentLocations?.coordinate.latitude)!, longitude: (self.currentLocations?.coordinate.longitude)!))
                    
                    
                    let target = CLLocationCoordinate2D(latitude: (self.currentLocations?.coordinate.latitude)!, longitude: (self.currentLocations?.coordinate.longitude)!)
                    // let projection = self.googleMapView.projection.visibleRegion()
                    // projection.farLeft
                    // self.googleMapView.camera = GMSCameraPosition.camera(withTarget: target, zoom: 19, bearing: projection.farRight, viewingAngle: 45)
                    self.mapView.camera = GMSCameraPosition.camera(withTarget: target, zoom: 18)
                    GMSStrokeStyle.gradient(from: .red, to: .yellow)
                    //  self.googleMapView.animate(toViewingAngle: 45)
//                    let icon = UIImage(named: "mapLocation")
//                    let marker                      = GMSMarker(position: CLLocationCoordinate2DMake((startLocation.coordinate.latitude), (startLocation.coordinate.longitude)))
//                    marker.icon                     = icon
//                     marker.title                    = "YOU"
//                    marker.map                      = self.mapView
                    polyline.strokeColor =  UIColor(red: 0.0/255.0 , green: 182.0/255.0, blue: 255.0/255.0, alpha: 1)
                    polyline.map = self.mapView
                    
                    
                }
            })
            
            
        }){ (error) in
            
        }
        
        
        
    }
    
    @objc func speaker(){
        if  self.speakerText != nil
        {
            self.myUtterance.rate = AVSpeechUtteranceDefaultSpeechRate;
            self.myUtterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            self.myUtterance = AVSpeechUtterance(string:  self.speakerText!)
            self.myUtterance.rate = 0.3
            self.synth.speak(self.myUtterance)
            timer1.invalidate()
        }
    }
    
    func updateMarker(coordinates: CLLocationCoordinate2D, degrees: CLLocationDegrees, duration: Double) {
        // Keep Rotation Short
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.5)
        marker.rotation = degrees
        CATransaction.commit()
        
        // Movement
        CATransaction.begin()
        CATransaction.setAnimationDuration(duration)
        marker.position = coordinates
        
        // Center Map View
        let camera = GMSCameraUpdate.setTarget(coordinates)
        mapView.animate(with: camera)
        
        CATransaction.commit()
    }
  func setupLocationManager(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        // locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        
        
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = manager.location!.coordinate
        let location = locationManager.location?.coordinate
        cameraMoveToLocation(toLocation: location)
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
       // loadmap()
        
    }
    
    
    func updateCoordinates()
    {
        var params: [String:Any] = [:]
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        params["userId"] =  profileData.userId
        params["latitude"]  =  UserDefaults.standard.string(forKey: "latitude")
        params["longitude"] =  UserDefaults.standard.string(forKey: "longtitude")
        params["isRequireMembersLocation"]  = "yes"
        params["batteryStatus"] =    batteryLevel
        params["geoHistoryFlag"] = false
        
        
        print(params)
        print(API.saveGeocoordinate)
        
        
      
        HttpWrapper.posts(with: API.saveGeocoordinate, parameters: params, headers: nil, completionHandler: { (response, error)  in
            // print(response)
            guard let data = response else { return }
            do {
                let loginRespone = try JSONDecoder().decode(ApiStatusmembersGeoList.self, from: data)
                let loginStatus = loginRespone.status
                
                if loginStatus!{
                    
                    print(loginRespone.response)
                    
                  
                    self.membersGeoList = loginRespone.response?.membersGeoList
                    
                    
                    let membersGeoListdata1 =  self.membersGeoList?.filter{$0.userId == self.memberId}
                    
                    print(membersGeoListdata1)
                    
                
                    if membersGeoListdata1?.count ?? 0 > 0
                {
                    
                    
                    
                    self.endLOC = CLLocation(latitude:Double(membersGeoListdata1![0].latitude!) as! CLLocationDegrees, longitude:  Double(membersGeoListdata1![0].longitude!) as! CLLocationDegrees)
                    
                    print(self.endLOC)
                    
                    self.loadmap()
                    
                    }else
                    {
                        
                        
                        
                        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                        
                        let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
                        let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
                        
                        let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
                        let msgAttrString = NSMutableAttributedString(string: "No location updates", attributes: msgFont)
                        
                        alert.setValue(titAttrString, forKey: "attributedTitle")
                        alert.setValue(msgAttrString, forKey: "attributedMessage")
                        
                        
                        let settingsAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                            if #available(iOS 10.0, *) {
                                
                            
                                
                                
                                self.navigationController?.popViewController(animated: true)
                            } else {
                                // Fallback on earlier versions
                            }
                        }
                        
                        
                        alert.addAction(settingsAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                    
                    
                }
            }
            catch let jsonErr {
                
                
                print(jsonErr)
                
            }
            
        }){ (error) in
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
        
        // print(params)
        
    }
    
    var batteryLevel: Int {
        return Int(round(UIDevice.current.batteryLevel * 100))
    }
    
   func loadmap()
   {
    let authStatus = CLLocationManager.authorizationStatus()
    
    // self.locationManager.requestWhenInUseAuthorization()
    if authStatus == CLAuthorizationStatus.authorizedWhenInUse || authStatus == CLAuthorizationStatus.authorizedAlways {
          currentLocations = CLLocation(latitude: (currentLocation?.latitude)!, longitude:  (currentLocation?.longitude)!)
        
        
    }
    
    if currentLocations?.coordinate.latitude != nil
    {
        self.startLOC = CLLocation(latitude: (currentLocations?.coordinate.latitude)!, longitude:  (currentLocations?.coordinate.longitude)!)
        let location = locationManager.location?.coordinate
        
        
    }
    distanceBetweenLocations =  endLOC.distance(from: startLOC)
    
    //convert To Miles
    distanceBetweenLocations = Utility.convertCLLocationDistanceToMiles(targetDistance: distanceBetweenLocations)
    
    milesLabel.text = "(\(String(format: "%.2f ", distanceBetweenLocations!) + "Miles"))"
    
    
  
    
    DispatchQueue.main.async {
        self.drawPath(startLocation: self.startLOC, endLocation: self.endLOC, mode:"Driving")
    }
    if modeuser == "walking"
    {
        // timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(moveMarker), userInfo: nil, repeats: true)
    }
    if modeuser == "Bicycling"
    {
        //  timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(moveMarker), userInfo: nil, repeats: true)
    }
    if modeuser == "Driving"
    {
        //#selector(moveMarker), userInfo: nil, repeats: true)
    }
    }
    
    
    
    func cameraMoveToLocation(toLocation: CLLocationCoordinate2D?) {
        if toLocation != nil {
            // marker.title = mapTasks.fetchedFormattedAddress
            marker.appearAnimation = GMSMarkerAnimation.pop
            marker.icon = GMSMarker.markerImage(with: UIColor.blue)
            marker.opacity = 0.75
            if let mylocation = mapView.myLocation {
                print("User's location: \(mylocation)")
            } else {
                print("User's location is unknown")
            }
            mapView.camera = GMSCameraPosition.camera(withTarget: toLocation!, zoom: 18)
            
        }
    }
    
    @IBAction func carButtonTapped(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        
        bikeButton.isSelected = false
        busButton.isSelected = false
        if sender.isSelected
        {
            sender.setImage(UIImage(named: "carSelected"), for: .normal)
            carButton.setImage(UIImage(named: "carSelected"), for: .normal)
            bikeButton.setImage(UIImage(named: "bikeUnselected"), for: .normal)
            busButton.setImage(UIImage(named: "busUnselected"), for: .normal)
        }
        
        
        
        if endLOC.coordinate.latitude != 0.0
        {
            DispatchQueue.main.async {
                self.drawPath(startLocation: self.startLOC, endLocation: self.endLOC, mode:"Driving")
            }
        }
        
        
        
        
    }
    
    @IBAction func bikeButtonTapped(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        carButton.isSelected = false
        busButton.isSelected = false
        
        if sender.isSelected
        {
            sender.setImage(UIImage(named: "bikeSelected"), for: .normal)
            bikeButton.setImage(UIImage(named: "bikeSelected"), for: .normal)
            carButton.setImage(UIImage(named: "carUnselected"), for: .normal)
            busButton.setImage(UIImage(named: "busUnselected"), for: .normal)
        }
        
        if endLOC.coordinate.latitude != 0.0
        {
            DispatchQueue.main.async {
                self.drawPath(startLocation: self.startLOC, endLocation: self.endLOC, mode:"Bicycling")
            }
        }
        
    }
    
    
    @IBAction func busButtonTapped(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
       //  busButton.isSelected = false
        carButton.isSelected = false
        bikeButton.isSelected = false
        if sender.isSelected
        {
            sender.setImage(UIImage(named: "busSelected"), for: .normal)
            busButton.setImage(UIImage(named: "busSelected"), for: .normal)
            bikeButton.setImage(UIImage(named: "bikeUnselected"), for: .normal)
            carButton.setImage(UIImage(named: "carUnselected"), for: .normal)
        }
        
        
        if endLOC.coordinate.latitude != 0.0
        {
            DispatchQueue.main.async {
                self.drawPath(startLocation: self.startLOC, endLocation: self.endLOC, mode:"walking")
            }
        }
        
        
    }
    @IBAction func dismissButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    
}
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
class Utility {
    
    class func convertCLLocationDistanceToMiles ( targetDistance : CLLocationDistance?) -> CLLocationDistance {
        var targetDistance = targetDistance
        targetDistance =  targetDistance!*0.00062137
        return targetDistance!
    }
    class func convertCLLocationDistanceToKiloMeters ( targetDistance : CLLocationDistance?) -> CLLocationDistance {
        var targetDistance = targetDistance
        targetDistance =  targetDistance!/1000
        return targetDistance!
    }
    
}
