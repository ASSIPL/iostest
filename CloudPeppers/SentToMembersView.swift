//
//  SentToMembersView.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 21/06/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import SVProgressHUD
import Firebase

class SentToMembersView: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var pendingCollectionView: UICollectionView!
    @IBOutlet weak var approvedCollectionView: UICollectionView!
    
    @IBOutlet weak var fromYouPendingReqLabel: UILabel!
    @IBOutlet weak var fromYouApprovedLabel: UILabel!
    var permissionRequest:PermissionRequest?
    
    var namesArray:Array = [String]()
    var imagesArray:Array = [UIImage]()
    var relationArray:Array = [String]()
    var approvedNamesArr:Array = [String]()
    var approveimagesArray:Array = [UIImage]()
    var approverelationArray:Array = [String]()
    
      var toyouApprove:[responses]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        addCornerRadius(label: fromYouPendingReqLabel)
        addCornerRadius(label: fromYouApprovedLabel)
        namesArray = ["Oliver","Thea","Moria"]
        imagesArray = [#imageLiteral(resourceName: "Jones"),#imageLiteral(resourceName: "120x120"),#imageLiteral(resourceName: "Robert1")]
        relationArray = ["(Son)","(Daughter)","(Mother)"]
        
        self.title = "Your requests sent to members"
        
        approvedNamesArr = ["Robert","Olivia","Jones"]
        approveimagesArray = [#imageLiteral(resourceName: "Thea1"),#imageLiteral(resourceName: "Moria"),#imageLiteral(resourceName: "DefaultProfileImg")]
        approverelationArray = ["(Father)","(Wife)","(Brother)"]
        
        callgetMemberApi()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        Analytics.logEvent("SentToMembers_Screen_Enter", parameters:nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("SentToMembers_Screen_Exit", parameters:nil)
    }
    func addCornerRadius(label:UILabel)
    {
        label.layer.cornerRadius = 10
        label.clipsToBounds = true
    }
    func callgetMemberApi()
    {
        
         SVProgressHUD.show()
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        let getFromYourToYouUrl = API.getFromYourToYou + "?userId=\(String(describing: profileData.userId!))"
        
        //let getFromYourToYouUrl = API.getFromYourToYou + "?userId=8"
        print(getFromYourToYouUrl)
        HttpWrapper.gets(with: getFromYourToYouUrl, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            print(response)
            
             SVProgressHUD.dismiss()
            
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatuspermissionRequest.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    self.permissionRequest  = loginRespone.response
                    
                    
                     self.toyouApprove = self.permissionRequest?.toyouApprove?.filter{$0.toYou_Approved == true}
                    self.pendingCollectionView.delegate = self
                    self.pendingCollectionView.dataSource = self
                    
                    
                    
                    self.approvedCollectionView.delegate = self
                    self.approvedCollectionView.dataSource = self
                    self.pendingCollectionView.reloadData()
                    self.approvedCollectionView.reloadData()
                    
                }
                
                
            }catch let jsonErr {
                
                  SVProgressHUD.dismiss()
                print("Error serializing json:", jsonErr)
                
            }
        }){ (error) in
              SVProgressHUD.dismiss()
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
            
            
            print(error)
        }
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == pendingCollectionView
        {
            
            
            
            let numOfSection: NSInteger =  permissionRequest?.toyouPending?.count ?? 0
            if numOfSection == 0
            {
                let noItemLabel = UILabel() //no need to set frame.
                noItemLabel.textAlignment = .center
                noItemLabel.textColor = .black
                noItemLabel.text = "No requests found"
                noItemLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
                collectionView.backgroundView = noItemLabel
                return permissionRequest?.toyouPending?.count ?? 0
            }else
            {
                collectionView.backgroundView = nil
                return permissionRequest?.toyouPending?.count ?? 0
            }
        }
        
        
        
        //  return permissionRequest?.toyouPending?.count ?? 0
        
        if collectionView == approvedCollectionView
        {
            
            let numOfSection: NSInteger =  toyouApprove?.count ?? 0
            if numOfSection == 0
            {
                let noItemLabel = UILabel() //no need to set frame.
                noItemLabel.textAlignment = .center
                noItemLabel.textColor = .black
                noItemLabel.text = "No requests found"
                noItemLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
                collectionView.backgroundView = noItemLabel
                return toyouApprove?.count ?? 0
            }else
            {
                collectionView.backgroundView = nil
                return toyouApprove?.count ?? 0
            }
            
            
        }
        
        
        return namesArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "permission", for: indexPath) as! PermissionCollectionViewCell
        
        if collectionView == pendingCollectionView
        {
            
            cell.nameLabel.text = permissionRequest?.toyouPending?[indexPath.row].firstName
            cell.relationLabel.text = "(\(String(describing: (permissionRequest?.toyouPending?[indexPath.row].relationShip)!)))"
            //  cell.displayImage.image = imagesArray[indexPath.row]
            
            
            cell.displayImage.layer.cornerRadius = cell.displayImage.frame.size.height/2
            cell.displayImage.clipsToBounds = true
            
            if permissionRequest?.toyouPending?[indexPath.row].profileImage != nil && permissionRequest?.toyouPending?[indexPath.row].profileImage != ""
            {
                cell.displayImage.sd_setImage(with: URL(string: (permissionRequest?.toyouPending?[indexPath.row].profileImage)!), placeholderImage: UIImage(named: "placeHolderProfile"))
            }
            
            
            
        }
        else if collectionView == approvedCollectionView
        {
            cell.nameLabel.text = toyouApprove?[indexPath.row].firstName
            cell.relationLabel.text = "(\(String(describing: (toyouApprove?[indexPath.row].relationShip)!)))"
            
            cell.displayImage.layer.cornerRadius = cell.displayImage.frame.size.height/2
            cell.displayImage.clipsToBounds = true
            
            if toyouApprove?[indexPath.row].profileImage != nil && toyouApprove?[indexPath.row].profileImage != ""
            {
                cell.displayImage.sd_setImage(with: URL(string: (toyouApprove?[indexPath.row].profileImage)!), placeholderImage: UIImage(named: "placeHolderProfile"))
            }
            
        }
        //        else if collectionView == fromYouPendingCV
        //        {
        //            cell.nameLabel.text = permissionRequest?.fromYouPending?[indexPath.row].firstName
        //            cell.relationLabel.text = permissionRequest?.fromYouPending?[indexPath.row].relationShip
        //
        //            if permissionRequest?.fromYouPending?[indexPath.row].profileImage != nil && permissionRequest?.fromYouPending?[indexPath.row].profileImage != ""
        //            {
        //                cell.displayImage.sd_setImage(with: URL(string: (permissionRequest?.fromYouPending?[indexPath.row].profileImage)!), placeholderImage: UIImage(named: "DefaultProfileImg"))
        //            }
        //            //  cell.displayImage.sd_setImage(with: URL(string: (permissionRequest?.fromYouPending?[indexPath.row].profileImage)!), placeholderImage: UIImage(named: "DefaultProfileImg"))
        //        }
        //        else if collectionView == toYouApprovedCV
        //        {
        //            cell.nameLabel.text = permissionRequest?.fromYouApprove?[indexPath.row].firstName
        //            cell.relationLabel.text = permissionRequest?.fromYouApprove?[indexPath.row].relationShip
        //            if permissionRequest?.fromYouApprove?[indexPath.row].profileImage != nil && permissionRequest?.fromYouApprove?[indexPath.row].profileImage != ""
        //            {
        //                cell.displayImage.sd_setImage(with: URL(string: (permissionRequest?.fromYouApprove?[indexPath.row].profileImage)!), placeholderImage: UIImage(named: "DefaultProfileImg"))
        //            }
        //            // cell.displayImage.sd_setImage(with: URL(string: (permissionRequest?.fromYouApprove?[indexPath.row].profileImage)!), placeholderImage: UIImage(named: "DefaultProfileImg"))
        //        }
        
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == pendingCollectionView
        {
            
            Analytics.logEvent("SentToMembers_Pending_Request_clicked", parameters:nil)
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmPermissionsVC") as! ConfirmPermissionsVC
            
            vc.approve = "pending"
            vc.fromOrTo = "toyou"
            vc.memberId = permissionRequest?.toyouPending?[indexPath.row].userId
            vc.reponsedata = permissionRequest?.toyouPending?[indexPath.row]
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
        if collectionView == approvedCollectionView
        {
            Analytics.logEvent("SentToMembers_Approve_Request_clicked", parameters:nil)
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "SetRulesViewController") as! SetRulesViewController
            
            //            vc.approve = "approve"
            //            vc.fromOrTo = "toyou"
            vc.getMemberDetails = toyouApprove?[indexPath.row]
            vc.memberId = toyouApprove?[indexPath.row].userId
            
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
        
        //        if collectionView == fromYouPendingCV
        //        {
        //            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmPermissionsVC") as! ConfirmPermissionsVC
        //
        //            vc.approve = "pending"
        //            vc.fromOrTo = "fromyou"
        //            vc.memberId = permissionRequest?.fromYouPending?[indexPath.row].userId
        //            vc.reponsedata = permissionRequest?.fromYouPending?[indexPath.row]
        //
        //
        //            self.navigationController?.pushViewController(vc, animated: true)
        //
        //        }
        //
        //        if collectionView == toYouApprovedCV
        //        {
        //            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmPermissionsVC") as! ConfirmPermissionsVC
        //
        //            vc.approve = "approve"
        //            vc.fromOrTo = "fromyou"
        //            vc.memberId = permissionRequest?.fromYouApprove?[indexPath.row].userId
        //            vc.reponsedata = permissionRequest?.fromYouApprove?[indexPath.row]
        //
        //            self.navigationController?.pushViewController(vc, animated: true)
        //
        //        }
        
    }
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //
    //        //        let viewWidth = UIScreen.main.bounds.size.width
    //        //
    //        //
    //        //            var width = (viewWidth - 20) / 5.5
    //        //            if UIDevice.current.userInterfaceIdiom == .pad {
    //        //                width = (viewWidth - 30) / 8.5
    //        //            }
    //        //            return CGSize(width: width, height: width)
    //        //
    //
    //        let collectionWidth = collectionView.bounds.width
    //        return CGSize(width: collectionWidth/5, height: collectionWidth/2)
    //    }
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    //        return 0
    //    }
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    //        return 60
    //    }
    //
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let viewWidth = UIScreen.main.bounds.size.width
        
        if collectionView == collectionView || collectionView == collectionView {
            var width = (viewWidth - 30) / 3.5
            var height = (viewWidth - 20) / 2.5
            if UIDevice.current.userInterfaceIdiom == .pad {
                width = (viewWidth - 30) / 6
            }
            return CGSize(width: width, height: height)
        } else  {
            return CGSize(width: 60, height: 60)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
