//
//  Constants.swift
//  CloudPeppers
//
//  Created by Allvy on 25/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import Foundation

struct Constants {
    
    static let alertTitle = "coolpad"
    static let colour = UIColor(hexString: "#3B3D3D")
        
      
        //UIColor(red: 68/255, green: 86/255, blue: 148/255, alpha: 1.0)
    static let titleColor = UIColor(hexString: "#00A9B7")
    static let termsPage  = "http://www.unfoldlabs.com"
    static let version = "1.0.20"
    static let date = "August 23, 2019"
    static let googlemapsKey = "AIzaSyB7cJxxacb1JymyupzwHRLUGjcvetDr6Pw"
    static let MilesForFoot:Float = 0.000189394
    static let MetersForMile:Float = 1609.34
    static let FeetsForMeters:Float = 0.3048
    
}
struct API {
    
    //*********************     Production Pointing     **********************************//
    
   static let ipAddress_dev = "http://34.213.234.171:8080/coolpad_api_v1_pre_prod/"
    

    //*************************      Local     *****************************************//
    
  //   static let ipAddress_dev = "http://172.26.9.64:8080/"
  //   static let ipAddress_dev = "http://172.26.9.64:8085/coolpad_api_v1_dev/"
  //   static let ipAddress_dev = "http://172.26.9.51:8080/"
    
    

    //***********************    Dev Pointing   **********************************//
    
    
  //  static let ipAddress_dev = "http://35.165.108.250:8080/coolpad_api_v1_dev/"
    

    //********************  Coolpad QA Pointing     ******************************//
    
    
 //     static let ipAddress_dev = "http://35.165.108.250:8080/coolpad_api_v1_qa/"
 //    static let ipAddress_dev = "http://35.165.108.250:8080/coolpad_api_v1_pre_qa/"
    

    static let runningIP = ipAddress_dev
    static let createAccount_URL = runningIP + "signUpUser"
    static let login_URL = runningIP + "loginParent"
    static let getOrganizationInfo = runningIP + "getOrganizationInfo"
    static let getMemberInfo = runningIP + "getMemberInfo"
    static let panic = runningIP + "panic"
    static let updateParentProfile = runningIP + "updateParentProfile"
    static let changePassword = runningIP + "changePassword"
    static let updateMemberProfile = runningIP + "updateMemberProfile"
    static let deleteMember = runningIP + "deleteMember"
    static let getAllNotificationsBYMemberId = runningIP  + "getAllNotificationsBYMemberId"
    static let sendPermissionInfo = runningIP  + "sendPermissionInfo"
    static let getMemberDetailsByMemberId = runningIP  + "getMemberDetailsByMemberId"
    static let getFromYourToYou = runningIP  + "getFromYouToYou"
    static let getMemberPermissionToYouFromYou = runningIP  + "getMemberPermissionToYouFromYou"
    static let grantPermission = runningIP  + "grantPermission"
    static let deleteAlarm = runningIP  + "deleteAlarm"
    static let deleteReminder = runningIP  + "deleteReminder"
    static let getRequestPermissions = runningIP  + "getRequestPermissions"
    static let aboutUs = runningIP + "aboutUs"
    static let resetPasswordOTP = runningIP + "resetPasswordOTP"
    static let resetPassword = runningIP + "resetPassword"
    static let saveGeocoordinate = runningIP + "saveGeocoordinate"
    static let verifyOTP = runningIP + "verifyOTP"
    static let reSendOTP = runningIP + "reSendOTP"
    static let addMember  = runningIP + "addMember"
    static let getFenceByIndividualMember = runningIP + "getFenceByIndividualMember"
    static let createFence = runningIP + "createFence"
    static let deleteFence = runningIP + "deleteFence"
    static let saveAlarm = runningIP + "saveAlarm"
    static let saveReminder = runningIP + "saveReminder"
    static let saveDailyTimeLimits = runningIP + "saveDailyTimeLimits"
    static let getAllDailyTimeLimits = runningIP + "getAllDailyTimeLimits"
    static let saveRestrictedTimeLimits = runningIP + "saveRestrictedTimeLimits"
    static let deleteDailyTimeLimitsByMemberId = runningIP + "deleteDailyTimeLimitsByMemberId"
    static let deleteRestrictedTimeLimitsByMemberId = runningIP  + "deleteRestrictedTimeLimitsByMemberId"
    static let getAllRestrictedTimeLimits = runningIP + "getAllRestrictedTimeLimits"
    static let getGamesAndApps = runningIP + "getAppsAndGames"
    static let saveAppsAndGames = runningIP + "saveAppsAndGames"
    static let getallContacts = runningIP + "getAllContactsByUser"
    static let resendPassword = runningIP + "resendPassword"
    static let uploadcontacts = runningIP + "uploadContacts"
    static let unlockMemberDevice = runningIP + "lockOrUnlockDevice"
    static let deActivate = runningIP + "deleteUserById"
    static let sendingSMS = runningIP + "sendingSMS"
    static let editPermission = runningIP + "editPermission"
    static let getBlockedAllowdURL = runningIP + "getBlockedAllowdURL"
    static let updatingURLs = runningIP + "updatingURLs"
    static let saveURL = runningIP + "saveURL"
    static let validateAccessPin = runningIP + "validateAccessPin"
    static let deleteNotificationById = runningIP + "deleteNotificationById"
    static let saveContacts = runningIP + "saveContacts"
    static let getReports = runningIP + "getReports"
    static let getAllPlans = runningIP + "getAllPlans"
    static let webUrls = runningIP + "getAllWebURL"
    static let saveUserSelectedPlan = runningIP + "saveUserSelectedPlan"
    static let renewalPlan = runningIP + "renewalPlan"
    static let getARNByMemberIdlan = runningIP + "getARNByMemberId?memberId="
    static let getGeoHistory = runningIP + "getGeoHistory?memberId="
    static let getLocationHistory = runningIP + "getGeoMemberHistory"
   

    
    // *******Staging Analytics URL ************
    
   //  static let urlAnalytices = "http://35.165.108.250:8080/"
    
    // *******Production Analytics URL ************
    
    static let urlAnalytices = "http://34.213.234.171:8080/"
    

 
    static let token =  urlAnalytices + "CloudPeppers_common/api/common/token"
    static let devicedetails = urlAnalytices + "CloudPeppers_common/api/common/devicedetails"
    static let deviceosdetails = urlAnalytices + "CloudPeppers_common/api/common/deviceosdetails"

    
    
    //*********Web Url's Loading*********
    static let cpadminportal = "http://35.165.108.250:8080/cpadminportalqa/faq.html"
    static let privacyPage = "http://unfoldlabs.com/dump/public_html/UnfoldPrints/walgreens/privacy.html"
     static let paymentTemrsNCon  =   "https://coolpad.us/terms/"
    
   

}

/*****User_Defaults USED****/

/*++++++++++++++++++++++++
 
 rememberme_id : Remember Me email ID
 rememberme_password : Remember Me Password
 profile_details : Login response (User details)
 
 ++++++++++++++++++++++++*/
