//
//  APIServices.swift
//  CloudPeppers
//
//  Created by APPLE on 3/26/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD
class NetworkManager {
    static let sharedInstance = NetworkManager()
    
    
    
    let defaultManager: Alamofire.SessionManager = {
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "services.com": .disableEvaluation
        ]
        let configuration = URLSessionConfiguration.default
        
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        
        return Alamofire.SessionManager(
            configuration: configuration,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
    }()
}

typealias CompletionHandler = (_ response:(Bool, Any)) -> Void

class APIServices: UIViewController {
    
    class func postDataToServer(url:String, parameters: NSMutableDictionary, controller: UIViewController,headers1: [String : String]? ,completionHandler: @escaping CompletionHandler)
    {
        SVProgressHUD.show()
        
      //  let headers1: [String : String]?
       // let sdf = headers: [String : String]?)
        Alamofire.request(url, method: .post, parameters: parameters as? [String:Any] ,encoding: JSONEncoding.default, headers:getHeaders(headers:headers1)).responseJSON {
            response in
            
            
            
            SVProgressHUD.dismiss()
            
            switch response.result {
            case .success:
                if let theJSONData = try? JSONSerialization.data(withJSONObject: response.value as Any, options:[])
                {
                    do{
                        if let responseInfo = try? JSONDecoder().decode(ApiStatus1.self, from: theJSONData) {
                            print(responseInfo)
                            if responseInfo.status == true{
                                completionHandler((true,response.value as Any))
                            }
                            else{
//                                if responseInfo.statusCode == 400
//                                {
//                                    completionHandler((true,response.value as Any))
//                                }else
                                //{
                                    let alert = UIAlertController(title: "", message: responseInfo.message, preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                                    controller.present(alert, animated: true, completion: nil)
                               // }
                            }
                        }
                    }
                   
                }
                
                
                break
            case .failure(let error):
                print(error)
                completionHandler((false,response.value as Any))
                
                let alert = UIAlertController(title: "", message: "Unable to connect server", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                controller.present(alert, animated: true, completion: nil)
            }
        }

    }
    
    
    class func getHeaders(headers: [String : String]?) -> [String : String]{
        
        if UserDefaults.standard.string(forKey: "jwtToken") != nil
            
        {
            
            
            let token = "Bearer" + " "  + "\(UserDefaults.standard.string(forKey: "jwtToken")!)"
            
            print(token)
            
            
            var defaultHeaders :HTTPHeaders = ["Authorization" : token]
            
            
            
            
            
            
            
            
            
            // var defaultHeaders :HTTPHeaders = ["imei" :   UserDefaults.standard.string(forKey: "imei")!,
            //        "authToken" :   UserDefaults.standard.string(forKey: "token")!]
            print(defaultHeaders)
            if let headers = headers{
                for header in headers {
                    defaultHeaders[header.key] = header.value
                    print(defaultHeaders)
                }
            }
            return defaultHeaders
        }else
        {
            var defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders as [String : String]
            
            print(defaultHeaders)
            if let headers = headers{
                for header in headers {
                    defaultHeaders[header.key] = header.value
                    print(defaultHeaders)
                }
            }
            return defaultHeaders
        }
    }
    
    
    class func postDataToServerre(url:String, parameters: NSMutableDictionary, controller: UIViewController ,headers1: [String : String]?,completionHandler: @escaping CompletionHandler)
    {
        SVProgressHUD.show()
        
        
        let data2 = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data2!)
        
        
        Alamofire.request(url, method: .post, parameters: parameters as? [String:Any] ,encoding: JSONEncoding.default, headers: getHeaders(headers:headers1)).responseJSON {
            response in
            
            
            
            SVProgressHUD.dismiss()
            
            switch response.result {
            case .success:
                
                let  responses = response.value as? [String:Any]
                
                
                let status = responses!["status"] as? Bool
                
                let statusCode = responses!["statusCode"] as? Int
                
                  if  statusCode == 400
                  {
                    completionHandler((true,response.value as Any))
                }
                
                
                if status == true
                {
                     completionHandler((true,response.value as Any))
                    return
                }
                
               print(response.value)
                
                if let theJSONData = try? JSONSerialization.data(withJSONObject: response.value as Any, options:[])
                {
                    do{
                        if let responseInfo = try? JSONDecoder().decode(ApiStatus12.self, from: theJSONData) {
                            print(responseInfo)
                            if responseInfo.status == true{
                                completionHandler((true,response.value as Any))
                            }
                            else{
                                var string = String()
                                
                                
                                for each in (responseInfo.response?.invalidpermissions!)!
                                {
                                    string.append("\n")
                                    
                                    
                                    for each1 in (organizationdetails?.rulez)!
                                    {
                                        
                                        if each1.rulesKey == each
                                        {
                                            string.append(each1.name!)
                                        }
                                    }
                                    
                                    
                                  
                                    
                                }
                                let alert = UIAlertController(title: "", message: responseInfo.message! + "\n"  + string, preferredStyle: .alert)
                                completionHandler((false,response.value as Any))
                               // let alert = UIAlertController(title: Constants.alertTitle, message: responseInfo.message, preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                                controller.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    
                }
                
                
                break
            case .failure(let error):
                print(error)
                completionHandler((false,response.value as Any))
                
                let alert = UIAlertController(title: "", message: "Unable to connect server", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                controller.present(alert, animated: true, completion: nil)
            }
        }
        
    }
//    class func registerUser(completion: @escaping ([String: Any]) -> Void, errorHandler: @escaping (Error)->Void) {
//
//        //let profileInfo = UserDefaultsHelper.GetProfileInfo()
//
//
//        let params = ["emailId": "emailId",
//                      "mobileNo": "123456789",
//                      "imeiNo": UIHelper.getUUID(),
//                      "address": "CA"] as [String: Any]
//
//        HttpWrapper.post(with: API.token,
//                         parameters: params,
//                         headers: nil,
//                         completionHandler: { (response) in
//                            print("REGISTER_USER_RESPONSE \(response)")
//                            if let userId = response["userId"] {
//
//                            }
//                            completion(response)
//        }) { (error) in
//            errorHandler(error)
//        }
//    }
    
    class func postDataToServerp(url:String, parameters: NSMutableDictionary, controller: UIViewController,headers1: [String : String]? ,completionHandler: @escaping CompletionHandler)
    {
        SVProgressHUD.show()
        
        
        
        
        
        Alamofire.request(url, method:.put, parameters: parameters as? [String:Any] ,encoding: JSONEncoding.default, headers: getHeaders(headers:headers1)).responseJSON {
            response in
            
            
            
            SVProgressHUD.dismiss()
            
            switch response.result {
            case .success:
                if let theJSONData = try? JSONSerialization.data(withJSONObject: response.value as Any, options:[])
                {
                    do{
                        if let responseInfo = try? JSONDecoder().decode(ApiStatus1.self, from: theJSONData) {
                            print(responseInfo)
                            if responseInfo.status == true{
                                completionHandler((true,response.data))
                            }
                            else{
                                 let alert = UIAlertController(title: "", message: responseInfo.message, preferredStyle: UIAlertController.Style.alert)
                                
                                completionHandler((false,response.data))
                              //  let alert = UIAlertController(title: Constants.alertTitle, message: responseInfo.message, preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                                controller.present(alert, animated: true, completion: nil)
                            }
                        }
                    }catch
                    {
                        
                        print(error)
                    }
                    
                }
                
                
                break
            case .failure(let error):
                print(error)
                completionHandler((false,response.value as Any))
                
                let alert = UIAlertController(title: "", message: "Unable to connect server", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                controller.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    class func putDataToServer(url:String, parameters: NSMutableDictionary, controller: UIViewController,headers1: [String : String]?  ,completionHandler: @escaping CompletionHandler)
    {
        SVProgressHUD.show()
        
        Alamofire.request(url, method: .put, parameters: parameters as? [String:Any] ,encoding: JSONEncoding.default, headers: getHeaders(headers:headers1)).responseJSON {
            response in
            
            
            
            SVProgressHUD.dismiss()
            
            switch response.result {
            case .success:
                if let theJSONData = try? JSONSerialization.data(withJSONObject: response.value as Any, options:[])
                {
                    do{
                        if let responseInfo = try? JSONDecoder().decode(ApiStatus1.self, from: theJSONData) {
                            print(responseInfo)
                            if responseInfo.status == true{
                                completionHandler((true,response.value as Any))
                            }
                            else{
                                completionHandler((false,response.value as Any))
                                let alert = UIAlertController(title: "", message: responseInfo.message, preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                                controller.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    
                }
                
                
                break
            case .failure(let error):
                print(error)
                completionHandler((false,response.value as Any))
                
                let alert = UIAlertController(title: "", message: "Unable to connect server", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                controller.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    class func getDataFromServer(url:String, parameters: [String : String]?, controller: UIViewController,headers1: [String : String]?  ,completionHandler: @escaping CompletionHandler)
    {
        SVProgressHUD.show()
        
        Alamofire.request(url, method: .get, parameters: parameters ,encoding: JSONEncoding.default, headers: getHeaders(headers:headers1)).responseJSON {
            response in
            
            SVProgressHUD.dismiss()
            
            switch response.result {
            case .success:
                if let theJSONData = try? JSONSerialization.data(withJSONObject: response.value as Any, options:[])
                {
                    do{
                        if let responseInfo = try? JSONDecoder().decode(ApiStatus1.self, from: theJSONData) {
                            print(responseInfo)
                            if responseInfo.status == true{
                                completionHandler((true,response.value as Any))
                            }
                            else{
                                completionHandler((false,response.value as Any))
                                let alert = UIAlertController(title: "", message: responseInfo.message, preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                                controller.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    
                }
                break
            case .failure(let error):
                print(error)
                completionHandler((false,response.value as Any))
                
                let alert = UIAlertController(title: "", message: "Unable to connect server", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                controller.present(alert, animated: true, completion: nil)
            }
        }
        
    }
}
class HttpWrapper {
    class func get(with url: String, parameters: [String : String]?, headers: [String : String]?, completionHandler: @escaping ([String : Any])->Void, errorHandler: @escaping ((Error))->Void) {
        _ = NetworkManager.sharedInstance.defaultManager
            
            .request(url, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: getHeaders(headers: headers)).validate(statusCode: 200..<299).responseJSON { (response) in
                
                if response.result.isSuccess{
                    if (response.result.value as? [Any]) != nil{
                        //completionHandler(jsonResponse as! [[String : String]])
                        print(response)
                        completionHandler(response.result.value as! [String : String])
                    }else{
                        print(response)
                        completionHandler(response.result.value as! [String : Any])
                        //  completionHandler(response.result.value as! [[String : String]])
                        //  completionHandler(response.result.value as! [Any])
                        
                    }
                }else{
                    errorHandler(response.error!)
                }
        }
        
    }
    class func gets(with url: String, parameters: [String : String]?, headers: [String : String]?, completionHandler: @escaping (Data?, Error?)->Void, errorHandler: @escaping ((Error))->Void) {
        _ = NetworkManager.sharedInstance.defaultManager
            
            .request(url, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: getHeaders(headers: headers)).validate(statusCode: 200..<299).responseJSON { (response) in
                print(response)
                
                print(request)
                if response.result.isSuccess{
                    if (response.result.value as? [Any]) != nil{
                        //completionHandler(jsonResponse as! [[String : String]])
                        print(response)
                        
                        completionHandler(response.data, nil)
                        
                        //   completionHandler(response.result.value as! [String : String])
                    }else{
                        completionHandler(response.data, nil)
                        print(response)
                        //   completionHandler(response.result.value as! [String : Any])
                        //  completionHandler(response.result.value as! [[String : String]])
                        //  completionHandler(response.result.value as! [Any])
                        
                    }
                }else{
                    errorHandler(response.error!)
                }
        }
        
}
    
    class func post(with url: String, parameters: [String : Any]?, headers: [String : String]?, completionHandler: @escaping (([String:Any]))->Void, errorHandler: @escaping ((Error))->Void) {
        let sessionManager = NetworkManager.sharedInstance.defaultManager
        sessionManager.session.configuration.timeoutIntervalForRequest = 1
        sessionManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getHeaders(headers: headers)).validate(statusCode: 200..<299).responseJSON { (response) in
            if response.result.isSuccess{
                if let jsonResponse = response.result.value as? [String:Any]{
                    completionHandler(jsonResponse)
                    //print(jsonResponse )
                }else{
                    
                    
                    
                    // completionHandler(response.result.value as! [String : Any])
                    //completionHandler((response.result.value as? [String:Any])!)
                    //print(response.error as! [String : Any])
                }
            }else{
                errorHandler(response.error!)
            }
        }
    }
    class func posts(with url: String, parameters: [String : Any]?, headers: [String : String]?, completionHandler: @escaping (Data?, Error?)->Void, errorHandler: @escaping ((Error))->Void) {
        let sessionManager = NetworkManager.sharedInstance.defaultManager
        
        sessionManager.session.configuration.timeoutIntervalForRequest = 1
        sessionManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getHeaders(headers: headers)).validate(statusCode: 200..<299).responseJSON { (response) in
            
            if response.result.isSuccess{
                
                if (response.result.value as? [String:Any]) != nil{
                     print(response.value)
                    completionHandler(response.data, nil)
                    
                }else{
                    print(response.result)
                    completionHandler(response.data, nil)
                    // print(response.data as Any)
                }
            }else{
                errorHandler(response.error!)
            }
        }
    }
    
     class func getHeaders(headers: [String : String]?) -> [String : String]{
        
        
          print(UserDefaults.standard.string(forKey: "jwtToken"))
        
        if UserDefaults.standard.string(forKey: "jwtToken") != nil
        
        {
            
            
            let token = "Bearer" + " "  + "\(UserDefaults.standard.string(forKey: "jwtToken")!)"
            
            print(token)
            
            
            var defaultHeaders :HTTPHeaders = ["Authorization" : token]

            
            
        
            
            
            
            
            
           // var defaultHeaders :HTTPHeaders = ["imei" :   UserDefaults.standard.string(forKey: "imei")!,
                                       //        "authToken" :   UserDefaults.standard.string(forKey: "token")!]
            print(defaultHeaders)
            if let headers = headers{
                for header in headers {
                    defaultHeaders[header.key] = header.value
                    print(defaultHeaders)
                }
            }
            return defaultHeaders
        }else
        {
            var defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders as [String : String]
            
            print(defaultHeaders)
            if let headers = headers{
                for header in headers {
                    defaultHeaders[header.key] = header.value
                    print(defaultHeaders)
                }
            }
            return defaultHeaders
        }
    }
}
