//
//  CPModel.swift
//  CloudPeppers
//
//  Created by APPLE on 3/26/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import Foundation


struct SharedData {
    static var data = SharedData()
    var organizationdetails:response?
    var responseappsGames:responseappsGames?
      var responsecalls:responsecalls?
    var responseWebUrl:ResponseWebUrls?
    
    
     var responsePlans:[ResponsePlans]?
  //   private init(){}
    var image1:UIImage?
    var imagedata:[Data]?
    var imagedata1:Data?
    var imagedata2:Data?
    var imagedata3:Data?
    var imagedata4:Data?
    var membersGeoList:[MembersGeoList]?
    var getMemberDetails:[responses]?
  //  var individualMemberFence:[IndividualMemberFence]?
}

struct ApiStatus: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: response?
}

//struct ChatViewModel:Codable
//{
//
//}

class Chat {
    
    var chatUser: String?
    var chatUser_Pic: String? // Profile pic url
    var chatMessage: String?
    var fromme: String?
    
    init(arg_user:String,arg_pic: String,arg_message:String,frommes: String) {
        
        chatUser = arg_user
        chatUser_Pic = arg_pic
        chatMessage = arg_message
        fromme = frommes
    }
}
protocol ChatViewDelegate
{
    func onItemAdded(name:String) -> ()
}

protocol ChatViewPresentable
{
    var newchat: String? {get}
}

class ChatViewModel: ChatViewPresentable {
    
    var chats: [Chat] = []
    var newchat: String?
    weak var viewc: ChatViewControllerDelegate?
    
    
    init(view: ChatViewControllerDelegate) {
        
        viewc = view
//        let chat1 = Chat(arg_user: "", arg_pic: "", arg_message: "Hi",frommes:"Me")
//        let chat2 = Chat(arg_user: "", arg_pic: "", arg_message: "Hi",frommes:"Me")
//        chats.append(contentsOf: [chat1,chat2])
    }
    
    deinit {
        print("ChatViewModel de-initialized")
    }
}

extension ChatViewModel: ChatViewDelegate
{
    func onItemAdded(name:String) {
        
        chats.append(Chat(arg_user: "", arg_pic: "", arg_message: newchat!,frommes: name))
        viewc?.didNewChatMessagesRecieved()
    }
}


struct ApiStatusProfile: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: [responses]?
}


struct ApiStatusaddmember: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: [responses]?
}

struct ApiStatusgetMemberDeatils: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: Responsedata?
}

struct ApiStatusgetDailyTimeLimit: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: [DailyTimedata]?
}


struct ApiStatusgetresTimeLimit: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: [resdata]?
}

struct resdata :Codable
{
    
    let restrictedTimeId: Int?
    let fromTime: String?
    let toTime: String?
    let days: String?
    let userId :Int?
    let memberId :Int?
    let createdDate: Int?
    let updatedDate: Int?
}


struct Responsedata :Codable
{
    let alarmsInfo:[AlarmsInfo]?
    let remindersInfo:[remainderList]?
    let  batteryStatus : String?
    let  email  : String?
    let firstName : String?
    let  lastName : String?
    let mobileNo : String?
    let platform : String?
    let relationShip : String?
    let userId  : Int?
}

struct DailyTimedata :Codable
{
    
    let createdDate :Int?
   let  dailyTimeId :Int?
    let dailyTimeInMinutes :Int?
   let  day : String?
   let  firstName : String?
   let lastName : String?
   let  memberId :Int?
   let updatedDate : String?
   let  userId :Int?
    
}
struct AlarmsInfo :Codable
{
    
    let  alaramKey  : String?
   let   alarmId  : Int?
   let  createdDate  : Int?
    let  days  : String?
    let description : String?
    let memberId: Int?
    let  repeatOrNot : Bool?
    let  time  : String?
    let  title  : String?
    let  updatedDate : String?
    let  userId  : Int?

}

struct ApiStatuspermissionRequest: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: PermissionRequest?
}


struct PermissionRequest :Codable
{
    var toyouPending :[responses]?
    var toyouApprove :[responses]?
    var fromYouPending:[responses]?
    var fromYouApprove:[responses]?
    
  
}



struct ApiStatusNotifications: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: NotifiationResponse?
}


struct responseappsGames :Codable
{
    let approve:[approve]?
    let blocked:[approve]?
}

struct responsecalls :Codable
{
    let allowed:[allowed]?
    let blocked:[allowed]?
}
struct ResponseWebUrls :Codable
{
    let allow:[allow]?
    let block:[allow]?
}


struct approve :Codable
{
    
    let appsAndGamesId:Int?
    let appName: String?
    let memberId:Int?
    let userId:Int?
    let packeage: String?
    let appImg: String?
    let appStatus: String?
    let kidOrMember: String?
    let firstName: String?
    let lastName: String?
    let createdDate:Int?
    let updatedDate: Int?
    
}
struct allowed :Codable
{

   let  contactId :Int?
   let  contactName : String?
   let  contactNumber:String?
   let  contactStatus : String?
   let  createdDate :Int?
   let  memberId :Int?
    let updatedDate : Int?
   let  userId: Int?
}

struct allow :Codable
{
    let bwfid:Int?
    let webFilterLKPId: Int?
    let title: String?
    let url: String?
    let addedBy: String?
    let createdDate: Int?
    let updatedDate:Int?
    let userId:Int?
    let memberId:Int?
    

}










struct ApiStatusAppsGames: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: responseappsGames?
    
    
}

struct ApiStatusCalls: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: responsecalls?
    
    
}


struct ApiStatusPlans: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: [ResponsePlans]?
    
    
}

struct ApiStatusWebUrl: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: [allow]?
    
    
}

struct ApiStatusLocationHistrory: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: [LocationHistrory]?
    
    
}

struct LocationHistrory :Codable
{
    let createdDate:Int?
    let geoHistroyId:Int?
    let inOrOut:String?
   let memberId :Int?
    let msg :String?
}




struct ApiStatusWebUrls: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: ResponseWebUrls?
    
    
}

struct ApiStatusHistrory: Decodable {
    
    let message : String?
    
    let status : Bool?
    
    let statusCode : Int?
    
    let response: [NewHistoryRecord]?
    
  
}

struct NewHistoryRecord :Codable
    
{
    
    var memberId :Int?
    
    var latitude : String?
    
    var longitude : String?
    
    var geoId :Int?
    
    var createdDate :Int?
    
}

struct ResponsePlans :Codable
{
    let customerPlansId : Int?
     let customerId : Int?
   let planSequenceId : Int?
 let planName : String?
    let  typeOfPlan : String?
   let featureName : String?
 let featureStatus : String?
 let priceForFeature : String?
 let fullPrice : String?
 let featureKey: String?
    let planKey: String?
 let planDescription1 : String?
 let planDescription2 : String?
 let addedFor : String?
 let createdDate : Int?
    let basePlusOrNot:String?
    let basePlanOfferPrice:Double?
 let basePluseGroupA:[customerPlansList]?
   let basePluseGroupB:[customerPlansList]?
    
}

struct customerPlansList :Codable {
    
    let  customerPlansId: Int?
     let  customerId: Int?
     let  planSequenceId: Int?
  let  planName: String?
    let  typeOfPlan: String?
    let  featureName: String?
  let  featureStatus: String?
    let planKey:String?
  let  priceForFeature: String?
    let  fullPrice: String?
    let  planDescription1: String?
 let  planDescription2: String?
    let basePlusOrNot:String?
    let featureInfo:String?
   let basePlanOfferPrice:Double?
   let  addedFor: String?
    let featureKey: String?
    let  createdDate: Int?
    let  updateDate: Int?
    
}

struct NotifiationResponse :Codable {
    
    let remainderList : [remainderList]?
    let allNotifications : [allNotifications]?
    
}

struct remainderList :Codable
{
     let  remainderId : Int?
     let  reminderDate : String?
     let  time : String?
     let  title : String?
     let  description: String?
     let  userId : Int?
     let  memberId : Int?
     let  createdDate : Int?
     let  updatedDate : String?
    
}


struct allNotifications :Codable
{
    
     var  notificationId : Int?
     var  userId :  Int??
     var  memberId : Int?
     var  userName : String?
     var  memberName : String?
     var  memberMobileNo : String?
     var  notificationText : String?
     var  notificationStatus : String?
     var  permissionIds : String?
     var  createdDate : Int?
     var  updatedDate :Int?
     var  title : String?
     var  type : String?
     var  imgForNotifi : String?
   
    
}



struct ApiStatusmembersGeoList: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: MembersGeoListresponse?
}




struct ApiStatuspermission: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: Permissionlist?
}

struct Permissionlist :Codable
    
{
    let give : [Give]?
    
    let ask  : [Ask]?
    
    
}

struct Give :Codable
{
    let permissionId: Int?
    let roleId: Int?
    let userId: Int?
    let memberId: Int?
    let userName : String?
    let memberName : String?
    let memberMobileNo: String?
    let ask: String?
    let give: String?
    let roleType: String?
    let statusOfRequest: String?
    let createdDate:  Int?
    let updatedDate:  Int?
    let flag: Bool?
    let approveOrReject :String?
    
}
struct Ask :Codable
{
    let permissionId: Int?
    let roleId: Int?
    let  userId: Int?
    let  memberId: Int?
    let  userName : String?
    let  memberName : String?
    let  memberMobileNo: String?
    let ask: String?
    let give: String?
    var roleType: String?
    let statusOfRequest: String?
    let createdDate:  Int?
    let updatedDate:  Int?
    let flag: Bool?
    let approveOrReject :String?
    
}




struct MembersGeoListresponse :Codable

{
    let membersGeoList:[MembersGeoList]?
    let isRequireMembersLocation:String?
}

struct MembersGeoList :Codable
    
{
    
    var coordinateLatitude : String?
    var coordinateLongitude : String?
    var createdDate : String?
    var geoId : Int?
    var inOrOut: String?
    var isRequireMembersLocation  : String?
    var latitude : String?
    var longitude : String?
    var updateDate : Double?
    var userId : Int?
    var batteryStatus : String?
    
    
    
}

//membersGeoList





struct responses :Codable {
   var memberId : Int?
   var firstName : String?
   var  lastName : String?
   var  relationShip : String?
   var  mobileNo : String?
   var batteryStatus : String?
   var  email : String?
    var login_type:String?
   var  password : String?
   var  fcmId : String?
   var  platform : String?
   var profileImage: String?
     var toYou_Approved: Bool?
   var  mobileStatus : String?
   var countryCode : String?
    var relationToMe:String?
    var accessPin:String?
   var  type : String?
   var  createdDate : Int?
   var  updatedDate : Int?
   var  userId : Int?
   var  nickName : String?
   var  temrsAccepted : Bool?
    var snsARN:String?
}
struct AnyValue: Codable {
    
    var int: Int?
    var string: String?
    var bool: Bool?
    var double: Double?
    
    init(_ int: Int) {
        self.int = int
    }
    
    init(_ string: String) {
        self.string = string
    }
    
    init(_ bool: Bool) {
        self.bool = bool
    }
    
    init(_ double: Double) {
        self.double = double
    }
    
    init(from decoder: Decoder) throws {
        if let int = try? decoder.singleValueContainer().decode(Int.self) {
            self.int = int
            return
        }
        
        if let string = try? decoder.singleValueContainer().decode(String.self) {
            self.string = string
            return
        }
        
        if let bool = try? decoder.singleValueContainer().decode(Bool.self) {
            self.bool = bool
            return
        }
        
        if let double = try? decoder.singleValueContainer().decode(Double.self) {
            self.double = double
        }
    }
    
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        
        if let anyValue = self.value() {
            if let value = anyValue as? Int {
                try container.encode(value)
                return
            }
            
            if let value = anyValue as? String {
                try container.encode(value)
                return
            }
            
            if let value = anyValue as? Bool {
                try container.encode(value)
                return
            }
            
            if let value = anyValue as? Double {
                try container.encode(value)
                return
            }
        }
        
        try container.encodeNil()
    }
    
    
    func value() -> Any? {
        return self.int ?? self.string ?? self.bool ?? self.double
    }
}

struct ApiStatus1: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
   
}

struct ApiStatus12: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response:responses1?
    
}

struct ApiStatusReports: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response:responseReports?
    
}


struct ApiStatusPayment: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response:ResponsePayment?
    
}
struct ResponsePayment: Codable

{
   // let amount : Int?
    let createdDate: Int?
    let expireDate: Int?
    let orderId :String?
    let status :String?
    let transactionId:String?
    let userPlans:UserPlans?
    
}




struct responseReports: Codable
    
{
    let reports:[Reports]
    
}
struct Reports: Codable
{

   var reportsId:Int?
   var type: String?
   var info: String?
   var createdDate:Int?
   var duration:String?
   var dateandtime:Double?
  
 
}


struct responses1: Codable
    
{
    
    let invalidpermissions:[String]?
    
}

struct AboutUsStatus: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: Aboutresponse?
    
}
struct Aboutresponse: Codable {
   var content : String?
   var logo : String?
   var  privacy : String?
   var  contactUs : String?

}

struct profile: Codable{
    let batteryStatus : String?
    let countryCode : String?
    let createdDate : Int?
    let email: String?
    let fcmId : String?
    let firstName : String?
    let lastName: String?
    let login_type: String?
    let mobileNo : String?
    let mobileStatus : String?
    let onlineStatus: String?
    let paymentMode: Bool?
    let otp : String?
    var jwtToken:String?
    let password : String?
    let platform : String?
    let profileImage : String?
    let temrsAccepted : Bool?
    let toYouPermisionList: String?
    let updateDate :  Int?
    let userId : Int?
    let whoUR: String?
    let userPlans:UserPlans?
}

struct  UserPlans: Codable{
    
    let featureList:[FeatureList]?
    let planDescription1:String?
     let fullPrice:String?
      let planKey:String?
      let planName:String?
      let planSequenceId:Int?
    let remainDays:Int?
    let expDate:Int?
    
}
struct  FeatureList: Codable{
    
    var featureInfo :String?
    var featureKey :String?
    var featureName :String?
    var priceForFeature :String?
    
}

struct  Organizationdetails: Codable{
    var response : response?
}
struct  response : Codable{
    var organization:organization?
    var countriesInfo:[countriesInfo]?
    var roles:[roles]?
    var rulez:[rulez]?
}
struct rulez : Codable {
    var createdDate : Int?
    var id: Int?
    var name : String?
    var rulesKey : String?
}
struct organization : Codable {
    var sId : Int?
    var versionId : String?
    var splashImage : String?
    var powerdBy : String?
    var logo : String?
    var orginationURL : String?
    var orginationName : String?
    var createdDate : String?
    var updatedDate : String?
}
struct countriesInfo: Codable {
    var  countryId:Int?
    var countryCode:String?
    var countryName:String?
}
struct roles: Codable {
    var roleInfoId:Int?
    var type:String?
    var roleKey:String?
    var fullAccess:Bool?
    var imageUrl:String?
}
struct IndividualMemberFence :Codable
{
    
    var geoId: Int?
    var userId: Int?
    var  memberId: Int?
    var serialKey : String?
    var longitude : String?
    var latitude : String?
    var name : String?
    var coordinateLongitude : String?
    var coordinateLatitude : String?
    var radius: Float?
    var isRadiusOrNot : String?
    var createdDate: Int?
    var updatedDate : String?
    
}
struct ApiStatusgetIndividualMember: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: [[IndividualMemberFence]]?
}
struct ApiStatusARN: Decodable {
    let message : String?
    let status : Bool?
    let statusCode : Int?
    let response: ARndata?
    
    
}
struct ARndata :Codable
{
    
    let platform:String?
    let snsARN:String?
}
