//
//  AppDelegate.swift
//  CloudPeppers
//
//  Created by Allvy on 14/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import SVProgressHUD
import GooglePlaces
import MapKit
import UserNotifications
import Firebase
import FirebaseMessaging
import CoreData
import AWSSNS

import EventKit
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,URLSessionDelegate{
    
    
    var window: UIWindow?
    var locManager = CLLocationManager()
    
    var currentLocation: CLLocation!
    var latitude : Double!
    var longtitude:Double!
    let gcmMessageIDKey = "gcm.message_id"
    
    var isinChatView:String?
      var isinDirectionView:String?
    
//    var notificationTap : Bool?
//    var notificationFenceID : String?
     var notificationTap: Bool?
    
    
  //  let SNSPlatformApplicationArn = "arn:aws:sns:us-east-1:167315797857:app/APNS_SANDBOX/CpiOSTest"
    
   //*****Staging***
  // let SNSPlatformApplicationArn = "arn:aws:sns:us-east-1:167315797857:app/APNS_SANDBOX/CpiOSTest"
    
    
     //*****Production***
    let SNSPlatformApplicationArn = "arn:aws:sns:us-east-1:167315797857:app/APNS/CPiOS"
    

   
    var notificationBargeCount:String?
    var badgeCount = 0
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        locManager.requestWhenInUseAuthorization()
        
      //  IQKeyboardManager.shared.enable = true
        
        let colors: [UIColor] = [.red, .white]
        UIDevice.current.isBatteryMonitoringEnabled = true
        print(batteryLevels)
        print(batteryLevel)
        print(UIDevice.current.batteryLevel)
        FirebaseApp.configure()
//
        isinChatView = "true"
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "chatView"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showDirectionView(_:)), name: NSNotification.Name(rawValue: "inDirectionView"), object: nil)
        
        isinDirectionView = "false"
        
        
        
        
        
        
       // UserDefaults.standard.set(nil, forKey: "jwtToken")
        

//        UserDefaults.standard.removeObject(forKey: "profile_details")
        
    
        UserDefaults.standard.set("fromChatfalse", forKey: "fromChat")
        
        UserDefaults.standard.set(nil, forKey: "datafromNotification")
//    let credentialsProviderss = AWSCognitoCredentialsProvider(
//            regionType: AWSRegionType.USEast1, identityPoolId: "us-east-1:957213fd-9b87-496c-9750-e0814c7ec8e1")
//
//
//
//    let credentialsProvider1 = AWSCredentials(accessKey: "AKIASN5GMO5QWDGFTHIO", secretKey: "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", sessionKey: nil, expiration: nil)
//
//        let defaultServiceConfiguration = AWSServiceConfiguration(
//            region: AWSRegionType.USEast1, credentialsProvider: credentialsProviderss)
//
//        AWSServiceManager.default().defaultServiceConfiguration = defaultServiceConfiguration
//
//
//        AWSCredentials.init(accessKey:  "AKIASN5GMO5QWDGFTHIO", secretKey: "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", sessionKey: nil, expiration: nil)
//
//        AWSTask(result: credentialsProvider1)
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: "AKIASN5GMO5QWDGFTHIO", secretKey:  "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")
        let configuration = AWSServiceConfiguration(region: AWSRegionType.USEast1, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        
        if launchOptions != nil {
            print("options")
            if (launchOptions![UIApplication.LaunchOptionsKey.location] != nil){
                //LocationService.sharedInstance.updateCustomerCoordinates()
                LocationService.sharedInstance.getLocation()
                self.locManager.stopMonitoringSignificantLocationChanges()
                
                LocationService.sharedInstance.getLocation()
                LocationService.sharedInstance.updateCoordinates()
                
            }
        }
        
        UNUserNotificationCenter.current().delegate = self
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            
            UNUserNotificationCenter.current().delegate = self
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        let tokens = Messaging.messaging().fcmToken
        print("FCM token: \(tokens ?? "")")
        let defaults = UserDefaults.standard
        defaults.set(tokens, forKey: "fcmToken")
        
        GMSServices.provideAPIKey("AIzaSyB7cJxxacb1JymyupzwHRLUGjcvetDr6Pw")
        GMSPlacesClient.provideAPIKey("AIzaSyB7cJxxacb1JymyupzwHRLUGjcvetDr6Pw")
        GoogleAnalytics.initiate()
        
        Analytics1.sendAnalytics()
        //  RunLoop.current.run(until: NSDate(timeIntervalSinceNow:1) as Date)
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
//
//         let sns1 = AWSSNS.default()
//        let request1 = AWSSNSCreatePlatformApplicationInput()
//
//        request1?.name = "coolpadiOS123"
//        request1?.platform = "APNS_SANDBOX"
//        request1?.attributes = ["key" : "AIzaSyCbV8TNgF2SDeXsE5gB6skfo01elZV8tMg"]
//
//
//         sns1.createPlatformApplication(request1!).continueWith { ( task ) -> AnyObject? in
//            print("error \(task.error), result:; \(task.result)")
//            return nil
//        }
//
        
        
//       
//        
//        
//        
//        
//        
        
//        
//        
//        let sns = AWSSNS.default()
//
//        let request = AWSSNSPublishInput()
//      //  let request = AWSSNSPlatformApplication()
//        
//        
//        request?.messageStructure = "json"
//       // request?.platformApplicationArn = SNSPlatformApplicationArn
//        
////
//      let   dict = ["default": "The default message", "APNS": "{\"aps\":{\"alert\": \"gdfhjsdghjfgjsdf\",\"sound\":\"default\",\"content_available\": \"1\",\"priority\": \"normal\",\"body\":\"great match!\",\"title\":\"Coolpad FamilyLabs\", \"badge\":\"1\",\"memberid\":\"dgfdgfdg\",\"loginendpointarn\":\"dfgdfg\",\"membername\":\"dfgdfg\",\"plateform\":\"iOS\"} }"
//        ]
//        
//    
// //var dict = ["default": "The default message"]
//        
//   //     let dict = ["default":"Naresh","GCM":"{\"notification\":{\"body\":\"great match!\",\"title\":\"Coolpad FamilyLabs\"},\"data\":{\"body\":\"great match!\",\"title\":\"Coolpad FamilyLabs\",\"msg\":\"Ashok\",\"membername\":\"testhga\",\"type\":\"awssnschat\",\"memberid\":\"102\"}}"]
//        
//        do
//        {
//        let jsonData = try JSONSerialization.data(withJSONObject: dict)
//            
//          //  request?.message = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
//         //   request?.targetArn = "arn:aws:sns:us-east-1:167315797857:endpoint/APNS/CPiOS/22ca769b-c433-3b7b-9fce-9397dd570629"
//            
//    //    request?.targetArn = "arn:aws:sns:us-west-2:167315797857:app/GCM/coolpad1234/7ab4990a-fa68-3ad3-93a3-8150e301eb91"
//            request?.message = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String?
//            
//            
//                //NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String?
//         
//            
//            //"test"
//      //  endpointArn: arn:aws:sns:us-east-1:167315797857:endpoint/APNS_SANDBOX/CpiOSTest/03838671-edc0-32f6-9835-471cc73bd38d
//        
//          // request?.
//   // request?.targetArn = "arn:aws:sns:us-east-1:167315797857:endpoint/APNS_SANDBOX/CpiOSTest/03838671-edc0-32f6-9835-471cc73bd38d"
//            
//            
//            if let end = UserDefaults.standard.string(forKey: "endpointArnForSNS")
//            {
//                request?.targetArn = end
//                
//            }
//         
//            
//       //     request?.targetArn = "arn:aws:sns:us-west-2:167315797857:endpoint/GCM/coolpad1234/4eca7a81-1608-3ad7-a90b-237bf71d71a2"
//            
//           
//
//            
//            
//            sns.publish(request!).continueWith { (task) -> AnyObject? in
//                print("error \(task.error), result:; \(task.result)")
//                return nil
//            }
//        }catch
//        {
//            
//        }
//
//

//
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
        
        
        
        if (UserDefaults.standard.value(forKey: "isFirstLaunch") as? Bool) != nil {
            
            initialViewController = storyboard.instantiateViewController(withIdentifier: "SplashscreenViewController")
            
            
            // initialViewController = storyboard.instantiateViewController(withIdentifier: "MainCallsViewController")
            //                        if UserDefaults.standard.value(forKey: "profile_details") == nil{
            //                        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
            //
            //                            if data == nilxf
            //                            {
            //
            //                            }
            //
            //                            let organizationdetails = try? JSONDecoder().decode(response.self, from: data?)
            //
            //                            if data == nil
            //                            {
            //                                   initialViewController = storyboard.instantiateViewController(withIdentifier: "SplashscreenViewController")
            //                            }else
            //                            {
            //                                let pickedOptionsValue =  UserDefaults.standard.string(forKey: "pickedOptions")
            //                                if pickedOptionsValue == "true"
            //                                {
            //                                     initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
            //                                }else
            //                                {
            //                                    initialViewController = storyboard.instantiateViewController(withIdentifier: "UserOptionViewController")
            //                                }
            //
            //
            //                            }
            
            //                        }else
            //                        {
            //                            initialViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
            //                            //  initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
            //                        }
            
        } else {
            initialViewController = storyboard.instantiateViewController(withIdentifier: "ConfigurationViewController")
            UserDefaults.standard.set(false, forKey: "isFirstLaunch")
            UserDefaults.standard.synchronize()
        }
        let navigationController = UINavigationController(rootViewController: initialViewController)
        navigationController.isNavigationBarHidden = true
        
        // or not, your choice.
        
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        
        IQKeyboardManager.shared.enable = true
        
      
    
        SVProgressHUD.setDefaultMaskType(.clear)
        
        return true
    }
    
    @objc func showSpinningWheel(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        
        
       
        if let dict = notification.userInfo as NSDictionary? {
            if let id = dict["isinChatView"] as? String{
                
                isinChatView = id
            }
        }
        
    }
    
    @objc func showDirectionView(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        
        
        
        if let dict = notification.userInfo as NSDictionary? {
            if let id = dict["isinChatView"] as? String{
                
                isinDirectionView = id
            }
        }
        
    }
    
    var batteryLevel: Int {
        return Int(round(UIDevice.current.batteryLevel * 100))
    }
    
    var batteryLevels: Float {
        return UIDevice.current.batteryLevel
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        let imageDataDict:[String: String] = ["message": userInfo["sdfhdf"] as? String ?? ""]
        
        
    //    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "messagenotification"), object: nil, userInfo: imageDataDict)
        // `default` is now a property, not a method call

        print(userInfo)
        //        var alert = NSDictionary()
        //        if #available(iOS 10.0, *) {
        //            let appDelegate = UIApplication.shared.delegate as? AppDelegate
        //            let managedContext = appDelegate?.persistentContainer.viewContext
        //            let entity: PushNotifications =  NSEntityDescription.insertNewObject(forEntityName: "PushNotifications", into: managedContext!) as! PushNotifications
        //
        //            print(userInfo)
        //            let pushData = userInfo["aps"] as? NSDictionary!
        //            let  alert = pushData??.value(forKey:"alert") as! NSDictionary
        //            print(pushData as Any)
        //
        //
        //
        //
        //
        //            entity.title = alert.value(forKey:"title") as? String
        //            entity.body = alert.value(forKey:"body") as? String
        //            entity.time = userInfo["time"] as? String
        //
        //            UIApplication.shared.applicationIconBadgeNumber = badgeCount
        //
        //            badgeCount += 1
        //
        //            do
        //            {
        //                try managedContext?.save()
        //            } catch let error as NSError
        //            {
        //                print("Could not save. \(error), \(error.userInfo)")
        //            }
        //
        //
        //
        //        }
        
        
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "nameOfNotification"), object: nil)
        
        
    }
    
    // Print full message.
    
    
    
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        
        
        print("APNs token retrieved: \(deviceToken)")
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        
        print("Device Token: \(token)")
        
        
        let tokens = Messaging.messaging().fcmToken
        print("FCM token: \(tokens ?? "")")
        let defaults = UserDefaults.standard
        defaults.set(tokens, forKey: "fcmToken")
        
        
        let sns = AWSSNS.default()
        let request = AWSSNSCreatePlatformEndpointInput()
       request?.token = token
        request?.platformApplicationArn = SNSPlatformApplicationArn
        
      //  print(request)
        
        
            sns.createPlatformEndpoint(request!).continueWith(executor: AWSExecutor.mainThread(), block: { (task: AWSTask!) -> AnyObject? in
                if task.error != nil {
                    print("Error: \(String(describing: task.error))")
                } else {
                    let createEndpointResponse = task.result! as AWSSNSCreateEndpointResponse
                    
                    if let endpointArnForSNS = createEndpointResponse.endpointArn {
                        print("endpointArn: \(endpointArnForSNS)")
                        UserDefaults.standard.set(endpointArnForSNS, forKey: "endpointArnForSNS")
                        
                        print(UserDefaults.standard.string(forKey: "endpointArnForSNS")!)
                    }
                }
                
                return nil
            })
            
       
   
//                let tokens = Messaging.messaging().fcmToken
//                print("FCM token: \(tokens ?? "")")
//                let defaults = UserDefaults.standard
//                defaults.set(tokens, forKey: "FCM token")
//
//
        //With swizzling disabled you must set the APNs token here.
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
//        
//        if isinChatView == "true"
//        {
//           deleteAllRecords()
//        }
        
       //   UserDefaults.standard.set(false, forKey: "notificationTap")
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        UserDefaults.standard.set(nil, forKey: "notificationTap")
       
        self.saveContext()
        
         deleteAllRecords()
    }
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "CloudPeppers")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    
    
    func deleteAllRecords() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "ChatData")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
}
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        
        print(notification)
        print(center)
        
        
        let userInfo = notification.request.content.attachments
        print(userInfo)
        let userInfo1 = notification.request.content.body
        print(userInfo1)
        let userInfo2 = notification.request.content.userInfo
        print(userInfo2)
        
        let  notificationId = userInfo2["notificationId"] as? String
        
        let alertdic = userInfo2["aps"] as? [AnyHashable:Any]
        
        let  memberId = alertdic?["memberid"] as? String
        let userInfo3 = notification.request.content.debugDescription
        print(userInfo3)
        let userInfo4 = notification.request.content.debugDescription
        print(userInfo4)
        
        
             let  type = userInfo2["type"] as? String
        if type == "imeiChanged"
        {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deviceChanged"), object: nil,userInfo: nil)
            
            return
        }
        
        if type == "Deactivate"
        {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deactivateAccount"), object: nil,userInfo: nil)
            
            return
            
        }
        
        
        if memberId != nil
        {
            let  notificationId = alertdic?["alert"] as? String
            let imageDataDict:[String: Any] = [
                "message":notificationId!,
                "memberid":memberId!]
            
            
            if #available(iOS 10.0, *) {
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                let managedContext = appDelegate?.persistentContainer.viewContext
                let entity: ChatData =  NSEntityDescription.insertNewObject(forEntityName: "ChatData", into: managedContext!) as! ChatData
                
                // badgeCount += 1
                // UIApplication.sharedApplication().applicationIconBadgeNumber = ++badgeCount
                //    UIApplication.shared.applicationIconBadgeNumber = badgeCount
                print( UIApplication.shared.applicationIconBadgeNumber )
                // UIApplication.shared.applicationIconBadgeNumber = 1
                let formatter = DateFormatter()
                // initially set the format based on your datepicker date / server String
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let myString = formatter.string(from: Date()) // string purpose I add here
                // convert your string to date
                let yourDate = formatter.date(from: myString)
                //then again set the date format whhich type of output you need
                formatter.dateFormat = "HH:mm"
                // again convert your date to string
                let myStringafd = formatter.string(from: yourDate!)
                
                print(myStringafd)
                
              
                
                entity.forme = "from"
                entity.memberid = memberId!
                entity.message = notificationId
                  entity.time = myStringafd
                
                
                // entity.timeStamp = String(describing: Int(NSDate().timeIntervalSince1970))
                
                
                do
                {
                    try managedContext?.save()
                    
                    
                  
                } catch let error as NSError
                {
                    print("Could not save. \(error), \(error.userInfo)")
                }
            }
            
            
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "messagenotification"), object: nil, userInfo: imageDataDict)
            
            if isinChatView == "true"
            {
                 completionHandler([.alert, .badge, .sound])
            }
            
            return
        }
        
        if userInfo1 != ""
        {
            
            
            if notificationId == nil || notificationId == ""
            {
            if #available(iOS 10.0, *) {
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                let managedContext = appDelegate?.persistentContainer.viewContext
                let entity: PushNotifications =  NSEntityDescription.insertNewObject(forEntityName: "PushNotifications", into: managedContext!) as! PushNotifications
                
                // badgeCount += 1
                // UIApplication.sharedApplication().applicationIconBadgeNumber = ++badgeCount
                //    UIApplication.shared.applicationIconBadgeNumber = badgeCount
                print( UIApplication.shared.applicationIconBadgeNumber )
                // UIApplication.shared.applicationIconBadgeNumber = 1
                print(notification)
                print(userInfo)
                
                
                entity.body = notification.request.content.body
                entity.title = notification.request.content.title
                entity.time = userInfo2["time"] as? String
                
                entity.timeStamp = String(describing: Int(NSDate().timeIntervalSince1970 * 1000))
                
                
                do
                {
                    try managedContext?.save()
                } catch let error as NSError
                {
                    print("Could not save. \(error), \(error.userInfo)")
                }
            }
                
            }else
            {
                
            }
            
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "nameOfNotification"), object: nil)
        completionHandler([.alert, .badge, .sound])
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
 
       
        
        let state = UIApplication.shared.applicationState
        if state == .background || state == .inactive {
            
              let userInfo2 = response.notification.request.content.userInfo
          
            let alertdic = userInfo2["aps"] as? [AnyHashable:Any]
            
            let  memberId = alertdic?["memberid"] as? String
            
            let userInfo1 = response.notification.request.content.body
            
         
            
            let  notificationId = userInfo2["notificationId"] as? String
            
            if userInfo1 != ""
            {
                
              
                let  type = userInfo2["type"] as? String
                
                if type == "imeiChanged"
                {
                    UserDefaults.standard.set("deviceChangedtrue", forKey: "deviceChangedtrue")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deviceChanged"), object: nil,userInfo: nil)
                    
                    return
                }
                
                if type == "Deactivate"
                {
                    UserDefaults.standard.set("deactivateAccountTrue", forKey: "deactivateAccount")
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deactivateAccount"), object: nil,userInfo: nil)
                    
                    return
                    
                }
                
                if type == "panic"
                {
                    
                    let  typeOfAlertId = userInfo2["typeOfAlertId"] as? String
                    if typeOfAlertId == "1" ||  typeOfAlertId == "2"
                    {
                        
                        let setMemberId = userInfo2["memberId"] as? String
                        
                        
                        let imageDataDict:[String: Any] = [
                            "setMemberId":setMemberId!]
                        
                        UserDefaults.standard.set("panicSOS", forKey: "panicSOS")
                        UserDefaults.standard.set(userInfo2, forKey: "panicSOSdata")
                        
                        
                        
                      if   isinDirectionView == "false"
                        {
                             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SOStap"), object: nil,userInfo: imageDataDict)
                        }

                        return
                    }
                    
                }
                
                
                if type == "geo"
                    
                {
                    
                    
                    
                    let setMemberId = userInfo2["memberId"] as? String
                
                    self.notificationTap = true
                    UserDefaults.standard.set(self.notificationTap, forKey: "notificationTap")
                  //  UserDefaults.standard.set(userInfo2["fenceId"], forKey: "notificationFenceID")
                    UserDefaults.standard.set(userInfo2["fenceId"], forKey: "notificationFenceID")
                    
                    let imageDataDict:[String: Any] = [
                        "memberid":setMemberId!]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "geo"), object: nil,userInfo: imageDataDict)
                    
                    return
                }
                
                if type == "approveOrReject" || type == "permission Updated"
                {
                    let setMemberId = userInfo2["memberId"] as? String
                    
                    
                    UserDefaults.standard.set("approve", forKey: "approve")
                    UserDefaults.standard.set(userInfo2, forKey: "approveOrReject")
                    
                    // UserDefaults.standard.set("approveOrReject", forKey: "fromapproveOrReject")
                    
                    
                    let imageDataDict:[String: Any] = [
                        "setMemberId":setMemberId!]
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setRulesNotificationTap"), object: nil,userInfo: imageDataDict)
                    
                   
                }else if notificationId != nil
                {
                    
                    UserDefaults.standard.set("ConfirmNotificationtap", forKey: "ConfirmNotificationtap")
                    
                    
                    UserDefaults.standard.set(userInfo2, forKey: "dataConfirmNotificationtap")
                    
                    let  memberName = userInfo2["userName"] as? String
                    let imageDataDict:[String: Any] = [
                        "notificationId":notificationId!,"membername":memberName!]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ConfirmNotificationtap"), object: nil,userInfo: imageDataDict)
                    
                    
                    
                }else
                {
                    let  memberId = alertdic?["memberid"] as? String
                    
                    if memberId != nil
                    {
                        
                        
                    }else
                    {
                        UserDefaults.standard.set("fromnotificationTap", forKey: "fromnotificationTap")
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Notificationtap"), object: nil,userInfo: nil)
                    }
                    
                   
                    
                    
                }
                    
                
                
                
                
                
         
                
               
            }

          
            
            
            if isinChatView == "true"
            {


   

                let userInfo2 = response.notification.request.content.userInfo
                print(userInfo2)
                
                
                let alertdic = userInfo2["aps"] as? [AnyHashable:Any]
                
                let  memberId = alertdic?["memberid"] as? String
                
                if memberId != nil
                {
                     UserDefaults.standard.set("fromChat", forKey: "fromChat")
                    
                    UserDefaults.standard.set(alertdic, forKey: "datafromNotification")
                    let logingendpointArn = alertdic?["loginendpointarn"] as? String
                    let platform =  alertdic?["platform"] as? String
                    
                    let name = alertdic?["membername"] as? String
                    
                    let  notificationId = alertdic?["alert"] as? String
                    let imageDataDict:[String: Any] = [
                        "logingendpointArn":logingendpointArn ?? "",
                        "memberid":memberId ?? "","name":name ?? "","platform":platform ?? ""]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Notification"), object: nil,userInfo: imageDataDict)
                    
                }
                
             
            }
            // background
        } else if state == .active {
               let userInfo1 = response.notification.request.content.body

               let userInfo2 = response.notification.request.content.userInfo

            let  notificationId = userInfo2["notificationId"] as? String

            if userInfo1 != ""
            {


                
                let  type = userInfo2["type"] as? String
                
                if type == "approveOrReject" || type == "permission Updated"
                {
                    let setMemberId = userInfo2["memberId"] as? String
                    
                    
                    let imageDataDict:[String: Any] = [
                        "setMemberId":setMemberId!]
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setRulesNotificationTap"), object: nil,userInfo: imageDataDict)
                    
                    return
                }
                
                if type == "imeiChanged"
                {
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deviceChanged"), object: nil,userInfo: nil)
                    
                    return
                }
                
                if type == "Deactivate"
                {
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deactivateAccount"), object: nil,userInfo: nil)
                    
                    return
                    
                }
                
              if type == "geo"
                
              {
                
                let setMemberId = userInfo2["memberId"] as? String
                self.notificationTap = true
                UserDefaults.standard.set(self.notificationTap, forKey: "notificationTap")
             //   UserDefaults.standard.set(userInfo2["fenceId"], forKey: "notificationFenceID")
             UserDefaults.standard.set(userInfo2["fenceId"], forKey: "notificationFenceID")
                
                let imageDataDict:[String: Any] = [
                    "memberid":setMemberId!]
                 NotificationCenter.default.post(name: NSNotification.Name(rawValue: "geo"), object: nil,userInfo: imageDataDict)
                
                return
                }
                
                if type == "panic"
                {
                    
                       let  typeOfAlertId = userInfo2["typeOfAlertId"] as? String
                    if typeOfAlertId == "1" ||  typeOfAlertId == "2"
                    {
                        
                        let setMemberId = userInfo2["memberId"] as? String
                        
                        
                        let imageDataDict:[String: Any] = [
                            "setMemberId":setMemberId!]
                        
                        if   isinDirectionView == "false"
                        {
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SOStap"), object: nil,userInfo: imageDataDict)
                            
                        }
                        
                       
                        return
                    }
                    
                }
                
               if notificationId != nil
               {
                
                   let  memberName = userInfo2["userName"] as? String
                let imageDataDict:[String: Any] = [
                    "notificationId":notificationId!,"membername":memberName!]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ConfirmNotificationtap"), object: nil,userInfo: imageDataDict)
                
                
                return
                }
                  let alertdic = userInfo2["aps"] as? [AnyHashable:Any]
                let  memberId = alertdic?["memberid"] as? String
                
                if memberId != nil
                {
                    
                    
                }else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Notificationtap"), object: nil,userInfo: nil)
                    
                    
                    
                    return
                }
                
                
      
            }

            if isinChatView == "true"
            {
                let userInfo2 = response.notification.request.content.userInfo
                print(userInfo2)


                   let alertdic = userInfo2["aps"] as? [AnyHashable:Any]

                 let  memberId = alertdic?["memberid"] as? String
                let logingendpointArn = alertdic?["loginendpointarn"] as? String
                if memberId != nil
                {
                    
                    let name = alertdic?["membername"] as? String
                      let platform =  alertdic?["platform"] as? String
                    let  notificationId = alertdic?["alert"] as? String
                    let imageDataDict:[String: Any] = [
                        "logingendpointArn":logingendpointArn ?? "",
                        "memberid":memberId ?? "","name":name ?? "","platform":platform ?? ""]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Notification"), object: nil,userInfo: imageDataDict)
                    
                    return
                    
                }
               
            }
//
            
            return
        }
        
       //  let userInfo = response.notification.request.content.userInfo
        let userInfo = response.notification.request.content.attachments
        print(userInfo)
        let userInfo1 = response.notification.request.content.body
        print(userInfo1)
        let userInfo2 = response.notification.request.content.userInfo
        print(userInfo2)
        
        let  notificationId = userInfo2["notificationId"] as? String
        
        let alertdic = userInfo2["aps"] as? [AnyHashable:Any]
        
        let  memberId = alertdic?["memberid"] as? String
        let userInfo3 = response.notification.request.content.debugDescription
        print(userInfo3)
        let userInfo4 = response.notification.request.content.debugDescription
        print(userInfo4)
        
        if memberId != nil
        {
            let  notificationId = alertdic?["alert"] as? String
            let imageDataDict:[String: Any] = [
                "message":notificationId!,
                "memberid":memberId!]
            
            
            if #available(iOS 10.0, *) {
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                let managedContext = appDelegate?.persistentContainer.viewContext
                let entity: ChatData =  NSEntityDescription.insertNewObject(forEntityName: "ChatData", into: managedContext!) as! ChatData
                
                // badgeCount += 1
                // UIApplication.sharedApplication().applicationIconBadgeNumber = ++badgeCount
                //    UIApplication.shared.applicationIconBadgeNumber = badgeCount
                print( UIApplication.shared.applicationIconBadgeNumber )
                // UIApplication.shared.applicationIconBadgeNumber = 1
                
                let formatter = DateFormatter()
                // initially set the format based on your datepicker date / server String
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let myString = formatter.string(from: Date()) // string purpose I add here
                // convert your string to date
                let yourDate = formatter.date(from: myString)
                //then again set the date format whhich type of output you need
                formatter.dateFormat = "HH:mm"
                // again convert your date to string
                let myStringafd = formatter.string(from: yourDate!)
                
                print(myStringafd)
                
                entity.forme = "from"
                entity.memberid = memberId!
                entity.message = notificationId
                entity.time = myStringafd
                
                
                
                
                // entity.timeStamp = String(describing: Int(NSDate().timeIntervalSince1970))
                
                
                do
                {
                    try managedContext?.save()
                    
                    
                    
                } catch let error as NSError
                {
                    print("Could not save. \(error), \(error.userInfo)")
                }
            }
            
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "messagenotification"), object: nil, userInfo: imageDataDict)
            
            return
        }
        
        
        if userInfo1 != ""
        {
            
            
            if notificationId == nil || notificationId == ""
            {
                if #available(iOS 10.0, *) {
                    let appDelegate = UIApplication.shared.delegate as? AppDelegate
                    let managedContext = appDelegate?.persistentContainer.viewContext
                    let entity: PushNotifications =  NSEntityDescription.insertNewObject(forEntityName: "PushNotifications", into: managedContext!) as! PushNotifications
                    
                    // badgeCount += 1
                    // UIApplication.sharedApplication().applicationIconBadgeNumber = ++badgeCount
                    //    UIApplication.shared.applicationIconBadgeNumber = badgeCount
                    print( UIApplication.shared.applicationIconBadgeNumber )
                    // UIApplication.shared.applicationIconBadgeNumber = 1
                   //print(notification)
                    print(userInfo)
                    
                    
                    entity.body = response.notification.request.content.body
                    entity.title = response.notification.request.content.title
                    entity.time = userInfo2["time"] as? String
                    
                    entity.timeStamp = String(describing: Int(NSDate().timeIntervalSince1970 * 1000))
                    
                    
                    do
                    {
                        try managedContext?.save()
                    } catch let error as NSError
                    {
                        print("Could not save. \(error), \(error.userInfo)")
                    }
                }
                
            }else
            {
                
            }
            
        }
        
        print("notification tapped here")
    }
    
    // Receive displayed notifications for iOS 10 devices.
    
}


extension UIApplication {
    class func topViewController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

