//
//  SearchViewController.swift
//  PullUpControllerDemo
//
//  Created by Mario on 03/11/2017.
//  Copyright © 2017 Mario. All rights reserved.
//

import UIKit
import MapKit
import Firebase


class SearchViewController: PullUpController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    enum InitialState {
        case contracted
        case expanded
    }
    
    @IBOutlet weak var noMemberLabel: UILabel!
    var initialState: InitialState = .contracted
    @IBOutlet var upArrow: UIImageView!
    @IBOutlet private weak var visualEffectView: UIVisualEffectView!
    @IBOutlet private weak var searchBoxContainerView: UIView!
    @IBOutlet private weak var searchSeparatorView: UIView! {
        didSet {
            searchSeparatorView.layer.cornerRadius = searchSeparatorView.frame.height/2
        }
    }
    // var membersGeoList:[MembersGeoList]?
    @IBOutlet private weak var firstPreviewView: UIView!
    @IBOutlet private weak var secondPreviewView: UIView!
    
    
    @IBOutlet var familycollectionView: UICollectionView!
    
    
    var initialPointOffset: CGFloat {
        switch initialState {
        case .contracted:
            return searchBoxContainerView?.frame.height ?? 0
        case .expanded:
            return pullUpControllerPreferredSize.height
        }
    }
    
    private var locations = [(title: String, location: CLLocationCoordinate2D)]()
    
    public var portraitSize: CGSize = .zero
    public var landscapeFrame: CGRect = .zero
    
    // MARK: - Lifecycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        noMemberLabel.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        searchSeparatorView.addGestureRecognizer(tap)
        
        searchSeparatorView.isUserInteractionEnabled = true
        
        familycollectionView.register(UINib(nibName: "familyMembersCell", bundle: nil),
                                      forCellWithReuseIdentifier: "familyMembersCell")
        familycollectionView.delegate = self
        familycollectionView.dataSource = self
        
        portraitSize = CGSize(width: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height),
                              height: secondPreviewView.frame.maxY)
        landscapeFrame = CGRect(x: 5, y: 50, width: 280, height: 300)
        
        //  tableView.attach(to: self)
        setupDataSource()
        
        
      //  callgetMemberApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("hdkghdkjgh adds s dssds")
        
        familycollectionView.reloadData()
        
        callgetMemberApi()
        
        
    }
    
    func callgetMemberApi()
    {
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        print(profileData)
        let addMemberUrl = API.getFromYourToYou + "?userId=\(String(describing: profileData.userId!))"
        print(addMemberUrl)
        HttpWrapper.gets(with: addMemberUrl, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            print(response)
            
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatuspermissionRequest.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    
                    
                    
                    
                    SharedData.data.getMemberDetails  = loginRespone.response?.toyouApprove
                    
                    self.familycollectionView.reloadData()
                }
                else
                    
                {
                    SharedData.data.getMemberDetails  = nil
                    self.familycollectionView.reloadData()
                    
                }
                
            }catch let jsonErr {
                
                
                SharedData.data.getMemberDetails=nil
                self.familycollectionView.reloadData()
                print("Error serializing json:", jsonErr)
                
            }
        }){ (error) in
            
            
            SharedData.data.getMemberDetails=nil
            self.familycollectionView.reloadData()
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
            print(error)
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        print("Hello World")
        if let lastStickyPoint = pullUpControllerAllStickyPoints.last {
            upArrow.image = #imageLiteral(resourceName: "DownArrow")
            pullUpControllerMoveToVisiblePoint(lastStickyPoint, animated: true, completion: nil)
            
        } else
        {
            pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[0], animated: true, completion: nil)
            
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        view.layer.cornerRadius = 12
    }
    
    override func pullUpControllerWillMove(to stickyPoint: CGFloat) {
        //        print("will move to \(stickyPoint)")
    }
    
    override func pullUpControllerDidMove(to stickyPoint: CGFloat) {
        //        print("did move to \(stickyPoint)")
        
        if stickyPoint == 101
        {
            upArrow.image = #imageLiteral(resourceName: "upArrow")
        }
    }
    
    override func pullUpControllerDidDrag(to point: CGFloat) {
        //        print("did drag to \(point)")
        upArrow.image = #imageLiteral(resourceName: "DownArrow")
    }
    
    private func setupDataSource() {
        locations.append(("Rome", CLLocationCoordinate2D(latitude: 41.9004041, longitude: 12.4432921)))
        locations.append(("Milan", CLLocationCoordinate2D(latitude: 45.4625319, longitude: 9.1574741)))
        locations.append(("Turin", CLLocationCoordinate2D(latitude: 45.0705805, longitude: 7.6593106)))
        locations.append(("London", CLLocationCoordinate2D(latitude: 51.5287718, longitude: -0.2416817)))
        locations.append(("Paris", CLLocationCoordinate2D(latitude: 48.8589507, longitude: 2.2770201)))
        locations.append(("Amsterdam", CLLocationCoordinate2D(latitude: 52.354775, longitude: 4.7585401)))
        locations.append(("Dublin", CLLocationCoordinate2D(latitude: 53.3244431, longitude: -6.3857869)))
        locations.append(("Reykjavik", CLLocationCoordinate2D(latitude: 64.1335484, longitude: -21.9224815)))
        locations.append(("London", CLLocationCoordinate2D(latitude: 51.5287718, longitude: -0.2416817)))
        locations.append(("Paris", CLLocationCoordinate2D(latitude: 48.8589507, longitude: 2.2770201)))
        locations.append(("Amsterdam", CLLocationCoordinate2D(latitude: 52.354775, longitude: 4.7585401)))
        locations.append(("Dublin", CLLocationCoordinate2D(latitude: 53.3244431, longitude: -6.3857869)))
        locations.append(("Reykjavik", CLLocationCoordinate2D(latitude: 64.1335484, longitude: -21.9224815)))
        locations.append(("London", CLLocationCoordinate2D(latitude: 51.5287718, longitude: -0.2416817)))
        locations.append(("Paris", CLLocationCoordinate2D(latitude: 48.8589507, longitude: 2.2770201)))
        locations.append(("Amsterdam", CLLocationCoordinate2D(latitude: 52.354775, longitude: 4.7585401)))
        locations.append(("Dublin", CLLocationCoordinate2D(latitude: 53.3244431, longitude: -6.3857869)))
        locations.append(("Reykjavik", CLLocationCoordinate2D(latitude: 64.1335484, longitude: -21.9224815)))
    }
    
    // MARK: - PullUpController
    
    override var pullUpControllerPreferredSize: CGSize {
        return portraitSize
    }
    
    override var pullUpControllerPreferredLandscapeFrame: CGRect {
        return landscapeFrame
    }
    
    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        switch initialState {
        case .contracted:
            // upArrow.image = #imageLiteral(resourceName: "leftarrow")
            return [firstPreviewView.frame.maxY]
        case .expanded:
            
            //  upArrow.image = #imageLiteral(resourceName: "PlusIcon")
            return [searchBoxContainerView.frame.maxY, firstPreviewView.frame.maxY]
        }
    }
    
    override var pullUpControllerBounceOffset: CGFloat {
        return 20
    }
    
    override func pullUpControllerAnimate(action: PullUpController.Action,
                                          withDuration duration: TimeInterval,
                                          animations: @escaping () -> Void,
                                          completion: ((Bool) -> Void)?) {
        switch action {
        case .move:
            
            //  upArrow.image = #imageLiteral(resourceName: "PlusIcon")
            UIView.animate(withDuration: 0.3,
                           delay: 0,
                           usingSpringWithDamping: 0.7,
                           initialSpringVelocity: 0,
                           options: .curveEaseInOut,
                           animations: animations,
                           completion: completion)
        default:
            //  upArrow.image = #imageLiteral(resourceName: "leftarrow")
            UIView.animate(withDuration: 0.3,
                           animations: animations,
                           completion: completion)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if SharedData.data.getMemberDetails?.count == 0
        {
            let noDataLabel: UILabel = UILabel(frame:CGRect(x: 0, y: 0, width: self.familycollectionView.frame.width, height: self.familycollectionView.frame.height))
            noDataLabel.backgroundColor = .white
            noDataLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            
            noDataLabel.text = "No members are available"
            noDataLabel.textAlignment = NSTextAlignment.center
            self.familycollectionView.backgroundView = noDataLabel
            return 0
        }else
        {
            self.familycollectionView.backgroundView = nil
            return SharedData.data.getMemberDetails?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "familyMembersCell",for: indexPath) as! familyMembersCell
        cell.contentView.layer.cornerRadius = 10
        cell.contentView.layer.borderWidth = 1.0
        
        cell.panicButton.addTarget(self, action: #selector(panicAction), for: .touchUpInside)
        
        cell.panicButton.tag = indexPath.row
        cell.setRuluesButton.tag = indexPath.row
        cell.resendButton.tag = indexPath.row
        
        
        cell.setRuluesButton.addTarget(self, action: #selector(setRuluesButtonAction), for: .touchUpInside)
        
         cell.resendButton.addTarget(self, action: #selector(resendButtonAction), for: .touchUpInside)
        
        if SharedData.data.getMemberDetails?[indexPath.row].relationShip != nil
        {
            
            cell.profileName.text = (SharedData.data.getMemberDetails?[indexPath.row].firstName)! + "(\(String(describing: (SharedData.data.getMemberDetails?[indexPath.row].relationShip!)!)))"
            
        }else
        {
            cell.profileName.text = (SharedData.data.getMemberDetails?[indexPath.row].firstName)!
        }
        
        if SharedData.data.getMemberDetails?[indexPath.row].profileImage != nil &&  SharedData.data.getMemberDetails?[indexPath.row].profileImage != ""
        {
            
            if SharedData.data.getMemberDetails?[indexPath.row].profileImage != nil && SharedData.data.getMemberDetails?[indexPath.row].profileImage != ""
            {
                cell.profilePick.sd_setImage(with: URL(string: (SharedData.data.getMemberDetails?[indexPath.row].profileImage)!), placeholderImage: UIImage(named: "placeHolderProfile"))
            }
            
            
            
            // print(SharedData.data.getMemberDetails?[indexPath.row].profileImage)
            
            
            
        }else
        {
            cell.profilePick.sd_setImage(with: URL(string: "placeHolderProfile"), placeholderImage: UIImage(named: "placeHolderProfile"))
        }
        
        if SharedData.data.getMemberDetails?[indexPath.row].toYou_Approved == false
        {
            
            
            print(SharedData.data.getMemberDetails?[indexPath.row].toYou_Approved)
            // cell.backgroundColor = UIColor.black
            cell.upperImage.image = #imageLiteral(resourceName: "gradeout")
          //  cell.isUserInteractionEnabled = false
//            cell.panicButton.backgroundColor = UIColor.gray
//            cell.setRuluesButton.backgroundColor = UIColor.gray
            cell.panicButton.isHidden = true
            cell.setRuluesButton.isHidden = true
            cell.resendButton.isHidden = false
            cell.resendView.isHidden = false
            cell.resendButton.layer.cornerRadius = 10
            cell.resendButton.clipsToBounds = true
           // familycollectionView.reloadData()
            
            
            
        }else
        {
            cell.panicButton.isHidden = false
            cell.setRuluesButton.isHidden = false
            cell.resendButton.isHidden = true
            cell.resendView.isHidden = true
            cell.upperImage.image = #imageLiteral(resourceName: "oliverBackGroundImage")
           // cell.isUserInteractionEnabled = true
            cell.panicButton.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.3333333333, blue: 0.3254901961, alpha: 1)
            cell.setRuluesButton.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.3333333333, blue: 0.3254901961, alpha: 1)
            
           // familycollectionView.reloadData()

        }
        
        
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true
        
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        
        
        return cell
    }
    @objc func panicAction(sender: UIButton!) {
        
        
        Analytics.logEvent("Home_SOS_Clicked", parameters:nil)
        (parent as? HomeViewController)?.showAlert(sender:sender.tag)
        
        
        //        let alert = Bundle.main.loadNibNamed("PanicView", owner: nil, options: nil) as? PanicView
        
        //let viewFromNib: PanicView? = Bundle.main.loadNibNamed("PanicView",
        //                                                            owner: nil,
        //                                                            options: nil)?.first as! PanicView
        //   panicView = Bundle.main.loadNibNamed("PanicView", owner: nil, options: nil)![0] as! PanicView
        // myView.
        
        // self.view.addSubview(PanicView!)
        print(sender.tag)
        //        print(SharedData.data.getMemberDetails?[sender.tag].memberId)
        //
        //        let parameter:NSMutableDictionary = NSMutableDictionary()
        //
        //        parameter.setValue(SharedData.data.getMemberDetails?[sender.tag].memberId, forKey: "memberId")
        //
        //
        //        APIServices.postDataToServer(url: API.panic, parameters: parameter, controller : self,completionHandler: { (status,response) -> Void in
        //            if status == true{
        //
        //                var derivedData = response as! [String : Any]
        //
        //                let message = derivedData["message"] as? String
        //
        //                print(derivedData)
        //
        //                self.showAlert(title: "Cloudpeppers", msg:message!)
        //
        //            }
        //        })
        //
        // self.showAlert(title: "Panic", msg: "messagae sent")
    }
    
    @objc func setRuluesButtonAction(sender: UIButton!) {
        
        print(SharedData.data.getMemberDetails?[sender.tag].userId)
        Analytics.logEvent("Home_Set_Rules_Clicked", parameters:nil)
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SetRulesViewController") as! SetRulesViewController
        vc.memberId = SharedData.data.getMemberDetails?[sender.tag].userId
        if SharedData.data.membersGeoList?.count ?? 0 > 0
        {
            for each in SharedData.data.membersGeoList!
            {
                
                if each.userId == SharedData.data.getMemberDetails?[sender.tag].userId
                {
                    vc.membersGeoList = each
                }
            }
        }
        
        
        vc.userId = SharedData.data.getMemberDetails?[sender.tag].userId
        vc.getMemberDetails = SharedData.data.getMemberDetails?[sender.tag]
        vc.imageString = SharedData.data.getMemberDetails?[sender.tag].profileImage
        
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // resend button action
    @objc func resendButtonAction(sender: UIButton!) {
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        print(profileData.userId)
        print(SharedData.data.getMemberDetails?[sender.tag].userId)
        
        var userProfileID = SharedData.data.getMemberDetails?[sender.tag].userId
        
        let resendPassword = API.resendPassword + "?userId=\(String(describing: profileData.userId!))" + "&memberId=\(userProfileID!)"
        
        print(resendPassword)
        
        HttpWrapper.post(with: resendPassword, parameters: nil, headers: nil, completionHandler: { (response) in
            self.showAlert(title: "", msg: "Password sent successfully")
            

                
            })
    { (error) in
            
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
       
        
        
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        Analytics.logEvent("Home_Member_Clicked", parameters:nil)

        view.endEditing(true)
        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[0], animated: true, completion: nil)
        
        (parent as? HomeViewController)?.showinMapSelectedUser(userId:(SharedData.data.getMemberDetails?[indexPath.row].userId)!)
    }
    
}

extension SearchViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let lastStickyPoint = pullUpControllerAllStickyPoints.last {
            pullUpControllerMoveToVisiblePoint(lastStickyPoint, animated: true, completion: nil)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}

//extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
//
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
//        view.endEditing(true)
//        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[0], animated: true, completion: nil)
//
//        //(parent as? MapViewController)?.zoom(to: locations[indexPath.row].location)
//    }
//}
