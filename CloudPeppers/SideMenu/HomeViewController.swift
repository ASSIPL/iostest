//
//  FamilyLabsViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 14/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import GoogleMaps
import EventKit
import SVProgressHUD
import Firebase
import AWSSNS
import AWSCognito
import IQKeyboardManagerSwift
struct FMData {
    
    var name : String
    var relation :String
    var image : UIImage
    var lat : CLLocationDegrees
    var long : CLLocationDegrees
    var icon : UIImage
}

class HomeViewController: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate {
    
    //UICollectionViewDelegate,UICollectionViewDataSource
    
    
    
    var tappedMarker : GMSMarker?
    
    @IBOutlet weak var messageViewBackground: UIView!
    @IBOutlet var messageTextField: UITextView!
    @IBOutlet var messageView: UIView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet var mapView: GMSMapView!
    var currentLocation: CLLocationCoordinate2D?
    var marker = GMSMarker()
    var geoCoder = GMSGeocoder()
    var locationManager = CLLocationManager()
    var intergerValue:Int?
    @IBOutlet var notificationRightBarButton: UIBarButtonItem!
    
     var featureList:UserPlans?
    @IBOutlet weak var messageCancelButton: UIButton!
    @IBOutlet weak var messageSendButton: UIButton!
    
    @IBOutlet var sendMessageButton: UIButton!
    
    @IBOutlet var addMemberButton: UIButton!
    
    typealias CompletionHandler = (_ address:String) -> Void
    var customInfoWindow = CustomInfoWindow()
    var membersGeoList:[MembersGeoList]?
    var selectedCellIdexpath : IndexPath? = nil
    var panicView: PanicView?
    var containerView : UIView?
    @IBOutlet weak var btnMenuButton: UIBarButtonItem!
    var fromsidemeue:String?
    
    
    var eventStore: EKEventStore!
    var reminders: [EKReminder]!
    
    
    
    
    var bottomArray:Array = [UIImage]()
    var namesArray:Array = [String]()
    var relationArray:Array = [String]()
    var familyArray:[FMData]?
    private var originalPullUpControllerViewSize: CGSize = .zero
    private func makeSearchViewControllerIfNeeded() -> SearchViewController {
        let currentPullUpController = children
            .filter({ $0 is SearchViewController })
            .first as? SearchViewController
        let pullUpController: SearchViewController = currentPullUpController ?? UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        
        pullUpController.initialState = .contracted
        
        if originalPullUpControllerViewSize == .zero {
            originalPullUpControllerViewSize = pullUpController.view.bounds.size
        }
        return pullUpController
    }
    let appleEventStore = EKEventStore()
    var calendars: [EKCalendar]?
    
    var locationtimer = Timer()
    var locationtimer1 = Timer()
    
    
    var notifiationResponse:NotifiationResponse?
    
    //  let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.tappedMarker = GMSMarker()
      //  messageView.dropShadow()
        messageView.layer.cornerRadius = 20
        messageView.clipsToBounds = true
        
        panicView?.emergencyButton.isSelected = true
        addBadge(itemvalue: appDelegate.notificationBargeCount ?? "")
        self.messageView.center.x = self.view.center.x
        self.messageView.center.y = self.view.center.y
        
        messageView.frame = self.view.frame
        messageView.isHidden = true
        
        
        //        notificationRightBarButton.addBadge(number: 177676)
        //        btnMenuButton.addBadge(number: 177676)
        
        //  notificationRightBarButton.badge.zPosition = 1000
        
      
        
        if fromsidemeue == "true"
        {
            callgetMemberApi()
        }else
        {
            
           // callOrganizeationApi()
        }
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.callgetMemberApi), name: NSNotification.Name(rawValue: "nameOfNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.deviceChanged), name: NSNotification.Name(rawValue: "deviceChanged"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.deactivateAccount), name: NSNotification.Name(rawValue: "deactivateAccount"), object: nil)
        
    //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deviceChanged"), object: nil,userInfo: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.Geo), name: NSNotification.Name(rawValue: "geo"), object: nil)
        
       //  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "geo"), object: nil,userInfo: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.Notification), name: NSNotification.Name(rawValue: "Notification"), object: nil)
        
        
            // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Notificationtap"), object: nil,userInfo: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.Notificationtap), name: NSNotification.Name(rawValue: "Notificationtap"), object: nil)
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.ConfirmNotificationtap), name: NSNotification.Name(rawValue: "ConfirmNotificationtap"), object: nil)
        
        
        
           NotificationCenter.default.addObserver(self, selector: #selector(self.setRulesNotificationTap), name: NSNotification.Name(rawValue: "setRulesNotificationTap"), object: nil)
        
        
        
        
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.sosNotificationTap), name: NSNotification.Name(rawValue: "SOStap"), object: nil)
        
         //    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setRulesNotificationTap"), object: nil,userInfo: imageDataDict)
        
    //    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ConfirmNotificationtap"), object: nil,userInfo: nil)
        
        
        
        if  UserDefaults.standard.string(forKey: "fromChat") == "fromChat"
        {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            vc.fromNotification = true
            
            
            
            
        let userInfo =  UserDefaults.standard.dictionary(forKey: "datafromNotification")
            print(userInfo)
            
            if let dict = userInfo as NSDictionary? {
                if let id = userInfo?["memberid"] as? String{
                    
                    vc.memberIds = id
                    
                }
                
                if let logingendpointArn = userInfo?["loginendpointarn"] as? String{
                    
                    vc.endpointArnfrom = logingendpointArn
                    
                }
                
                
                
                if let name = userInfo?["membername"] as? String{
                    
                    vc.name = name
                    
                }
                
                if let platform = userInfo?["platform"] as? String
                
                {
                    vc.plateform = platform
                }
                
                
                
                
                
                
            }
            
            //   let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
        
            if  UserDefaults.standard.string(forKey: "deviceChangedtrue") == "deviceChangedtrue"
        
            {
                
                
               logoutMethod()
                return
        }
        
        if  UserDefaults.standard.string(forKey: "deactivateAccount") == "deactivateAccountTrue"
            
        {
            
            
            logoutMethod()
            return
        }
        
        
        
        
        if  UserDefaults.standard.string(forKey: "fromnotificationTap") == "fromnotificationTap"
        {
            
            
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
            
            //   let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
            
            
        }
        
        
        if  UserDefaults.standard.string(forKey:"approve") == "approve"
        {
            
            
            
        let userInfo =  UserDefaults.standard.dictionary(forKey: "approveOrReject")
            
            
            
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "SetRulesViewController") as! SetRulesViewController
            vc.fromNotificationTap = "true"
            
            if let dict = userInfo as NSDictionary? {
                if let id = dict["memberId"] as? String{
                    
                    vc.memberId = Int(id)
//                    print(id)
//
//                    for each in SharedData.data.getMemberDetails!
//                    {
//                        if each.userId == Int(id)
//                        {
//                            vc.getMemberDetails = each
//                        }
//                    }
                    
                    
                    
                    
                    
                    
                }
                
            }
            
            
            
            self.navigationController?.pushViewController(vc, animated: true)
           
            
        }
        
        
        if  UserDefaults.standard.string(forKey:"panicSOS") == "panicSOS"
        {
            
            
          

            
            
            
            let userInfo =  UserDefaults.standard.dictionary(forKey: "panicSOSdata")
            
            
            
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "DirectionsViewController") as! DirectionsViewController
        
            if let dict = userInfo as NSDictionary? {
                if let id = dict["memberId"] as? String{
                    
                    vc.memberId = Int(id)

                }
                
            }
            
            
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
        
        
     
     
        
        
        
        if  UserDefaults.standard.string(forKey: "ConfirmNotificationtap") == "ConfirmNotificationtap"
        {
            let userInfo =  UserDefaults.standard.dictionary(forKey: "dataConfirmNotificationtap")
          
            
            
            
            
            
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmPermissionsVC") as! ConfirmPermissionsVC
            if let dict = userInfo as NSDictionary? {
                if let id = dict["notificationId"] as? String{
                    
                    
                    print(id)
                    
                    vc.notificationId = Int(id)
                    
                }
                
                if let name = dict["userName"] as? String{
                    
                    
                    
                    vc.membername = name
                    
                    
                }
                
            }
            
            vc.fromNotificationtap = "true"
            
            
            
            
            vc.fromNotification = "true"
            
            
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
            
        }
        
        
        
        
            
            
        
        generateEvent()
        callLoginapi()
        
        // LocationService.sharedInstance.updateCustomerCoordinates()
        getLocation()
        let checkfamilymember = UserDefaults.standard.string(forKey: "checkfamilymember")
      //  addMemberButton.isHidden = checkfamilymember == "0" ? true : false
        //    UserDefaults.standard.set(organizationdetails?.roles?[indexPath.row].fullAccess, forKey: "checkfamilymember")
        addPullUpController()
        updateCoordinates()
        self.title = "Home"
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        
        if let mylocation = mapView.myLocation {
            print("User's location: \(mylocation)")
        } else {
            print("User's location is unknown")
        }
        //  mapView.settings.compassButton = true
        mapView.delegate = self
        
        navigationController?.navigationBar.barTintColor = Constants.colour
        
        btnMenuButton.target = revealViewController()
        btnMenuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        self.changeFontForViewController()
        addPullUpController()
        
        
        
    }
    func  callOrganizeationApi()
    {
        SVProgressHUD.show()
        
        
        HttpWrapper.gets(with: API.getOrganizationInfo, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            print(response)
            
            SVProgressHUD.dismiss()
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatus.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    
                    UserDefaults.standard.removeObject(forKey: "organizationdetails")
                    
                    if let data = try? JSONEncoder().encode(loginRespone.response) {
                        UserDefaults.standard.set(data, forKey: "organizationdetails")
                    }
                    
                    
                    SharedData.data.organizationdetails  = loginRespone.response
                    
                    
                    
                }
                
                
                
                
            } catch let jsonErr {
                
                SVProgressHUD.dismiss()
                
                print("Error serializing json:", jsonErr)
                
            }
            
            
        }){ (error) in
            
            SVProgressHUD.dismiss()
            
            print(error)
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
        
        
    }
    
    func callLoginapi()
    {
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        
        
        
      
        
        
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
      
        
        parameter.setValue(profileData.mobileNo, forKey: "mobileNo")
        parameter.setValue(profileData.password, forKey: "password")
        parameter.setValue(UserDefaults.standard.string(forKey: "fcmToken"), forKey: "fcmId")
        parameter.setValue(UserDefaults.standard.string(forKey: "endpointArnForSNS"), forKey: "snsARN")
        
           parameter.setValue(UIHelper.getUUID(), forKey: "imei")
        
        parameter.setValue(UserDefaults.standard.string(forKey: "roleKey"), forKey: "login_type")
        
        
        
        
        parameter.setValue(profileData.countryCode, forKey: "countryCode")
        parameter.setValue("iOS", forKey: "platform")
        
        print(parameter)
        
        
        APIServices.postDataToServer(url: API.login_URL, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                
                
                
                
                
                
//                if self.checkBoxBtnTap.isSelected == true{
//                    Analytics.logEvent("Login_Remember_Me_Checked", parameters:nil)
//                    
//                    UserDefaults.standard.set(self.emaidAddTF.text, forKey: "rememberme_id")
//                    UserDefaults.standard.set(self.passwordTF.text, forKey: "rememberme_password")
//                    UserDefaults.standard.set(self.mobileExtension.text, forKey: "rememberme_mobie")
//                    
//                }
//                else{
//                    UserDefaults.standard.set("", forKey: "rememberme_id")
//                    UserDefaults.standard.set("", forKey: "rememberme_password")
//                    UserDefaults.standard.set("", forKey: "rememberme_mobie")
//                }
//                
                print(response)
                
                var derivedData = response as! [String : Any]
                
                print(derivedData)
                if let theJSONData = try? JSONSerialization.data(withJSONObject: derivedData["response"] as Any , options:[])
                {
                    do{
                        
                        let loginRespone = try JSONDecoder().decode(profile.self, from: theJSONData)
                        
                        
                        
                        UserDefaults.standard.removeObject(forKey: "profile_details")
                        UserDefaults.standard.removeObject(forKey: "Paymentdata")
                        
                        UserDefaults.standard.removeObject(forKey: "jwtToken")
                        
                        
                        
                        UserDefaults.standard.set(loginRespone.jwtToken, forKey: "jwtToken")
                        
                        if let data = try? JSONEncoder().encode(loginRespone) {
                            UserDefaults.standard.set(data, forKey: "profile_details")
                            
                            
                            
                            
                        }
                        if let  paymentData = try? JSONEncoder().encode(loginRespone.userPlans) {
                            UserDefaults.standard.set(paymentData, forKey: "Paymentdata")
                        }
                        
                        //                        if let profileInfo = try? JSONDecoder().decode(profile.self, from: theJSONData) {
                        //                            print(profileInfo)
                        //
                        //                            if profileInfo.profileImage != nil &&  profileInfo.profileImage?.count != 0
                        //                            {
                        //                                let url = URL(string: profileInfo.profileImage!)
                        //                                //   profileInfo.profilePic = try? Data(contentsOf: url!)
                        //
                        //
                        //                            }
                        //
                        //                            if let data = try? JSONEncoder().encode(profileInfo) {
                        //                                UserDefaults.standard.set(data, forKey: "profile_details")
                        //                            }
                        //
                        //                        }
                    }catch let jsonErr {
                        
                        
                        print("Error serializing json:", jsonErr)
                        
                    }
                    
                }
                
                
    }
            
        })
        
    }
    
    
    
    
    @objc func sosNotificationTap(_ notification: NSNotification)
        
    {
        
        
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "DirectionsViewController") as! DirectionsViewController
        
        
        if let dict = notification.userInfo as NSDictionary? {
            if let id = dict["setMemberId"] as? String{

                vc.memberId = Int(id)
                print(id)

            




            }

        }

        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    @objc func setRulesNotificationTap(_ notification: NSNotification)
        
    {
        
        
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SetRulesViewController") as! SetRulesViewController

        
        if let dict = notification.userInfo as NSDictionary? {
            if let id = dict["setMemberId"] as? String{
                
                  vc.memberId = Int(id)
                print(id)
                
                for each in SharedData.data.getMemberDetails!
                {
                    if each.userId == Int(id)
                    {
                         vc.getMemberDetails = each
                    }
                }
                
                
                
                
             
                
            }
        
    }
        
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func ConfirmNotificationtap(_ notification: NSNotification)
        
    {
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmPermissionsVC") as! ConfirmPermissionsVC
        if let dict = notification.userInfo as NSDictionary? {
            if let id = dict["notificationId"] as? String{
                
                
                print(id)
                
                vc.notificationId = Int(id)
                
            }
            
            if let name = dict["membername"] as? String{
                
                
              
                  vc.membername = name
              
                
            }
            
        }
            
        vc.fromNotificationtap = "true"
      
        
   
        
        vc.fromNotification = "true"
     
        
            
         self.navigationController?.pushViewController(vc, animated: true)
       
        
    }
    
    
    @objc func Notificationtap(_ notification: NSNotification)
        
    {
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController

        //   let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController

        self.navigationController?.pushViewController(vc, animated: true)


       
        
        
    }
    
    
    @objc func Geo(_ notification: NSNotification)
        
    {
//        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
//        
//
        
        
        
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SetGeoFenceViewController") as! SetGeoFenceViewController
        
        if let dict = notification.userInfo as NSDictionary? {
            if let id = dict["memberid"] as? String{
                
               vc.memberId = Int(id)
                
                  print(Int(id))
                 print(Int(id))
            }
            
            
        }
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        vc.userId =  profileData.userId
          vc.titleName = "Set Geofence"
        
        
        print(profileData.userId)
        
        
        
//        vc.memberId = memberId
//        vc.userId = userId
//        vc.membersGeoList = membersGeoList
//
//        for each in   (organizationdetails?.rulez)!
//        {
//            if each.rulesKey == "key.geo"
//            {
//                vc.titleName   = each.name
//
//
//
//            }
//        }
        //    vc.titleName =
        
        self.navigationController?.pushViewController(vc, animated: true)
        return
       
        
    }
    
     @objc func Notification(_ notification: NSNotification)
   
    {
        
        print(notification.userInfo)
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        
           vc.fromNotification = true
        
        if let dict = notification.userInfo as NSDictionary? {
            if let id = dict["memberid"] as? String{
                
                 vc.memberIds = id
                
            }
            
            if let logingendpointArn = dict["logingendpointArn"] as? String{
                
                vc.endpointArnfrom = logingendpointArn
                
            }
            
            
            
            if let name = dict["name"] as? String{
                
                vc.name = name
                
            }
            
            if let platform = dict["platform"] as? String{
                
                vc.plateform = platform
                
            }
            
            
            
            
            
            
            
            
        }
       
    
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
    }
    
    @objc func rightButtonAction(sender: UIBarButtonItem)
    {
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let vc = mainStoryboard.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
        
      //   let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
        
        
    }
    
    
    func addBadge(itemvalue: String) {
        
        let bagButton = BadgeButton()
        
        bagButton.tintColor = UIColor.white
        bagButton.setImage(UIImage(named: "Bell")?.withRenderingMode(.alwaysTemplate), for: .normal)
        
        if itemvalue == "" || itemvalue == "0"
        {
            bagButton.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            bagButton.badgeEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            bagButton.badge = itemvalue
            bagButton.badgeLabel.isHidden = true
        }else
        {
            bagButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
            bagButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
            bagButton.badge = itemvalue
            
        }
        
        
        bagButton.addTarget(self, action: #selector(rightButtonAction(sender:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: bagButton)
        
    }
    
    
    func generateEvent() {
        let status = EKEventStore.authorizationStatus(for: EKEntityType.event)
        
        switch (status)
        {
        case EKAuthorizationStatus.notDetermined:
            // This happens on first-run
            requestAccessToCalendar()
        case EKAuthorizationStatus.authorized:
            // User has access
            print("User has access to calendar")
            //  self.addAppleEvents()
            
            
            
            
            
            
            
            
        case EKAuthorizationStatus.restricted, EKAuthorizationStatus.denied:
            // We need to help them give us permission
            noPermission()
        }
    }
    func noPermission()
    {
        print("User has to change settings...goto settings to view access")
    }
    func requestAccessToCalendar() {
        appleEventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                DispatchQueue.main.async {
                    print("User has access to calendar")
                    
                    // self.addAppleEvents()
                }
            } else {
                DispatchQueue.main.async{
                    self.noPermission()
                }
            }
        })
    }
    //    func addAppleEvents()
    //    {
    //        let event:EKEvent = EKEvent(eventStore: appleEventStore)
    //        event.title = "Test Event"
    //
    //         print(NSDate() as Date)
    //        event.startDate = NSDate() as Date
    //        event.endDate = date
    //        event.notes = "This is a note"
    //        event.calendar = appleEventStore.defaultCalendarForNewEvents
    //
    //        do {
    //            try appleEventStore.save(event, span: .thisEvent)
    //            print("events added with dates:")
    //        } catch let e as NSError {
    //            print(e.description)
    //            return
    //        }
    //        print("Saved Event")
    //    }
    //
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        if (UserDefaults.standard.value(forKey: "isFirstLaunchpass") as? Bool) != nil {
            
            
//            UserDefaults.standard.set(false, forKey: "isFirstLaunchpass")
//            UserDefaults.standard.synchronize()
//            
//            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//            let vc : HomeHintViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeHintViewController") as!  HomeHintViewController
//            // MyCardsHint.frame = CGRect.init(x: 0, y: 0, width: 100, height: 200)
//            //var vc = MyCardsHint(coder: CGRect(x: 0, y: 20, width: self.view.frame.width, height: self.view.frame.height))
//            vc.modalPresentationStyle = .overFullScreen
//            
//               self.present(vc, animated:true, completion:nil)
//            vc.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
//            self.view .addSubview(vc.view)
//            self.addChild(vc)
            // print("this is not the first launch")
        } else {
            //  print("this is the first launch")
            UserDefaults.standard.set(false, forKey: "isFirstLaunchpass")
            UserDefaults.standard.synchronize()
            
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : HomeHintViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeHintViewController") as!  HomeHintViewController
            // MyCardsHint.frame = CGRect.init(x: 0, y: 0, width: 100, height: 200)
            //var vc = MyCardsHint(coder: CGRect(x: 0, y: 20, width: self.view.frame.width, height: self.view.frame.height))
            self.present(vc, animated:true, completion:nil)
            // self.navigationController?.pushViewController(vc, animated: true)
            //self.present(vc, animated: true, completion: nil)
        }
        
        
        
        
        
        //  LocationService.sharedInstance.locationUpdate()
        self.containerView?.removeFromSuperview()
        self.panicView?.removeFromSuperview()
        
        if let data = UserDefaults.standard.value(forKey: "Paymentdata") as? Data,
            let profileData = try? JSONDecoder().decode(UserPlans.self, from: data)
        {
            
            featureList = profileData
            
            //            if UserPlans?.remainDays ?? 0 <= 0
            //            {
            //
            //            }
            
            
            
            
            //  return
            
        }else
        {
            
        }
        
        UserDefaults.standard.set(nil, forKey: "approveOrReject")
        UserDefaults.standard.set(nil, forKey: "approveOrReject")
        
        
           IQKeyboardManager.shared.enable = true
        GoogleAnalytics.screen(screen: AnalyticsEventCategory.HomeScreen, event: .HomeScreenEntered)
         Analytics.logEvent("HomeScreenEntered", parameters:nil)
        addBadge(itemvalue: appDelegate.notificationBargeCount ?? "")
        
        LocationService.sharedInstance.StopLocationtimer()
        locationtimer = Timer.scheduledTimer(timeInterval: 40.0, target: self, selector: #selector(locationUpdate), userInfo: nil, repeats: true)
        
         LocationService.sharedInstance.StopLocationtimer1()
        locationtimer1 = Timer.scheduledTimer(timeInterval: 1800.0, target: self, selector: #selector(locationUpdateEveryThirtyMinutes), userInfo: nil, repeats: true)
        
        // addPullUpController()
        //   updateCoordinates()
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined,.restricted,.denied:
                print("notDetermined")
                
                AlertFun.openSettingApp(title: "", message: "Please enable location services", in: self)
                //ShowAlert(title: "Chomp", message: "Please select not more than 3", in: self)
                return
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                
            }
        } else {
            AlertFun.openSettingApp(title: "", message: "Please enable location services", in: self)
            
            return
            
        }
        
    }
    
    
    @IBAction func addMemeber(_ sender: Any) {
        
        Analytics.logEvent("Home_Add_Mem_Icon_Clicked", parameters:nil)
        
        
        if let data = UserDefaults.standard.value(forKey: "Paymentdata") as? Data,
            let profileData = try? JSONDecoder().decode(UserPlans.self, from: data)
        {
            
         
            
            if profileData.remainDays ?? 0 <= 0
            {
                self.showCustomAlertWith(message: "", descMsg:"Please select a payment plan to access this feature.", itemimage: #imageLiteral(resourceName: "AboutUs"), actions: nil)
            }else
            {
                if   (featureList?.featureList?.contains(where: { name in name.featureKey == "key.addmember"}) ?? false)
                    
                {
                    
                    
                    
                    print("match")
                }else
                {
                    print("not match")
                    
                    self.showCustomAlertWith(message: "", descMsg:"your not buy this feature", itemimage: #imageLiteral(resourceName: "AboutUs"), actions: nil)
                    
                    
                    return
                }
            }
            
            
      
            //  return
            
        }else
        {
            
//            let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
//
//            let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
//            let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
//
//            let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
//            let msgAttrString = NSMutableAttributedString(string: "Are you sure you want to continue payment?", attributes: msgFont)
//
//            alert.setValue(titAttrString, forKey: "attributedTitle")
//            alert.setValue(msgAttrString, forKey: "attributedMessage")
//
//
//            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
//            let settingsAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
//
//                self.paymentAdd()
//
//            }
//
//            alert.addAction(cancelAction)
//            alert.addAction(settingsAction)
//            self.present(alert, animated: true, completion: nil)
            
          //    self.showCustomAlertWith(message: "", descMsg:"Please add payment ", itemimage: #imageLiteral(resourceName: "AboutUs"), actions: nil)
            
            let actionDic : [String: () -> Void] = [ "YES" : { (
                
                Analytics.logEvent("Logout_Yesbutton_clicked", parameters:nil),
                
                _ = self.paymentAdd()
                
                ) }, "NO" : { (
                    Analytics.logEvent("Logout_Nobutton_clicked", parameters:nil)
                    
                    
                    ) }]
            
            self.showCustomAlertWith(
                message: "",
                descMsg: "Please select a payment plan to access this feature",
                itemimage: #imageLiteral(resourceName: "LogOut"),
                actions: actionDic)
            
            return
        }
        
        
    
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddFamilyMemberVC") as! AddFamilyMemberVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    func paymentAdd() {
        let revealViewController:SWRevealViewController = self.revealViewController()
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desController = mainStoryboard.instantiateViewController(withIdentifier: "PaymentsViewController") as! PaymentsViewController
        let newFrontViewController = UINavigationController.init(rootViewController:desController)
        revealViewController.pushFrontViewController(newFrontViewController, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        locationtimer.invalidate()
        locationtimer1.invalidate()
        
         Analytics.logEvent("HomeScreenExited", parameters:nil)
        GoogleAnalytics.screen(screen: AnalyticsEventCategory.HomeScreen, event: .HomeScreenExited)
        LocationService.sharedInstance.locationUpdate()
        LocationService.sharedInstance.updateCoordinatesEveryThirtyMinutes()
        
    }
    
    @objc func locationUpdate(){
        updateCoordinates()
        
        // LocationService.sharedInstance.updateCoordinates()
        
    }
    @objc func locationUpdateEveryThirtyMinutes(){
        updateCoordinatesEveryThirtyMinutes()
        
        // LocationService.sharedInstance.updateCoordinates()
        
    }
    
    
    @objc func getLocation()
    {
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.distanceFilter = 10
        // self.locationManager.desiredAccuracy = 15
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.pausesLocationUpdatesAutomatically = false
        self.locationManager.startUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
        self.locationManager.delegate = self
        //  self.locationManager.allowDeferredLocationUpdates(untilTraveled: 10, timeout: 5)
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        if currentLocation == nil
        {
            currentLocation = manager.location!.coordinate
            setupMarker()
            
        }
        
        currentLocation = manager.location!.coordinate
        let location = locationManager.location?.coordinate
        appDelegate.latitude = (location?.latitude)!
        appDelegate.longtitude = (location?.longitude)!
        
        UserDefaults.standard.set(appDelegate.latitude, forKey: "latitude")
        UserDefaults.standard.set(appDelegate.longtitude, forKey: "longtitude")
        
        
        //  setupMarker()
        //. updateCoordinates()
    }
    func updateCoordinates()
    {
        var params: [String:Any] = [:]
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        params["userId"] =  profileData.userId
        params["latitude"]  =  UserDefaults.standard.string(forKey: "latitude")
        params["longitude"] =  UserDefaults.standard.string(forKey: "longtitude")
        params["isRequireMembersLocation"]  = "yes"
        params["batteryStatus"] =    batteryLevel
        params["geoHistoryFlag"] = false
        
        print(params)
        print(API.saveGeocoordinate)
        
        
        Analytics.logEvent("saveGeocoordinateapihit", parameters:nil)
        HttpWrapper.posts(with: API.saveGeocoordinate, parameters: params, headers: nil, completionHandler: { (response, error)  in
            // print(response)
            guard let data = response else { return }
            do {
                let loginRespone = try JSONDecoder().decode(ApiStatusmembersGeoList.self, from: data)
                let loginStatus = loginRespone.status
                
                if loginStatus!{
                    
                    print(loginRespone.response)
                    
                    self.membersGeoList = loginRespone.response?.membersGeoList
                    SharedData.data.membersGeoList = loginRespone.response?.membersGeoList
                    
                    
                    
                    self.addMembersinmap(data:self.membersGeoList!)
                    
                }
            }
            catch let jsonErr {
                
                
                print(jsonErr)
                
            }
            
        }){ (error) in
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
        
        // print(params)
        
    }
    

    func updateCoordinatesEveryThirtyMinutes()
    {
        var params: [String:Any] = [:]
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        params["userId"] =  profileData.userId
        params["latitude"]  =  UserDefaults.standard.string(forKey: "latitude")
        params["longitude"] =  UserDefaults.standard.string(forKey: "longtitude")
        params["isRequireMembersLocation"]  = "yes"
        params["batteryStatus"] =    batteryLevel
        params["geoHistoryFlag"] = true
        
        print(params)
        print(API.saveGeocoordinate)
        
        
        Analytics.logEvent("saveGeocoordinateapihit", parameters:nil)
        HttpWrapper.posts(with: API.saveGeocoordinate, parameters: params, headers: nil, completionHandler: { (response, error)  in
            // print(response)
            guard let data = response else { return }
            do {
                let loginRespone = try JSONDecoder().decode(ApiStatusmembersGeoList.self, from: data)
                let loginStatus = loginRespone.status
                
                if loginStatus!{
                    
                    print(loginRespone.response)
                    
//                    self.membersGeoList = loginRespone.response?.membersGeoList
//                    SharedData.data.membersGeoList = loginRespone.response?.membersGeoList
//                    
//                    
//                    
//                    self.addMembersinmap(data:self.membersGeoList!)
                    
                }
            }
            catch let jsonErr {
                
                
                print(jsonErr)
                
            }
            
        }){ (error) in
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
        
        // print(params)
        
    }
    
    
    
    
    
    
    var batteryLevel: Int {
        return Int(round(UIDevice.current.batteryLevel * 100))
    }
    func addMembersinmap(data:[MembersGeoList])
    {
        // var bounds = GMSCoordinateBounds()
        
        mapView.clear()
        for eachMember in data
        {
            
            let lat = eachMember.latitude!
            let latitude = Double(lat)
            let lng = eachMember.longitude!
            
            let longitude = Double(lng)
            let marker = GMSMarker()
            
            //let camera = GMSCameraPosition.camera(withLatitude: latitude! , longitude:longitude!, zoom: 14.0)
            let icon = UIImage(named: "mapLocation")
            marker.position   = CLLocationCoordinate2D(latitude:latitude!, longitude:longitude!)
            marker.icon                     = icon
            marker.tracksInfoWindowChanges = true
            marker.userData = eachMember.updateDate
            if SharedData.data.getMemberDetails != nil
            {
                
                for each in SharedData.data.getMemberDetails!
                {
                    
                    
                    let mobileNumber = each.countryCode! +  each.mobileNo!
                    
                    print(mobileNumber)
                    
                    if eachMember.userId == each.userId
                    {
                        marker.userData           =    "\(String(describing: eachMember.updateDate!))" + "," + "\(each.firstName!)" +  ","  + "\(mobileNumber)" + "," + "\(String(describing: each.userId!))"
                    }
                }
            }
            
            
            
            
            marker.map = self.mapView
            
            //  mapView.animate(with: update)
            //  bounds = bounds.includingCoordinate(marker.position)
            
        }
        //        let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
        //        mapView.animate(with: update)
        
        
    }
    
    func showAlert(sender:Int)
    {
        
        
        
        
        
//        if   (featureList?.featureList?.contains(where: { name in name.featureKey == "key.sosalert"}) ?? false)
//
//        {
//
//
//
//            print("match")
//        }else
//        {
//            print("not match")
//
//            self.showCustomAlertWith(message: "", descMsg:"your not buy this feature", itemimage: #imageLiteral(resourceName: "AboutUs"), actions: nil)
//
//
//            return
//        }
//
        
        let screenSize: CGRect = UIScreen.main.bounds
        containerView = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width , height: screenSize.height))
        containerView?.backgroundColor = UIColor.black
        containerView?.alpha = 0.85
     //   self.view.addSubview(containerView!)
        
        
        
        panicView = Bundle.main.loadNibNamed("PanicView", owner: self, options: nil)?.first as? PanicView
        
        panicView?.frame = self.view.frame
      //  panicView?.backgroundColor = UIColor.white
    //    panicView?.center = (self.view.center)
        panicView?.cancelButton.layer.cornerRadius = 15
        panicView?.cancelButton.clipsToBounds = true
        
        panicView!.callButton.layer.cornerRadius = 15
         panicView?.callButton.clipsToBounds = true
    
        panicView?.trackButton.clipsToBounds = true
        panicView?.trackButton.layer.cornerRadius = 20
        
        panicView?.firstView.dropShadow()
        panicView?.secondView.dropShadow()
        panicView?.okButton.tag = sender
        
        
      
    
        panicView?.sendingLabel.text = "Do you want send an Alert to \(String(describing: (SharedData.data.getMemberDetails?[sender].firstName)!))?"
        panicView?.emergencyButton.setImage(UIImage(named: "radioButtonSelected"), for: .normal)
        
        
        intergerValue = 1
        
        panicView?.cancelButton.addTarget(self, action: #selector(dismissSelf), for: .touchUpInside)
        panicView?.okButton.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        panicView?.crossCancel.addTarget(self, action: #selector(cancelTapped), for: .touchUpInside)
        panicView?.callButton.addTarget(self, action: #selector(callButtonTapped), for: .touchUpInside)
        
        panicView?.emergencyButton.addTarget(self, action: #selector(emergencyTapped), for: .touchUpInside)
        panicView?.comeButton.addTarget(self, action: #selector(comeTapped), for: .touchUpInside)
        panicView?.justFunButton.addTarget(self, action: #selector(justTapped), for: .touchUpInside)
        
        panicView?.trackButton.addTarget(self, action: #selector(trackButtonTapped), for: .touchUpInside)
        panicView?.trackButton.tag = SharedData.data.getMemberDetails?[sender].userId ?? 0
        panicView?.trackButton.setTitle("Track \(String(describing: (SharedData.data.getMemberDetails?[sender].firstName)!))", for: .normal)
        
         self.view.addSubview(panicView!)
 //       containerView?.addSubview(panicView!)
       //   self.addChild(panicView)
        
        
    }
    
     @objc func callButtonTapped(sender: UIButton) {
        print("Call 911")
        
        let mobileNos = "911"
        //   print(getMemberDetails?.mobileNo?.getStringFromPhoneNumber())
        
        let url: NSURL = URL(string: "TEL://\(mobileNos ?? ""))")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    
    
    @objc func trackButtonTapped(sender: UIButton) {
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "DirectionsViewController") as! DirectionsViewController
        vc.memberId = sender.tag
        
        
        if membersGeoList != nil
        {
            let membersGeoListdata = membersGeoList?.map{$0.userId == sender.tag}
            print(membersGeoListdata!)
            let membersGeoListdata1 = membersGeoList?.filter{$0.userId == sender.tag}
            if (membersGeoListdata1?.count)! > 0
            {
                let lat = membersGeoListdata1![0].latitude!
                let Latitude = Double(lat)
                let lng =  membersGeoListdata1![0].longitude!
                let longitude = Double(lng)
                
                vc.markerLocation = CLLocationCoordinate2D(latitude: Latitude!, longitude:  longitude!)
                
            }
        }
        vc.currentLocation = currentLocation
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    @objc func buttonTapped(sender: UIButton) {
        
        //        if panicView?.emergencyButton.isSelected == false && panicView?.comeButton.isSelected == false && panicView?.justFunButton.isSelected == false
        //        {
        //            showAlert(title: Constants.alertTitle, msg: "Please select atleast one")
        //        }
        
        print(sender.tag)
        
        if intergerValue! == 3
        {
            panicView?.trackButton.isHidden = true
         //   panicView?.douYouWantTrackLbl.isHidden = true
            panicView?.doYouWantHeightCon.constant = 0
            panicView?.emojiiiTopCons.constant = 25
            panicView?.secondViewHeight.constant = 300
            
        }
        else
        {
            panicView?.trackButton.isHidden = false
            // panicView?.douYouWantTrackLbl.isHidden = false
            panicView?.doYouWantHeightCon.constant = 45
            panicView?.emojiiiTopCons.constant = 10
            panicView?.secondViewHeight.constant = 300
        }
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        
        
        
        parameter.setValue(SharedData.data.getMemberDetails?[sender.tag].userId, forKey: "memberId")
        
        parameter.setValue(intergerValue, forKey: "typeOfAlertId")
        // typeOfAlertId
        
        //      parameter.setValue(1, forKey: "memberId")
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        parameter.setValue(profileData.userId, forKey: "userId")
        
        
        print(parameter)
        
        
        APIServices.postDataToServer(url: API.panic, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                var derivedData = response as! [String : Any]
             //   self.panicView?.successLabel.text =  derivedData["message"] as? String
                self.panicView?.firstView.isHidden = true
                
            }else
            {
                self.containerView?.removeFromSuperview()
                self.panicView?.removeFromSuperview()
            }
            
        })
        
        
        
        
    }
    @objc func cancelTapped(sender: UIButton) {
        Analytics.logEvent("SOS_Notif_Dismiss_Btn_Clicked", parameters:nil)

        containerView?.removeFromSuperview()
        panicView?.removeFromSuperview()
    }
    
    @objc func dismissSelf(){
        
        containerView?.removeFromSuperview()
        panicView?.removeFromSuperview()
        
    }
    
    
    @objc func callTapped(_ sender: UIButton)
    {
        
        Analytics.logEvent("Home_Map_Mem_Call_Clicked", parameters:nil)

        let phoneNo = sender.tag
        
        let url: NSURL = URL(string: "TEL://\(String(describing: phoneNo))")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        
        
    }
    
    
    
    @objc func directionTapped(_ sender: UIButton)
    {
          Analytics.logEvent("Home_Map_Mem_Direc_Clicked", parameters:nil)
        if  currentLocation  == nil
        {
            
            self.showAlert(title: "", msg: "User location not avaiable")
            return
        }
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "DirectionsViewController") as! DirectionsViewController
        vc.memberId = sender.tag
        
        
        if membersGeoList != nil
        {
            let membersGeoListdata = membersGeoList?.map{$0.userId == sender.tag}
            print(membersGeoListdata!)
            let membersGeoListdata1 = membersGeoList?.filter{$0.userId == sender.tag}
            if (membersGeoListdata1?.count)! > 0
            {
                let lat = membersGeoListdata1![0].latitude!
                let Latitude = Double(lat)
                let lng =  membersGeoListdata1![0].longitude!
                let longitude = Double(lng)
                
                vc.markerLocation = CLLocationCoordinate2D(latitude: Latitude!, longitude:  longitude!)
                
            }
        }
        
        
        
        vc.currentLocation = currentLocation
        
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
        
    }
    
    @objc func messageTapped(_ sender: UIButton)
    {
        Analytics.logEvent("Home_Map_Mem_SMS_Clicked", parameters:nil)
        
//        messageTextField.layer.borderWidth = 1.5
//        messageTextField.layer.borderColor = UIColor(red: 53/255, green: 154/255, blue: 210/255, alpha: 1.0).cgColor
//        messageTextField.layer.cornerRadius = 10
//        messageTextField.layer.masksToBounds = true
//
//
//
//
//        sendMessageButton.tag = sender.tag
//
//
//        messageView.isHidden = false
//        self.view.addSubview(messageView)
//
//        messageTextField.text = nil
        
        
        if  SharedData.data.getMemberDetails?.count ?? 0 > 0
            
        {
            
            print(sender.tag)
            
            
            let   getMemberDetails1 = SharedData.data.getMemberDetails?.filter{$0.userId == sender.tag}
            
            
            
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            
            
            
            for each in getMemberDetails1!
                
            {
                
                vc.getMemberDetails = each
                
            }
            
            
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
            
        
            
            
        }
    }
    
    
    @IBAction func messageCancel(_ sender: Any) {
        
        messageView.isHidden = true
    }
    
    
    
    @IBAction func messageSendAction(_ sender: UIButton) {
        
        if messageTextField.text!.isEmpty || messageTextField.text == ""
        {
            self.showAlert(title: "", msg: "Please enter message")
            
            return
        }
        let phoneNo = sender.tag
        
        
        
        
//        for each in SharedData.data.getMemberDetails!
//        {
//
//            if each.mobileNo ==  String(describing: sender.tag)
//           {
//            print(each)
//
//
//            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            // let vc = mainStoryboard.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
//
//            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
//            vc.name = "\(String(describing: (each.firstName)!))" + " " + "\(String(describing: (each.lastName)!))"
//            vc.memberId = each.userId
//            vc.getMemberDetails = each
//
//            self.navigationController?.pushViewController(vc, animated: true)
//
//
//            }
//        }
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue("+\(phoneNo)", forKey: "mobileNo")
        parameter.setValue(messageTextField.text, forKey: "msg")
        print(API.sendingSMS)
        print(parameter)
        
        APIServices.postDataToServer(url: API.sendingSMS, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                self.messageView.isHidden = true
                
                print(response)
                
                let  respons = response as? [String:Any]
                
                self.showAlert(title: "", msg: respons?["message"] as! String)
                
            }else
            {
                
                
                
            }
        })
    }
    
    
    
    
    
    
    @objc func emergencyTapped(_ sender: UIButton)
    {
        Analytics.logEvent("Home_SOS_Emergency_Selected", parameters:nil)
        intergerValue = 1
        panicView?.emergencyButton.setImage(UIImage(named: "radioButtonSelected"), for: .normal)
        panicView?.comeButton.setImage(UIImage(named: "radioButton"), for: .normal)
        panicView?.justFunButton.setImage(UIImage(named: "radioButton"), for: .normal)
        
        sender.isSelected = !sender.isSelected
    }
    @objc func comeTapped(_ sender: UIButton)
        
    {
        Analytics.logEvent("Home_SOS_Come_Get_Me_Selected", parameters:nil)
        intergerValue = 2
        panicView?.emergencyButton.setImage(UIImage(named: "radioButton"), for: .normal)
        panicView?.comeButton.setImage(UIImage(named: "radioButtonSelected"), for: .normal)
        panicView?.justFunButton.setImage(UIImage(named: "radioButton"), for: .normal)
        
        sender.isSelected = !sender.isSelected
    }
    @objc func justTapped(_ sender: UIButton)
        
        
    {
        Analytics.logEvent("Home_SOS_Just_For_Fun_Selected", parameters:nil)
        intergerValue = 3
        panicView?.emergencyButton.setImage(UIImage(named: "radioButton"), for: .normal)
        panicView?.comeButton.setImage(UIImage(named: "radioButton"), for: .normal)
        panicView?.justFunButton.setImage(UIImage(named: "radioButtonSelected"), for: .normal)
        
        sender.isSelected = !sender.isSelected
    }
    
    
    
    func showinMapSelectedUser(userId:Int)
    {
        print(userId)
        
        
        
        if membersGeoList != nil
        {
            let membersGeoListdata1 = membersGeoList?.filter{$0.userId == userId}
            if (membersGeoListdata1?.count)! > 0
            {


            }else
            {
                showAlert(title: "", msg: "User location details not updated")
            }

            
            
            
           mapView.clear()
            for each1 in  SharedData.data.membersGeoList!
            {
                if each1.userId == userId
                {
                    let lat = each1.latitude!
                    let Latitude = Double(lat)
                    let lng =  each1.longitude!
                    let longitude = Double(lng)
                    let camera = GMSCameraPosition.camera(withLatitude: Latitude! , longitude:longitude!, zoom: 23.0)
                    let icon = UIImage(named: "maplocationoran")
                    let marker  = GMSMarker(position: CLLocationCoordinate2DMake(Latitude!, longitude!))
                    marker.icon                     = icon
                    marker.tracksInfoWindowChanges = true
                    
                    if SharedData.data.getMemberDetails != nil
                    {

                        for each in SharedData.data.getMemberDetails!
                        {

                     let mobileNumber = each.countryCode! +  each.mobileNo!
                            print(mobileNumber)
                            
                            if each1.userId == each.userId
                            {
                                marker.userData           =    "\(String(describing: each1.updateDate))" + "," + "\(each.firstName!)" +  "," +
                                    "\(mobileNumber)" + "," +   "\(String(describing: each.userId!))"
                            }
                        }
                    }
                    
                    //  marker.userData           =    membersGeoListdata1![0].updateDate
                    marker.map                      = self.mapView
                    mapView.camera = camera
                }else
                {
                    
                    
                          //  showAlert(title: "", msg: "User location details not updated")
                    let lat = each1.latitude!
                    let latitude = Double(lat)
                    let lng = each1.longitude!
                    
                    let longitude = Double(lng)
                    let marker = GMSMarker()
                    
                    //let camera = GMSCameraPosition.camera(withLatitude: latitude! , longitude:longitude!, zoom: 14.0)
                    let icon = UIImage(named: "mapLocation")
                    marker.position   = CLLocationCoordinate2D(latitude:latitude!, longitude:longitude!)
                    marker.icon                     = icon
                    marker.tracksInfoWindowChanges = true
                    marker.userData = each1.updateDate
                    if SharedData.data.getMemberDetails != nil
                    {

                        for each in SharedData.data.getMemberDetails!
                        {




                            if each1.userId == each.userId
                            {
                                marker.userData           =    "\(String(describing: each1.updateDate!))" + "," + "\(each.firstName!)" +  ","  + "\(String(describing: each.mobileNo!))" + "," + "\(String(describing: each.userId!))"
                            }
                        }
                    }

                    
                   
                    
                    marker.map = self.mapView
                }
                
            }
            
           
            
//            let membersGeoListdata = membersGeoList?.map{$0.userId == userId}
//            print(membersGeoListdata!)
//            let membersGeoListdata1 = membersGeoList?.filter{$0.userId == userId}
//            if (membersGeoListdata1?.count)! > 0
//            {
//                let lat = membersGeoListdata1![0].latitude!
//                let Latitude = Double(lat)
//                let lng =  membersGeoListdata1![0].longitude!
//                let longitude = Double(lng)
//                let camera = GMSCameraPosition.camera(withLatitude: Latitude! , longitude:longitude!, zoom: 20.0)
//                let icon = UIImage(named: "maplocationoran")
//                let marker  = GMSMarker(position: CLLocationCoordinate2DMake(Latitude!, longitude!))
//                marker.icon                     = icon
//                marker.tracksInfoWindowChanges = true
//                
//                if SharedData.data.getMemberDetails != nil
//                {
//                    
//                    for each in SharedData.data.getMemberDetails!
//                    {
//                        
//                        
//                        if membersGeoListdata1![0].userId == each.userId
//                        {
//                            marker.userData           =    "\(String(describing: membersGeoListdata1![0].updateDate!))" + "," + "\(each.firstName!)" +  "," +
//                                "\(String(describing: each.mobileNo!))" + "," +   "\(String(describing: each.userId!))"
//                        }
//                    }
//                }
//                
//                //  marker.userData           =    membersGeoListdata1![0].updateDate
//                marker.map                      = self.mapView
//                mapView.camera = camera
//            }else
//            {
//                showAlert(title: "", msg: "User location details not updated")
//            }
            
        }else
        {
            showAlert(title: "", msg: "User location details not updated")
        }
        
        
    }
    
    func setupMarker(){
        // self.googleMapView.clear()
        
        // let icon = UIImage(named: "RestuarantLogo")
        
        if let location = currentLocation{
            var bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: location, coordinate: location)
            let camera                      = GMSCameraPosition(target: location, zoom: 17, bearing: 0, viewingAngle: 0)
            //  let marker                      = GMSMarker(position: location)
            //  marker.icon                     = icon
            //  marker.title                    = "My Location"
            //  marker.map                      = googleMapView
            //self.mapView.setMinZoom(1, maxZoom: 20)
            mapView.animate(to: camera)
            bounds                          = bounds.includingCoordinate(marker.position)
            //self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 15.0))
        }
        // }
    }
    
    
    private func addPullUpController() {
        let pullUpController = makeSearchViewControllerIfNeeded()
        _ = pullUpController.view // call pullUpController.viewDidLoad()
        addPullUpController(pullUpController,
                            initialStickyPointOffset: pullUpController.initialPointOffset,
                            animated: true)
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
        //
        //            return self.customInfoWindow
        //
        //        }
        
        return UIView()
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (tappedMarker != nil){
            
            customInfoWindow.center = mapView.projection.point(for: tappedMarker!.position)
            customInfoWindow.center.y -= 150
        }
    }
    
    
    //   func mapview
    //    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
    //
    //
    //        customInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)?[0] as! CustomInfoWindow
    //
    //        geocodeCoordinates(location: marker.position, completionHandler: { (address) -> Void in
    //            print(address)
    //
    //            self.customInfoWindow.infoWindowAddress.text = address
    //
    //        })
    //
    //        if marker.userData != nil{
    //
    //            print(marker.userData as! String)
    //
    //            var myStringArr = (marker.userData as? String)?.components(separatedBy: ",")
    //
    //
    //
    //
    //            self.customInfoWindow.memberName.text = myStringArr?[1] as! String
    //
    //            print(myStringArr?[0] as? String)
    //
    //            print(myStringArr?[1] as! String)
    //
    //            print(myStringArr?[2] as! String)
    //
    //            self.customInfoWindow.callAction.tag = Int(myStringArr?[2] as! String)!
    //
    //
    //            self.customInfoWindow.messagebutton.tag = Int(myStringArr?[2] as! String)!
    //
    //
    //
    //            self.customInfoWindow.callAction?.addTarget(self, action: #selector(callTapped), for: .touchUpInside)
    //
    //            self.customInfoWindow.messagebutton?.addTarget(self, action: #selector(messageTapped), for: .touchUpInside)
    //
    //
    //            let dobul = Double(myStringArr?[0] as! String)
    //            //   let double = myStringArr?[0] as? Any
    //
    //            //     let unixTimeStamp: Double = Double(((marker.userData as? Double)!)) / 1000.0
    //
    //            let unixTimeStamp: Double = dobul! / 1000.0
    //
    //            let date = NSDate(timeIntervalSince1970:unixTimeStamp)
    //
    //            let date1 = Date()
    //            let diff = Int(date1.timeIntervalSince1970 - date.timeIntervalSince1970)
    //            customInfoWindow.infoWindowTimeStamp.text = timeAgoDisplay(time:Int(diff))
    //            //                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
    //            //                        return self.customInfoWindow
    //            //                    }
    //        }
    //
    //
    //        self.mapView.addSubview(customInfoWindow)
    //
    //        print("infowindow tapped")
    //
    ////        if  currentLocation  == nil
    ////        {
    ////
    ////            self.showAlert(title: Constants.alertTitle, msg: "User location not avaiable")
    ////            return
    ////        }
    ////
    ////        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    ////        let vc = mainStoryboard.instantiateViewController(withIdentifier: "DirectionsViewController") as! DirectionsViewController
    ////        vc.currentLocation = currentLocation
    ////        vc.markerLocation = marker.position
    ////
    ////
    ////        self.navigationController?.pushViewController(vc, animated: true)
    ////
    ////        print("You tapped : \(marker.position.latitude),\(marker.position.longitude)")
    ////
    //
    //    }
    //
    
    @IBAction func NotificationBarButtonAction(_ sender: UIBarButtonItem) {
        
        Analytics.logEvent("Home_Notific_Menu_Clicked", parameters:nil)


        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController

        self.navigationController?.pushViewController(vc, animated: true)
      
        
        
    }
    
     @objc func deviceChanged()
     {
    logoutMethod()
    }
    @objc func deactivateAccount()
    {
        logoutMethodForDeact()
    }
    
    func logoutMethod() {
        
        
        
    
        
        
       
        

        
        
        if let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) {
            let alertController = UIAlertController(title: "", message: "Looks like you have logged in another device so please tap on Ok to logout in this device.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.callAPI()
            }
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:nil)
            
            
        }
        
        
    }
    
    
    func logoutMethodForDeact() {
        
        if let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) {
            let alertController = UIAlertController(title: "", message: "Your account has been deactivated by admin.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.callAPI()
            }
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:nil)
            
            
        }
        
        
    }
    
    func  callAPI()
    {
        if  self.revealViewController() == nil
        {
            return
        }
        
        
        let revealViewController:SWRevealViewController = self.revealViewController()
        UserDefaults.standard.removeObject(forKey: "profile_details")
        UserDefaults.standard.removeObject(forKey: "Paymentdata")
        
        UserDefaults.standard.set(nil, forKey: "deviceChangedtrue")
        
        UserDefaults.standard.set(nil, forKey: "deactivateAccount")
        
        UserDefaults.standard.removeObject(forKey: "jwtToken")
        
        UserDefaults.standard.set(nil, forKey: "jwtToken")
        self.deleteAllRecords()
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let newFrontViewController = UINavigationController.init(rootViewController:desController)
        revealViewController.pushFrontViewController(newFrontViewController, animated: true)
    }
    func deleteAllRecords() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "PushNotifications")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    @objc func callgetMemberApi()
    {
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        let addMemberUrl = API.getAllNotificationsBYMemberId + "?userId=\(String(describing: profileData.userId!))"
        print(addMemberUrl)
        HttpWrapper.gets(with: addMemberUrl, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            // print(response)
            
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatusNotifications.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    self.notifiationResponse = loginRespone.response
                    self.addBadge(itemvalue: String(describing: (self.notifiationResponse?.allNotifications?.count)!))
                    self.appDelegate.notificationBargeCount = String(describing: (self.notifiationResponse?.allNotifications?.count)!)
                    //String(describing: self.notifiationResponse?.allNotifications?.count)
                    
                    
                    
                }
                else
                    
                {
                    
                    
                    
                }
                
            }catch let jsonErr {
                
                
                print("Error serializing json:", jsonErr)
                
            }
        }){ (error) in
            
            
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
            
            print(error)
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        customInfoWindow.removeFromSuperview()
    }
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        Analytics.logEvent("Home_Map_Mem_Pointer_Clicked", parameters:nil)

        
        
        //   let location = CLLocationCoordinate2D(latitude: marker.lat, longitude: marker.)
        
        customInfoWindow.removeFromSuperview()
        
        customInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)?[0] as! CustomInfoWindow
        
        sendMessageButton.changeButtonCornerRadius()
        messageCancelButton.changeButtonCornerRadius()
     
        
        geocodeCoordinates(location: marker.position, completionHandler: { (address) -> Void in
            print(address)
            
            self.customInfoWindow.infoWindowAddress.text = address
            
        })
        
        if marker.userData != nil{
            
            print(marker.userData as! String)
            
            var myStringArr = (marker.userData as? String)?.components(separatedBy: ",")
            
            print(marker.userData)
           if myStringArr?[1] != nil
            {
                self.customInfoWindow.memberName.text = myStringArr?[1] as! String
            }
            
            
            if myStringArr?[2] != nil
            {
                self.customInfoWindow.callAction.tag = Int(myStringArr?[2] as! String)!
               
                
            }
            
            if myStringArr?[3] != nil
            {
              
              
                self.customInfoWindow.directionButton.tag = Int(myStringArr?[3] as! String)!
                 self.customInfoWindow.messagebutton.tag = Int(myStringArr?[3] as! String)!
                
            }
            
        
            
        
            
         
            
            
            
            self.customInfoWindow.callAction?.addTarget(self, action: #selector(callTapped), for: .touchUpInside)
            
         
            
            self.customInfoWindow.directionButton?.addTarget(self, action: #selector(directionTapped), for: .touchUpInside)
            
            
            
            self.customInfoWindow.messagebutton?.addTarget(self, action: #selector(messageTapped), for: .touchUpInside)
            
            
            var dobul:Double?
            if myStringArr?[0] != nil
            {
                    dobul = Double(myStringArr?[0] as! String)
            }
            
            
         
            //   let double = myStringArr?[0] as? Any
            
            //     let unixTimeStamp: Double = Double(((marker.userData as? Double)!)) / 1000.0
            
            
            if dobul != nil
            {
                let unixTimeStamp: Double = dobul! / 1000.0
                
                let date = NSDate(timeIntervalSince1970:unixTimeStamp)
                
                let date1 = Date()
                let diff = Int(date1.timeIntervalSince1970 - date.timeIntervalSince1970)
                customInfoWindow.infoWindowTimeStamp.text = timeAgoDisplay(time:Int(diff))
            }
            
            //                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            //                        return self.customInfoWindow
            //                    }
        }
        
        
        tappedMarker = marker
        
        let position = marker.position
        mapView.animate(toLocation: position)
        let point = mapView.projection.point(for: position)
        let newPoint = mapView.projection.coordinate(for: point)
        let camera = GMSCameraUpdate.setTarget(newPoint)
        mapView.animate(with: camera)
        
        customInfoWindow.center = mapView.projection.point(for: position)
        customInfoWindow.center.y -= 150
        
        self.mapView.addSubview(customInfoWindow)
        
        
        
        return false
    }
    
    func timeAgoDisplay(time:Int) -> String {
        let secondsAgo = time
        
        let minute = 60
        let hour = 60 * minute
        let day = 24 * hour
        let week = 7 * day
        
        if secondsAgo < minute {
            return "\(secondsAgo) sec ago"
        } else if secondsAgo < hour {
            return "\(secondsAgo / minute) min ago"
        } else if secondsAgo < day {
            return "\(secondsAgo / hour) hrs ago"
        } else if secondsAgo < week {
            return "\(secondsAgo / day) days ago"
        }
        return "\(secondsAgo / week) weeks ago"
    }
    
    func offsetFrom(date : Date) -> String {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: date);
        
        let seconds = "\(difference.second ?? 0)s"
        let minutes = "\(difference.minute ?? 0)m" + " " + seconds
        let hours = "\(difference.hour ?? 0)h" + " " + minutes
        let days = "\(difference.day ?? 0)d" + " " + hours
        
        if let day = difference.day, day          > 0 { return days }
        if let hour = difference.hour, hour       > 0 { return hours }
        if let minute = difference.minute, minute > 0 { return minutes }
        if let second = difference.second, second > 0 { return seconds }
        return ""
    }
    
    func geocodeCoordinates(location : CLLocationCoordinate2D,completionHandler: @escaping CompletionHandler){
        
        var postalAddress  = ""
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(location, completionHandler: {response,error in
            if let gmsAddress = response!.firstResult(){
                
                if  (gmsAddress.lines?.count)! > 0
                {
                    postalAddress += gmsAddress.lines![0]
                }
                //                for line in  gmsAddress.lines! {
                //                    postalAddress += line + " "
                //                }
                completionHandler(postalAddress)
                
                print(postalAddress)
                return
            }
        })
        return
    }
    func addEventToCalendar(title: String, description: String?, startDate: Date, endDate: Date, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = description
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let e as NSError {
                    completion?(false, e)
                    return
                }
                completion?(true, nil)
            } else {
                completion?(false, error as NSError?)
            }
        })
    }
    
    
    
}
public class AlertFun {
    class func ShowAlert(title: String, message: String, in vc: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    class func openSettingApp(title: String, message: String, in vc: UIViewController)
    {
        let alertController = UIAlertController (title: "", message:message , preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, options: [:])
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        vc.present(alertController, animated: true, completion: nil)
    }
}

extension String {
    
    func toDate()-> Date?{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+5:30")
        guard let date = dateFormatter.date(from: self) else {
            fatalError()
        }
        
        
        return date
        
    }
    
    
    
    
    
}


extension CAShapeLayer {
    func drawCircleAtLocation(location: CGPoint, withRadius radius: CGFloat, andColor color: UIColor, filled: Bool) {
        fillColor = filled ? color.cgColor : UIColor.white.cgColor
        strokeColor = color.cgColor
        let origin = CGPoint(x: location.x - radius, y: location.y - radius)
        path = UIBezierPath(ovalIn: CGRect(origin: origin, size: CGSize(width: radius * 2, height: radius * 2))).cgPath
    }
}

private var handle: UInt8 = 0;

extension UIBarButtonItem {
    private var badgeLayer: CAShapeLayer? {
        if let b: AnyObject = objc_getAssociatedObject(self, &handle) as AnyObject? {
            return b as? CAShapeLayer
        } else {
            return nil
        }
    }
    
    func addBadge(number: Int, withOffset offset: CGPoint = CGPoint.zero, andColor color: UIColor = UIColor.red, andFilled filled: Bool = true) {
        guard let view = self.value(forKey: "view") as? UIView else { return }
        
        badgeLayer?.removeFromSuperlayer()
        
        var badgeWidth = 8
        var numberOffset = 4
        
        if number > 9 {
            badgeWidth = 12
            numberOffset = 6
        }
        
        // Initialize Badge
        let badge = CAShapeLayer()
        let radius = CGFloat(7)
        let location = CGPoint(x: view.frame.width - (radius + offset.x), y: (radius + offset.y))
        badge.drawCircleAtLocation(location: location, withRadius: radius, andColor: color, filled: filled)
        view.layer.addSublayer(badge)
        
        // Initialiaze Badge's label
        let label = CATextLayer()
        label.string = "\(number)"
        label.alignmentMode = CATextLayerAlignmentMode.center
        label.fontSize = 11
        label.frame = CGRect(origin: CGPoint(x: location.x - CGFloat(numberOffset), y: offset.y), size: CGSize(width: badgeWidth, height: 16))
        label.foregroundColor = filled ? UIColor.white.cgColor : color.cgColor
        label.backgroundColor = UIColor.clear.cgColor
        label.contentsScale = UIScreen.main.scale
        badge.addSublayer(label)
        
        // Save Badge as UIBarButtonItem property
        objc_setAssociatedObject(self, &handle, badge, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        badge.zPosition = 1000
    }
    
    func updateBadge(number: Int) {
        if let text = badgeLayer?.sublayers?.filter({ $0 is CATextLayer }).first as? CATextLayer {
            text.string = "\(number)"
        }
    }
    
    func removeBadge() {
        badgeLayer?.removeFromSuperlayer()
    }
}
class BadgeButton: UIButton {
    
    var badgeLabel = UILabel()
    
    var badge: String? {
        didSet {
            addbadgetobutton(badge: badge)
        }
    }
    
    public var badgeBackgroundColor = UIColor.red {
        didSet {
            badgeLabel.backgroundColor = badgeBackgroundColor
        }
    }
    
    public var badgeTextColor = UIColor.white {
        didSet {
            badgeLabel.textColor = badgeTextColor
        }
    }
    
    public var badgeFont = UIFont.systemFont(ofSize: 12.0) {
        didSet {
            badgeLabel.font = badgeFont
        }
    }
    
    public var badgeEdgeInsets: UIEdgeInsets? {
        didSet {
            addbadgetobutton(badge: badge)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addbadgetobutton(badge: nil)
    }
    
    func addbadgetobutton(badge: String?) {
        badgeLabel.text = badge
        badgeLabel.textColor = badgeTextColor
        badgeLabel.backgroundColor = badgeBackgroundColor
        badgeLabel.font = badgeFont
        badgeLabel.sizeToFit()
        badgeLabel.textAlignment = .center
        let badgeSize = badgeLabel.frame.size
        
        let height = max(18, Double(badgeSize.height) + 5.0)
        let width = max(height, Double(badgeSize.width) + 10.0)
        
        var vertical: Double?, horizontal: Double?
        if let badgeInset = self.badgeEdgeInsets {
            vertical = Double(badgeInset.top) - Double(badgeInset.bottom)
            horizontal = Double(badgeInset.left) - Double(badgeInset.right)
            
            let x = (Double(bounds.size.width) - 10 + horizontal!)
            let y = -(Double(badgeSize.height) / 2) - 10 + vertical!
            badgeLabel.frame = CGRect(x: x, y: y, width: width, height: height)
        } else {
            let x = self.frame.width - CGFloat((width / 2.0))
            let y = CGFloat(-(height / 2.0))
            badgeLabel.frame = CGRect(x: x, y: y, width: CGFloat(width), height: CGFloat(height))
        }
        
        badgeLabel.layer.cornerRadius = badgeLabel.frame.height/2
        badgeLabel.layer.masksToBounds = true
        addSubview(badgeLabel)
        badgeLabel.isHidden = badge != nil ? false : true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addbadgetobutton(badge: nil)
        fatalError("init(coder:) is not implemented")
    }
}
