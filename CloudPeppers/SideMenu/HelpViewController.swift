//
//  HelpViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 14/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//
import UIKit
import WebKit
import SVProgressHUD
class HelpViewController: UIViewController,WKNavigationDelegate {
    
    @IBOutlet weak var btnMenuButton: UIBarButtonItem!
    
    
    @IBOutlet weak var helpTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
        
        navigationController?.navigationBar.barTintColor = Constants.colour
        self.navigationItem.title = "Help"
        self.changeFontForViewController()
        
//        guard let url = URL(string: API.cpadminportal) else { return }
//        UIApplication.shared.open(url)
    
        //  navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        btnMenuButton.target = revealViewController()
        btnMenuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        
        
//        let webView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
//         webView.navigationDelegate = self
//        self.view.addSubview(webView)
//        //  let url = URL(string: "https://www.youtube.com")
//        let url = URL(string: API.cpadminportal)
//        webView.load(URLRequest(url: url!))
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        // hide/stop indicator
        SVProgressHUD.dismiss()
        
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        //show your activity
      //   SVProgressHUD.show()
        
        if Reachability.isConnectedToNetwork(){
           SVProgressHUD.show()
        }else{
            self.showAlert(title: "", msg: "No network connection")
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
