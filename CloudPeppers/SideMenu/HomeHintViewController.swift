//
//  HomeHintViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 25/09/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class HomeHintViewController: UIViewController {
    @IBOutlet weak var infoView: UIImageView!
    
    override func viewDidLoad() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.infoView.addGestureRecognizer(tap)
        infoView.isUserInteractionEnabled = true
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.removeFromSuperview()
      dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
