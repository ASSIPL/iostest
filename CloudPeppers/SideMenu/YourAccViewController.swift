//
//  YourAccViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 14/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase

class YourAccViewController: UIViewController {
    
    
    @IBOutlet weak var disView: UIView!
    @IBOutlet weak var btnMenuButton: UIBarButtonItem!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var resetPassword: UIButton!
    @IBOutlet weak var emaiIdlLB: UILabel!
    @IBOutlet weak var mobileNumLB: UILabel!
    @IBOutlet weak var lastNameLB: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        profilePicture.layer.cornerRadius = profilePicture.frame.height/2
        profilePicture.clipsToBounds = true
       // disView.displayViewShadow()
        
       disView.clipsToBounds = true
      disView.layer.cornerRadius = 10
        disView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        navigationController?.navigationBar.barTintColor = Constants.colour
        self.navigationItem.title = "My Profile"
        self.changeFontForViewController()
        //    navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        resetPassword.changeButtonCornerRadius()
        btnMenuButton.target = revealViewController()
        btnMenuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        
        
        if let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) {
            
            //   lastNameLB.text = profileData.lastName
            lastNameLB.text =  "\(profileData.firstName!)" + " " + "\(profileData.lastName!)"
            //\(profileData.firstName") + \("profileData.lastName")
            mobileNumLB.text = "\(profileData.countryCode!)" + " " +  (profileData.mobileNo?.getPhoneNumberFromString())!
         //   mobileNumLB.text = profileData.mobileNo?.getPhoneNumberFromString()
            //    addressLB.text = profileData.address
            emaiIdlLB.text = profileData.email
            
            if profileData.profileImage != nil &&  profileData.profileImage?.count != 0
            {
                self.profilePicture.sd_setImage(with: URL(string: (profileData.profileImage)!), placeholderImage: UIImage(named: "placeHolderProfile"))
                
                //                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { // Change `2.0` to the desired number of seconds.
                //                    let url = URL(string: profileData.profileImage!)
                //                    let data = try? Data(contentsOf: url!)
                //                    self.profilePicture.image = UIImage(data: data!)
                //                }
                //
                //    self.profilePicture.image = UIImage(data: profileData.profilePic!)
                
            }
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Analytics.logEvent("MyProfile_Screen_Enter", parameters:nil)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("MyProfile_Screen_Exit", parameters:nil)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    @IBAction func resetPasswordTapped(_ sender: Any) {
    }
    
}
