
import UIKit
import Firebase
protocol customDelegate: class {
    func didSelectData(_ result: responses)
    
}

protocol customMobileNumberDelegate: class {
    func didSelectMobileNumber(_ result:String)
    
}





class AddFamilyMemberVC: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,customDelegate1,customMobileNumberDelegate {
    
    
    var delegate: customDelegate?
    
    @IBOutlet weak var addContactImageButton: UIButton!
    @IBOutlet weak var myRelationshipToLabel: UILabel!
    @IBOutlet weak var parentBackgroundView: UIView!
    
    @IBOutlet var parentViewheightConstant: NSLayoutConstraint!
    
    @IBOutlet var barButton: UIBarButtonItem!
    
    var fromsidemenu:Bool?
    
    @IBOutlet var pinView: UIView!
    
    @IBOutlet var pinTextFiled: UITextField!
    @IBOutlet var pinViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var userOptHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var myRelationLabel: UILabel!
    @IBOutlet weak var chooseUserOption: UILabel!
    @IBOutlet weak var myRelationshipTF: UITextField!
    @IBOutlet weak var relationView: UIView!
    @IBOutlet var familyButton: UIButton!
    @IBOutlet var thingButton: UIButton!
    @IBOutlet var mobileExtension: UITextField!
    @IBOutlet weak var parentButton: UIButton!
    @IBOutlet var kidButton: UIButton!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var mobileNumTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet var relation: UITextField!
    @IBOutlet weak var addButton: UIButton!
    
    let pickerView = UIPickerView()
    
    var seletedValueType:String?
    
    
    
    var getMemberDetails:responses?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Analytics.logEvent("AddFamilyMemberEnter", parameters:nil)
        familyButton.setImage(UIImage(named: "radioButtonSelected"), for: .normal)
        seletedValueType = "member.type"
        
        firstNameTF.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        if  UserDefaults.standard.string(forKey: "roleKey") == "member.type"
        {
            parentBackgroundView.isHidden = false
            parentViewheightConstant.constant = 38
        }
        else
        {
            parentBackgroundView.isHidden = true
            parentViewheightConstant.constant = 0
        }
        
        
        pinViewHeightConstant.constant = 0
        pinView.isHidden = true
        pinTextFiled.delegate = self
        myRelationLabel.text = "My relationship to him/her*"
        
        if fromsidemenu == true
        {
            navigationController?.navigationBar.barTintColor = Constants.colour
            
            self.changeFontForViewController()
            // navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            
            barButton.target = revealViewController()
            
            
            barButton.action = #selector(SWRevealViewController.revealToggle(_:))
            //
        }else
        {
            barButton.target = self
            
            barButton.image = UIImage(named: "BackArrow1")
            
            navigationController?.navigationBar.barTintColor = Constants.colour
            barButton.action = #selector(buttonClicked(sender:))
            
            
            
        }
        
        
        
        
        
        
        // navigationController?.navigationBar.isHidden = true
        addButton.changeButtonCornerRadius()
        
        emailTF.useUnderline()
        lastNameTF.useUnderline()
        mobileNumTF.useUnderline()
        firstNameTF.useUnderline()
        relation.useUnderline()
        myRelationshipTF.useUnderline()
        mobileExtension.setBottomBorder()
        pinTextFiled.useUnderline()
        
        mobileExtension.delegate = self
        mobileExtension.backgroundColor = UIColor.white
        
        pickerView.delegate = self
        pickerView.reloadAllComponents()
        
        
        mobileExtension.addTarget(self, action: #selector(myTargetFunction), for: .allTouchEvents)
        //  mobileExtension.inputView = pickerView
        self.title = "Add Member"
        firstNameTF.delegate = self
        lastNameTF.delegate = self
        relation.delegate = self
        mobileNumTF.delegate = self
        emailTF.delegate = self
        relationView.isHidden = false
        chooseUserOption.isHidden = false
        myRelationshipTF.delegate = self
        
        if isEditing
            
        {
            firstNameTF.text = getMemberDetails?.firstName
            lastNameTF.text =  getMemberDetails?.lastName
            relation.text = getMemberDetails?.relationShip
            mobileExtension.text = getMemberDetails?.countryCode
            relation.isUserInteractionEnabled = false
            myRelationshipTF.isUserInteractionEnabled = false
            myRelationshipTF.text = getMemberDetails?.relationToMe
            pinTextFiled.text = getMemberDetails?.accessPin
            addContactImageButton.isHidden = true
            
            
            
            myRelationLabel.text = "My relationship to \(String(describing: firstNameTF.text!))"
            
            print(UserDefaults.standard.string(forKey: "roleKey"))
            //
            //            if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type" || UserDefaults.standard.string(forKey: "roleKey") == "thing.type"
            //            {
            //
            //                pinView.isHidden = false
            //                pinViewHeightConstant.constant = 45
            //            }
            
            
            if getMemberDetails?.login_type == "kid.type" ||  getMemberDetails?.login_type  == "kid.type"
            {
                pinView.isHidden = false
                pinViewHeightConstant.constant = 45
            }
            
            
            
            //            relation.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.55859375)
            //            mobileNumTF.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            //            emailTF.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            //            mobileExtension.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            relation.textColor = #colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
            mobileNumTF.textColor = #colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
            emailTF.textColor = #colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
            mobileExtension.textColor = #colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
            myRelationshipTF.textColor = #colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
            myRelationshipToLabel.textColor = #colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
            mobileExtension.isUserInteractionEnabled = false
            mobileNumTF.isUserInteractionEnabled = false
            emailTF.isUserInteractionEnabled = false
            addContactImageButton.isUserInteractionEnabled = false
            relationView.isHidden = true
            chooseUserOption.isHidden = true
            parentBackgroundView.isHidden = true
            userOptHeightConstraint.constant = 0
            heightConstraint.constant = 0
            
            
            mobileNumTF.text = getMemberDetails?.mobileNo!.getPhoneNumberFromString()
            emailTF.text = getMemberDetails?.email
            addButton.setTitle("Update", for: .normal)
            self.title = "Edit Member"
            
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func didSelectMobileNumber(_ result: String) {
        
        mobileNumTF.text = result
        
        let prefix = "+1"
        print(result.toPhoneNumber())
        print(result.toPhoneNumber().hasPrefix(prefix))
        mobileNumTF.text = result.toPhoneNumber()
        
        if  result.hasPrefix(prefix)  {
            print(result.dropFirst(prefix.count).trimmingCharacters(in: .whitespacesAndNewlines).toPhoneNumber())
            
            mobileNumTF.text = result.dropFirst(prefix.count).trimmingCharacters(in: .whitespacesAndNewlines).toPhoneNumber()
            mobileExtension.text = "+1"
        }
        
        let prefix1 = "+91"
        if  result.hasPrefix(prefix1)
        {
            print(result.dropFirst(prefix1.count).trimmingCharacters(in: .whitespacesAndNewlines).toPhoneNumber())
            
            mobileNumTF.text = result.dropFirst(prefix1.count).trimmingCharacters(in: .whitespacesAndNewlines).toPhoneNumber()
            
            mobileExtension.text = "+91"
        }
        
    }
    
    
    @IBAction func contactsNavigation(_ sender: UIButton) {
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
        vc.delegate = self
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    
    
    @objc func myTargetFunction() {
        
        
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CountryPickViewController") as! CountryPickViewController
        vc.delegate = self
        self.present(vc, animated:true, completion:nil)
        //self.present(vc, animated:true, completion:nil)
    }
    func didSelectCountry(_ result: String) {
        mobileExtension.text = result
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("AddFamilyMemberExit", parameters:nil)
    }
    @IBAction func pinVisibleButton(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        /***If eye non - visible (defalut) secured***/
        pinTextFiled.isSecureTextEntry = !sender.isSelected
        
    }
    
    
    
    @objc func buttonClicked(sender: UIBarButtonItem) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        self.navigationController?.pushViewController(vc!, animated: true)

        
    }
    
    
    
    @IBAction func parentButtonTapped(_ sender: UIButton) {
        
        
        pinViewHeightConstant.constant = 0
        pinView.isHidden = true
        pinTextFiled.text = nil
        seletedValueType = "parent.type"
        sender.setImage(UIImage(named: "radioButtonSelected"), for: .normal)
        parentButton.setImage(UIImage(named: "radioButtonSelected"), for: .normal)
        thingButton.setImage(UIImage(named: "radioButton"), for: .normal)
        familyButton.setImage(UIImage(named: "radioButton"), for: .normal)
        kidButton.setImage(UIImage(named: "radioButton"), for: .normal)
        
        
    }
    @IBAction func kidButton(_ sender: UIButton) {
        
        
        pinViewHeightConstant.constant = 45
        pinView.isHidden = false
        seletedValueType = "kid.type"
        sender.setImage(UIImage(named: "radioButtonSelected"), for: .normal)
        familyButton.setImage(UIImage(named: "radioButton"), for: .normal)
        thingButton.setImage(UIImage(named: "radioButton"), for: .normal)
        parentButton.setImage(UIImage(named: "radioButton"), for: .normal)
        kidButton.setImage(UIImage(named: "radioButtonSelected"), for: .normal)
        
        
    }
    
    @IBAction func familyMemberButton(_ sender: UIButton) {
        
        pinViewHeightConstant.constant = 0
        pinView.isHidden = true
        pinTextFiled.text = nil
        seletedValueType = "member.type"
        sender.setImage(UIImage(named: "radioButtonSelected"), for: .normal)
        familyButton.setImage(UIImage(named: "radioButtonSelected"), for: .normal)
        thingButton.setImage(UIImage(named: "radioButton"), for: .normal)
        parentButton.setImage(UIImage(named: "radioButton"), for: .normal)
        kidButton.setImage(UIImage(named: "radioButton"), for: .normal)
        
        
        
    }
    @IBAction func thingButton(_ sender: UIButton) {
        
        
        pinViewHeightConstant.constant = 45
        pinView.isHidden = false
        seletedValueType = "thing.type"
        sender.setImage(UIImage(named: "radioButtonSelected"), for: .normal)
        familyButton.setImage(UIImage(named: "radioButton"), for: .normal)
        thingButton.setImage(UIImage(named: "radioButtonSelected"), for: .normal)
        parentButton.setImage(UIImage(named: "radioButton"), for: .normal)
        kidButton.setImage(UIImage(named: "radioButton"), for: .normal)
        
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
        
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        
        
        return (organizationdetails?.countriesInfo!.count)!
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        
        return "\(String(describing: (organizationdetails?.countriesInfo![row].countryCode!)!))" + " " + "\(String(describing: (organizationdetails?.countriesInfo![row].countryName!)!))"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        let cid = organizationdetails?.countriesInfo![row].countryCode!
        mobileExtension.text = String(describing: cid!)
    }
    
    @IBAction func addButtonTapped(_ sender: Any) {
        
        Analytics.logEvent("Add_Mem_Add_Clicked", parameters:nil)
        
        if isAccountHasValidDetails() == true{
            
            guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                let profileData = try? JSONDecoder().decode(profile.self, from: data) else
            {
                
                return
                
            }
            
            
            if firstNameTF.text?.isEmpty == true || lastNameTF.text?.isEmpty == true || mobileNumTF.text?.isEmpty == true || emailTF.text?.isEmpty == true || relation.text?.isEmpty == true || mobileExtension.text?.isEmpty == true{
                showAlert(title: "", msg: "Fill all fileds")
                return
            }
            
            let parameter:NSMutableDictionary = NSMutableDictionary()
            
            if isEditing
            {
                
                let id =  getMemberDetails?.userId
                parameter.setValue(id,forKey: "memberId")
                
                
                guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                    let profileData = try? JSONDecoder().decode(profile.self, from: data) else
                {
                    
                    return
                    
                }
                parameter.setValue(profileData.userId,forKey: "userId")
                
                
            }
            
            
            
            parameter.setValue(firstNameTF.text, forKey: "firstName")
            parameter.setValue(lastNameTF.text, forKey: "lastName")
            parameter.setValue(mobileNumTF.text!.getStringFromPhoneNumber(), forKey: "mobileNo")
            parameter.setValue(relation.text, forKey: "relationShip")
            parameter.setValue(emailTF.text, forKey: "email")
            parameter.setValue(profileData.userId, forKey: "userId")
            // parameter.setValue(UserDefaults.standard.string(forKey: "fcmToken"), forKey: "fcmId")
            
            Analytics.logEvent(seletedValueType ?? "", parameters:nil)
            if pinTextFiled.text != nil
            {
                parameter.setValue( pinTextFiled.text, forKey: "accessPin")
            }
            parameter.setValue(mobileExtension.text, forKey: "countryCode")
            parameter.setValue("iOS", forKey: "platform")
            parameter.setValue("true", forKey: "isTemrsAccepted")
            parameter.setValue(myRelationshipTF.text, forKey: "relationToMe")
            parameter.setValue(seletedValueType, forKey: "type")
            
            print(parameter)
            
            
            if isEditing
            {
                
                APIServices.postDataToServerp(url: API.updateMemberProfile, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
                    if status == true{
                        
                        let data = response
                        do {
                            
                            let loginRespone = try JSONDecoder().decode(ApiStatusProfile.self, from: data as! Data)
                            let loginStatus = loginRespone.status
                            
                            
                            self.delegate?.didSelectData((loginRespone.response?[0])!)
                        }catch  let jsonErr
                        {
                            print("Error serializing json:", jsonErr)
                        }
                        
                        
                        print(response)
                        
                        
                        //                        let alertController = UIAlertController(title: Constants.alertTitle, message: "Profile updated successfully", preferredStyle: .alert)
                        //
                        //
                        //                        let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                        //
                        //                            self.navigationController?.popViewController(animated: true)
                        //
                        //                        }
                        //
                        //                        alertController.addAction(settingsAction)
                        //                        self.present(alertController, animated: true, completion: nil)
                        //
                        //                        print(response)
                        
                        
                        
                        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                        
                        let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
                        let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
                        
                        
                        let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
                        let msgAttrString = NSMutableAttributedString(string: "Profile Updated Successfully", attributes: msgFont)
                        
                        alert.setValue(titAttrString, forKey: "attributedTitle")
                        alert.setValue(msgAttrString, forKey: "attributedMessage")
                        
                        let saveAction = UIAlertAction(title: "Ok", style: .default, handler: { alert -> Void in
                            
                            self.navigationController?.popViewController(animated: true)
                            
                            
                            
                        })
                        // alertController.addAction(cancelAction)
                        alert.addAction(saveAction)
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                })
                
                
                
                return
            }
            
            
            
            
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddMemVerificationVC") as! AddMemVerificationVC
            vc.parameter1 = parameter
            vc.seletedValueType = seletedValueType
            vc.membersNames = firstNameTF.text
            vc.relations = relation.text
            vc.mobileNumber = mobileNumTF.text
            vc.countryCode = mobileExtension.text
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
            
            
            
            
        }
        
    }
    
    func isAccountHasValidDetails()  -> Bool {
        
        if firstNameTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Please enter First Name")
            return false
        }else if (firstNameTF.text?.count)! < 3 || (firstNameTF.text?.count)! > 30{
            showAlert(title: "", msg: "First Name should contain minimum 3 & maximum 30 characters")
            return false
        }
        
        if lastNameTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Please enter Last Name")
            return false
        }
        else if (lastNameTF.text?.count)! < 2 || (lastNameTF.text?.count)! > 30 {
            showAlert(title: "", msg: "Last Name should contain minimum 2 & maximum 30 characters")
            return false
        }
        if mobileNumTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Please enter valid Mobile Number")
            return false
            
        }
        else if (mobileNumTF.text?.count)! < 13 || (mobileNumTF.text?.count)! > 17
        {
            showAlert(title: "", msg: "Please enter valid Mobile Number")
            return false
        }
        if emailTF.text?.isEmpty == true{
            showAlert(title: "", msg: "Please Enter Valid Email Address")
            return false
        }else if(emailTF!.text!.isValidEmail() == false){
            showAlert(title: "", msg: "Please Enter Valid Email Address")
            return false
        }
        
        if relation.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Please enter Relationship to you")
            return false
        }else if (relation.text?.count)! < 3 || (relation.text?.count)! > 30 {
            showAlert(title: "", msg: "Relationship  should contain minimum 3 & maximum 30 characters")
            return false
        }
        if myRelationshipTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Please enter my relationship to him/her")
            return false
        }else if (relation.text?.count)! < 3 || (relation.text?.count)! > 30 {
            showAlert(title: "", msg: "Relationship  should contain minimum 3 & maximum 30 characters")
            return false
        }
        
        
        
        
        
        if seletedValueType  == nil
        {
            
            if isEditing
            {
                return true
            }
            showAlert(title: "", msg: "Select a category")
            
        }
        
        if seletedValueType == "kid.type" || seletedValueType == "thing.type"
        {
            if pinTextFiled.text?.isEmpty == true
            {
                
                if seletedValueType == "kid.type"
                {
                    showAlert(title: "", msg: "Please enter passcode for Kid")
                    return false
                }
                if seletedValueType == "thing.type"
                {
                    showAlert(title: "", msg: "Please enter passcode for Device")
                    return false
                }
                
                
                
            }else if (pinTextFiled.text?.count)! < 4  {
                showAlert(title: "", msg: "Passcode  should contain minimum 4 digits")
                return false
            }
        }
        
        
        
        //                if kidButton.isSelected == false && familyButton.isSelected == false && thingButton.isEnabled == false{
        //                    showAlert(title: Constants.alertTitle , msg: "Please select One")
        //                    return false
        //                }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        
        
        
        if textField == firstNameTF
        {
            if firstNameTF.text?.count == 0 || firstNameTF.text == nil
            {
                myRelationLabel.text = "My relationship to him/her*"
            }
        }
        
        if textField == mobileExtension
        {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "CountryPickViewController") as! CountryPickViewController
            vc.delegate = self
            self.present(vc, animated:true, completion:nil)
        }
        
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == firstNameTF
        {
            if firstNameTF.text?.count == 0 || firstNameTF.text == nil
            {
                myRelationLabel.text = "My relationship to him/her"
            }
        }
        
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        
        
        return true;
    }
    
    @objc func textFieldDidChange(_textFiled:UITextField)
    {
        if _textFiled.text != ""
        {
            myRelationLabel.text = "My relationship to \((_textFiled.text!))"
        }
        else
        {
            myRelationLabel.text = "My relationship to him/her*"
        }
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == firstNameTF
        {
            if firstNameTF.text != nil
            {
                myRelationLabel.text = "My relationship to \((firstNameTF.text!))"
            }
        }
        
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == firstNameTF
        {
            
            print(firstNameTF.text)
            if firstNameTF.text?.count == 0 || firstNameTF.text == nil
            {
                myRelationLabel.text = "My relationship to him/her*"
            }else
            {
                myRelationLabel.text = "My relationship to \((firstNameTF.text!))"
            }
            
            if range.location < 30{
                let allowedCharacters = CharacterSet.letters
                let characterSet = CharacterSet(charactersIn: string)
                return allowedCharacters.isSuperset(of: characterSet)
            }
            else
            {
                return range.location < 30
                
            }
            
        }
        
        if  textField == lastNameTF || textField == relation
        {
            
            
            
            
            if range.location < 30{
                let allowedCharacters = CharacterSet.letters
                let characterSet = CharacterSet(charactersIn: string)
                return allowedCharacters.isSuperset(of: characterSet)
            }
            else
            {
                return range.location < 30
                
            }
        }
        else if (textField == mobileNumTF)
        {
            let newPosition = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
            
            if range.length > 0 {
                return true
            }
            if range.location >= 17 {
                return false
            }
            if string == "" {
                return false
            }
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: " ", with: "")
            
            // Verify entered text is a numeric value
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
            
            //Put / after 2 digit
            if range.location == 0 {
                originalText?.append("(")
                textField.text = originalText
            }
            if range.location == 4 {
                originalText?.append(")")
                textField.text = originalText
            }
            if range.location == 8 {
                originalText?.append("-")
                textField.text = originalText
            }
            return true
        }
        
        
        if  (textField == pinTextFiled)
        {
            
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder();
        return true;
    }
    
}


extension UITextField {
    
    func useUnderline() {
        let border = CALayer()
        let borderWidth = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(origin: CGPoint(x: 0,y :self.frame.size.height - borderWidth), size: CGSize(width: self.frame.size.width, height: self.frame.size.height))
        border.borderWidth = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}

