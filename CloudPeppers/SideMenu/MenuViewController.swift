//
//  MenuViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 14/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//
//
//  MenuViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 14/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import CoreData
import Firebase
class MenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate ,CropViewControllerDelegate{
    private var croppingStyle = CropViewCroppingStyle.default
    private var croppedRect = CGRect.zero
    private var croppedAngle = 0
    @IBOutlet var gradientImageView: UIImageView!
    
    
    
    @IBOutlet weak var coolpadLinkImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var poweredbylabel: UILabel!
    
    @IBOutlet weak var dataDisView: UIView!
    
    @IBOutlet weak var displayProfileImg: UIImageView!
    
    var displayProfileImgupdated:UIImage!
    let text = "Powered by coolpad"
    @IBOutlet weak var mailIDLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    var menuNameArray:Array = [String]()
    var iconsArray:Array = [UIImage]()
    var SettingsArray:Array = [String]()
    var imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        
//        poweredbylabel.text = text
//        self.poweredbylabel.textColor =  UIColor.black
//        let underlineAttriString = NSMutableAttributedString(string: text)
//        let range1 = (text as NSString).range(of: "coolpad")
//        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
//        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Montserrat-Bold", size: 17)!, range: range1)
//        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value:Constants.titleColor, range: range1)
//        poweredbylabel.attributedText = underlineAttriString
//        poweredbylabel.isUserInteractionEnabled = true
//        poweredbylabel.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(tapLabel(gesture:))))
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedMe))
        coolpadLinkImage.addGestureRecognizer(tap)
        coolpadLinkImage.isUserInteractionEnabled = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.deviceChanged), name: NSNotification.Name(rawValue: "deviceChanged"), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.deactivateAccount), name: NSNotification.Name(rawValue: "deactivateAccount"), object: nil)
        
        tableView.tableFooterView = UIView()
        
        menuNameArray = ["Home","My Profile","Add Member","Permission Requests","Payments","Settings","Help","Logout"]
        iconsArray = [#imageLiteral(resourceName: "Home"),#imageLiteral(resourceName: "myprofile"),#imageLiteral(resourceName: "addmember"),#imageLiteral(resourceName: "permissionreq"),#imageLiteral(resourceName: "payment"),#imageLiteral(resourceName: "Settings"),#imageLiteral(resourceName: "Help"),#imageLiteral(resourceName: "LogOut")]
        displayProfileImg.layer.cornerRadius = displayProfileImg.frame.size.height/2
        displayProfileImg.clipsToBounds = true
        displayProfileImg.layer.borderColor = UIColor(hexString: "#3B3D3D").cgColor
        displayProfileImg.layer.borderWidth = 1
        
        
        
        
        
        if let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) {
            
            if profileData.profileImage != nil && profileData.profileImage != ""
            {
                displayProfileImg.sd_setImage(with: URL(string: (profileData.profileImage)!), placeholderImage: UIImage(named: "placeHolderProfile"))
                
                
                gradientImageView.sd_setImage(with: URL(string: (profileData.profileImage)!), placeholderImage: UIImage(named: "menuGradient"))
                gradientImageView.alpha = 0.6
                
//                if profileData.profileImage == "https://s3-us-west-2.amazonaws.com/2020wallet/78204598.jpg"
//                {
//               
//                    
//                }
//                else
//                {
//                    gradientImageView.sd_setImage(with: URL(string: (profileData.profileImage)!), placeholderImage: UIImage(named: ""))
//                    gradientImageView.alpha = 0.6
//                    
//                }
//                
                
            }else
            {
                
            }
            
            
        }
        //   tableView.setContentOffset(tableView.contentOffset, animated: false)
        
    }
    @objc func deviceChanged()
    {
        
        
        
        if let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) {
            
            let alertController = UIAlertController(title: "", message: "Looks like you have logged in another device so please tap on Ok to logout in this device.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.logoutMethod()
            }
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:nil)
            
        }
        
        
    }
    
    @objc func deactivateAccount()
    {
        
        
        
        if let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) {
            
            let alertController = UIAlertController(title: "", message: "Your account has been deactivated by admin.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.logoutMethod()
            }
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:nil)
            
        }
        
        
    }
    
    
    @IBAction func profileImgTapped(_ sender: Any) {
        
        Analytics.logEvent("Nav_Edit_Pic_Clicked", parameters:nil)
        
        let alert:UIAlertController=UIAlertController(title: "Choose Option", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        imagePicker.delegate = self
        // imagePicker.allowsEditing = true
        
        //Display Action Sheet Alert For iPad
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX - 45, y: self.view.bounds.midY - 15 + 500, width: 100, height: 0)
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            self .present(imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertView()
            alert.title = "Warning"
            alert.message = "You don't have camera"
            alert.addButton(withTitle: "OK")
            alert.show()
        }
    }
    
    
    func openGallary(){
        
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            
            
            let cropController = CropViewController(croppingStyle: croppingStyle, image: image)
            cropController.delegate = self
            if croppingStyle == .circular {
                picker.pushViewController(cropController, animated: true)
            }
            else { //otherwise dismiss, and then present from the main controller
                picker.dismiss(animated: true, completion: {
                    self.present(cropController, animated: true, completion: nil)
                })
            }
            
            
            
            
            
            
        }
        // pickedImage = true
        dismiss(animated: true, completion: nil)
    }
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        
        displayProfileImgupdated = image
        callUpdatedApi()
        if cropViewController.croppingStyle != .circular {
            cropViewController.dismiss(animated: true, completion: nil)
        }
        else {
            
            cropViewController.dismiss(animated: true, completion: nil)
        }
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        print("picker cancel.")
        dismiss(animated: true, completion: nil)
    }
    
    func callUpdatedApi()
    {
        
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            return
        }
        
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(profileData.userId, forKey: "userId")
        
        
        let imageData = displayProfileImgupdated!.pngData()
        let datastring =   imageData!.base64EncodedString()
        
        parameter.setValue(datastring, forKey: "profileImage")
        
        
        
        
        parameter.setValue(profileData.firstName, forKey: "firstName")
        parameter.setValue(profileData.lastName, forKey: "lastName")
        parameter.setValue(profileData.mobileNo, forKey: "mobileNo")
        parameter.setValue(profileData.email, forKey: "email")
        
        parameter.setValue("iOS", forKey: "platform")
        
        parameter.setValue(UserDefaults.standard.string(forKey: "fcmToken"), forKey: "fcmId")
        
        
        //   print(parameter)
        APIServices.postDataToServer(url: API.updateParentProfile, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                print(response)
                
                self.displayProfileImg.image = self.displayProfileImgupdated
                
                
                self.gradientImageView.image =  self.displayProfileImg.image
                self.gradientImageView.alpha = 0.6
                
                self.showAlert(title: "", msg: "Profile updated successfully.")
                
                
                var derivedData = response as! [String : Any]
                
                print(derivedData)
                if let theJSONData = try? JSONSerialization.data(withJSONObject: derivedData["response"] as Any , options:[])
                {
                    do{
                        
                        let loginRespone = try JSONDecoder().decode(Array<profile>.self, from: theJSONData)
                        
                        if let data = try? JSONEncoder().encode(loginRespone[0]) {
                            UserDefaults.standard.set(data, forKey: "profile_details")
                            
                        }
                        
                        
                        
                        //                        if let profileInfo = try? JSONDecoder().decode(profile.self, from: theJSONData) {
                        //                            print(profileInfo)
                        //
                        //                            if profileInfo.profileImage != nil &&  profileInfo.profileImage?.count != 0
                        //                            {
                        //                                let url = URL(string: profileInfo.profileImage!)
                        //                                //   profileInfo.profilePic = try? Data(contentsOf: url!)
                        //
                        //
                        //                            }
                        //
                        //                            if let data = try? JSONEncoder().encode(profileInfo) {
                        //                                UserDefaults.standard.set(data, forKey: "profile_details")
                        //                            }
                        //
                        //                        }
                    }catch let jsonErr {
                        
                        
                        print("Error serializing json:", jsonErr)
                        
                    }
                    
                }
                
                
                
                
                
            }
            
        })
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) {
            
            nameLabel.text =  profileData.firstName
            mailIDLabel.text = profileData.email
            
            if profileData.profileImage != nil &&  profileData.profileImage?.count != 0
            {
                
                displayProfileImg.sd_setImage(with: URL(string: (profileData.profileImage)!), placeholderImage: UIImage(named: "placeHolderProfile"))
                gradientImageView.sd_setImage(with: URL(string: (profileData.profileImage)!), placeholderImage: UIImage(named: "menuGradient"))
             gradientImageView.alpha = 0.6
                
//
//                if profileData.profileImage == "https://s3-us-west-2.amazonaws.com/2020wallet/78204598.jpg"
//                {
//
//
//                }
//                else
//                {
//                    gradientImageView.sd_setImage(with: URL(string: (profileData.profileImage)!), placeholderImage: UIImage(named: ""))
//                    gradientImageView.alpha = 0.6
//
//                }
//
                //                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { // Change `2.0` to the desired number of seconds.
                //
                //                    let url = URL(string: profileData.profileImage!)
                //                    let data = try? Data(contentsOf: url!)
                //
                //                    if let imageData = data {
                //                        let image = UIImage(data: imageData)
                //                        self.displayProfileImg.image = image
                //                    }
                //                }
                
                //  let image = UIImage(data: profileData.profilePic!)
                //   self.displayProfileImg.image = image
                
            }        }
    }
    
    @IBAction func editButtonTapped(_ sender: UIButton) {
        
        Analytics.logEvent("Nav_Edit_Profile_Clicked", parameters:nil)
        
        let revealViewController:SWRevealViewController = self.revealViewController()
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desController = mainStoryboard.instantiateViewController(withIdentifier: "EditProfViewController") as! EditProfViewController
        let newFrontViewController = UINavigationController.init(rootViewController:desController)
        revealViewController.pushFrontViewController(newFrontViewController, animated: true)
        
        
        
        print("Edit")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNameArray.count
    }
    
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuViewController") as! MenuTableViewCell
        cell.menuListImg.image = iconsArray[indexPath.row]
        cell.menuListLabel.text = menuNameArray[indexPath.row]
        
        
        if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type"
        {
            if cell.menuListLabel.text == "Add Member"
            {
                cell.backgroundColor = UIColor.lightGray
                cell.selectionStyle = .none
            }
            
            
        }
        cell.layer.borderColor = #colorLiteral(red: 0.4156862745, green: 0.3921568627, blue: 0.6274509804, alpha: 1)
        cell.layer.borderWidth = 0.0
        
        //        let checkfamilymember = UserDefaults.standard.string(forKey: "checkfamilymember")
        //
        //        if indexPath.row == 2
        //        {
        //            if checkfamilymember == "0"
        //            {
        //
        //                cell.backgroundColor = UIColor.lightGray
        //
        //            }
        //        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 4
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let revealViewController:SWRevealViewController = self.revealViewController()
        
        
        let cell:MenuTableViewCell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        
        if indexPath.row == 0
        {
            Analytics.logEvent("Nav_Home_Clicked", parameters:nil)
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            desController.fromsidemeue = "true"
            let newFrontViewController = UINavigationController.init(rootViewController:desController)
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
            
            
        }
        if indexPath.row == 1
        {
            Analytics.logEvent("Nav_My_Profile_Clicked", parameters:nil)
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "YourAccViewController") as! YourAccViewController
            let newFrontViewController = UINavigationController.init(rootViewController:desController)
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
            
        }
        
        
        if indexPath.row == 2
        {
            Analytics.logEvent("Nav_Add_Mem_Clicked", parameters:nil)
            if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type"
            {
                
                return
            }
            
            
            if let data = UserDefaults.standard.value(forKey: "Paymentdata") as? Data,
                let profileData = try? JSONDecoder().decode(UserPlans.self, from: data)
            {
                
                
                
                if profileData.remainDays ?? 0 <= 0
                {
                    self.showCustomAlertWith(message: "", descMsg:"your plane expired feature", itemimage: #imageLiteral(resourceName: "AboutUs"), actions: nil)
                }else
                {
                    if   (profileData.featureList?.contains(where: { name in name.featureKey == "key.addmember"}) ?? false)
                        
                    {
                        
                        
                        
                        print("match")
                    }else
                    {
                        print("not match")
                        
                        self.showCustomAlertWith(message: "", descMsg:"your not buy this feature", itemimage: #imageLiteral(resourceName: "AboutUs"), actions: nil)
                        
                        
                        return
                    }
                }
                
                
                
                //  return
                
            }else
            {
                
                
                
                
//
//                 let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
//
//                 let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
//                 let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
//
//                 let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
//                 let msgAttrString = NSMutableAttributedString(string: "Are you sure you want to continue payment?", attributes: msgFont)
//
//                 alert.setValue(titAttrString, forKey: "attributedTitle")
//                 alert.setValue(msgAttrString, forKey: "attributedMessage")
//
//
//                 let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
//                 let settingsAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
//
//                    self.paymentAdd()
//
//                 }
//
//                 alert.addAction(cancelAction)
//                 alert.addAction(settingsAction)
//                 self.present(alert, animated: true, completion: nil)
                
                
                let actionDic : [String: () -> Void] = [ "YES" : { (
                    
                    Analytics.logEvent("Logout_Yesbutton_clicked", parameters:nil),
                    
                    _ = self.logoutMethod1()
                    
                    ) }, "NO" : { (
                        Analytics.logEvent("Logout_Nobutton_clicked", parameters:nil)
                        
                        
                        ) }]
                
                self.showCustomAlertWith(
                    message: "",
                    descMsg: "Please select a payment plan to access this feature",
                    itemimage: #imageLiteral(resourceName: "LogOut"),
                    actions: actionDic)
                
               
                return
            }
            
            
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "AddFamilyMemberVC") as! AddFamilyMemberVC
            desController.fromsidemenu = true
            let newFrontViewController = UINavigationController.init(rootViewController:desController)
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
            
        }
        
        
        
        if indexPath.row == 3
        {
            Analytics.logEvent("Nav_Perm_Req_Clicked", parameters:nil)
            //                let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            //                let desController = mainStoryboard.instantiateViewController(withIdentifier: "PermissionRequestsVC") as! PermissionRequestsVC
            //                let newFrontViewController = UINavigationController.init(rootViewController:desController)
            //                revealViewController.pushFrontViewController(newFrontViewController, animated: true)
            
            
            
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "PermissionsRequestVc") as! PermissionsRequestVc
            let newFrontViewController = UINavigationController.init(rootViewController:desController)
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
            //PermissionsRequestVc
            
            
            //     addMemberButton.isHidden = checkfamilymember == "0" ? true : false
            
        }
        if indexPath.row == 4
        {
            Analytics.logEvent("Nav_Payments_Clicked", parameters:nil)
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "PaymentsViewController") as! PaymentsViewController
            let newFrontViewController = UINavigationController.init(rootViewController:desController)
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
            
        }
        
        
        if indexPath.row == 5
        {
            Analytics.logEvent("Nav_settings_Clicked", parameters:nil)
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            let newFrontViewController = UINavigationController.init(rootViewController:desController)
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
            
        }
        if indexPath.row == 6
        {
            guard let url = URL(string: API.cpadminportal) else { return }
            UIApplication.shared.open(url)
            
            Analytics.logEvent("Nav_Help_Clicked", parameters:nil)
//            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let desController = mainStoryboard.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
//            let newFrontViewController = UINavigationController.init(rootViewController:desController)
//            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
            
        }
        
        
        
        
        
        
        //        if indexPath.row == 4
        //        {
        //            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //            let desController = mainStoryboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        //            let newFrontViewController = UINavigationController.init(rootViewController:desController)
        //            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
        //
        //        }
        //        if indexPath.row == 5
        //        {
        //            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //            let desController = mainStoryboard.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
        //            let newFrontViewController = UINavigationController.init(rootViewController:desController)
        //            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
        //
        //        }
        
        //        if indexPath.row == 6
        //        {
        //
        //            let text = "I tried Coolpad on iOS and it is awesome thought you would love it too download it"
        //            let shareVC = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        //
        //            if UIDevice.current.userInterfaceIdiom == .pad {
        //                shareVC.popoverPresentationController?.sourceView = self.view
        //
        //                let frame = UIScreen.main.bounds
        //                shareVC.popoverPresentationController?.sourceRect = CGRect(x: frame.size.width/2,y: frame.size.height/2,
        //                                                                           width: 0, height: 0)
        //
        //                shareVC.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
        //            }
        //
        //            self.present(shareVC, animated: true, completion: nil)
        //            return
        //        }
        //
        //        if indexPath.row == 7
        //        {
        //            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //            let desController = mainStoryboard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        //            let newFrontViewController = UINavigationController.init(rootViewController:desController)
        //            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
        //
        //        }
        
        if indexPath.row == 7
        {
            Analytics.logEvent("Nav_Logout_Clicked", parameters:nil)
            
            
            if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type" || UserDefaults.standard.string(forKey: "roleKey") == "thing.type"
            {
                
                let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
                alertController.addTextField { (textField : UITextField!) -> Void in
                    
                    textField.keyboardType = .numberPad
                    textField.placeholder = "Enter Passcode"
                    
                    
                    textField.delegate = self
                    
                    //                    var rightButton  = UIButton(type: .custom)
                    //                    rightButton.frame = CGRect(x:0, y:0, width:20, height:20)
                    //                    rightButton.setImage(#imageLiteral(resourceName: "DownArrow"), for: .normal)
                    //                    rightButton.addTarget(self, action: #selector(<#T##@objc method#>), for: .touchUpInside)
                    //
                    //                    textField.rightViewMode = .always
                    //                    textField.rightView = rightButton
                }
                let saveAction = UIAlertAction(title: "Ok", style: .default, handler: { alert -> Void in
                    
                    let firstTextField = alertController.textFields![0] as UITextField
                    
                    firstTextField.keyboardType = .numberPad
                    
                    if firstTextField.text == nil || (firstTextField.text?.isEmpty)!
                    {
                        
                        self.showAlert(title: "", msg: "Please enter Passcode")
                        return
                    }
                    
                    if firstTextField.text!.count < 4
                    {
                        self.showAlert(title: "", msg: "Please enter 4 digit Passcode")
                        return
                    }
                    
                    
                    guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                        let profileData = try? JSONDecoder().decode(profile.self, from: data) else
                    {
                        
                        return
                        
                    }
                    //  validateAccessPin?userId="+sm.getIntegerData(AppStrings.Session.USER_ID)+"&pin="+pinForKid
                    //sendingSMS
                    
                    let validateAccessPin = API.validateAccessPin + "?userId=\(String(describing: profileData.userId!))" + "&pin=\(String(describing: firstTextField.text!))"
                    
                    HttpWrapper.gets(with: validateAccessPin, parameters: nil, headers: nil, completionHandler: { (response, error)  in
                        print(response)
                        
                        guard let data = response else { return }
                        do {
                            
                            let loginRespone = try JSONDecoder().decode(ApiStatus1.self, from: data)
                            let loginStatus = loginRespone.status
                            if  loginStatus  ==  true
                            {
                                UserDefaults.standard.removeObject(forKey: "profile_details")
                                let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let desController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                                let newFrontViewController = UINavigationController.init(rootViewController:desController)
                                revealViewController.pushFrontViewController(newFrontViewController, animated: true)
                            }
                            else
                                
                            {
                                
                                self.showAlert(title: "", msg: loginRespone.message!)
                                
                                
                            }
                            
                        }catch let jsonErr {
                            
                            print("Error serializing json:", jsonErr)
                            
                        }
                    }){ (error) in
                        
                        print(error)
                        if Reachability.isConnectedToNetwork(){
                            print("Internet Connection Available!")
                        }else{
                            self.showAlert(title: "", msg: "No network connection")
                        }
                    }
                    
                    
                    
                })
                
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action : UIAlertAction!) -> Void in })
                
                alertController.addAction(cancelAction)
                alertController.addAction(saveAction)
                
                
                self.present(alertController, animated: true, completion: nil)
                
                
                
                
            }else
            {
                //  let alertController = UIAlertController(title: Constants.alertTitle, message: "Are you sure want to logout?", preferredStyle: .alert)
                
                /*
                 let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                 
                 let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
                 let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
                 
                 let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
                 let msgAttrString = NSMutableAttributedString(string: "Are you sure you want to logout?", attributes: msgFont)
                 
                 alert.setValue(titAttrString, forKey: "attributedTitle")
                 alert.setValue(msgAttrString, forKey: "attributedMessage")
                 
                 
                 let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
                 let settingsAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                 
                 self.deleteAllRecords()
                 
                 UserDefaults.standard.removeObject(forKey: "profile_details")
                 let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                 let desController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                 let newFrontViewController = UINavigationController.init(rootViewController:desController)
                 revealViewController.pushFrontViewController(newFrontViewController, animated: true)
                 
                 }
                 
                 alert.addAction(cancelAction)
                 alert.addAction(settingsAction)
                 self.present(alert, animated: true, completion: nil)
                 
                 */
                
                let actionDic : [String: () -> Void] = [ "YES" : { (
                    
                    Analytics.logEvent("Logout_Yesbutton_clicked", parameters:nil),
                    
                    _ = self.logoutMethod()
                    
                    ) }, "NO" : { (
                        Analytics.logEvent("Logout_Nobutton_clicked", parameters:nil)
                        
                        
                        ) }]
                
                self.showCustomAlertWith(
                    message: "",
                    descMsg: "Are you sure you want to Logout?",
                    itemimage: #imageLiteral(resourceName: "LogOut"),
                    actions: actionDic)
                
                // self.showCustomAlertWith(message: "", descMsg: "This member is using iOS device, you can not access this feature.", itemimage: #imageLiteral(resourceName: "LogOut"), actions: nil)
            }
            
            
            
            
        }
        
    }
    
    func logoutMethod1() {
       
        self.paymentAdd()
    }
    
    // MARK: - Custom Methods
    func logoutMethod() {
        self.deleteAllRecords()
        let revealViewController:SWRevealViewController = self.revealViewController()
        UserDefaults.standard.removeObject(forKey: "profile_details")
          UserDefaults.standard.removeObject(forKey: "Paymentdata")
        
        
        
         UserDefaults.standard.removeObject(forKey: "jwtToken")
        
           UserDefaults.standard.set(nil, forKey: "deviceChangedtrue")
         UserDefaults.standard.set(nil, forKey: "deactivateAccount")
        
          UserDefaults.standard.set(nil, forKey: "jwtToken")
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let newFrontViewController = UINavigationController.init(rootViewController:desController)
        revealViewController.pushFrontViewController(newFrontViewController, animated: true)
    }
    
    func logoutMethodForDeact() {
        self.deleteAllRecords()
        let revealViewController:SWRevealViewController = self.revealViewController()
        UserDefaults.standard.removeObject(forKey: "profile_details")
        UserDefaults.standard.removeObject(forKey: "Paymentdata")
        
        
        
        UserDefaults.standard.removeObject(forKey: "jwtToken")
        
        UserDefaults.standard.set(nil, forKey: "deviceChangedtrue")
        UserDefaults.standard.set(nil, forKey: "deactivateAccount")
        
        UserDefaults.standard.set(nil, forKey: "jwtToken")
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let newFrontViewController = UINavigationController.init(rootViewController:desController)
        revealViewController.pushFrontViewController(newFrontViewController, animated: true)
    }
    
    func paymentAdd() {
          let revealViewController:SWRevealViewController = self.revealViewController()
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desController = mainStoryboard.instantiateViewController(withIdentifier: "PaymentsViewController") as! PaymentsViewController
        let newFrontViewController = UINavigationController.init(rootViewController:desController)
        revealViewController.pushFrontViewController(newFrontViewController, animated: true)
    }
    func deleteAllRecords() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "PushNotifications")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    @objc func tappedMe()
    {
        // let termsRange = (text as NSString).range(of: "coolpad")
        // comment for now
        //let privacyRange = (text as NSString).range(of: "Privacy Policy")
        
        //        if gesture.didTapAttributedTextInLabel(label: powerBy, inRange: termsRange) {
        //            print("Tapped terms")
        //
        
        
        //            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        //            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        //
        //            let urlString = organizationdetails?.organization?.orginationURL
        //
        //            guard let url = URL(string: urlString!) else { return }
        //
        //            let svc = SFSafariViewController(url: url)
        //            present(svc, animated: true, completion: nil)
        //UIApplication.shared.open(url)
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "TermsCondViewController") as! TermsCondViewController
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        vc.urlString = organizationdetails?.organization?.orginationURL
        self.present(vc, animated: true, completion: nil)
        //
        // }
        //        } else if gesture.didTapAttributedTextInLabel(label: poweredByLabel, inRange: privacyRange) {
        //            print("Tapped privacy")
        //        else {
        //            print("Tapped none")
        //        }
        
    }
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        let termsRange = (text as NSString).range(of: "coolpad")
        // comment for now
        //let privacyRange = (text as NSString).range(of: "Privacy Policy")
        
        if gesture.didTapAttributedTextInLabel(label: poweredbylabel, inRange: termsRange) {
            print("Tapped terms")
            //                        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
            //                        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
            //
            //            let urlString = organizationdetails?.organization?.orginationURL
            
            //            guard let url = URL(string: urlString!) else { return }
            //            UIApplication.shared.open(url)
            ////
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "TermsCondViewController") as! TermsCondViewController
            
            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
            
            vc.urlString = organizationdetails?.organization?.orginationURL
            self.present(vc, animated: true, completion: nil)
            
        }
            //        } else if gesture.didTapAttributedTextInLabel(label: poweredByLabel, inRange: privacyRange) {
            //            print("Tapped privacy")
        else {
            print("Tapped none")
        }
    }
    
    
}

