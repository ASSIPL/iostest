
//
//  VerificationViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 24/04/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase

class AddMemVerificationVC: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var codeExpireTimeLabel: UILabel!
    @IBOutlet weak var verificationCodeSentLabel: UILabel!
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var firstTF: UITextField!
    
    @IBOutlet weak var thirdTF: UITextField!
    
    @IBOutlet weak var fourthTF: UITextField!
    
    @IBOutlet weak var secondTF: UITextField!
    
    var count = 600
    
    
    
    var OTPtimer = Timer()
    
    var parameter1:NSMutableDictionary = NSMutableDictionary()
    var seletedValueType:String?
    var membersNames:String?
    var relations:String?
    var mobileNumber:String?
    var countryCode:String?
    
    
    var profiledata:profile?
    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  count = 600
        
        OTPtimer =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        
        
       
        
        resendOTP()
        
      
        
        
        Analytics.logEvent("VerificationEntered", parameters:nil)
        firstTF.delegate = self
        secondTF.delegate = self
        thirdTF.delegate = self
        fourthTF.delegate = self
        verifyButton.changeButtonCornerRadius()
        
        if count == 0{
            codeExpireTimeLabel.text = "Code expired"
        }
        
        verificationCodeSentLabel.text = "Please enter the verification code sent to \(countryCode!)\(mobileNumber!)"
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("VerificationExit", parameters:nil)
        OTPtimer.invalidate()
        
    }
    
    @objc func updateCounter() {
        //example functionality
         if(count > 0){
            
         //   let minutes = Int(count) / 60 % 60
         //   let seconds = Int(count) % 60
     
            codeExpireTimeLabel.isHidden = false
    
    codeExpireTimeLabel.text = "Code expires in \(timeString(time: TimeInterval(count))) min"
    
           // minutes + ":" + seconds
            count -= 1
        }
        else
        {
          //  codeExpireTimeLabel.isHidden = true
            
            codeExpireTimeLabel.text = "OTP Expired"
          //  showAlert(title:"" , msg: "OTP Expired")
        //    OTPtimer.invalidate()
            
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        
        // let hours = Int(time) / 3600
        
        let minutes = Int(time) / 60 % 60
        
        let seconds = Int(time) % 60
        
        return String(format:"%02i:%02i", minutes, seconds)
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //         var otpString = "\(String(describing: firstTF.text!))" +  "\(String(describing: secondTF.text!))" + "\(String(describing: thirdTF.text!))" + "\(String(describing: fourthTF.text!))"
        //
        //   string = "6"
        if (range.length == 0)
            
            // (((textField.text?.count)!) < 1 ) && (string.count > 0)
        {
            if textField == firstTF {
                secondTF.becomeFirstResponder()
            }
            if textField == secondTF
            {
                thirdTF.becomeFirstResponder()
            }
            if textField == thirdTF
            {
                fourthTF.becomeFirstResponder()
            }
            if textField == fourthTF
            {
                fourthTF.becomeFirstResponder()
            }
            textField.text = string
            return false
        }
            
        else if (range.length == 1)
            //(((textField.text?.count)!) >= 1 ) && (string.count == 0)
        {
            if textField == secondTF {
                firstTF.becomeFirstResponder()
            }
            if textField == thirdTF
            {
                secondTF.becomeFirstResponder()
            }
            if textField == fourthTF
            {
                thirdTF.becomeFirstResponder()
            }
            if textField == firstTF
            {
                firstTF.becomeFirstResponder()
            }
            textField.text = ""
            return false
        }
        else if ((textField.text?.count)!) >= 1
        {
            textField.text = string
            return false
        }
        return true
    }
    
    
    @IBAction func verifyButton(_ sender: Any) {
        
//        if count == 0
//        {
//            showAlert(title: "", msg: "OTP Expired")
//        }
        
        Analytics.logEvent("Verific_Code_Verify_Clicked", parameters:nil)
        
        if firstTF.text?.isEmpty == true || secondTF.text?.isEmpty  == true  || thirdTF.text?.isEmpty  == true  || fourthTF.text?.isEmpty  == true
        {
            showAlert(title: "", msg: "Please enter Verification code")
            
            return
        }
        
        let otpString = "\(String(describing: firstTF.text!))" +  "\(String(describing: secondTF.text!))" + "\(String(describing: thirdTF.text!))" + "\(String(describing: fourthTF.text!))"
        
        
        print(otpString)
        
//        guard let mobileNumber = (UserDefaults.standard.string(forKey: "mobileNumber")) else {
//
//            return
//        }
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(mobileNumber!.getStringFromPhoneNumber(), forKey: "mobileNo")
        parameter.setValue(otpString, forKey: "otp")
     //   parameter.setValue(mobileNumber, forKey: "mobileNo")
        
        
    print(parameter)
        
        APIServices.postDataToServer(url: API.verifyOTP, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                
                print(response)
        //        var derivedData = response as! [String : Any]
//
//                print(derivedData)
//                if let theJSONData = try? JSONSerialization.data(withJSONObject: derivedData["response"] as Any , options:[])
//                {
//                    do{
//                        if let profileInfo = try? JSONDecoder().decode(profile.self, from: theJSONData) {
//
//                            print(profileInfo)
//
//                            UserDefaults.standard.set(profileInfo.jwtToken, forKey: "jwtToken")
//
//                            if let data = try? JSONEncoder().encode(profileInfo) {
//                                UserDefaults.standard.set(data, forKey: "profile_details")
//
//                            }
//
//
//                            print( UserDefaults.standard.string(forKey: "jwtToken"))
//                        }
//                    }
//                }
                
        
                
                
                
//                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
//                self.present(nextViewController, animated:true, completion:nil)
                
                let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "MemberPermissionsVC") as! MemberPermissionsVC
                vc.parameter = self.parameter1
                vc.seletedValueType = self.seletedValueType
                vc.membersNames = self.membersNames
                vc.relations = self.relations
               
                
                self.navigationController?.pushViewController(vc, animated: true)
                
                
            }else
            {

                UserDefaults.standard.removeObject(forKey: "profile_details")

            }
        })
        
        
    }
    
    
    
    
    func resendOTP()
    {
        
      updateCounter()
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(mobileNumber!.getStringFromPhoneNumber(), forKey: "mobileNo")
        parameter.setValue(countryCode, forKey: "countryCode")
        
        print(parameter)
        APIServices.postDataToServer(url: API.reSendOTP, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
//                self.showAlert(title: "", msg: "OTP resent successfully")
                
            }
        })
    }
    
    @IBAction func resendotp(_ sender: Any) {
        
         count = 600
        
          OTPtimer =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
       
    
      //  updateCounter()
        
    
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(mobileNumber!.getStringFromPhoneNumber(), forKey: "mobileNo")
        parameter.setValue(countryCode, forKey: "countryCode")
        
        print(parameter)
        APIServices.postDataToServer(url: API.reSendOTP, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                self.showAlert(title: "", msg: "OTP Resent Successfully")
                
            }
        })
        
     }
        

    
}
