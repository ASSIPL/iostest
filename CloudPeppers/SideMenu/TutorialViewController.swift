//
//  TutorialViewController.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 21/06/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase

class TutorialViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var getStartedButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var scrollView = UIScrollView()
    
    var fromFirstTime:Bool?
    
    var yPosition:CGFloat = 0
    var scrollViewContentSize:CGFloat=0;
    
    var timer = Timer()
    
    let imagelist = ["t1.jpg","t2.jpg","t4.jpg","t3.jpg"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         Analytics.logEvent("TutorialEntered", parameters:nil)
        self.navigationController?.navigationBar.isHidden = true
        
        
        
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        
        scrollView.delegate = self
        self.automaticallyAdjustsScrollViewInsets = false
        
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.showsVerticalScrollIndicator = false
        
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
        self.view.addSubview(scrollView)
        for  i in stride(from: 0, to: imagelist.count, by: 1) {
            var frame = CGRect.zero
            frame.origin.x = self.scrollView.frame.size.width * CGFloat(i)
            frame.origin.y = 0
            frame.size = self.scrollView.frame.size
            self.scrollView.isPagingEnabled = true
            
            let myImage:UIImage = UIImage(named: imagelist[i])!
            let myImageView:UIImageView = UIImageView()
            myImageView.image = myImage
            myImageView.contentMode = UIView.ContentMode.scaleAspectFit
            myImageView.frame = frame
            
            
            scrollView.addSubview(myImageView)
            
            // Do any additional setup after loading the view, typically from a nib.
        }
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width * CGFloat(imagelist.count), height: self.scrollView.frame.size.height)
        // pageControl.addTarget(self, action: Selector(("changePage:")), for: UIControlEvents.valueChanged)
        // Do any additional setup after loading the view.
        configurePageControl()
        
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        Analytics.logEvent("TutorialExit", parameters:nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    
    @IBAction func SkipAction(_ sender: Any) {
        
        
        Analytics.logEvent("Tutorials_Skip_Clicked", parameters:nil)
        if fromFirstTime == true
        {
            
            let id = "LoginViewController"
            let vc = UIStoryboard.getViewController(storyboardName: "Main", storyboardId: id)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = vc
                appDelegate.window?.makeKeyAndVisible()
            }
        }else
        {
             self.navigationController?.popViewController(animated: true)
        }
        
     
        
        
    }
    
    

    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        self.pageControl.numberOfPages = imagelist.count
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.red
        self.pageControl.pageIndicatorTintColor = #colorLiteral(red: 0, green: 0.7152789235, blue: 0.7682691216, alpha: 1)
        self.pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.8588235294, green: 0.8941176471, blue: 0.9333333333, alpha: 1)
        // self.view.addSubview(pageControloutlet)
        
        self.view.bringSubviewToFront(pageControl)
        self.view.bringSubviewToFront(getStartedButton)
        //  self.view.bringSubview(toFront: detailLabel)
        //self.view.bringSubview(toFront: logoView)
        
    }
    
    @objc func updateCounting(){
        NSLog("counting..")
        if pageControl.currentPage == imagelist.count-1
        {
            getStartedButton.setTitle("Done", for: .normal)
            timer.invalidate()
        }else
        {
             getStartedButton.setTitle("Skip", for: .normal)
        }
        pageControl.currentPage =  pageControl.currentPage + 1
        changePage(sender:pageControl)
    }
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * scrollView.frame.size.width
        scrollView.setContentOffset(CGPoint(x: x,y :0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        
        
        pageControl.currentPage = Int(pageNumber)
        
        if pageControl.currentPage == imagelist.count-1
        {
            getStartedButton.setTitle("Done", for: .normal)
        }else
        {
            getStartedButton.setTitle("Skip", for: .normal)
        }
    }
    
    
    
}

