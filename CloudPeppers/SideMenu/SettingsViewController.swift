//
//  SettingsViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 22/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase

class SettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet var settingTableView: UITableView!
    
    
    var textArray:Array = [String]()
 //   var iconsArray:Array = [UIImage]()
    
    @IBOutlet weak var btnMenuButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Analytics.logEvent("Settings_Screen_Entered", parameters:nil)
        settingTableView.tableFooterView = UIView()
        
    //    textArray = ["Recommend Us","About Us","Deactivate Account","Tutorials"]
        
        
         textArray = ["Tutorials","Recommend Us","About Us","Deactivate Account"]
      //  iconsArray = [#imageLiteral(resourceName: "RecommandUs"),#imageLiteral(resourceName: "AboutUs"),#imageLiteral(resourceName: "LogOut"),#imageLiteral(resourceName: "Help")]
        navigationController?.navigationBar.barTintColor = Constants.colour
        self.navigationItem.title = "Settings"
        self.changeFontForViewController()
        // navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        btnMenuButton.target = revealViewController()
        btnMenuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         Analytics.logEvent("Settings_Screen_Exit", parameters:nil)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell", for: indexPath) as! SettingsTableViewCell
        
     //   cell.disImage.image = iconsArray[indexPath.row]
        cell.disTitle.text = textArray[indexPath.row]
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //    let cell:SettingsTableViewCell = tableView.cellForRow(at: indexPath) as! SettingsTableViewCell
        
        if indexPath.row == 0
        {
            Analytics.logEvent("Settings_Tutorails_Clcked", parameters:nil)
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TutorialViewController") as? TutorialViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            
            return
            
            
        }
        if indexPath.row == 1
        {
           
            //            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            //            let desController = mainStoryboard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
            //            let newFrontViewController = UINavigationController.init(rootViewController:desController)
            //            revealViewController().pushFrontViewController(newFrontViewController, animated: true)
            //
            
            
            Analytics.logEvent("RecommendUs_Clicked", parameters:nil)
            let text = "I tried coolpad on iOS and it is awesome thought you would love it too download it"
            let shareVC = UIActivityViewController(activityItems: [text], applicationActivities: nil)
            
            if UIDevice.current.userInterfaceIdiom == .pad {
                shareVC.popoverPresentationController?.sourceView = self.view
                
                let frame = UIScreen.main.bounds
                shareVC.popoverPresentationController?.sourceRect = CGRect(x: frame.size.width/2,y: frame.size.height/2,
                                                                           width: 0, height: 0)
                
                shareVC.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
            }
            
            self.present(shareVC, animated: true, completion: nil)
            return
            
        }
        
        if indexPath.row == 2
        {
            
            Analytics.logEvent("AboutsUs_Clicked", parameters:nil)
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AboutUsViewController") as? AboutUsViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            
            
            
            return
            
         
            
        }
        
        
        
        
        if indexPath.row == 3
        {
            
            
            
            Analytics.logEvent("DecativateAccount_Clicked", parameters:nil)
            
            // let alertController = UIAlertController(title: Constants.alertTitle, message: "Deactivating your account will permanently delete your account.Are you sure you want to continue?", preferredStyle: .alert)
            
            let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
            
            let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
            let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
            
            let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
            let msgAttrString = NSMutableAttributedString(string: "Deactivating your account will permanently delete your account.Are you sure you want to continue?", attributes: msgFont)
            
            alert.setValue(titAttrString, forKey: "attributedTitle")
            alert.setValue(msgAttrString, forKey: "attributedMessage")
            
            let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                Analytics.logEvent("Decativate_YesButton_Clicked", parameters:nil)
                
                if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type" || UserDefaults.standard.string(forKey: "roleKey") == "thing.type"
                {
                    self.callapi()
                    
                    return
                }
                
                self.decativeApi()
            }
            
            let cancelAction = UIKit.UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { (UIAlertAction) in
                Analytics.logEvent("Decativate_NoButton_Clicked", parameters:nil)
            }
            alert.addAction(cancelAction)
            alert.addAction(settingsAction)
            self.present(alert, animated: true, completion: nil)
            
            
            
            return
            
        }
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 4
    }
    
    func callapi()
    {
        if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type" || UserDefaults.standard.string(forKey: "roleKey") == "thing.type"
        {
            
            let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
            alertController.addTextField { (textField : UITextField!) -> Void in
                
                textField.keyboardType = .numberPad
                textField.placeholder = "Enter Passcode"
                textField.delegate = self
              //  textField.isSecureTextEntry = true
            }
            let saveAction = UIAlertAction(title: "Yes", style: .default, handler: { alert -> Void in
                
                let firstTextField = alertController.textFields![0] as UITextField
                
                firstTextField.keyboardType = .numberPad
                
                if firstTextField.text == nil || (firstTextField.text?.isEmpty)!
                {
                    
                    self.showAlert(title: "", msg: "Please enter Passcode")
                    return
                }
                
                if firstTextField.text!.count < 4
                {
                    self.showAlert(title: "", msg: "Please enter 4 digit Passcode")
                    return
                }
                
                
                guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                    let profileData = try? JSONDecoder().decode(profile.self, from: data) else
                {
                    
                    return
                    
                }
                
                
                //  validateAccessPin?userId="+sm.getIntegerData(AppStrings.Session.USER_ID)+"&pin="+pinForKid
                //sendingSMS
                
                
                
                
                
                
                let validateAccessPin = API.validateAccessPin + "?userId=\(String(describing: profileData.userId!))" + "&pin=\(String(describing: firstTextField.text!))"
                
                HttpWrapper.gets(with: validateAccessPin, parameters: nil, headers: nil, completionHandler: { (response, error)  in
                    print(response)
                    
                    guard let data = response else { return }
                    do {
                        
                        let loginRespone = try JSONDecoder().decode(ApiStatus1.self, from: data)
                        let loginStatus = loginRespone.status
                        if  loginStatus  ==  true
                        {
                            
                            self.decativeApi()
                        }
                        else
                            
                        {
                            
                            self.showAlert(title: "", msg: loginRespone.message!)
                            
                            
                        }
                        
                    }catch let jsonErr {
                        
                        print("Error serializing json:", jsonErr)
                        
                    }
                }){ (error) in
                    
                    print(error)
                    if Reachability.isConnectedToNetwork(){
                        print("Internet Connection Available!")
                    }else{
                        self.showAlert(title: "", msg: "No network connection")
                    }
                }
                
                
                
            })
            
            
            let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: { (action : UIAlertAction!) -> Void in })
            
            alertController.addAction(cancelAction)
            alertController.addAction(saveAction)
            
            
            self.present(alertController, animated: true, completion: nil)
            
            
        }
        
    }
    
    
    func decativeApi()
    {
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        let url = API.deActivate + "?userId=" + String(describing: (profileData.userId!))
        
        
        
        
        
        APIServices.getDataFromServer(url:url, parameters: nil, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                let revealViewController:SWRevealViewController = self.revealViewController()
                UserDefaults.standard.removeObject(forKey: "jwtToken")
                
                UserDefaults.standard.set(nil, forKey: "jwtToken")
                
                UserDefaults.standard.removeObject(forKey: "Paymentdata")
                UserDefaults.standard.removeObject(forKey: "profile_details")
                let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let desController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                let newFrontViewController = UINavigationController.init(rootViewController:desController)
                revealViewController.pushFrontViewController(newFrontViewController, animated: true)
                
                print(response)
                
            }
        })
        
        
    }
    
}


