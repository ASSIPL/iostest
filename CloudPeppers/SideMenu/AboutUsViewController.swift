//
//  AboutUsViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 22/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import MessageUI
import SafariServices
import SVProgressHUD
import SDWebImage
import SystemConfiguration
import Firebase

class AboutUsViewController: UIViewController {
    
    
    @IBOutlet weak var btnMenuButton: UIBarButtonItem!
    @IBOutlet var companyLogo: UIImageView!
    @IBOutlet var versionName: UILabel!
    @IBOutlet var termsofservice: UIButton!
    @IBOutlet var contactsUs: UIButton!
    var aboutUsResponse:Aboutresponse?
    
    @IBOutlet weak var aboutUSTextView: UITextView!
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        let underlineAttriString = NSMutableAttributedString(string: "Terms of Service")
        let range1 = ("Terms of Service" as NSString).range(of: "Terms of Service")
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Montserrat-Regular", size: 15)!, range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value:UIColor.blue, range: range1)
        // poweredByLabel.attributedText = underlineAttriString
        termsofservice.setAttributedTitle(underlineAttriString, for: .normal)
        
        
        
        let underlineAttriString1 = NSMutableAttributedString(string: "Contact Us")
        let range2 = ("Contact Us" as NSString).range(of: "Contact Us")
        underlineAttriString1.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range2)
        underlineAttriString1.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Montserrat-Regular", size: 15)!, range: range2)
        underlineAttriString1.addAttribute(NSAttributedString.Key.foregroundColor, value:UIColor.blue, range: range2)
        // poweredByLabel.attributedText = underlineAttriString
        contactsUs.setAttributedTitle(underlineAttriString1, for: .normal)
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        
        SVProgressHUD.show()
        HttpWrapper.gets(with: API.aboutUs, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            //   print(response)
            
            SVProgressHUD.dismiss()
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(AboutUsStatus.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    
                    self.aboutUsResponse = loginRespone.response
                    self.versionName.text  = "Version: 1.0.26 dated Nov 29,2019"
                    
                    
                    
                    self.aboutUSTextView.text = loginRespone.response?.content?.html2String
                    self.aboutUSTextView.font = UIFont(name: "Montserrat-Regular", size: 15)
                    
//                    let mainText =  loginRespone.response?.content! as! NSString
//                    let terms: NSString = "Terms of Service"
//                    let unfoldInc: NSString = "coolpad.us"
//                    let webUrl1 = URL(string: self.aboutUsResponse?.privacy ?? "")!
//                 //   let webUrl2 = URL(string: "http://www.unfoldlabs.com/products/termsofservice.html")!
//
//                    let range1 = mainText.range(of: unfoldInc as String)
//
//
//
//
//                     let range12 = mainText.range(of:  self.aboutUSTextView.text as String)
//                 //   let range2 = mainText.range(of: terms as String)
//
//                    // let txtAttributes = [NSAttributedStringKey.foregroundColor: UIColor.blue]
//                    let attributedStr = NSMutableAttributedString(string: self.aboutUSTextView.text, attributes: nil)
//                    attributedStr.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Montserrat-Regular", size: 18)!, range:  range12)
//                    self.aboutUSTextView.linkTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.blue ]
                    
            //     attributedStr.addtr
                    
                    
                    
                //   loginRespone.response?.content!.linkTextAttributes = [NSAttributedString.Key.foregroundColor.rawValue: UIColor(red: 109.0/255.0, green: 176.0/255.0, blue: 51.0/255.0, alpha: 1) ]
//                    
//                    attributedStr.beginEditing()
//                    attributedStr.addAttribute(NSAttributedString.Key.link, value: webUrl1, range: range1)
//               
//                   attributedStr.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
//                    
//                    
//                    
//                    
//                    
//                     attributedStr.addAttribute(NSAttributedString.Key.foregroundColor, value:UIColor.blue, range: range1)
//                    
//                    
//               //     attributedStr.addAttribute(NSAttributedString.Key.link, value: webUrl2, range: range2)
//              //      attributedStr.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range2)
//                    attributedStr.endEditing()
//                    
//                    self.aboutUSTextView.attributedText = attributedStr
                    
                    self.companyLogo.sd_setImage(with: URL(string: (organizationdetails?.organization?.splashImage)!), placeholderImage: UIImage(named: "cool"))
                    
                }
                
            } catch let jsonErr {
                 SVProgressHUD.dismiss()
                
                
                if Reachability.isConnectedToNetwork(){
                    print("Internet Connection Available!")
                }else{
                    self.showAlert(title: "", msg: "No network connection")
                }
               if let err = error as? URLError, err.code  ==  URLError.Code.notConnectedToInternet
                    {
                        
                         self.showAlert(title: "", msg: "No internet connection")
                       // print("No internet")
                        // No internet
                }
                else
                {
                    // Other errors
                }
                
                print("Error serializing json:", jsonErr)
                
            }
            
            
        }){ (error) in
             SVProgressHUD.dismiss()
            
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
            print(error)
        }
        
        //      aboutUSTextView.text = "Version : \(Constants.version) dated \(Constants.date) CloudPeppers and the CloudPeppers Logos are trademarks of CloudPeppers Inc.All Rights Reserved."
        navigationController?.navigationBar.barTintColor = Constants.colour
        self.navigationItem.title = "About Us"
        self.changeFontForViewController()
        btnMenuButton.target = revealViewController()
        btnMenuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
         Analytics.logEvent("AboutUsEntered", parameters:nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        Analytics.logEvent("AboutUsExit", parameters:nil)
    }
    
    @IBAction func contactUSBtnTapped(_ sender: UIButton) {
        Analytics.logEvent("AboutUs_ContactUs_Clicked", parameters:nil)
        showMail()
    }
    
    @IBAction func termsofService(_ sender: Any) {
        
        Analytics.logEvent("AboutUs_TermsandService_Clicked", parameters:nil)
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "TermsCondViewController") as! TermsCondViewController
        vc.urlString = self.aboutUsResponse?.privacy
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    
    
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
}
extension AboutUsViewController: MFMailComposeViewControllerDelegate {
    
    func setupUI() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showMail))
        gesture.numberOfTapsRequired = 1
        // contactLabel.isUserInteractionEnabled = true
        //contactLabel.addGestureRecognizer(gesture)
    }
    
    @objc func showMail() {
        
        if !MFMailComposeViewController.canSendMail() {
            print("Check in real device")
            showAlert(title: "", msg: "Please login to your mail")
            return
        }
        
        let subject = "UnfoldLabs: Query or FeedBack"
        
        
        let recipents = [self.aboutUsResponse?.contactUs ?? "unfoldlabs@gmail.com"]
        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self
        mailComposer.setSubject(subject)
        mailComposer.setMessageBody("", isHTML: false)
        mailComposer.setToRecipients(recipents)
        self.present(mailComposer, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .cancelled:
            print("Cancelled")
        case .saved:
            print("Saved")
        case .failed:
            print("Failed")
        case .sent:
            print("Sent mail")
        }
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    
}


public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}

