//
//  PermissionRequestsVC.swift
//  CloudPeppers
//
//  Created by Allvy on 25/04/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

enum TabTitle: String {
    case albums       = "ALBUMS"
    case allPictures    = "ALL PICTURES"
}

class PermissionRequestsVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var fromYouPendingReqLabel: UILabel!
    @IBOutlet weak var fromYouApprovedLabel: UILabel!
    @IBOutlet weak var toYouApprovedCV: UICollectionView!
    @IBOutlet weak var fromYouPendingCV: UICollectionView!
    @IBOutlet weak var fromYouView: UIView!
    @IBOutlet weak var toYouView: UIView!
    @IBOutlet weak var pendingReqLabel: UILabel!
    @IBOutlet weak var approvedReqLabel: UILabel!
    @IBOutlet weak var pendingCollectionView: UICollectionView!
    @IBOutlet weak var approvedCollectionView: UICollectionView!
    var isAlbumTab:Bool    = true
    
    @IBOutlet weak var indicatorBottomCons: NSLayoutConstraint!
    @IBOutlet weak var indicatorTrailingCons: NSLayoutConstraint!
    @IBOutlet weak var heightCons: NSLayoutConstraint!
    @IBOutlet weak var widthCons: NSLayoutConstraint!
    @IBOutlet weak var btnMenuButton: UIBarButtonItem!
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var toyouButton: UIButton!
    @IBOutlet weak var fromyouButton: UIButton!
    
    var namesArray:Array = [String]()
    var imagesArray:Array = [UIImage]()
    var relationArray:Array = [String]()
    
    var approvedNamesArr:Array = [String]()
    var approveimagesArray:Array = [UIImage]()
    var approverelationArray:Array = [String]()
    var permissionRequest:PermissionRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //http://localhost:8080/getFromYourToYou?userId=2
        
        navigationController?.navigationBar.barTintColor = Constants.colour
        self.navigationItem.title = "Permission Requests"
        self.changeFontForViewController()
        
        approvedCollectionView.delegate = self
        
        
        toYouView.roundedTop()
        fromYouView.roundedTop()
        
        
        
        addCornerRadius(label: pendingReqLabel)
        addCornerRadius(label: approvedReqLabel)
        addCornerRadius(label: fromYouPendingReqLabel)
        addCornerRadius(label: fromYouApprovedLabel)
        
        btnMenuButton.target = revealViewController()
        btnMenuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        
        namesArray = ["Oliver","Thea","Moria"]
        imagesArray = [#imageLiteral(resourceName: "Jones"),#imageLiteral(resourceName: "120x120"),#imageLiteral(resourceName: "Robert1")]
        relationArray = ["(Son)","(Daughter)","(Mother)"]
        
        approvedNamesArr = ["Robert","Olivia","Jones"]
        approveimagesArray = [#imageLiteral(resourceName: "Thea1"),#imageLiteral(resourceName: "Moria"),#imageLiteral(resourceName: "DefaultProfileImg")]
        approverelationArray = ["(Father)","(Wife)","(Brother)"]
        
        callgetMemberApi()
        
        // Do any additional setup after loadimaing the view.
    }
    
    func callgetMemberApi()
    {
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        let getFromYourToYouUrl = API.getFromYourToYou + "?userId=\(String(describing: profileData.userId!))"
        
        //let getFromYourToYouUrl = API.getFromYourToYou + "?userId=8"
        print(getFromYourToYouUrl)
        HttpWrapper.gets(with: getFromYourToYouUrl, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            print(response)
            
            
            
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatuspermissionRequest.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    self.permissionRequest  = loginRespone.response
                    self.toYouApprovedCV.reloadData()
                    self.fromYouPendingCV.reloadData()
                    self.pendingCollectionView.reloadData()
                    self.approvedCollectionView.reloadData()
                    
                }
                
                
            }catch let jsonErr {
                
                
                print("Error serializing json:", jsonErr)
                
            }
        }){ (error) in
            
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
            
            
            print(error)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func allPicsButtonAction() {
        self.isAlbumTab             = false
        self.toyouButton.setTitleColor(.red, for: .normal)
        self.fromyouButton.setTitleColor  (.black,for:.normal)
        
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == pendingCollectionView
        {
            return permissionRequest?.toyouPending?.count ?? 0
        }
        if collectionView == approvedCollectionView
        {
            return permissionRequest?.toyouApprove?.count ?? 0
        }
        
        if collectionView == fromYouPendingCV
        {
            return permissionRequest?.fromYouPending?.count ?? 0
        }
        
        if collectionView == toYouApprovedCV
        {
            return permissionRequest?.fromYouApprove?.count ?? 0
        }
        
        
        return namesArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "permission", for: indexPath) as! PermissionCollectionViewCell
        
        if collectionView == pendingCollectionView
        {
            cell.nameLabel.text = permissionRequest?.toyouPending?[indexPath.row].firstName
            cell.relationLabel.text = permissionRequest?.toyouPending?[indexPath.row].relationShip
            //  cell.displayImage.image = imagesArray[indexPath.row]
            
        
            cell.displayImage.layer.cornerRadius = cell.displayImage.frame.size.height/2
            cell.displayImage.clipsToBounds = true
            
            if permissionRequest?.toyouPending?[indexPath.row].profileImage != nil && permissionRequest?.toyouPending?[indexPath.row].profileImage != ""
            {
                cell.displayImage.sd_setImage(with: URL(string: (permissionRequest?.toyouPending?[indexPath.row].profileImage)!), placeholderImage: UIImage(named: "DefaultProfileImg"))
            }
            
            
            
        }
        else if collectionView == approvedCollectionView
        {
            cell.nameLabel.text = permissionRequest?.toyouApprove?[indexPath.row].firstName
            cell.relationLabel.text = permissionRequest?.toyouApprove?[indexPath.row].relationShip
            
            cell.displayImage.layer.cornerRadius = cell.displayImage.frame.size.height/2
            cell.displayImage.clipsToBounds = true
            
            if permissionRequest?.toyouApprove?[indexPath.row].profileImage != nil && permissionRequest?.toyouApprove?[indexPath.row].profileImage != ""
            {
                cell.displayImage.sd_setImage(with: URL(string: (permissionRequest?.toyouApprove?[indexPath.row].profileImage)!), placeholderImage: UIImage(named: "DefaultProfileImg"))
            }
            
        }
        else if collectionView == fromYouPendingCV
        {
            cell.nameLabel.text = permissionRequest?.fromYouPending?[indexPath.row].firstName
            cell.relationLabel.text = permissionRequest?.fromYouPending?[indexPath.row].relationShip
            
            cell.displayImage.layer.cornerRadius = cell.displayImage.frame.size.height/2
            cell.displayImage.clipsToBounds = true
            
            if permissionRequest?.fromYouPending?[indexPath.row].profileImage != nil && permissionRequest?.fromYouPending?[indexPath.row].profileImage != ""
            {
                cell.displayImage.sd_setImage(with: URL(string: (permissionRequest?.fromYouPending?[indexPath.row].profileImage)!), placeholderImage: UIImage(named: "DefaultProfileImg"))
            }
            //  cell.displayImage.sd_setImage(with: URL(string: (permissionRequest?.fromYouPending?[indexPath.row].profileImage)!), placeholderImage: UIImage(named: "DefaultProfileImg"))
        }
        else if collectionView == toYouApprovedCV
        {
            cell.nameLabel.text = permissionRequest?.fromYouApprove?[indexPath.row].firstName
            cell.relationLabel.text = permissionRequest?.fromYouApprove?[indexPath.row].relationShip
            
            cell.displayImage.layer.cornerRadius = cell.displayImage.frame.size.height/2
            cell.displayImage.clipsToBounds = true
            
            if permissionRequest?.fromYouApprove?[indexPath.row].profileImage != nil && permissionRequest?.fromYouApprove?[indexPath.row].profileImage != ""
            {
                cell.displayImage.sd_setImage(with: URL(string: (permissionRequest?.fromYouApprove?[indexPath.row].profileImage)!), placeholderImage: UIImage(named: "DefaultProfileImg"))
            }
            // cell.displayImage.sd_setImage(with: URL(string: (permissionRequest?.fromYouApprove?[indexPath.row].profileImage)!), placeholderImage: UIImage(named: "DefaultProfileImg"))
        }
        
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == pendingCollectionView
        {
            
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmPermissionsVC") as! ConfirmPermissionsVC
            
            vc.approve = "pending"
            vc.fromOrTo = "toyou"
            vc.memberId = permissionRequest?.toyouPending?[indexPath.row].userId
            vc.reponsedata = permissionRequest?.toyouPending?[indexPath.row]
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
        if collectionView == approvedCollectionView
        {
            
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "SetRulesViewController") as! SetRulesViewController
            
            //            vc.approve = "approve"
            //            vc.fromOrTo = "toyou"
            vc.getMemberDetails = permissionRequest?.toyouApprove?[indexPath.row]
            vc.memberId = permissionRequest?.toyouApprove?[indexPath.row].userId
            
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
        
        if collectionView == fromYouPendingCV
        {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmPermissionsVC") as! ConfirmPermissionsVC
            
            vc.approve = "pending"
            vc.fromOrTo = "fromyou"
            vc.memberId = permissionRequest?.fromYouPending?[indexPath.row].userId
            vc.reponsedata = permissionRequest?.fromYouPending?[indexPath.row]
            
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        if collectionView == toYouApprovedCV
        {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmPermissionsVC") as! ConfirmPermissionsVC
            
            vc.approve = "approve"
            vc.fromOrTo = "fromyou"
            vc.memberId = permissionRequest?.fromYouApprove?[indexPath.row].userId
            vc.reponsedata = permissionRequest?.fromYouApprove?[indexPath.row]
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    
    
    
    @IBAction func toYouButtonTapped(_ sender: UIButton) {
        
        pendingCollectionView.reloadData()
        approvedCollectionView.reloadData()
        
        self.fromYouView.isHidden = true
        
        self.toYouView.isHidden = false
        
        self.indicatorTrailingCons.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //        let viewWidth = UIScreen.main.bounds.size.width
        //
        //
        //            var width = (viewWidth - 20) / 5.5
        //            if UIDevice.current.userInterfaceIdiom == .pad {
        //                width = (viewWidth - 30) / 8.5
        //            }
        //            return CGSize(width: width, height: width)
        //
        
        let collectionWidth = collectionView.bounds.width
        return CGSize(width: collectionWidth/5, height: collectionWidth/2)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 60
    }
    
    @IBAction func fromYouButtonTapped(_ sender: UIButton) {
        
        pendingCollectionView.reloadData()
        
        approvedCollectionView.reloadData()
        
        self.toYouView.isHidden = true
        self.fromYouView.isHidden = false
        
        self.indicatorTrailingCons.constant = self.toyouButton.frame.size.width + 2
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
        
    }
    
    func addCornerRadius(label:UILabel)
    {
        label.layer.cornerRadius = 10
        label.clipsToBounds = true
    }
}
