//
//  RestrictedTimeLimitsVC.swift
//  CloudPeppers
//
//  Created by Allvy on 28/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase


class RestrictedTimeLimitsVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var switchButton: UISwitch!
    
    @IBOutlet weak var mondayButton: UIButton!
    @IBOutlet weak var sundayButton: UIButton!
    @IBOutlet weak var saturdayButton: UIButton!
    @IBOutlet weak var fridayButton: UIButton!
    @IBOutlet weak var thurdayButton: UIButton!
    @IBOutlet weak var tuesdayButton: UIButton!
    @IBOutlet weak var wednesdayButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var restrictedDaysView: UIView!
    @IBOutlet weak var restrictedTimeTV: UITableView!
    @IBOutlet weak var formatView: UIView!
    
    @IBOutlet weak var fromTF: UITextField!
    var titleName:String?
    @IBOutlet weak var toTF: UITextField!
    
    var memberId:Int?
    var userId:Int?
    
    
    var timeString:String?
    var timeString1:String?
    
    var getMemberDetailsbyId:[resdata]?
    
    var weekarray = [String]()
    let fromDatePicker = UIDatePicker()
    let toDatePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switchButton.isOn = false
        
        addButton.changeButtonCornerRadius()
        formatView.dropShadow()
        restrictedDaysView.dropShadow()
        self.title = titleName
        buttonUnSelected(button: mondayButton)
        buttonUnSelected(button: tuesdayButton)
        buttonUnSelected(button: wednesdayButton)
        buttonUnSelected(button: thurdayButton)
        buttonUnSelected(button: fridayButton)
        buttonUnSelected(button: saturdayButton)
        buttonUnSelected(button: sundayButton)
        
        fromDatePickerFunc()
        toDatePickerFunc()
        
        callApidailyTimeLimit()
        
        fromTF.layer.cornerRadius = 10
        fromTF.clipsToBounds = true
        
        toTF.layer.cornerRadius = 10
        toTF.clipsToBounds = true
        
        let swh : UISwitch = switchButton as! UISwitch
        if(swh.isOn){
            fromDatePicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
            toDatePicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
            
        }
        else{
            fromDatePicker.locale = NSLocale(localeIdentifier: "en_US") as Locale
            toDatePicker.locale = NSLocale(localeIdentifier: "en_US") as Locale
        }
        
        fromDatePicker.tintColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7254901961, alpha: 1)
        toDatePicker.tintColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7254901961, alpha: 1)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        Analytics.logEvent("RestrictedTimeLimitsVCEntered", parameters:nil)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("RestrictedTimeLimitsVCExit", parameters:nil)
    }
    @IBAction func mondayButtonTapped(_ sender: UIButton) {
        
        let monString = "Monday"
        
        if mondayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(monString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(monString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: monString)!)
                
            }
            
        }
        sender.isSelected = !sender.isSelected
        
    }
    
    @IBAction func tuesdayButtonTapped(_ sender: UIButton) {
        
        let tueString = "Tuesday"
        
        if tuesdayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(tueString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(tueString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: tueString)!)
                
            }
            
        }
        sender.isSelected = !sender.isSelected
        
    }
    @IBAction func wednesdayButtonTapped(_ sender: UIButton) {
        
        let wedString = "Wednesday"
        
        if wednesdayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(wedString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(wedString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: wedString)!)
                
            }
        }
        sender.isSelected = !sender.isSelected
    }
    @IBAction func thursdayButtonTapped(_ sender: UIButton) {
        
        let thurString = "Thursday"
        
        if thurdayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(thurString)
        }
        else
        {
            buttonUnSelected(button: sender)
            
            if (weekarray.contains(thurString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: thurString)!)
                
            }
        }
        sender.isSelected = !sender.isSelected
    }
    @IBAction func fridayButtonTapped(_ sender: UIButton) {
        
        let friString = "Friday"
        
        if fridayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(friString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(friString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: friString)!)
                
            }
        }
        sender.isSelected = !sender.isSelected
    }
    @IBAction func saturdayButtonTapped(_ sender: UIButton) {
        
        let satString = "Saturday"
        
        if saturdayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(satString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(satString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: satString)!)
                
            }
        }
        sender.isSelected = !sender.isSelected
    }
    @IBAction func sundayButtonTapped(_ sender: UIButton) {
        
        let sunString = "Sunday"
        
        if sundayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(sunString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(sunString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: sunString)!)
                
            }
        }
        sender.isSelected = !sender.isSelected
    }
    
    func buttonSelected(button:UIButton)
    {
        button.layer.cornerRadius = button.frame.height/2
        button.clipsToBounds = true
        button.layer.borderWidth = 0.5
        button.layer.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
        button.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
        button.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .selected)
    }
    func buttonUnSelected(button:UIButton)
    {
        button.layer.cornerRadius = button.frame.height/2
        button.clipsToBounds = true
        button.layer.borderWidth = 0.5
        button.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        button.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
    }
    @IBAction func switchButtonTapped(_ sender: UISwitch) {
        Analytics.logEvent("RestricedTimes_Screen_X_Timeformat_clicked", parameters:nil)
        
        let swh : UISwitch = sender as! UISwitch
        if(swh.isOn){
            fromDatePicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
            toDatePicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
            
        }
        else{
            fromDatePicker.locale = NSLocale(localeIdentifier: "en_US") as Locale
            toDatePicker.locale = NSLocale(localeIdentifier: "en_US") as Locale
        }
        restrictedTimeTV.reloadData()
        
    }
    func toDatePickerFunc(){
        Analytics.logEvent("RestricedTimes_Screen_ToTime_Selected", parameters:nil)
        //Formate Date
        toDatePicker.datePickerMode = .time
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.tintColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7254901961, alpha: 1)
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        toTF.inputAccessoryView = toolbar
        toTF.inputView = toDatePicker
        
    }
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        toTF.text = formatter.string(from: toDatePicker.date)
        print(toTF.text!) //Print Date like 11:56 PM
        
        let date = formatter.date(from: toTF.text!)
        formatter.dateFormat = "HH:mm:ss"
        let date24 = formatter.string(from: date!)
        print(date24)
        timeString1 = date24
        
        if(switchButton.isOn)
        {
            
            toTF.text = timeString1
        }
        print(timeString1!) //Print Date like  17:06:00
        self.view.endEditing(true)
        
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    func fromDatePickerFunc(){
        Analytics.logEvent("RestricedTimes_Screen_FromTime_Selected", parameters:nil)
        //Formate Date
        fromDatePicker.datePickerMode = .time
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(fromDonedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(fromCancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
          toolbar.tintColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7254901961, alpha: 1)
        fromTF.inputAccessoryView = toolbar
        fromTF.inputView = fromDatePicker
        
        
    }
    
    @objc func fromDonedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        fromTF.text = formatter.string(from: fromDatePicker.date)
        print(fromTF.text!) //Print Date like 11:56 PM
        
        
        
        
        
        let date = formatter.date(from: fromTF.text!)
        formatter.dateFormat = "HH:mm:ss"
        let date24 = formatter.string(from: date!)
        print(date24)
        timeString = date24
        
        if(switchButton.isOn)
        {
            
            fromTF.text = timeString
        }
        print(timeString!) //Print Date like  17:06:00
        self.view.endEditing(true)
    }
    
    @objc func fromCancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    
    func callApidailyTimeLimit()
    {
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        // http://35.165.108.250:8080/CloudPeppers-V2_dev/getAllDailyTimeLimits?userId=2&memberId=1
        let addMemberUrl = API.getAllRestrictedTimeLimits + "?userId=\(String(describing: profileData.userId!))" + "&memberId=\(String(describing: memberId!))"
        print(addMemberUrl)
        HttpWrapper.gets(with: addMemberUrl, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            
            
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatusgetresTimeLimit.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    self.getMemberDetailsbyId  = loginRespone.response
                    
                    if self.getMemberDetailsbyId?.count == 0
                    {
                        //   self.restrictedTimeTV.isHidden = true
                        
                        self.restrictedTimeTV.reloadData()
                        return
                    }else
                    {
                        self.restrictedTimeTV.isHidden = false
                    }
                    if self.getMemberDetailsbyId != nil
                    {
                        self.restrictedTimeTV.reloadData()
                        
                        
                        
                        //                        self.alarmReminderTableview.dataSource = self
                        //                        self.alarmReminderTableview.delegate = self
                        //
                        //                        self.alarmReminderTableview.reloadData()
                        // self.updateUi(sender: self.getMemberDetailsbyId)
                    }
                }
                else
                    
                {
                    SharedData.data.getMemberDetails  = nil
                    
                    self.restrictedTimeTV.reloadData()
                }
                
            }catch let jsonErr {
                //  SharedData.data.getMemberDetails=nil
                
                print("Error serializing json:", jsonErr)
                
            }
        }){ (error) in
            
            //  SharedData.data.getMemberDetails=nil
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
            
            print(error)
        }
    }
    
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        Analytics.logEvent("RestricedTimes_Screen_AddButton_clicked", parameters:nil)
        
        if timeString == nil
        {
            showAlert(title: "", msg: "Please select From time")
            return
        }
        
        if timeString1 == nil
        {
            showAlert(title: "", msg: "Please select To time")
            return
        }
        if timeString1!.compare(timeString!) == .orderedAscending {
            print("First Date is smaller then second date")
            showAlert(title: "", msg: "From time should not be greater than or equal to To time")
            return
        }
        if timeString1 == timeString
        {
            showAlert(title: "", msg: "From time & To time should not be same")
            return
        }
        
        if weekarray.isEmpty
        {
            showAlert(title: "", msg: "Please select any day(s)")
            return
        }
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(weekarray.joined(separator: ", "), forKey: "days")
        parameter.setValue(timeString!, forKey: "fromTime")
        parameter.setValue(timeString1!, forKey: "toTime")
        parameter.setValue(userId, forKey: "userId")
        parameter.setValue(memberId, forKey: "memberId")
        
        print(parameter)
        
        APIServices.postDataToServer(url: API.saveRestrictedTimeLimits, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                
                
                self.weekarray.removeAll()
                self.mondayButton.isSelected = false
                self.tuesdayButton.isSelected = false
                self.wednesdayButton.isSelected = false
                self.thurdayButton.isSelected = false
                self.fridayButton.isSelected = false
                
                self.saturdayButton.isSelected = false
                
                self.sundayButton.isSelected = false
                
                self.buttonUnSelected(button: self.mondayButton)
                self.buttonUnSelected(button: self.tuesdayButton)
                self.buttonUnSelected(button: self.wednesdayButton)
                self.buttonUnSelected(button: self.thurdayButton)
                self.buttonUnSelected(button: self.fridayButton)
                self.buttonUnSelected(button: self.saturdayButton)
                self.buttonUnSelected(button: self.sundayButton)
                
                self.fromTF.text = nil
                self.toTF.text = nil
                
                self.callApidailyTimeLimit()
                
                //   let alertController = UIAlertController(title: Constants.alertTitle, message: "Restricted Time added successfully", preferredStyle: .alert)
                
                
                let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                
                let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
                let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
                
                let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
                let msgAttrString = NSMutableAttributedString(string: "Restricted Time added successfully", attributes: msgFont)
                
                alert.setValue(titAttrString, forKey: "attributedTitle")
                alert.setValue(msgAttrString, forKey: "attributedMessage")
                
                
                
                let settingsAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                    if #available(iOS 10.0, *) {
                        //    self.navigationController?.popViewController(animated: true)
                    } else {
                        // Fallback on earlier versions
                    }
                }
                
                
                alert.addAction(settingsAction)
                self.present(alert, animated: true, completion: nil)
                print(response)
                
            }
        })
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        
        let numOfSection: NSInteger =  getMemberDetailsbyId?.count ?? 0
        print(numOfSection)
        
        if numOfSection == 0
        {
            
            let noDataLabel: UILabel = UILabel(frame:CGRect(x: 0, y: 0, width: self.restrictedTimeTV.frame.width, height: self.restrictedTimeTV.frame.height))
            noDataLabel.backgroundColor = .white
            noDataLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            restrictedTimeTV.separatorStyle = .none
            noDataLabel.text = "No restricted times found"
            noDataLabel.textAlignment = NSTextAlignment.center
            self.restrictedTimeTV.backgroundView = noDataLabel
            return 1
        }
        else
        {
            
            self.restrictedTimeTV.backgroundView = nil
            
            return 1
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  getMemberDetailsbyId?.count ?? 0
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestrictedTimeTableViewCell", for: indexPath) as? RestrictedTimeTableViewCell
        
        
        if(switchButton.isOn){
            cell?.displayTimeLabel.text = "\(String(describing: (getMemberDetailsbyId?[indexPath.row].fromTime!)!))" + "-" + "\(String(describing: (getMemberDetailsbyId?[indexPath.row].toTime!)!))"
            
            
            
            
            
            
        }else
        {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            let dateAsString = getMemberDetailsbyId?[indexPath.row].fromTime
            let date = dateFormatter.date(from: dateAsString!)
            dateFormatter.dateFormat = "h:mm a"
            let Date12 = dateFormatter.string(from: date!)
            print("12 hour formatted Date:",Date12)
            
            
            let dateAsString1 = getMemberDetailsbyId?[indexPath.row].toTime
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "HH:mm:ss"
            
            let date1 = dateFormatter1.date(from: dateAsString1!)
            dateFormatter1.dateFormat = "h:mm a"
            let Date121 = dateFormatter.string(from: date1!)
            print("12 hour formatted Date:",Date121)
            
            cell?.displayTimeLabel.text = "\(Date12)" + "-" + "\(Date121)"
            
        }
        
        //   cell?.displayTimeLabel.text = "\(String(describing: (getMemberDetailsbyId?[indexPath.row].fromTime!)!))" + "-" + "\(String(describing: (getMemberDetailsbyId?[indexPath.row].toTime!)!))"
        
        let array = getMemberDetailsbyId?[indexPath.row].days?.components(separatedBy: ",")
        
        if array?.contains("Monday") ?? false
        {
            cell!.monday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.monday.setTitleColor(UIColor.white, for: .normal)
            cell!.monday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.monday.borderWidth = 0.5
            
        }else
        {
            cell!.monday.backgroundColor = UIColor.white
            cell!.monday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
            cell!.monday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.monday.borderWidth = 0.5
        }
        if array?.contains("Tuesday") ?? false
            
        {
            cell!.thuesday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.thuesday.setTitleColor(UIColor.white, for: .normal)
            cell!.thuesday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.thuesday.borderWidth = 0.5
            
        }else
        {
            cell!.thuesday.backgroundColor = UIColor.white
            cell!.thuesday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
            cell!.thuesday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.thuesday.borderWidth = 0.5
        }
        
        if array?.contains("Wednesday") ?? false
            
        {
            cell!.wednesday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.wednesday.setTitleColor(UIColor.white, for: .normal)
            cell!.wednesday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.wednesday.borderWidth = 0.5
            
        }else
        {
            cell!.wednesday.backgroundColor = UIColor.white
            cell!.wednesday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
            cell!.wednesday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.wednesday.borderWidth = 0.5
        }
        
        if array?.contains("Thursday") ?? false
        {
            cell!.thursday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.thursday.setTitleColor(UIColor.white, for: .normal)
            cell!.thursday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.thursday.borderWidth = 0.5
            
        }else
        {
            cell!.thursday.backgroundColor = UIColor.white
            cell!.thursday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
            cell!.thursday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.thursday.borderWidth = 0.5
        }
        
        if array?.contains("Friday") ?? false
        {
            cell!.friday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.friday.setTitleColor(UIColor.white, for: .normal)
            cell!.friday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.friday.borderWidth = 0.5
            
        }else
        {
            cell!.friday.backgroundColor = UIColor.white
            cell!.friday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
            cell!.friday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.friday.borderWidth = 0.5
        }
        
        if array?.contains("Saturday") ?? false
        {
            cell!.saturday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.saturday.setTitleColor(UIColor.white, for: .normal)
            cell!.saturday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.saturday.borderWidth = 0.5
            
        }else
        {
            cell!.saturday.backgroundColor = UIColor.white
            cell!.saturday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
            cell!.saturday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.saturday.borderWidth = 0.5
        }
        
        if array?.contains("Sunday") ?? false
        {
            cell!.sunday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.sunday.setTitleColor(UIColor.white, for: .normal)
            cell!.sunday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.sunday.borderWidth = 0.5
            
        }else
        {
            cell!.sunday.backgroundColor = UIColor.white
            cell!.sunday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
            cell!.sunday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            cell!.sunday.borderWidth = 0.5
        }
        
        cell?.deleteButton.tag = indexPath.row
        cell?.deleteButton.addTarget(self, action: #selector(deleteRemainder), for: .touchUpInside)
        return cell!
        
        
        return UITableViewCell()
    }
    
    
    @objc func deleteRemainder(sender: UIButton!) {
        
        Analytics.logEvent("RestricedTimes_Screen_DeleteRestricedTime_Clicked", parameters:nil)
        let alertController = UIAlertController(title: "", message: "Do you want to remove?", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "NO", style: .cancel, handler: { (action : UIAlertAction!) -> Void in })
        
        let saveAction = UIAlertAction(title: "YES", style: .default, handler: { alert -> Void in
            
            //   String url = "deleteRestrictedTimeLimitsByMemberId?restrictedTimeId="+dailyTimeId+"&memberId="+memberUserId+"&userId="+sessionManager.getIntegerData(AppStrings.Session.USER_ID);
            
            //   String url = "deleteDailyTimeLimitsByMemberId?dailyTimeId="+dailyTimeId+"&userId="+sm.getIntegerData(AppStrings.Session.USER_ID)+"&memberId="+memberUserId;
            
            let deleteAlarm = API.deleteRestrictedTimeLimitsByMemberId + "?restrictedTimeId=\(String(describing: (self.getMemberDetailsbyId?[sender.tag].restrictedTimeId!)!))" + "&userId=\(self.userId!)" + "&memberId=\(self.memberId!)"
            //    print(deleteAlarm)
            
            HttpWrapper.gets(with: deleteAlarm, parameters: nil, headers: nil, completionHandler: { (response, error)  in
                
                guard let data = response else { return }
                do {
                    
                    let loginRespone = try JSONDecoder().decode(ApiStatus1.self, from: data)
                    let loginStatus = loginRespone.status
                    if  loginStatus  ==  true
                    {
                        self.showAlert(title: "", msg: "Deleted Successfully")
                        self.callApidailyTimeLimit()
                    }
                    
                }catch let jsonErr {
                    
                    print("Error serializing json:", jsonErr)
                }
            }){ (error) in
                
                print(error)
                if Reachability.isConnectedToNetwork(){
                    print("Internet Connection Available!")
                }else{
                    self.showAlert(title: "", msg: "No network connection")
                }
            }
            
        })
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    
    
    
}


