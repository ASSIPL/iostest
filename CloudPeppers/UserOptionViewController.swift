//
//  UserOptionViewController.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 23/04/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import SDWebImage
import SafariServices
import Firebase

class UserOptionViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var coolpadLinkImage: UIImageView!
    @IBOutlet var powerBy: UILabel!
    
    let text = "Powered by coolpad"
    
    @IBOutlet var optionsTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
        
//        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
//            AnalyticsParameterItemID: "id-\(01)",
//            AnalyticsParameterItemName: "useroption",
//            AnalyticsParameterContentType: "cont"
//            ])
        Analytics.logEvent("useroptions", parameters:nil)
     //   Analytics.logEvent("useroptions", parameters:nil)

        let tap = UITapGestureRecognizer(target: self, action: #selector(UserOptionViewController.tappedMe))
        coolpadLinkImage.addGestureRecognizer(tap)
        coolpadLinkImage.isUserInteractionEnabled = true
        
        
//        powerBy.text = text
//        self.powerBy.textColor =  UIColor.black
//        let underlineAttriString = NSMutableAttributedString(string: text)
//        let range1 = (text as NSString).range(of: "coolpad")
//        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
//        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Montserrat-Bold", size: 17)!, range: range1)
//        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(hexString: "#00A9B7"), range: range1)
//        powerBy.attributedText = underlineAttriString
//        powerBy.isUserInteractionEnabled = true
//        powerBy.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(tapLabel(gesture:))))
        
        optionsTableview.dataSource = self
        optionsTableview.delegate = self
        optionsTableview.tableFooterView = UIView()
        //  self.optionsTableview.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: -20, right: 0);
        //    optionsTableview.tableFooterView?.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    @objc func tappedMe()
    {
       // let termsRange = (text as NSString).range(of: "coolpad")
        // comment for now
        //let privacyRange = (text as NSString).range(of: "Privacy Policy")
        
//        if gesture.didTapAttributedTextInLabel(label: powerBy, inRange: termsRange) {
//            print("Tapped terms")
//
        
            
            //            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
            //            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
            //
            //            let urlString = organizationdetails?.organization?.orginationURL
            //
            //            guard let url = URL(string: urlString!) else { return }
            //
            //            let svc = SFSafariViewController(url: url)
            //            present(svc, animated: true, completion: nil)
            //UIApplication.shared.open(url)
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "TermsCondViewController") as! TermsCondViewController
            
            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
            
            vc.urlString = organizationdetails?.organization?.orginationURL
            self.present(vc, animated: true, completion: nil)
            //
       // }
            //        } else if gesture.didTapAttributedTextInLabel(label: poweredByLabel, inRange: privacyRange) {
            //            print("Tapped privacy")
//        else {
//            print("Tapped none")
//        }

    }
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        let termsRange = (text as NSString).range(of: "coolpad")
        // comment for now
        //let privacyRange = (text as NSString).range(of: "Privacy Policy")
        
        if gesture.didTapAttributedTextInLabel(label: powerBy, inRange: termsRange) {
            print("Tapped terms")
            
            
            
//            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
//            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
//
//            let urlString = organizationdetails?.organization?.orginationURL
//
//            guard let url = URL(string: urlString!) else { return }
//
//            let svc = SFSafariViewController(url: url)
//            present(svc, animated: true, completion: nil)
            //UIApplication.shared.open(url)
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "TermsCondViewController") as! TermsCondViewController

            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)

            vc.urlString = organizationdetails?.organization?.orginationURL
            self.present(vc, animated: true, completion: nil)
//
        }
            //        } else if gesture.didTapAttributedTextInLabel(label: poweredByLabel, inRange: privacyRange) {
            //            print("Tapped privacy")
        else {
            print("Tapped none")
        }
    }
    
    //    func numberOfSections(in tableView: UITableView) -> Int {
    //        return (responsedata?.roles?.count)!
    //    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        return  2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OptionsTableViewCell") as! OptionsTableViewCell
        
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 20
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        if    SharedData.data.imagedata != nil
        {
            cell.OptionImage.sd_setImage(with: URL(string: (organizationdetails?.roles?[indexPath.row].imageUrl)!), placeholderImage: UIImage(named: "ViewBackGround"))
            cell.OptionImage.image = UIImage(data: SharedData.data.imagedata![indexPath.row])
            //
        }else
        {
            cell.OptionImage.sd_setImage(with: URL(string: (organizationdetails?.roles?[indexPath.row].imageUrl)!), placeholderImage: UIImage(named: "ViewBackGround"))
        }
        if indexPath.row == 0
        {
            for each in (organizationdetails?.roles)!
            {
                if each.roleKey == "parent.type"
                {
                    cell.optionLabel.text =  each.type
                    cell.OptionImage.sd_setImage(with: URL(string:each.imageUrl!), placeholderImage: UIImage(named: "ViewBackGround"))
                }
            }
        }
        
        if indexPath.row == 1
        {
            for each in (organizationdetails?.roles)!
            {
                if each.roleKey == "member.type"
                {
                    cell.optionLabel.text =  each.type
                    cell.OptionImage.sd_setImage(with: URL(string:each.imageUrl!), placeholderImage: UIImage(named: "ViewBackGround"))
                }
            }
            
            
            
        }
       
        
       // cell.optionLabel.text =  (organizationdetails?.roles?[indexPath.row].type)!
//        cell.layer.borderColor = UIColor.clear.cgColor
//        cell.layer.borderWidth = 0.2
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        _ = UIApplication.shared.delegate as! AppDelegate
        let id = "LoginViewController"
        let vc = UIStoryboard.getViewController(storyboardName: "Main", storyboardId: id) as? LoginViewController
        
        //   let vc = UIStoryboard.getViewController(storyboardName: "Main", storyboardId: id) as? MainAppsViewController
        
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        if indexPath.row == 0
        {
            for each in (organizationdetails?.roles)!
            {
                if each.roleKey == "parent.type"
                {
                    
                    
                    UserDefaults.standard.set(each.fullAccess, forKey: "checkfamilymember")
                    
                    
                    UserDefaults.standard.set(each.type, forKey: "checkfamilymembertype")
                    
                    Analytics.logEvent(each.type ?? "", parameters:nil)
                    
                    UserDefaults.standard.set(each.roleKey, forKey: "roleKey")
                    
                }
            }
        }
        
        if indexPath.row == 1
        {
            for each in (organizationdetails?.roles)!
            {
                if each.roleKey == "member.type"
                {
                    
                    UserDefaults.standard.set(each.fullAccess, forKey: "checkfamilymember")
                    
                    
                    UserDefaults.standard.set(each.type, forKey: "checkfamilymembertype")
                    
                    Analytics.logEvent(each.type ?? "", parameters:nil)
                    
                    UserDefaults.standard.set(each.roleKey, forKey: "roleKey")
                }
            }
            
            
            
        }
        
        
        
        //        if indexPath.row == 0
        //        {
        //              UserDefaults.standard.set("parent", forKey: "checkfamilymembertype")
        //        }
        //        if indexPath.row == 1
        //        {
        //            UserDefaults.standard.set("kid", forKey: "checkfamilymembertype")
        //        }
        //        if indexPath.row == 3
        //        {
        //            UserDefaults.standard.set("family member", forKey: "checkfamilymembertype")
        //        }
        //        if indexPath.row == 4
        //        {
        //            UserDefaults.standard.set("thing", forKey: "checkfamilymembertype")
        //        }
        //
        
        
        
        
        
        
        UserDefaults.standard.set("true", forKey: "pickedOptions")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = vc
            appDelegate.window?.makeKeyAndVisible()
        }
        
        
    }
    
    
}
