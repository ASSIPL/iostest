//
//  MemberPermissionsVC.swift
//  CloudPeppers
//
//  Created by Allvy on 03/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Alamofire
import Firebase
class MemberPermissionsVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIPopoverPresentationControllerDelegate {
    
    @IBOutlet var membersName: UILabel!
    
    var seletedValueType:String?
    @IBOutlet var relation: UILabel!
    
    
    var membersNames:String?
    var relations:String?
    
    var askarraycompair = [String]()
    
    @IBOutlet var allAskButton: UIButton!
    
    
    @IBOutlet var allGiveButton: UIButton!
    
    
    @IBOutlet weak var senRequestBtn: UIButton!
    
    @IBOutlet weak var displayView: UIView!
    @IBOutlet weak var permissionsTV: UITableView!
    
    var askarray = [String]()
    
    var givearray = [String]()
    
    var parameter:NSMutableDictionary = NSMutableDictionary()
    
    var menuNameArray:Array = [String]()
    var iconsArray:Array = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logoutBarButtonItem = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(BackButton))
        
        self.navigationItem.leftBarButtonItem  = logoutBarButtonItem
        
        self.title = "Member Permissions"
         Analytics.logEvent("MemberPermissionsEnter", parameters:nil)
        
        //  self.navigationItem.title = " "
        membersName.text = membersNames
        
        relation.text = "(\(relations!))"
        print(relation.text!)
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        for each in (organizationdetails?.rulez)!
        {
            if self.askarraycompair.contains((each.rulesKey)!) {
                
                
            }else
            {
                self.askarraycompair.append((each.rulesKey)!)
            }
            
        }
        
        if   seletedValueType == "kid.type" || seletedValueType == "thing.type"
        {
            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
            
            
            
            for each in (organizationdetails?.rulez)!
            {
                
                if self.askarray.contains((each.rulesKey)!) {
                    
                    
                    self.askarray.remove(at: self.askarray.index(of: ((each.rulesKey!)))!)
                } else {
                    self.askarray.append((each.rulesKey)!)
                }
            }
            
            allAskButton.isEnabled = false
            allAskButton.setImage(UIImage(named: "checknew"), for: .normal)
            
            
            
            
        }
        
        
        displayView.roundedTop()
        menuNameArray = ["Set Geofence","Apps & Games","Lock Device","Daily Time Limits","Calls & SMS","Alarms & Reminders","Restricted Times","Web Filtering","Panic Button"]
        //    iconsArray = [#imageLiteral(resourceName: "Set Geofence"),#imageLiteral(resourceName: "Apps & Games"),#imageLiteral(resourceName: "Lock Device"),#imageLiteral(resourceName: "Daily Time Limits"),#imageLiteral(resourceName: "Calls & SMS"),#imageLiteral(resourceName: "Alarms & Reminders"),#imageLiteral(resourceName: "Restricted Times"),#imageLiteral(resourceName: "Web Filtering"),#imageLiteral(resourceName: "Panic Button")]
        
        senRequestBtn.changeButtonCornerRadius()
        
        // Do any additional setup after loading the view.
    }
    
    @objc func BackButton(){
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddFamilyMemberVC") as? AddFamilyMemberVC
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("MemberPermissionsExit", parameters:nil)
    }
    @IBAction func SelectAllAsk(_ sender: UIButton) {
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        if sender.isSelected
        {
            self.askarray.removeAll()
            permissionsTV.reloadData()
            
        }else
        {
            
            
            for each in (organizationdetails?.rulez)!
            {
                self.askarray.append(each.rulesKey!)
            }
            
            
            
            
            
            permissionsTV.reloadData()
        }
        
        sender.isSelected = !sender.isSelected
        
        
    }
    
    
    @IBAction func SelectAllGive(_ sender: UIButton) {
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        if sender.isSelected
        {
            self.givearray.removeAll()
            permissionsTV.reloadData()
            
        }else
        {
            
            
            for each in (organizationdetails?.rulez)!
            {
                self.givearray.append(each.rulesKey!)
            }
            
            
            
            
            
            permissionsTV.reloadData()
        }
        
        sender.isSelected = !sender.isSelected
    }
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        return (organizationdetails?.rulez?.count)!
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MemberPermissionsCell") as! MemberPermissionsCell
        //
        //        if organizationdetails?.rulez?[indexPath.row].name == "key.lockAndUnlock" || organizationdetails?.rulez?[indexPath.row].name == "Lock Device"
        //        {
        //            cell.imageViewIcon.image = #imageLiteral(resourceName: "Lock Device")
        //        }
        //        else{
        //            cell.imageViewIcon.image = UIImage(named: (organizationdetails?.rulez?[indexPath.row].rulesKey)!)
        //        }
        //
        
        cell.imageViewIcon.image = UIImage(named: (organizationdetails?.rulez?[indexPath.row].rulesKey)!)
        
        //        if organizationdetails?.rulez?[indexPath.row].name == "Apps & Games"
        //        {
        //            cell.giveButton.setImage(UIImage(named: "UnCheck"), for: .normal)
        //            cell.giveButton.isEnabled = false
        //        }
        //        else{
        //             cell.giveButton.isEnabled = true
        //        }

        if self.askarray.contains((organizationdetails?.rulez?[indexPath.row].rulesKey)!)
            
        {
            
            cell.askButton.setImage(UIImage(named: "checknew"), for: .normal)
            
        }else
        {
            cell.askButton.setImage(UIImage(named: "UnCheck"), for: .normal)
            
        }
        
        if   seletedValueType == "kid.type" || seletedValueType == "thing.type"
        {
            cell.askButton.isEnabled = false
        }
        
        if self.givearray.contains((organizationdetails?.rulez?[indexPath.row].rulesKey)!)
            
        {
            
            cell.giveButton.setImage(UIImage(named: "checknew"), for: .normal)
            
        }else
        {
            cell.giveButton.setImage(UIImage(named: "UnCheck"), for: .normal)
            
        }
        //        if self.givearray.contains((organizationdetails?.rulez?[indexPath.row].name)!)
        //
        //        {
        //
        //            cell.giveButton.setImage(UIImage(named: "CheckBox"), for: .normal)
        //
        //        }else
        //        {
        //            cell.giveButton.setImage(UIImage(named: "UnCheck"), for: .normal)
        //
        //        }
        
        
        
        
        
        
        
        
        cell.permissionsLabel.text = organizationdetails?.rulez?[indexPath.row].name
        cell.askButton.tag = indexPath.row
        cell.giveButton.tag = indexPath.row
        cell.askButton.addTarget(self, action: #selector(askButtonTapped), for: .touchUpInside)
        cell.giveButton.addTarget(self, action: #selector(giveButtonTapped), for: .touchUpInside)
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    @IBAction func askButtonTapped(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        
        if self.askarray.contains((organizationdetails?.rulez?[sender.tag].rulesKey)!) {
            self.askarray.remove(at: self.askarray.index(of: ((organizationdetails?.rulez?[sender.tag].rulesKey!)!))!)
        } else {
            self.askarray.append((organizationdetails?.rulez?[sender.tag].rulesKey)!)
        }
        for each in askarray
        {
            if each == "key.dailyTimeLimits" || each == "key.restrictedTimes"
            {
                if self.askarray.contains("key.lockAndUnlock") {
                    
                } else {
                    self.askarray.append("key.lockAndUnlock")
                }
            }else
            {
                
            }
            if self.askarray.containsSameElements(as: self.askarraycompair)
            {
                allAskButton.isSelected = true
                
            }else
            {
                allAskButton.isSelected = false
            }}
        
        permissionsTV.reloadData()
        
    }
    
    @IBAction func giveButtonTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        
        
        if self.givearray.contains((organizationdetails?.rulez?[sender.tag].rulesKey)!) {
            self.givearray.remove(at: self.givearray.index(of: ((organizationdetails?.rulez?[sender.tag].rulesKey)!))!)
        } else {
            self.givearray.append((organizationdetails?.rulez?[sender.tag].rulesKey)!)
            
        }
        
        
        for each in givearray
        {
            if each == "key.dailyTimeLimits" || each == "key.restrictedTimes"
            {
                if self.givearray.contains("key.lockAndUnlock") {
                    
                } else {
                    self.givearray.append("key.lockAndUnlock")
                }
            }else
            {
                
            }
        }
        if self.givearray.containsSameElements(as: self.askarraycompair)
        {
            allGiveButton.isSelected = true
            
        }else
        {
            allGiveButton.isSelected = false
        }
        
        permissionsTV.reloadData()
        
    }
    
    
    
    @IBAction func sendRequestTapped(_ sender: UIButton) {
        
        
        Analytics.logEvent("Mem_Perm_Send_req_Btn_Clicked", parameters:nil)
//        if askarray.count == 0  &&  givearray.count == 0
//        {
//            showAlert(title: "", msg: "Please select permissions")
//            return
//        }
        
        if askarray.count == 0
        {
            showAlert(title: "", msg: "Please ask at least one permission to send request")
            return
        }
        
        let permissionRequestList:NSMutableArray = NSMutableArray()
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        //  let RequestList:NSMutableDictionary = NSMutableDictionary()
        for each in (organizationdetails?.rulez)!
        {
            let innerRequestList:NSMutableDictionary = NSMutableDictionary()
            
            if  self.askarray.contains(each.rulesKey!) || self.givearray.contains(each.rulesKey!)
            {
                
                
                if self.askarray.contains(each.rulesKey!)
                {
                    innerRequestList.setValue("true", forKey: "ask")
                }else
                {
                    innerRequestList.setValue("false", forKey: "ask")
                }
                
                if self.givearray.contains(each.rulesKey!)
                {
                    innerRequestList.setValue("true", forKey: "give")
                }else
                {
                    innerRequestList.setValue("false", forKey: "give")
                }
                
                
                innerRequestList.setValue(each.rulesKey, forKey: "roleType")
                innerRequestList.setValue(each.id, forKey: "roleId")
                innerRequestList.setValue("true", forKey: "flag")
                permissionRequestList.add(innerRequestList)
            }
            
            
            
            
        }
        
        
        print(permissionRequestList)
        
        parameter.setValue(permissionRequestList, forKey: "permissionRequestList")
        
        print(parameter)
        print(API.sendPermissionInfo)
        
        
        
        
        
        //let parameters: [String: Any] = parameter
        var request = URLRequest(url: URL(string: API.sendPermissionInfo)!)
        request.httpMethod = "POST"
        
        
        
        let id = UserDefaults.standard.string(forKey: "jwtToken")
        
        if id != nil && id != ""
        {
            let token = "Bearer" + " "  + "\(UserDefaults.standard.string(forKey: "jwtToken")!)"
            
            print(token)
             request.setValue(token, forHTTPHeaderField: "Authorization")
        }
        
        
      
        
      
        
        
        
        
        
        
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let dataToSync = parameter
        request.httpBody = try! JSONSerialization.data(withJSONObject: dataToSync)
        
        
        Alamofire.request(request).responseJSON { (response) in
            
            
            print(response)
            
            // print("Success: \(response.data)")
            
            switch response.result{
            case .success:
                let statusCode: Int = (response.response?.statusCode)!
                switch statusCode{
                case 200:
                    
                    guard let data = response.data else { return }
                    do {
                        
                        let responseValue = response.value as? [String:Any]
                        
                        let internalStatus = responseValue?["status"] as? Bool
                        
                        let internalStatusCode = responseValue?["statusCode"] as? Int
                        
                        let internalMessge = responseValue?["message"] as? String
                        
                        if internalStatus == true
                        {
                            let alertController = UIAlertController(title: "", message: internalMessge!, preferredStyle: .alert)
                            let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                                
                                let revealViewController:SWRevealViewController = self.revealViewController()
                                let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let desController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                let newFrontViewController = UINavigationController.init(rootViewController:desController)
                                revealViewController.pushFrontViewController(newFrontViewController, animated: true)
                                
                            }
                            alertController.addAction(settingsAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                        if internalStatusCode == 400
                        {
                            let alertController = UIAlertController(title: "", message: internalMessge, preferredStyle: .alert)
                            let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel)
                            
                            alertController.addAction(settingsAction)
                            self.present(alertController, animated: true, completion: nil)
                            return
                            
                        }
                        
                        if internalStatusCode == 301
                        {
                            let loginRespone =  try JSONDecoder().decode(ApiStatus12.self, from: data)
                            
                            var string = String()
                            var invalidPermissinArray :Array = [String]()
                            
                            for each in (loginRespone.response?.invalidpermissions!)!
                            {
                                string.append("\n")
                                
                                
                                for each1 in (organizationdetails?.rulez)!
                                {
                                    
                                    if each1.rulesKey == each
                                    {
                                        string.append(each1.name!)
                                        invalidPermissinArray.append(each1.name!)
                                    }
                                }
                            }
                            
                            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popup") as! PopViewController
                            let modalStyle: UIModalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            popOverVC.sharValeStr = internalMessge
                            popOverVC.invalidPermissionShareArray = invalidPermissinArray
                            popOverVC.statusCodeValue = internalStatusCode
                            popOverVC.modalTransitionStyle = modalStyle
                            popOverVC.modalPresentationStyle = .overCurrentContext
                            self.present(popOverVC, animated: true, completion: nil)
                            
                        }
                        
                        
                        
                        
                        
                    }catch
                    {
                        print(error)
                        
                    }
                    //
                    //
                    
                    break
                default:
                    
                    break
                }
                break
            case .failure:
                
                break
            }
        }
        
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ask"
        {
            let popOverViewController = segue.destination
            popOverViewController.popoverPresentationController?.delegate = self
        }
        else if segue.identifier == "give"
        {
            let popOverViewController = segue.destination
            popOverViewController.popoverPresentationController?.delegate = self
        }
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
}

class PasswordTextField: UITextField {
    override func becomeFirstResponder() -> Bool {
        let wasFirstResponder = isFirstResponder
        
        let success = super.becomeFirstResponder()
        if !wasFirstResponder, let text = self.text {
            
            guard self.isSecureTextEntry == true else { return true }
            insertText("\(text)+")
            // self.text = text
            deleteBackward()
        }
        return success
        
        //        guard super.becomeFirstResponder() else { return false }
        //        guard self.isSecureTextEntry == true else { return true }
        //        guard let existingText = self.text else { return true }
        //        self.deleteBackward() // triggers a delete of all text, does NOT call delegates
        //        self.insertText(existingText) // does NOT call delegates
        //        return true
    }
}
