//
//  ReportsViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 21/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import Firebase


class ReportsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var reportTableviewView: UITableView!
    
    @IBOutlet weak var activityHistoryLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var displayDateTF: UITextField!
      var permissionlist:Permissionlist?
    
    @IBOutlet weak var calenderbUTTON: UIButton!
    var dateString:String?
    let datePicker = UIDatePicker()
    
    var memberId:Int?
    var typeArray:Array = [String]()
    var featureImageArray:Array = [UIImage]()
    var dateArray:Array = [String]()
    var timeArray:Array = [String]()
    
    var memberName:String?
    
    var reports = [Reports]()
    
    var innerReportFilterArray = [Reports]()
    var outerReportFilterArray = [Reports]()
    var reArrangeReportsArray = [Reports]()

    
      var reports1 = [Reports]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.tintColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7254901961, alpha: 1)
        
        reportTableviewView.rowHeight = UITableView.automaticDimension
        reportTableviewView.estimatedRowHeight = UITableView.automaticDimension
        
        nameLabel.text = memberName
        //
        //        typeArray = ["www.facebook.com","www.instagram.com","www.chat.com","www.fb.com"]
        //        featureImageArray = [#imageLiteral(resourceName: "globe"),#imageLiteral(resourceName: "globe"),#imageLiteral(resourceName: "globe"),#imageLiteral(resourceName: "globe")]
        //        dateArray = ["08/08/2019","08/08/2019","08/08/2019","08/08/2019"]
        //        timeArray = ["10:45PM","10:45PM","10:45PM","10:45PM"]
        //
      //  activityHistoryLabel.layer.cornerRadius = 10
     //   activityHistoryLabel.clipsToBounds = true
        
        showDatePicker()
        navigationController?.navigationBar.barTintColor = Constants.colour
        self.navigationItem.title = "About Us"
        self.changeFontForViewController()
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let result = formatter.string(from: date)
        
        //   let result11 = formatter.string(from: date)
        datePicker.maximumDate =  Date()
        dateLabel.text = result
        
        
        let date12 = Date()
        let formatter12 = DateFormatter()
        formatter12.dateFormat = "yyyy-MM-dd"
        let result12 = formatter12.string(from: date12)
        
        print(result12)
        
        //   let result11 = formatter.string(from: date)
        datePicker.maximumDate =  Date()
        
        
        let date1 = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd"
        let result1 = formatter1.string(from: date1!)
        print(result1)
        
        reportsApi(date:result12,enddate: result1)
        // Do any additional setup after loading the view.
    }
    
    
    func  reportsApi(date:String,enddate:String)
    {
        
        reports1.removeAll()
        
        SVProgressHUD.show()
        
        
        var params: [String:Any] = [:]
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        params["userId"] =  profileData.userId
        params["memberId"]  = memberId
        params["startDate"] = date
        params["endDate"] = enddate
        
        print(API.getReports)
        print(params)
        HttpWrapper.posts(with: API.getReports, parameters: params, headers: nil, completionHandler: { (response, error)  in
            
            SVProgressHUD.dismiss()
                  print(response)
            guard let data = response else { return }
            do {
                let loginRespone = try JSONDecoder().decode(ApiStatusReports.self, from: data)
                let loginStatus = loginRespone.status
                
                if loginStatus!{
                    
                    self.reports = loginRespone.response!.reports
                    print(loginRespone.response!.reports)
                    
                    for each1 in  self.reports
                    {
                        for each12 in (self.permissionlist?.ask)!
                        {
                            if each1.type == each12.roleType
                            {
                                self.reports1.append(each1)
                            }
                            
                        }
                        
                        if each1.type == "key.screen.monitor"
                            
                        {
                            
                            self.reports1.append(each1)
                            
                        }
                        if each1.type == "key.callAndSms"
                        {
                            self.reports1.append(each1)
                        }
                        
                     
                    }
                    
                    for item in self.reports1
                    {
                        if item.type  == "key.screen.monitor"
                        {
                            self.innerReportFilterArray.append(item)
                        }
                        else
                        {
                            self.outerReportFilterArray.append(item)
                        }
                    }
                    
                   self.reArrangeReportsArray = self.innerReportFilterArray + self.outerReportFilterArray
                    
                    print("Reports array objects \(self.reports1)")
                    print("Final array objects \(self.reArrangeReportsArray)")
                
                    
                    self.reportTableviewView.reloadData()
                    
                }else
                {
                    
                    self.reports1.removeAll()
                    self.reportTableviewView.reloadData()
                    
                }
            }
            catch let jsonErr {
               self.reports1.removeAll()
                self.reportTableviewView.reloadData()
                
                SVProgressHUD.dismiss()
                print(jsonErr)
                
            }
            
        }){ (error) in
            self.reports1.removeAll()
            self.reportTableviewView.reloadData()
            SVProgressHUD.dismiss()
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
        
        
        
        
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        Analytics.logEvent("Reports_Screen_Entered", parameters:nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        Analytics.logEvent("Reports_Screen_Exited", parameters:nil)
        
    }
    
    
    func showDatePicker(){
        
        Analytics.logEvent("Reports_Calendar_icon_Clicked", parameters:nil)
        //Formate Date
        datePicker.datePickerMode = .date
        
        datePicker.tintColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7254901961, alpha: 1)
        
        datePicker.maximumDate =  Date()
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        toolbar.tintColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7254901961, alpha: 1)
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        displayDateTF.inputAccessoryView = toolbar
        displayDateTF.inputView = datePicker
        
        
        
        //        let now = Date();
        //        datePicker.minimumDate = now
        
    }
    @objc func donedatePicker(){
        Analytics.logEvent("Reports_Screen_Date_Selected", parameters:nil)
        let formatter = DateFormatter()
        //   formatter.dateFormat = "MM-dd-yyyy"
        
        formatter.dateFormat = "yyyy-MM-dd"
        dateString = formatter.string(from: datePicker.date)
        
        let date1 = Calendar.current.date(byAdding: .day, value: 1, to: datePicker.date)
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd"
        let result1 = formatter.string(from: date1!)
        print(result1)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let date = dateFormatter.string(from: datePicker.date)
        
        reportsApi(date:dateString!, enddate: result1)
        dateLabel.text = date
        
        self.view.endEditing(true)
        
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        let numOfSection: NSInteger =  reports1.count
        print(reports1.count)
        
        if numOfSection == 0
        {
            
            let noDataLabel: UILabel = UILabel(frame:CGRect(x: 0, y: 0, width: self.reportTableviewView.frame.width, height: self.reportTableviewView.frame.height))
            noDataLabel.backgroundColor = .white
            noDataLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            reportTableviewView.separatorStyle = .none
            noDataLabel.text = "No reports found"
            noDataLabel.textAlignment = NSTextAlignment.center
            self.reportTableviewView.backgroundView = noDataLabel
            return 1
        }
        else
        {
            
            self.reportTableviewView.backgroundView = nil
            
            return 1
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reports1.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportsTableViewCell") as! ReportsTableViewCell
        //  cell.reportImage.image = featureImageArray[indexPath.row]
        
       print(reArrangeReportsArray.count)
        
        
        cell.typeLabel.text = reArrangeReportsArray[indexPath.row].info
        //  cell.dateLabel.text = dateArray[indexPath.row].cr
        
        if reArrangeReportsArray.count == indexPath.row + 1
        {
            cell.dottedImage.isHidden = true
        }else
        {
            cell.dottedImage.isHidden = false
        }
        //   cell.timeLabel.text = reports[indexPath.row].info
    
        if reArrangeReportsArray[indexPath.row].createdDate != nil
        {
            
            
            let unixTimeStamp: Double = Double((reArrangeReportsArray[indexPath.row].createdDate)!) / 1000.0
            
            cell.dateLabel.text = getDateFromTimeStamp(timeStamp : unixTimeStamp)
        }
        
        print(reArrangeReportsArray[indexPath.row].type)
        if reArrangeReportsArray[indexPath.row].type == "key.appsAndGames"
        {
            cell.reportImage.image = UIImage(named: "App")
             cell.callImage.image = #imageLiteral(resourceName: "popup-bg-500X200")
            cell.widthConstant.constant = 0
        
            if reArrangeReportsArray[indexPath.row].duration != nil && reArrangeReportsArray[indexPath.row].duration != ""
            {
              //  cell.durationLabel.text = "Duration : \(String(describing: reports1[indexPath.row].duration!))"
              //  cell.durationLabel.text = "Duration : \(String(describing: convertTime(miliseconds: Int(reports1[indexPath.row].duration!)!)))"
                
                var str = convertTime(miliseconds: Int(reArrangeReportsArray[indexPath.row].duration!)!)
                print(str)
            
                if str == nil || str == ""
                {
                     cell.durationLabel.text = ""
                }
                else
                {
                    cell.durationLabel.text = "Duration : \(String(describing: convertTime(miliseconds: Int(reArrangeReportsArray[indexPath.row].duration!)!)))"
                }
        
            }
            else
            {
                cell.durationLabel.text = ""
            }
            
           print(cell.durationLabel.text)
            
        }
       else if reArrangeReportsArray[indexPath.row].type == "key.callAndSms"
        {
            cell.reportImage.image = UIImage(named: "callingg")
            
            if (reArrangeReportsArray[indexPath.row].info?.contains("To"))!
            {
                cell.callImage.image = #imageLiteral(resourceName: "outgoing")
                cell.widthConstant.constant = 20

            }
            else if (reArrangeReportsArray[indexPath.row].info?.contains("From"))!
            {
                cell.callImage.image = #imageLiteral(resourceName: "incomming")
                cell.widthConstant.constant = 20
            }
            else if (reArrangeReportsArray[indexPath.row].info?.contains("Missed"))!
            {
                cell.callImage.image = #imageLiteral(resourceName: "missedcall")
                cell.widthConstant.constant = 20
            }
            else
            {
                cell.callImage.image = #imageLiteral(resourceName: "popup-bg-500X200")
                cell.widthConstant.constant = 0
                
            }
         //   cell.durationTimeLabel.text = reports1[indexPath.row].duration
            
            if reArrangeReportsArray[indexPath.row].duration != nil && reArrangeReportsArray[indexPath.row].duration != ""
            {
                cell.durationLabel.text = "Duration : \(String(describing: reArrangeReportsArray[indexPath.row].duration!))"
                
            }
            
            
//            cell.durationLabel.text = "Duration : \(String(describing: reArrangeReportsArray[indexPath.row].duration!))"
           
            
        }
        
        else if reArrangeReportsArray[indexPath.row].type == "key.web.filter"
        {
            cell.reportImage.image = UIImage(named: "webUrl")
             cell.callImage.image = #imageLiteral(resourceName: "popup-bg-500X200")
            cell.widthConstant.constant = 0
             cell.durationLabel.text = ""
            
        }
        //cell.dateLabel.text = convertTime(miliseconds: 1*60*1000)
        else if reArrangeReportsArray[indexPath.row].type == "key.screen.monitor"
        {
            cell.reportImage.image = UIImage(named: "phone")
             cell.callImage.image = #imageLiteral(resourceName: "popup-bg-500X200")
            cell.widthConstant.constant = 0
            cell.durationLabel.text = ""
            
            cell.typeLabel.text = "Device active time for the day"
            
            if reArrangeReportsArray[indexPath.row].info != nil
            {
                // let unixTimeStamp: Double = Double((reports[indexPath.row].info)!) / 1000.0
                
                cell.dateLabel.text = convertTime(miliseconds: Int(reArrangeReportsArray[indexPath.row].info!)!*60*1000)
                
                //  cell.dateLabel.text = convertTime(miliseconds: 1*60*1000)
                //getDateFromTimeStamp1(timeStamp : unixTimeStamp)
            }
        }
        
        
        
        
        
        return cell
    }
    
    func getDateFromTimeStamp(timeStamp : Double) -> String {
        
        let date = NSDate(timeIntervalSince1970: timeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        //  dayTimePeriodFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        dayTimePeriodFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
        
        //    "yyyy-MM-dd HH:mm:ss"
        
        // UnComment below to get only time
        //  dayTimePeriodFormatter.dateFormat = "hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
    
    
    func getDateFromTimeStamp1(timeStamp : Double) -> String {
        
        let date = NSDate(timeIntervalSince1970: timeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        //  dayTimePeriodFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        dayTimePeriodFormatter.dateFormat = "hh"
        
        //    "yyyy-MM-dd HH:mm:ss"
        
        // UnComment below to get only time
        //  dayTimePeriodFormatter.dateFormat = "hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //        return UITableView.automaticDimension
    //    }
    //
    
    func convertTime(miliseconds: Int) -> String {
        
        var seconds: Int = 0
        var minutes: Int = 0
        var hours: Int = 0
        var days: Int = 0
        var secondsTemp: Int = 0
        var minutesTemp: Int = 0
        var hoursTemp: Int = 0
        
        if miliseconds < 1000 {
            return ""
        } else if miliseconds < 1000 * 60 {
            seconds = miliseconds / 1000
            return "\(seconds) sec"
        } else if miliseconds < 1000 * 60 * 60 {
            secondsTemp = miliseconds / 1000
            minutes = secondsTemp / 60
            seconds = (miliseconds - minutes * 60 * 1000) / 1000
            return "\(minutes) min \(seconds) sec"
        } else if miliseconds < 1000 * 60 * 60 * 24 {
            minutesTemp = miliseconds / 1000 / 60
            hours = minutesTemp / 60
            minutes = (miliseconds - hours * 60 * 60 * 1000) / 1000 / 60
            seconds = (miliseconds - hours * 60 * 60 * 1000 - minutes * 60 * 1000) / 1000
            return "\(hours) hr \(minutes) min \(seconds) sec"
        } else {
            hoursTemp = miliseconds / 1000 / 60 / 60
            days = hoursTemp / 24
            hours = (miliseconds - days * 24 * 60 * 60 * 1000) / 1000 / 60 / 60
            minutes = (miliseconds - days * 24 * 60 * 60 * 1000 - hours * 60 * 60 * 1000) / 1000 / 60
            seconds = (miliseconds - days * 24 * 60 * 60 * 1000 - hours * 60 * 60 * 1000 - minutes * 60 * 1000) / 1000
            return "\(days) days \(hours) hr \(minutes) min \(seconds) sec"
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
}
extension String {
    
    func toDate(format: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: self)
    }
    
    func getDateFromTimeStamp(timeStamp : Double) -> String {
        
        let date = NSDate(timeIntervalSince1970: timeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        //  dayTimePeriodFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        dayTimePeriodFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
        
        //    "yyyy-MM-dd HH:mm:ss"
        
        // UnComment below to get only time
        //  dayTimePeriodFormatter.dateFormat = "hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
    
}

extension Date {
    
    func toString(format: String) -> String? {
        
        let df = DateFormatter()
        df.dateFormat = format
        return df.string(from: self)
    }
    
    func add(component: Calendar.Component, value: Int) -> Date? {
        
        return Calendar.current.date(byAdding: component, value: value, to: self)
    }
}



extension TimeInterval {
    var minuteSecondMS: String {
        return String(format:"%d:%02d.%03d", minute, second, millisecond)
    }
    var minute: Int {
        return Int((self/60).truncatingRemainder(dividingBy: 60))
    }
    var second: Int {
        return Int(truncatingRemainder(dividingBy: 60))
    }
    var millisecond: Int {
        return Int((self*1000).truncatingRemainder(dividingBy: 1000))
    }
}

extension Int {
    var msToSeconds: Double {
        return Double(self) / 1000
    }
}
