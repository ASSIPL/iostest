//
//  ReportsViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 21/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import Firebase
import GoogleMaps




class LocationHistoryVC: UIViewController,GMSMapViewDelegate{
    
    @IBOutlet weak var locationMapView: GMSMapView!
    @IBOutlet weak var displayDateTF: UITextField!
    @IBOutlet weak var calenderbUTTON: UIButton!
    var firstLocValue: CLLocationCoordinate2D? = nil
    var dateString:String?
    let datePicker = UIDatePicker()
    
    
    var arryCount: Int?
    var customInfoWindow = CustomInfoWindow()
    var tappedMarker : GMSMarker?
    
    var updatedDateInt: Double?
    
    
    typealias CompletionHandler = (_ address:String) -> Void
    
    var newReport = [NewHistoryRecord]()
    
    
    
    
    @IBOutlet weak var displayLocHisLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    var selectedItem:String?
    
    var permissionlist:Permissionlist?
    var memberName:String?
    
    var memberId:Int?
 var locationtimer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tappedMarker = GMSMarker()
        locationMapView.delegate = self
        
        
        displayLocHisLabel.layer.cornerRadius = 15
        displayLocHisLabel.clipsToBounds = true
        nameLabel.text = "Track Location History"
        displayLocHisLabel.text = "\(String(describing: memberName!))'s Location History."
        
        navigationController?.navigationBar.barTintColor = Constants.colour
        //  self.navigationItem.title = "About Us"
        self.changeFontForViewController()
        
        datePicker.tintColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7254901961, alpha: 1)
        showDatePicker()
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let fromDate = formatter.string(from: date)
        
        print(fromDate)
        
        dateLabel.text = fromDate
        
        
        //   let result11 = formatter.string(from: date)
        datePicker.maximumDate =  Date()
        
        
        
        let date12 = Date()
        let formatter12 = DateFormatter()
        formatter12.dateFormat = "yyyy-MM-dd"
        
        datePicker.maximumDate =  Date()
        
        let date1 = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd"
        let toDate = formatter1.string(from: date1!)
        print(toDate)
        
      //   locationtimer = Timer.scheduledTimer(timeInterval: 40.0, target: self, selector: #selector(locationUpdate), userInfo: nil, repeats: true)
    
        
        if SharedData.data.membersGeoList?.count ?? 0 > 0
        {
            for each in SharedData.data.membersGeoList!
            {
                if each.userId == memberId
                {
                    let lat = each.latitude ?? ""
                    let Latitude = Double(lat)
                    let lng =  each.longitude ?? ""
                    let longitude = Double(lng)
                    updatedDateInt = each.updateDate
                    
                    firstLocValue = CLLocationCoordinate2D(latitude: Latitude!, longitude:  longitude!)
                    
                    
                    
                }
            }
        }
        
        print(SharedData.data.membersGeoList!)
        
         getLocationHistoryAPI(fromDate: fromDate, toDate: toDate)
        
        
    }
    
    
        
    
    func getLocationHistoryAPI(fromDate:String,toDate:String)
        
    {
        
        SVProgressHUD.show()
        let getLocationUrl = API.getLocationHistory + "?to=\(toDate)" + "&memberId=\(memberId!)" + "&from=\(fromDate)"
        
        print(getLocationUrl)
        HttpWrapper.posts(with: getLocationUrl, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            SVProgressHUD.dismiss()
            guard let data = response else { return }
            
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatusHistrory.self, from: data)
                
                let loginStatus = loginRespone.status
                
                
                
                if loginStatus  ==  true {
                    
                    self.newReport = loginRespone.response!
                    print( self.newReport.count)
                    
                    if self.newReport.count >= 2
                    {
                        self.locationMapView.clear()
                        let rect = GMSMutablePath()
                        
                        for item in self.newReport{
                            let lat = (item.latitude! as NSString).doubleValue
                            let lon = (item.longitude! as NSString).doubleValue
                            
                            rect.add(CLLocationCoordinate2D(latitude: lat, longitude: lon))
                        }
                        let polyline = GMSPolyline(path: rect)
                        
                        polyline.strokeColor = .blue
                        
                        polyline.strokeWidth = 3.0
                        
                        polyline.map = self.locationMapView
                        let bounds = GMSCoordinateBounds(path: rect)
                        
                        let cameraUpdate = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 40, left: 15, bottom: 10, right: 15))
                        
                        self.locationMapView.animate(with: cameraUpdate)
                        
                        self.arryCount = self.newReport.count
                        
                        print("get array count \(self.arryCount)")
                        var i = 0
                        
                        for each in self.newReport
                            
                        {
                            
                            //  print(i)
                            
                            if i == 0
                            {
                                
                                let marker = GMSMarker()
                                
                                let icon = UIImage(named: "startPointLocation")
                                
                                let lat = (each.latitude! as! NSString).doubleValue
                                
                                let lon = (each.longitude! as! NSString).doubleValue
                                
                                let firstmarkPos = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                                
                                marker.position   = firstmarkPos
                                
                                marker.icon    = icon
                                
                                marker.tracksInfoWindowChanges = true
                                
                                marker.userData = each.createdDate
                                
                                marker.map = self.locationMapView
                                
                            }
                                
                            else if i == self.arryCount!-1
                                
                            {
                               
                                let marker = GMSMarker()
                                
                                let icon = UIImage(named: "endPointLocation")
                                
                                let lat = (each.latitude! as! NSString).doubleValue
                                
                                let lon = (each.longitude! as! NSString).doubleValue
                                
                                let lastmarker = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                                
                                marker.position   = lastmarker
                                
                                marker.icon    = icon
                                
                                marker.tracksInfoWindowChanges = true
                                
                                marker.userData = each.createdDate
                                
                                marker.map = self.locationMapView
                                
                            }
                                
                            else
                            {
                                
                                let marker = GMSMarker()
                                
                                 let icon = UIImage(named: "PointsDisplay")
                                
                                let lat = (each.latitude! as! NSString).doubleValue
                                
                                let lon = (each.longitude! as! NSString).doubleValue
                                
                                let lastmarker = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                            
                                marker.position = lastmarker
                                marker.icon    = icon
                                marker.tracksInfoWindowChanges = true
                                marker.userData = each.createdDate
                                
                                marker.map = self.locationMapView
                                
                            }
                            
                            i += 1
                            
                        }
                        
                 
                      //  self.locationMapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 70))
                    }
                    else
                    
                    {
                        if SharedData.data.membersGeoList?.count ?? 0 > 0
                        {
                            self.locationMapView.clear()
                            let marker = GMSMarker()
                            
                            let icon = UIImage(named: "mapLocation")
                            
                            marker.position   = self.firstLocValue!
                            
                            marker.icon    = icon
                            marker.tracksInfoWindowChanges = true
                            
                            marker.userData = self.updatedDateInt
                            
                            marker.map = self.locationMapView
                            
                            let rect = GMSMutablePath()
                            
                            let polygon = GMSPolygon()
                            var bounds1: GMSCoordinateBounds = GMSCoordinateBounds()
                         //   let Coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                            bounds1 = bounds1.includingCoordinate(self.firstLocValue!)
                            
                           //  rect.add(bounds1)
                            
                            self.locationMapView.animate(with: GMSCameraUpdate.fit(bounds1, withPadding: 70))
                        }
                        else
                        {
                             self.showAlert(title: "", msg: "User location details not updated")
                            
                        }
                      
                    }
                    
                }
                    
                else
                {
                    self.locationMapView.clear()
                    print("No coordicnates in this location")
                }
            }
                
            catch let jsonErr {
                SVProgressHUD.dismiss()
                
                print(jsonErr)
            }
        }){ (error) in
            
            
            
            SVProgressHUD.dismiss()
            
            if Reachability.isConnectedToNetwork(){
                
                print("Internet Connection Available!")
                
            }else{
                
                self.showAlert(title: "", msg: "No network connection")
                
            }
            
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        return self.customInfoWindow
        
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        if (tappedMarker != nil){
            
            
            
            customInfoWindow.center = mapView.projection.point(for: tappedMarker!.position)
            
            customInfoWindow.center.y -= 150
            
        }
        
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        customInfoWindow.removeFromSuperview()
        
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        customInfoWindow.removeFromSuperview()
        customInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)?[0] as! CustomInfoWindow
        
        self.customInfoWindow.callAction.isHidden = true
        self.customInfoWindow.messagebutton.isHidden = true
        self.customInfoWindow.directionButton.isHidden = true
        self.customInfoWindow.lineLabel.isHidden = true
        self.customInfoWindow.directionImageView.isHidden = true
        self.customInfoWindow.getDirectionsLabel.isHidden = true
        self.customInfoWindow.memberName.isHidden = true
        
        geocodeCoordinates(location: marker.position, completionHandler: { (address) -> Void in
            
            print(address)
            self.customInfoWindow.infoWindowAddress.text = address
            
            
            
        })
        
        if marker.userData != nil {
            
            
            
            print(marker.userData as! Int)
            

            
            var dobul:Double?
            
            dobul = Double(marker.userData as! Int)
  
            
            if dobul != nil
                
            {
                
                let unixTimeStamp: Double = dobul! / 1000.0
                let date = NSDate(timeIntervalSince1970:unixTimeStamp)
                let date1 = Date()
                let diff = Int(date1.timeIntervalSince1970 - date.timeIntervalSince1970)
                customInfoWindow.infoWindowTimeStamp.text = dayStringFromTime(unixTime: unixTimeStamp)
                
                
                
            }
        }
        
        
        
        tappedMarker = marker
        let position = marker.position
        
        mapView.animate(toLocation: position)
        
        let point = mapView.projection.point(for: position)
        
        let newPoint = mapView.projection.coordinate(for: point)
        
        let camera = GMSCameraUpdate.setTarget(newPoint)
        
        mapView.animate(with: camera)
        
        
        
        customInfoWindow.center = mapView.projection.point(for: position)
        
        customInfoWindow.center.y -= 150
        
        
        
        self.locationMapView.addSubview(customInfoWindow)
        
        
        
        
        
        return false
        
    }
  
    
    
    func dayStringFromTime(unixTime: Double) -> String {
        
        
        
        
        let dateFormatter = DateFormatter()
        


        let date = NSDate(timeIntervalSince1970: unixTime)
        
        dateFormatter.locale = NSLocale(localeIdentifier: NSLocale.current.identifier) as Locale
        
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
        
        return dateFormatter.string(from: date as Date)
        
    }
    

    func geocodeCoordinates(location : CLLocationCoordinate2D,completionHandler: @escaping CompletionHandler){
        
        
        
        var postalAddress  = ""
        
        
        
        let geocoder = GMSGeocoder()
        
        geocoder.reverseGeocodeCoordinate(location, completionHandler: {response,error in
            
            if let gmsAddress = response!.firstResult(){
                
                
                
                if  (gmsAddress.lines?.count)! > 0
                    
                {
                    
                    postalAddress += gmsAddress.lines![0]
                    
                }
                
                //                for line in  gmsAddress.lines! {
                
                //                    postalAddress += line + " "
                
                //                }
                
                completionHandler(postalAddress)
                
                print(postalAddress)
                
                
                
                return
                
            }
            
        })
        
        return
        
    }

    
    
    func showDatePicker(){
        
        
        //Formate Date
        datePicker.datePickerMode = .date
        
        datePicker.tintColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7254901961, alpha: 1)
        
        datePicker.maximumDate =  Date()
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        toolbar.tintColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7254901961, alpha: 1)
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        displayDateTF.inputAccessoryView = toolbar
        displayDateTF.inputView = datePicker
        
        
        
        //        let now = Date();
        //        datePicker.minimumDate = now
        
    }
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        //   formatter.dateFormat = "MM-dd-yyyy"
        
        formatter.dateFormat = "yyyy-MM-dd"
        dateString = formatter.string(from: datePicker.date)
        
        let date1 = Calendar.current.date(byAdding: .day, value: 1, to: datePicker.date)
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd"
        let toDate = formatter.string(from: date1!)
        print(toDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let fromDate = dateFormatter.string(from: datePicker.date)
        
        print(fromDate)
        dateLabel.text = fromDate
        
        getLocationHistoryAPI(fromDate: fromDate, toDate: toDate)
        
        self.view.endEditing(true)
        
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        Analytics.logEvent("Lochistory_Screen_Entered", parameters:nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        Analytics.logEvent("Lochistory__Screen_Exited", parameters:nil)
        
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    

    
}

