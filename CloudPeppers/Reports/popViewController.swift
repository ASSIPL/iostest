//
//  popViewController.swift
//  popOver
//
//  Created by Ashok Reddy G on 16/09/19.
//  Copyright © 2019 Ashok Reddy G. All rights reserved.
//

import UIKit

class popViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet var tableView: UITableView!
    
    var fromContacts:String?
     //  var delegate: Deletesdates?
      var delegate3:Deletescontacts?
    var selectedItem:String?
    var dayArray:Array = [String]()
    let dateFormatter = DateFormatter()
    
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        tableView.tableFooterView = UIView()
     
        tableView.dataSource = self
           tableView.delegate = self
        
        self.navigationController?.navigationBar.isHidden = true
        
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        
        if  fromContacts == "true"
        {
            dayArray = ["Contact","Area Code"]
            
        }else
        {
            dayArray = ["Last 7 days","Last 15 days","Last Month"]
            
        }

        
 
    
      
        // Do any additional setup after loading the view.
    }

    
    
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dayArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = tableView.dequeueReusableCell(withIdentifier: "PopOverTableViewCell") as! PopOverTableViewCell
        
        
        cell.label.text = dayArray[indexPath.row]
        
        
        
        return cell
        
        
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         selectedItem = dayArray[indexPath.row]
        print(selectedItem!)
        
        
        
        if  fromContacts == "true"
        {
            
             delegate3?.didSelectdate(indexPath.row)
        }else
        {
            
        }
        
        
       
        
         dismiss(animated: true, completion: nil)
        
        
    }
    
    


}


