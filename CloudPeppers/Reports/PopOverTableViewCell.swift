//
//  TableViewCell.swift
//  popOver
//
//  Created by Ashok Reddy G on 16/09/19.
//  Copyright © 2019 Ashok Reddy G. All rights reserved.
//

import UIKit

class PopOverTableViewCell: UITableViewCell {

    @IBOutlet var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
