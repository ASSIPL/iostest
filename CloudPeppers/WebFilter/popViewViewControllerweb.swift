//
//  popViewViewControllerweb.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 08/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class popViewViewControllerweb: UIViewController,UITextFieldDelegate {

    
    @IBOutlet weak var popUpHeadTextLb: UILabel!
    
    @IBOutlet var bAButton: UIButton!
    @IBOutlet var urlname: UITextField!
    
  var url: String!
     var delegate2: customDelegate2?
      var allowedorblock:String?
    var memberId:Int?
    var userId:Int?
    var loginType:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if allowedorblock == "block"
        {
            popUpHeadTextLb.text = "Do you want to add the website to blocked list?"
            
        }else
        {
            popUpHeadTextLb.text = "Do you want to add the website to allowed list?"
            
        }
       
        urlname.useUnderline()
     //   url.useUnderline()
        urlname.delegate = self
      //  url.delegate = self
        
        print(memberId)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        
        urlname.delegate = self
    //    url.delegate = self
        // Do any additional setup after loading the view.
    }
    
    
    
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        if  textField == urlname
//        {
//            url.becomeFirstResponder()
//        }
//        
//        if  textField == url
//        {
//            url.resignFirstResponder()
//        }
//        return false
//    }
    
    @IBAction func cancel(_ sender: Any) {
        
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
    @IBAction func segmentController(_ sender: UIButton) {
        
        // if sender.selectedSegmentIndex
//        if sender.selectedSegmentIndex == 0 {
//
//            allowedorblock = "block"
//        }
//        else {
//            allowedorblock = "allow"
//
//        }
        
    }
    
    @IBAction func add(_ sender: Any) {
        
        if urlname.text!.isEmpty || urlname.text == ""
        {
            self.showAlert(title: "", msg: "Invalid title")
            
            return
        }
        
//        if url.text!.isEmpty || url.text == ""
//        {
//            self.showAlert(title: "", msg: "Invalid URL")
//
//            return
//        }
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(urlname.text, forKey: "title")
        parameter.setValue(url, forKey: "url")
        parameter.setValue(self.userId, forKey: "userId")
        parameter.setValue(self.memberId, forKey: "memberId")
        parameter.setValue(self.loginType, forKey: "typeOfUser")
        parameter.setValue(allowedorblock, forKey: "allowdOrBloked")
        print(parameter)
        print(API.saveURL)
        APIServices.postDataToServer(url: API.saveURL, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                self.view.removeFromSuperview()
                self.removeFromParent()
                self.delegate2?.callApi()
                print(response)
//                self.urlName.text = nil
//                self.url.text = nil
//                self.callgetMemberApi()
                let  respons = response as? [String:Any]
                
                self.showAlert(title: "", msg: respons?["message"] as! String)
                
            }else
            {
                
                
                
            }
        })
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
