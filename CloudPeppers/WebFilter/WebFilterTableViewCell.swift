//
//  WebFilterTableViewCell.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 20/07/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class WebFilterTableViewCell: UITableViewCell {
    
   
    
    @IBOutlet weak var disView: RoundUIView!
    @IBOutlet var selectButton: UIButton!
    @IBOutlet var urlName: UILabel!
    @IBOutlet var url: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
