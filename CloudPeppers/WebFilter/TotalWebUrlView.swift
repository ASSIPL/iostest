//
//  TotalWebUrlView.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 20/07/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SVProgressHUD

class TotalWebUrlView: UIViewController, UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {
    @IBOutlet var callsTableView: UITableView!
    
    //var approve1:[[allowed]]?
    var searchActive : Bool = false
    var blocked:[allow]?
    
    @IBOutlet var searchBar: UISearchBar!
    var approve1:[allow]?
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        
        
        
        
//        callsTableView.register(UINib(nibName: "WebFilterTableViewCell", bundle: nil), forCellReuseIdentifier: "WebFilterTableViewCell")
//        callsTableView.dataSource = self
//        callsTableView.delegate = self
//        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("Totalcallsweb"), object: nil)
//        
        // Do any additional setup after loading the view.
        
        
        
        
        
        SVProgressHUD.show()
        print(API.webUrls)
        
        HttpWrapper.gets(with: API.webUrls , parameters: nil, headers: nil, completionHandler: { (response, error)  in
            print(response)
            
            SVProgressHUD.dismiss()
            
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatusWebUrl.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    
                    
                    self.approve1 = loginRespone.response
                    
                   
                    self.callsTableView.reloadData()
                    // self.familycollectionView.reloadData()
                }
                else
                    
                {
                    
                }
                
            }catch let jsonErr {
                SVProgressHUD.dismiss()
                
                print("Error serializing json:", jsonErr)
                
            }
        }){ (error) in
            SVProgressHUD.dismiss()
            print(error)
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
        
        
        
        
        
        
        
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = false
        //  callsTableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        callsTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        
        searchBar.text = nil
        searchBar.resignFirstResponder()
        callsTableView.reloadData()
        
        self.searchBar.showsCancelButton = false
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.searchActive = true;
        self.searchBar.showsCancelButton = true
        
        
        blocked = approve1?.filter{ $0.url!.localizedStandardContains(searchText) || $0.title!.localizedStandardContains(searchText) }
        
        callsTableView.reloadData()
        
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        var approve2 = [allow]()
        
        
        for each in (SharedData.data.responseWebUrl?.allow)!
        {
            approve2.append(each)
            
            print(approve2.count)
            
        }
        
        for each in (SharedData.data.responseWebUrl?.block)!
        {
            approve2.append(each)
        }
        
        approve1 = approve2
        callsTableView.reloadData()
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive){
            return blocked?.count ?? 0
            
        }
        return approve1?.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger =  approve1?.count ?? 0
        if(searchActive){
            numOfSection = blocked?.count ?? 0
        }
        
        
        if numOfSection == 0
        {
            
            let noDataLabel: UILabel = UILabel(frame:CGRect(x: 0, y: 0, width: self.callsTableView.frame.width, height: self.callsTableView.frame.height))
            noDataLabel.backgroundColor = .white
            noDataLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            callsTableView.separatorStyle = .none
           // noDataLabel.text = "No URL(s) found"
            noDataLabel.text = ""
            noDataLabel.textAlignment = NSTextAlignment.center
            self.callsTableView.backgroundView = noDataLabel
            return 1
        }
        else
        {
            
            self.callsTableView.backgroundView = nil
            
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WebFilterTableViewCell", for: indexPath) as! WebFilterTableViewCell
        
        
        if(searchActive){
            cell.urlName.text = blocked?[indexPath.row].title
            cell.url.text =  blocked?[indexPath.row].url
            
          //  cell.buttonWidthLayout.constant = 0
            cell.selectButton.isHidden = true
            
        }else
        {
            cell.urlName.text = approve1?[indexPath.row].title
            cell.url.text =  approve1?[indexPath.row].url
            
           // cell.buttonWidthLayout.constant = 0
            cell.selectButton.isHidden = true
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 75.0
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
