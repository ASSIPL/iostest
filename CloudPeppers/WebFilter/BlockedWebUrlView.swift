//
//  BlockedWebUrlView.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 20/07/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

class BlockedWebUrlView: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UIPopoverPresentationControllerDelegate {
    var selectedArray = [String]()
    var memberId:Int?
    var userId:Int?
     var loginType:String?
    //  var responseappsGames:responseappsGames?
    var approve:[[allow]]?
    var blocked:[allow]?
    var searchActive : Bool = false
    
    @IBOutlet var searchBar: UISearchBar!
    
    @IBOutlet weak var unblockButton: UIButton!
    @IBOutlet var callsTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        
        callsTableView.register(UINib(nibName: "WebFilterTableViewCell", bundle: nil), forCellReuseIdentifier: "WebFilterTableViewCell")
        callsTableView.dataSource = self
        callsTableView.delegate = self
        unblockButton.changeButtonCornerRadius()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("Blockedcallsweb"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "block"
        {
            let popOverViewController = segue.destination
            popOverViewController.popoverPresentationController?.delegate = self
        }
       
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = false
        //   callsTableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        callsTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        
        searchBar.text = nil
        searchBar.resignFirstResponder()
        callsTableView.reloadData()
        
        self.searchBar.showsCancelButton = false
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.searchActive = true;
        self.searchBar.showsCancelButton = true
        
        
        blocked = SharedData.data.responseWebUrl?.block?.filter{ $0.url!.localizedStandardContains(searchText) || $0.title!.localizedStandardContains(searchText) }
        
        callsTableView.reloadData()
        
    }
    
    
    @IBAction func UnBlockAction(_ sender: UIButton) {
        
        Analytics.logEvent("WebFilter_Screen_UnblockedButton_Clicked", parameters:nil)
//        
//        if selectedArray.isEmpty || selectedArray.count == 0
//        {
//            showAlert(title: "", msg: "Please select at least one URL to unblock")
//            return
//        }
//        
//        
        
        //    let alertController = UIAlertController(title: Constants.alertTitle, message: "Are you sure want to unblock the selected contact(s)?", preferredStyle: .alert)
        
        
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        
        let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
        let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
        
        let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
        let msgAttrString = NSMutableAttributedString(string: "Are you sure want to unblock the selected Website(s)?", attributes: msgFont)
        
        alert.setValue(titAttrString, forKey: "attributedTitle")
        alert.setValue(msgAttrString, forKey: "attributedMessage")
        
        
        let cancelAction = UIAlertAction(title: "NO", style: .cancel, handler: { (action : UIAlertAction!) -> Void in })
        
        let saveAction = UIAlertAction(title: "YES", style: .default, handler: { alert -> Void in
            
            //  let parameter:NSMutableDictionary = NSMutableDictionary()
            
            //  parameter.setValue(self.memberId, forKey: "memberId")
            
            
            let innerRequestList:NSMutableDictionary = NSMutableDictionary()
            
            //             innerRequestList.setValue(each.bwfid, forKey: "bwfid")
            
            
            if(self.searchActive){
                
                
                
                innerRequestList.setValue(self.blocked?[sender.tag].title, forKey: "title")
                innerRequestList.setValue(self.blocked?[sender.tag].url, forKey: "url")
                
                //                innerRequestList.setValue(blocked?[indexPath.row].bwfid, forKey: "bwfid")
                innerRequestList.setValue(self.blocked?[sender.tag].webFilterLKPId, forKey: "webFilterLKPId")
                
                
                   innerRequestList.setValue(self.blocked?[sender.tag].bwfid, forKey: "bwfid")
                
                innerRequestList.setValue(self.blocked?[sender.tag].addedBy, forKey: "addedBy")
                
            }else
            {
                
                
                
                
                innerRequestList.setValue(SharedData.data.responseWebUrl?.block?[sender.tag].title, forKey: "title")
                innerRequestList.setValue(SharedData.data.responseWebUrl?.block?[sender.tag].url, forKey: "url")
                     innerRequestList.setValue(SharedData.data.responseWebUrl?.block?[sender.tag].bwfid, forKey: "bwfid")
                
                //                innerRequestList.setValue(approve1?[indexPath.row].bwfid, forKey: "bwfid")
                innerRequestList.setValue(SharedData.data.responseWebUrl?.block?[sender.tag].webFilterLKPId, forKey: "webFilterLKPId")
                
                innerRequestList.setValue(SharedData.data.responseWebUrl?.block?[sender.tag].addedBy, forKey: "addedBy")
                
                
            }
            //                innerRequestList.setValue(each.webFilterLKPId, forKey: "webFilterLKPId")
            
            
            
            if self.loginType == "kid.type"
            {
                
                innerRequestList.setValue("block", forKey: "allowdOrBloked")
            }
            if self.loginType == "member.type" || self.loginType == "parent.type"
            {
                
                innerRequestList.setValue("allow", forKey: "allowdOrBloked")
            }
            
            
          
            innerRequestList.setValue(self.loginType, forKey: "typeOfUser")
            
            innerRequestList.setValue(self.userId, forKey: "userId")
            innerRequestList.setValue(self.memberId, forKey: "memberId")
            
            //  permissionRequestList.add(innerRequestList)
            
            
            
            //   parameter.setValue(permissionRequestList, forKeyPath: "contacts")
            //       print(parameter)
            let parameters: [String: Any] = innerRequestList as! [String : Any]
            
            print(parameters)
            
            
            let innerRequestList1:NSMutableDictionary = NSMutableDictionary()
            
            //  innerRequestList1.addEntries(from: parameters)
            
            innerRequestList1.setDictionary(parameters)
            print(innerRequestList1)
            
            let innerRequestListarray:NSMutableArray = NSMutableArray()
            
            innerRequestListarray.add(innerRequestList1)
            print(innerRequestListarray)
            
            guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                let profileData = try? JSONDecoder().decode(profile.self, from: data) else
            {
                
                return
                
            }
            let updatingURLs = API.updatingURLs + "?userId=\(String(describing: profileData.userId!))" + "&memberId=\(self.memberId!)"
            var request = URLRequest(url: URL(string: updatingURLs)!)
            request.httpMethod = "POST"
            let id = UserDefaults.standard.string(forKey: "jwtToken")
            
            if id != nil && id != ""
            {
                let token = "Bearer" + " "  + "\(UserDefaults.standard.string(forKey: "jwtToken")!)"
                
                print(token)
                request.setValue(token, forHTTPHeaderField: "Authorization")
            }
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let dataToSync = innerRequestListarray
            
            
            request.httpBody = try! JSONSerialization.data(withJSONObject: dataToSync)
            
            print(request)
            
            Alamofire.request(request).responseJSON { (response) in
                
                switch response.result{
                case .success:
                    let statusCode: Int = (response.response?.statusCode)!
                    switch statusCode{
                    case 200:
                        
                        guard let data = response.data else { return }
                        do {
                            let loginRespone =  try JSONDecoder().decode(ApiStatus1.self, from: data)
                            
                            let loginStatus = loginRespone.status
                            
                            
                            if loginStatus!
                            {
                                
                                NotificationCenter.default.post(name: Notification.Name("internalApiweb"), object: nil)
                                
                                
                                
                                NotificationCenter.default.post(name: Notification.Name("callApi1web"), object: nil)
                                
                                //                            let vc = MainAppsViewController()
                                //
                                //                            vc.memberId = self.memberId
                                //                            vc.userId = self.userId
                                //                             vc.callgetMemberApi()
                                
//                                let alertController = UIAlertController(title: "", message: loginRespone.message!, preferredStyle: .alert)
//                                let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
//                                }
//
//                                alertController.addAction(settingsAction)
//                                self.present(alertController, animated: true, completion: nil)
//
                            }
                            else
                            {
                                
                                let alertController = UIAlertController(title: "", message: loginRespone.message!, preferredStyle: .alert)
                                let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel) { (UIAlertAction) in
                                }
                                alertController.addAction(settingsAction)
                                self.present(alertController, animated: true, completion: nil)
                            }
                            
                        }catch
                        {
                            print(error)
                            
                            
                        }
                        
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure:
                    
                    break
                }
            }
            
            
            
        })
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        self.present(alert, animated: true, completion: nil)
        
        
        
        
    }
    
    
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        selectedArray.removeAll()
        callsTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        callsTableView.reloadData()
        
        print("First VC will appear")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive){
            return blocked?.count ?? 0
            
        }
        if SharedData.data.responseWebUrl?.block?.count == 0
        {
            unblockButton.isHidden = true
            
        }else{
            unblockButton.isHidden = true
            
        }
        return SharedData.data.responseWebUrl?.block?.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger =  SharedData.data.responseWebUrl?.block?.count ?? 0
        if(searchActive){
            numOfSection = blocked?.count ?? 0
        }
        
        
        if numOfSection == 0
        {
            
            let noDataLabel: UILabel = UILabel(frame:CGRect(x: 0, y: 0, width: self.callsTableView.frame.width, height: self.callsTableView.frame.height))
            noDataLabel.backgroundColor = .white
            noDataLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            callsTableView.separatorStyle = .none
            noDataLabel.text = "No website added to blocked list"
            noDataLabel.textAlignment = NSTextAlignment.center
            self.callsTableView.backgroundView = noDataLabel
            return 1
        }
        else
        {
            
            self.callsTableView.backgroundView = nil
            
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WebFilterTableViewCell", for: indexPath) as! WebFilterTableViewCell
        
        
        
        if(searchActive){
            // return allowed?.count ?? 0
            
            cell.urlName.text = blocked?[indexPath.row].title
            cell.url.text = blocked?[indexPath.row].url
            cell.selectButton.tag = indexPath.row
            
//            if selectedArray.count > 0
//            {
//                if self.selectedArray.contains((blocked?[indexPath.row].url)!)
//                {
//                    cell.selectButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
//                }else
//                {
//                    cell.selectButton.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
//                }
//            }
            
            
        }else
        {
            cell.urlName.text = SharedData.data.responseWebUrl?.block?[indexPath.row].title
            cell.url.text = SharedData.data.responseWebUrl?.block?[indexPath.row].url
            cell.selectButton.tag = indexPath.row
//            if self.selectedArray.contains((SharedData.data.responseWebUrl?.block?[indexPath.row].url)!)
//                
//            {
//                
//                cell.selectButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
//                
//                
//            }else
//            {
//                
//                cell.selectButton.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
//                
//                
//            }
        }
        
        
        cell.selectButton.addTarget(self, action: #selector(UnBlockAction), for: .touchUpInside)
        return cell
    }
    @objc func buttonAction(sender: UIButton!) {
        
        
        if(searchActive){
            
            
            if self.selectedArray.contains((blocked?[sender.tag].url)!) {
                self.selectedArray.remove(at: self.selectedArray.index(of: (blocked?[sender.tag].url)!)!)
            } else {
                self.selectedArray.append((blocked?[sender.tag].url)!)
            }
            
            
        }else
        {
            if self.selectedArray.contains((SharedData.data.responseWebUrl?.block?[sender.tag].url)!) {
                self.selectedArray.remove(at: self.selectedArray.index(of: (SharedData.data.responseWebUrl?.block?[sender.tag].url)!)!)
            } else {
                self.selectedArray.append((SharedData.data.responseWebUrl?.block?[sender.tag].url)!)
            }
            
        }
        
        
        callsTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 75.0
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


