//
//  AddWebUrlViewController.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 09/10/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SVProgressHUD
import Alamofire

class AddWebUrlViewController: UIViewController, UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,customDelegate2 {
   
    @IBOutlet weak var addWebsiteHeightCon: NSLayoutConstraint!
    @IBOutlet weak var addWebSiteButton: UIButton!
    @IBOutlet var callsTableView: UITableView!
    var memberId:Int?
    var userId:Int?
    var loginType:String?
      var popUpVC  = popViewViewControllerweb()
       var delegate2: customDelegate2?
    //var approve1:[[allowed]]?
    var searchActive : String? = "false"
    var blocked:[allow]?
    
    @IBOutlet var searchBar: UISearchBar!
    var approve1:[allow]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addWebSiteButton.isHidden = true
        addWebsiteHeightCon.constant = 0
        
        searchBar.delegate = self
        
        callsTableView.delegate = self
        callsTableView.dataSource = self
        
        
                callsTableView.register(UINib(nibName: "WebFilterTableViewCell", bundle: nil), forCellReuseIdentifier: "WebFilterTableViewCell")
        //        callsTableView.dataSource = self
        //        callsTableView.delegate = self
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("Totalcallsweb"), object: nil)
        //
        // Do any additional setup after loading the view.
        
        
        
        
        
        SVProgressHUD.show()
        print(API.webUrls)
        
        HttpWrapper.gets(with: API.webUrls , parameters: nil, headers: nil, completionHandler: { (response, error)  in
            print(response)
            
            SVProgressHUD.dismiss()
            
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatusWebUrl.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    
                    
                    self.approve1 = loginRespone.response
                    print(self.approve1)
                    
                    self.callsTableView.reloadData()
                    // self.familycollectionView.reloadData()
                }
                else
                    
                {
                    
                }
                
            }catch let jsonErr {
                SVProgressHUD.dismiss()
                
                print("Error serializing json:", jsonErr)
                
            }
        }){ (error) in
            SVProgressHUD.dismiss()
            print(error)
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
        
        
        
        
        
        
        
    }
    func callApi() {
        self.delegate2?.callApi()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addWebUrlNew(_ sender: Any) {
        
        if searchBar.text == nil || searchBar.text!.isEmpty
        {
             self.showAlert(title: "", msg: "Search field can't be empty")
            return
        }
        
        
                popUpVC = storyboard?.instantiateViewController(withIdentifier: "popViewViewControllerweb") as! popViewViewControllerweb
                popUpVC.memberId = memberId
        popUpVC.url = searchBar.text
        
        
        if self.loginType == "kid.type"
        {
              popUpVC.allowedorblock = "allow"
            
           
        }
        if self.loginType == "member.type" || self.loginType == "parent.type"
        {
            
              popUpVC.allowedorblock = "block"
            
        }
        
      
         // allowedorblock = "block"
        
                popUpVC.userId = userId
        
                popUpVC.loginType = loginType
               popUpVC.delegate2 = self
        
        
                self.view.addSubview(popUpVC.view)
                self.addChild(popUpVC)
                
        
        
    }
    
    
    
    
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = false
//        //  callsTableView.reloadData()
//    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchActive == "true"
        {
            searchActive = "true"
        }else
        {
            searchActive = "false"
        }    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = "false";
        
        searchBar.text = nil
        searchBar.resignFirstResponder()
        callsTableView.reloadData()
        
        self.searchBar.showsCancelButton = false
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = "false"
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.searchActive = "true";
        self.searchBar.showsCancelButton = true
        
        
        blocked = approve1?.filter{ $0.url!.localizedStandardContains(searchText) || $0.title!.localizedStandardContains(searchText) }
        
        callsTableView.reloadData()
        
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        var approve2 = [allow]()
        
        
        for each in (SharedData.data.responseWebUrl?.allow)!
        {
            approve2.append(each)
            
            print(approve2.count)
            
        }
        
        for each in (SharedData.data.responseWebUrl?.block)!
        {
            approve2.append(each)
        }
        
        approve1 = approve2
        callsTableView.reloadData()
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive == "true"){
            return blocked?.count ?? 0
            
        }
        return approve1?.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger =  approve1?.count ?? 0
        if(searchActive == "true"){
            numOfSection = blocked?.count ?? 0
        }
        
        
        if numOfSection == 0
        {
            
            addWebSiteButton.isHidden  = false
            addWebsiteHeightCon.constant = 40
            
            let noDataLabel: UILabel = UILabel(frame:CGRect(x: 0, y: 0, width: self.callsTableView.frame.width, height: self.callsTableView.frame.height))
            noDataLabel.backgroundColor = .white
            noDataLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            callsTableView.separatorStyle = .none
         //   noDataLabel.text = "No URL(s) found"
            noDataLabel.text = ""
            
            noDataLabel.textAlignment = NSTextAlignment.center
            self.callsTableView.backgroundView = noDataLabel
            return 1
        }
        else
        {
            addWebSiteButton.isHidden = true
            addWebsiteHeightCon.constant = 0
            self.callsTableView.backgroundView = nil
            
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WebFilterTableViewCell", for: indexPath) as! WebFilterTableViewCell
        
        
        if(searchActive ==  "true"){
            cell.urlName.text = blocked?[indexPath.row].title
            cell.url.text =  blocked?[indexPath.row].url
            
            //  cell.buttonWidthLayout.constant = 0
            cell.selectButton.isHidden = true
            
        }else
        {
            cell.urlName.text = approve1?[indexPath.row].title
            cell.url.text =  approve1?[indexPath.row].url
            
            // cell.buttonWidthLayout.constant = 0
            cell.selectButton.isHidden = true
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 75.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
       
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        
        let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
        let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
        
        let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
        let msgAttrString:NSMutableAttributedString?
        
        if self.loginType == "kid.type"
        {
             msgAttrString = NSMutableAttributedString(string: "Are you sure want to allow the selected Website(s)?", attributes: msgFont)
        }
        else{
               msgAttrString = NSMutableAttributedString(string: "Are you sure want to block the selected Website(s)?", attributes: msgFont)
        }
        
        
        alert.setValue(titAttrString, forKey: "attributedTitle")
        alert.setValue(msgAttrString, forKey: "attributedMessage")
        
        let cancelAction = UIAlertAction(title: "NO", style: .cancel, handler: { (action : UIAlertAction!) -> Void in })
        
        let saveAction = UIAlertAction(title: "YES", style: .default, handler: { alert -> Void in
            
            let innerRequestList:NSMutableDictionary = NSMutableDictionary()
            
            //             innerRequestList.setValue(each.bwfid, forKey: "bwfid")
            
            
            if(self.searchActive == "true"){
                
                
                
                innerRequestList.setValue(self.blocked?[indexPath.row].title, forKey: "title")
                innerRequestList.setValue(self.blocked?[indexPath.row].url, forKey: "url")
                
                //                innerRequestList.setValue(blocked?[indexPath.row].bwfid, forKey: "bwfid")
                innerRequestList.setValue(self.blocked?[indexPath.row].webFilterLKPId, forKey: "webFilterLKPId")
                
                innerRequestList.setValue("web", forKey: "addedBy")
                
            }else
            {
                
                
                
                
                innerRequestList.setValue(self.approve1?[indexPath.row].title, forKey: "title")
                innerRequestList.setValue(self.approve1?[indexPath.row].url, forKey: "url")
                
                
                //                innerRequestList.setValue(approve1?[indexPath.row].bwfid, forKey: "bwfid")
                innerRequestList.setValue(self.approve1?[indexPath.row].webFilterLKPId, forKey: "webFilterLKPId")
                
                innerRequestList.setValue("web", forKey: "addedBy")
                
                
            }
            //                innerRequestList.setValue(each.webFilterLKPId, forKey: "webFilterLKPId")
            
            print(self.loginType)
            
            if self.loginType == "kid.type"
            {
                
                innerRequestList.setValue("allow", forKey: "allowdOrBloked")
            }
            if self.loginType == "member.type" || self.loginType == "parent.type"
            {
                
                innerRequestList.setValue("block", forKey: "allowdOrBloked")
            }
            
            
            
            innerRequestList.setValue(self.loginType, forKey: "typeOfUser")
            
            innerRequestList.setValue(self.userId, forKey: "userId")
            innerRequestList.setValue(self.memberId, forKey: "memberId")
            
            //  permissionRequestList.add(innerRequestList)
            
            
            
            //   parameter.setValue(permissionRequestList, forKeyPath: "contacts")
            //       print(parameter)
            let parameters: [String: Any] = innerRequestList as! [String : Any]
            
            print(parameters)
            
            
            let innerRequestList1:NSMutableDictionary = NSMutableDictionary()
            
            //  innerRequestList1.addEntries(from: parameters)
            
            innerRequestList1.setDictionary(parameters)
            print(innerRequestList1)
            
            let innerRequestListarray:NSMutableArray = NSMutableArray()
            
            innerRequestListarray.add(innerRequestList1)
            print(innerRequestListarray)
            
            //        if(searchActive){
            //
            //
            //            parameter.setValue(blocked?[indexPath.row].title, forKey: "title")
            //            parameter.setValue(blocked?[indexPath.row].url, forKey: "url")
            //
            //
            //
            //        }else
            //        {
            //
            //            parameter.setValue(approve1?[indexPath.row].title, forKey: "title")
            //            parameter.setValue(approve1?[indexPath.row].url, forKey: "url")
            //
            //
            //        }
            //        parameter.setValue(self.userId, forKey: "userId")
            //        parameter.setValue(self.memberId, forKey: "memberId")
            //        parameter.setValue(self.loginType, forKey: "typeOfUser")
            //
           // print(loginType)
            
            
            guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                let profileData = try? JSONDecoder().decode(profile.self, from: data) else
            {
                
                return
                
            }
            let updatingURLs = API.updatingURLs + "?userId=\(String(describing: profileData.userId!))" + "&memberId=\(self.memberId!)"
            
            
            
            var request = URLRequest(url: URL(string: updatingURLs)!)
            request.httpMethod = "POST"
            let id = UserDefaults.standard.string(forKey: "jwtToken")
            
            if id != nil && id != ""
            {
                let token = "Bearer" + " "  + "\(UserDefaults.standard.string(forKey: "jwtToken")!)"
                
                print(token)
                request.setValue(token, forHTTPHeaderField: "Authorization")
            }
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let dataToSync = innerRequestListarray
            
            print(dataToSync)
            request.httpBody = try! JSONSerialization.data(withJSONObject: dataToSync)
            
            print(request)
            
            Alamofire.request(request).responseJSON { (response) in
                
                switch response.result{
                case .success:
                    let statusCode: Int = (response.response?.statusCode)!
                    switch statusCode{
                    case 200:
                        
                        guard let data = response.data else { return }
                        do {
                            let loginRespone =  try JSONDecoder().decode(ApiStatus1.self, from: data)
                            
                            let loginStatus = loginRespone.status
                            
                            
                            if loginStatus!
                            {
                                
                                self.delegate2?.callApi()
                                self.navigationController?.popViewController(animated: true)
                                
                            }
                            else
                            {
                                self.showAlert(title: "", msg: loginRespone.message ?? "")
                            }
                            
                        }catch
                        {
                            
                            
                            
                        }
                        
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure:
                    
                    break
                }
            }
            
            
            
        
        })
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        self.present(alert, animated: true, completion: nil)
            
        
        
        
        
   
    
    
    
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
