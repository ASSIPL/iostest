//
//  MainWebFilterViewController.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 20/07/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase


protocol customDelegate2: class {
    func callApi()
    
    
    
}



class MainWebFilterViewController: UIViewController,UITextFieldDelegate,customDelegate2{
    
    
    @IBOutlet var segmentController: UISegmentedControl!
    
    
    @IBOutlet var urlName: UITextField!
    
    
    @IBOutlet var url: UITextField!
    
    
    
    @IBOutlet var addUrlView: UIView!
    
    var allowedorblock:String?
    
    var popUpVC  = popViewViewControllerweb()
    
    @IBOutlet var blockedView: UIView!
    @IBOutlet var blockedNumbers: UILabel!
    @IBOutlet var blockedText: UILabel!
    @IBOutlet var allowedView: UIView!
    @IBOutlet var allowedText: UILabel!
    @IBOutlet var allowedNumber: UILabel!
    @IBOutlet var totalView: UIView!
    @IBOutlet var totalNumber: UILabel!
    @IBOutlet var totalText: UILabel!
    var titleName:String?
    @IBOutlet weak var contentView: UIView!
    enum TabIndex : Int {
        case firstChildTab = 0
        case secondChildTab = 1
        case ThirdViewController = 2
    }
    var memberId:Int?
    var userId:Int?
    var loginType:String?
    
    var currentViewController: UIViewController?
    lazy var firstChildTabVC: UIViewController? = {
        let firstChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "BlockedWebUrlView") as? BlockedWebUrlView
        firstChildTabVC?.memberId = memberId
        firstChildTabVC?.userId = userId
          firstChildTabVC?.loginType = loginType
        
        
        
        return firstChildTabVC
    }()
    lazy var secondChildTabVC : UIViewController? = {
        let secondChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "UnblockedWebUrlView") as? UnblockedWebUrlView
        
        secondChildTabVC?.memberId = memberId
        secondChildTabVC?.userId = userId
         secondChildTabVC?.loginType = loginType
        
        return secondChildTabVC
    }()
    
    lazy var thirdViewdTabVC : UIViewController? = {
        let ThirdViewController = self.storyboard?.instantiateViewController(withIdentifier: "TotalWebUrlView") as? TotalWebUrlView
        
        return ThirdViewController
    }()
        var rightButtonItem1:UIBarButtonItem?
    override func viewDidLoad() {
        
        urlName.useUnderline()
        url.useUnderline()
        
      //  segmentController.layer.cornerRadius = 10
        segmentController.clipsToBounds = true
        
           rightButtonItem1 = UIBarButtonItem.init(image: #imageLiteral(resourceName: "whitePlus"), style: .done, target: self, action: #selector(rightButtonAction(sender:)))
         self.navigationItem.rightBarButtonItem = rightButtonItem1
        
          allowedorblock = "block"
        
        self.addUrlView.center.x = self.view.center.x
        self.addUrlView.center.y = self.view.center.y
        addUrlView.backgroundColor = UIColor.black
        
        addUrlView.frame = self.view.frame
        addUrlView.isHidden = true
        
        //    blockedView.layer.cornerRadius = 5
        //   blockedView.clipsToBounds = true
        
        //        allowedView.layer.cornerRadius = 5
        //        allowedView.clipsToBounds = true
        
        //   totalView.layer.cornerRadius = 5
        //    totalView.clipsToBounds = true
        
        super.viewDidLoad()
        
//        IQKeyboardManager.shared.enable = false
//        IQKeyboardManager.shared.enableAutoToolbar = false
        
        IQKeyboardManager.shared.layoutIfNeededOnUpdate = true
        IQKeyboardManager.shared.canAdjustAdditionalSafeAreaInsets = true
        
        
        urlName.delegate = self
        url.delegate = self
      
      //  self.hideKeyboardWhenTappedAround()
        
      
        displayCurrentTab(1)
        b1(UIButton.self)
        if loginType == "member.type"
        {
              displayCurrentTab(1)
             b1(UIButton.self)
        }
        if loginType == "kid.type"
        {
            displayCurrentTab(2)
            b2(UIButton.self)
        }
       
        self.title = titleName
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("internalApiweb"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification1(notification:)), name: Notification.Name("callApi1web"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification2(notification:)), name: Notification.Name("callApi2web"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    
    func callApi() {
        callgetMemberApi()
    }
    override func viewWillAppear(_ animated: Bool) {
        Analytics.logEvent("WebFilter_Screen_Enter", parameters:nil)

    }
    
    
    @IBAction func canceluRL(_ sender: Any) {
        
         addUrlView.isHidden = true
        
    }
    
    @IBAction func segmentController(_ sender: UISegmentedControl) {
        
      // if sender.selectedSegmentIndex
        if sender.selectedSegmentIndex == 0 {
        
            allowedorblock = "block"
        }
        else {
             allowedorblock = "allow"
           
        }
      
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("WebFilter_Screen_Exit", parameters:nil)
//
//        IQKeyboardManager.shared.enable = true
//        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    @IBAction func Addurl(_ sender: Any) {
        Analytics.logEvent("WebFilter_Screen_AddwebUrl_Button_Clicked", parameters:nil)

        
        if urlName.text!.isEmpty || urlName.text == ""
        {
            self.showAlert(title: "", msg: "Invalid URL Name")
            
            return
        }
        
        if url.text!.isEmpty || url.text == ""
        {
            self.showAlert(title: "", msg: "Invalid URL")
            
            return
        }
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(urlName.text, forKey: "title")
        parameter.setValue(url.text?.lowercased(), forKey: "url")
        parameter.setValue(self.userId, forKey: "userId")
        parameter.setValue(self.memberId, forKey: "memberId")
        parameter.setValue(self.loginType, forKey: "typeOfUser")
        parameter.setValue(allowedorblock, forKey: "allowdOrBloked")
        
        
        APIServices.postDataToServer(url: API.saveURL, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                      self.addUrlView.isHidden = true
                
                print(response)
                self.urlName.text = nil
                self.url.text = nil
                self.callgetMemberApi()
                let  respons = response as? [String:Any]
                
                self.showAlert(title: "", msg: respons?["message"] as! String)
                
            }else
            {
                
                
                
            }
        })
        
    }
    
    @objc func rightButtonAction(sender: UIBarButtonItem)
    {
        
        
   //    popUpVC = storyboard?.instantiateViewController(withIdentifier: "popViewViewControllerweb") as! popViewViewControllerweb
//        popUpVC.memberId = memberId
//        
//        popUpVC.userId = userId
//        
//        popUpVC.loginType = loginType
//        popUpVC.delegate2 = self
//        
//        
//        self.view.addSubview(popUpVC.view)
//        self.addChild(popUpVC)
//        
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddWebUrlViewController") as! AddWebUrlViewController
         vc.delegate2 = self
        
        vc.memberId = memberId
        
                vc.userId = userId
        
                vc.loginType = loginType
          self.navigationController?.pushViewController(vc, animated: true)
        
        
//        addUrlView.isHidden = false
//       addUrlView?.alpha = 0.99
//        self.view.addSubview(addUrlView)
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        callgetMemberApi()
        
    }
    
    @objc func methodOfReceivedNotification1(notification: Notification) {
      //  b2(UIButton.self)
        // callgetMemberApi()
        
    }
    
    @objc func methodOfReceivedNotification2(notification: Notification) {
      //  b1(UIButton.self)
        // callgetMemberApi()
        
    }
    
    
    
    
    
    
    @IBAction func b1(_ sender: Any) {
        Analytics.logEvent("WebFilter_Screen_BlockedTab_Clicked", parameters:nil)
        displayCurrentTab(1)
        
        callgetMemberApi()
//        blockedView.backgroundColor =  UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1)
//
//        blockedView.layer.borderColor = UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1).cgColor
        
        blockedView.backgroundColor =  #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        blockedView.layer.borderColor = #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        
        blockedView.layer.borderWidth = 1.0
        allowedView.backgroundColor = UIColor.white
        allowedView.layer.borderColor = UIColor.gray.cgColor
        allowedView.layer.borderWidth = 1.0
        totalView.backgroundColor = UIColor.white
        
        totalView.layer.borderColor = UIColor.gray.cgColor
        totalView.layer.borderWidth = 1.0
        
        blockedNumbers.textColor = UIColor.white
        blockedText.textColor = UIColor.white
        
        
        totalNumber.textColor = UIColor.black
        totalText.textColor = UIColor.black
        allowedNumber.textColor = UIColor.black
        allowedText.textColor = UIColor.black
        
        if let vc = viewControllerForSelectedSegmentIndex(2)
        {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
        
        if let vc = viewControllerForSelectedSegmentIndex(3)
        {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
    }
    func callgetMemberApi()
    {
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        print(memberId)
        
        
        
        
        let getGamesAndAppsUrl = API.getBlockedAllowdURL + "?userId=\(String(describing: profileData.userId!))" + "&memberId=\(memberId!)"
        print(getGamesAndAppsUrl)
        HttpWrapper.posts(with: getGamesAndAppsUrl, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            print(response)
            
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatusWebUrls.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    SharedData.data.responseWebUrl = loginRespone.response
                    
                    NotificationCenter.default.post(name: Notification.Name("Blockedcallsweb"), object: nil)
                    
                    NotificationCenter.default.post(name: Notification.Name("Allowedcallsweb"), object: nil)
                    
                    NotificationCenter.default.post(name: Notification.Name("Totalcallsweb"), object: nil)
                    
                    
                    if (SharedData.data.responseWebUrl?.block?.count)! > 0 || !(SharedData.data.responseWebUrl?.block!.isEmpty)!
                    {
                        self.blockedNumbers.text =  String(describing: (SharedData.data.responseWebUrl?.block?.count)!)
                    }else
                    {
                        self.blockedNumbers.text = "0"
                    }
                    if  (SharedData.data.responseWebUrl?.allow?.count)! > 0
                    {
                        self.allowedNumber.text = String(describing: (SharedData.data.responseWebUrl?.allow?.count)!)
                    }else
                    {
                        self.allowedNumber.text = "0"
                    }
                    
                    
                    self.totalNumber.text = String(describing: SharedData.data.responseWebUrl!.allow!.count + SharedData.data.responseWebUrl!.block!.count)
                    
                    // self.familycollectionView.reloadData()
                }
                else
                    
                {
                    
                }
                
            }catch let jsonErr {
                
                
                print("Error serializing json:", jsonErr)
                
            }
        }){ (error) in
            
            print(error)
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
    }
    @IBAction func b2(_ sender: Any) {
        Analytics.logEvent("WebFilter_Screen_UnblockedTab_Clicked", parameters:nil)
        displayCurrentTab(2)
        
        callgetMemberApi()
        
        blockedView.backgroundColor = UIColor.white
        
        blockedView.layer.borderColor = UIColor.gray.cgColor
        blockedView.layer.borderWidth = 1.0
        
        allowedView.backgroundColor =  #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        allowedView.layer.borderColor = #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        
//        allowedView.backgroundColor = UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1)
//
//        allowedView.layer.borderColor = UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1).cgColor
        
        
        allowedView.layer.borderWidth = 1.0
        
        totalView.backgroundColor = UIColor.white
        totalView.layer.borderColor = UIColor.gray.cgColor
        totalView.layer.borderWidth = 1.0
        
        
        allowedNumber.textColor = UIColor.white
        allowedText.textColor = UIColor.white
        
        
        totalNumber.textColor = UIColor.black
        totalText.textColor = UIColor.black
        
        
        
        blockedNumbers.textColor = UIColor.black
        blockedText.textColor = UIColor.black
        if let vc = viewControllerForSelectedSegmentIndex(1)
        {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
        
        if let vc = viewControllerForSelectedSegmentIndex(3)
        {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
    }
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        if  textField == urlName
//        {
//            url.becomeFirstResponder()
//        }
//
//        if  textField == url
//        {
//            url.resignFirstResponder()
//        }
//        return false
//    }
    
    @IBAction func b3(_ sender: Any) {
        Analytics.logEvent("WebFilter_Screen_TotalTab_Clicked", parameters:nil)
        displayCurrentTab(3)
        callgetMemberApi()
        blockedView.backgroundColor = UIColor.white
        blockedView.layer.borderColor = UIColor.gray.cgColor
        blockedView.layer.borderWidth = 1.0
        allowedView.backgroundColor = UIColor.white
        
        allowedView.layer.borderColor = UIColor.gray.cgColor
        allowedView.layer.borderWidth = 1.0
//        totalView.backgroundColor =  UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1)
//
//        totalView.layer.borderColor = UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1).cgColor
        
        totalView.backgroundColor =  #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        totalView.layer.borderColor = #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        
        totalView.layer.borderWidth = 1.0
        
        totalNumber.textColor = UIColor.white
        totalText.textColor = UIColor.white
        
        allowedNumber.textColor = UIColor.black
        allowedText.textColor = UIColor.black
        blockedNumbers.textColor = UIColor.black
        blockedText.textColor = UIColor.black
        
        if let vc = viewControllerForSelectedSegmentIndex(2)
        {
            // vc.removeFromParent()
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
            //contentView.willRemoveSubview(vc)
        }
        
        if let vc = viewControllerForSelectedSegmentIndex(1)
        {
            vc.removeFromParent()
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
        
        
    }
    
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            
            
            
            self.addChild(vc)
            vc.didMove(toParent: self)
            
            vc.view.frame = self.contentView.bounds
            self.contentView.addSubview(vc.view)
            
            self.currentViewController = vc
        }
    }
    
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case 1:
            vc = firstChildTabVC
        case 2 :
            vc = secondChildTabVC
        default:
            vc = thirdViewdTabVC
        }
        
        return vc
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}
//extension MainWebFilterViewController {
//    func hideKeyboardWhenTappedAround() {
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainWebFilterViewController.dismissKeyboard))
//        tap.cancelsTouchesInView = false
//        view.addGestureRecognizer(tap)
//    }
//    
//    @objc func dismissKeyboard() {
//        view.endEditing(true)
//    }
//}
