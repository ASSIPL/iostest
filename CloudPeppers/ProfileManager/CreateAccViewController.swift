//
//  CreateAccViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 15/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//
import UIKit
import Firebase

class CreateAccViewController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,CropViewControllerDelegate,customDelegate1  {
    
    private var croppingStyle = CropViewCroppingStyle.default
    private var croppedRect = CGRect.zero
    private var croppedAngle = 0
    
    
    @IBOutlet weak var segmentControlType: UISegmentedControl!
    @IBOutlet var countryCode: UITextField!
    
    @IBOutlet weak var passwordVisible: UIButton!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var displayView: UIView!
    @IBOutlet weak var profilePicBtnn: UIButton!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var mobileNumTF: UITextField!
    @IBOutlet weak var emailIdTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var adddressTF: UITextField!
    @IBOutlet weak var createAccButton: UIButton!
    let pickerView = UIPickerView()
    
    var signUpType:String?
    
    @IBOutlet weak var cancelButton: UIButton!
    var profiledata:profile?
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentControlType.layer.cornerRadius = 18
        segmentControlType.clipsToBounds = true
        segmentControlType.layer.borderColor = #colorLiteral(red: 0, green: 0.7152789235, blue: 0.7682691216, alpha: 1)
        segmentControlType.layer.borderWidth = 1.5
        
      //  segmentControlType.setTitleTextAttributes( NSAttributedString.Key.font: UIFont(name: "YourFont", size: 18.0)!, for: .Normal)
        
       
    
        
        if segmentControlType.selectedSegmentIndex == 0
        {
            signUpType = "parent.type"
        }
        else
        {
            signUpType = "member.type"
        }
        passwordTF.clearsOnInsertion = false
        passwordTF.clearsOnBeginEditing = false
        
        
        imagePicker.delegate = self
        displayView.displayViewShadow()
        profilePicBtnn.layer.cornerRadius = profilePicBtnn.frame.height/2
        profilePicBtnn.clipsToBounds = true
        profilePicBtnn.layer.borderColor = UIColor(hexString: "#3B3D3D").cgColor
        profilePicBtnn.layer.borderWidth = 1
        
        
        
        passwordTF.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        firstNameTF.becomeFirstResponder()
        pickerView.delegate = self
        pickerView.reloadAllComponents()
        //  countryCode.inputView = pickerView
        
        createAccButton.changeButtonCornerRadius()
        cancelButton.changeButtonCornerRadius()
        /*   firstNameTF.setBottomBorder()
         lastNameTF.setBottomBorder()
         mobileNumTF.setBottomBorder()
         passwordTF.setBottomBorder()
         emailIdTF.setBottomBorder()
         countryCode.setBottomBorder()   */
        
        
        firstNameTF.useUnderline()
        lastNameTF.useUnderline()
        mobileNumTF.useUnderline()
        passwordTF.useUnderline()
        emailIdTF.useUnderline()
        countryCode.useUnderline()
        
        firstNameTF.delegate = self
        lastNameTF.delegate = self
        mobileNumTF.delegate = self
        passwordTF.delegate = self
        
        
        mobileNumTF.keyboardType = .numberPad
        
        
        countryCode.addTarget(self, action: #selector(myTargetFunction), for: .touchDown)
        
        // Do any additional setup after loading the view.
    }
    
    
    @objc func myTargetFunction() {
        print("myTargetFunction")
        
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CountryPickViewController") as! CountryPickViewController
        vc.delegate = self
        self.present(vc, animated:true, completion:nil)
        //self.present(vc, animated:true, completion:nil)
    }
    func didSelectCountry(_ result: String) {
        countryCode.text = result
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Analytics.logEvent("SignUpEnter", parameters:nil)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("SignUpExit", parameters:nil)
    }
    @IBAction func createAccBtnTapped(_ sender: Any) {
        Analytics.logEvent("Sign_Up_SignUp_Clicked", parameters:nil)
        
        if isAccountHasValidDetails() == true{
            let parameter:NSMutableDictionary = NSMutableDictionary()
            
            
            let profileImage = profilePicBtnn.image(for: UIControl.State.normal)
            
            if profileImage != UIImage.init(named: "placeHolderProfile")
            {
                let imageData = profileImage!.pngData()
                let datastring =   imageData!.base64EncodedString()
                parameter.setValue(datastring, forKey: "profileImage")
            }
            
            parameter.setValue(firstNameTF.text, forKey: "firstName")
            parameter.setValue(lastNameTF.text, forKey: "lastName")
            
              parameter.setValue(UIHelper.getUUID(), forKey: "imei")
            parameter.setValue(mobileNumTF.text?.getStringFromPhoneNumber(), forKey: "mobileNo")
            parameter.setValue(emailIdTF.text, forKey: "email")
            parameter.setValue(passwordTF.text, forKey: "password")
            parameter.setValue(UserDefaults.standard.string(forKey: "endpointArnForSNS"), forKey: "snsARN")
            
            //   mobileExtension.text
            
      //      parameter.setValue(UserDefaults.standard.string(forKey: "roleKey"), forKey: "login_type")
            
             parameter.setValue(signUpType, forKey: "login_type")
             UserDefaults.standard.set(signUpType, forKey: "roleKey")
            
            
            //   parameter.setValue(UserDefaults.standard.string(forKey: "login_type"), forKey: "roleKey")
            
            
            
            //    parameter.setValue(UserDefaults.standard.string(forKey: "checkfamilymembertype"), forKey: "login_type")
            
            parameter.setValue(countryCode.text, forKey: "countryCode")
            parameter.setValue("iOS", forKey: "platform")
            
            parameter.setValue(UserDefaults.standard.string(forKey: "fcmToken"), forKey: "fcmId")
            parameter.setValue("true", forKey: "isTemrsAccepted")
            
            print(parameter)
            APIServices.postDataToServer(url: API.createAccount_URL, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
                if status == true{
                    print(response)
                    
                    var derivedData = response as! [String : Any]
                    
                    print(derivedData)
                    if let theJSONData = try? JSONSerialization.data(withJSONObject: derivedData["response"] as Any , options:[])
                    {
                        do{
                            if var profileInfo = try? JSONDecoder().decode(profile.self, from: theJSONData) {
                                // var jwtToken:String?
                                
                                
                                
                              
                                
                                
                            //    UserDefaults.standard.string(forKey: "jwtToken")
                                
                                print(profileInfo.jwtToken)
                                
                                self.profiledata = profileInfo
                                print(profileInfo)
                                if profileInfo.profileImage != nil &&  profileInfo.profileImage?.count != 0
                                {
                                    let url = URL(string: profileInfo.profileImage!)
                                    //  profileInfo.profilePic = try? Data(contentsOf: url!)
                                }
                                
                                //                                if let data = try? JSONEncoder().encode(profileInfo) {
                                //                                    UserDefaults.standard.set(data, forKey: "profile_details")
                                //                                }
                            }
                        }
                        
                    }
                    
                    UserDefaults.standard.set(self.mobileNumTF.text?.getStringFromPhoneNumber(), forKey: "mobileNumber")
                    
                    UserDefaults.standard.set(self.countryCode.text, forKey: "countryCode")
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "VerificationViewController") as! VerificationViewController
                    nextViewController.profiledata = self.profiledata
                    self.present(nextViewController, animated:true, completion:nil)
                    
                }
                
            })
            
        }
    }
    
    func isAccountHasValidDetails()  -> Bool {
        
        if firstNameTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Please enter First Name")
            return false
        }else if (firstNameTF.text?.count)! < 3 || (firstNameTF.text?.count)! > 30{
            showAlert(title: "" , msg: "First Name should contain minimum 3 & maximum 30 characters")
            return false
        }
        
        if lastNameTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Please enter Last Name")
            return false
        }
        else if (lastNameTF.text?.count)! < 2 || (lastNameTF.text?.count)! > 30 {
            showAlert(title: "", msg: "Last Name should contain minimum 2 & maximum 30 characters")
            return false
        }
        
        if mobileNumTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Please enter valid mobile Number")
            return false
            
        }
            
        else if (mobileNumTF.text?.count)! < 13 || (mobileNumTF.text?.count)! > 17
        {
            showAlert(title: "", msg: "Please enter Valid Mobile Number")
            return false
        }
        
        if emailIdTF.text?.isEmpty == true{
            showAlert(title: "", msg: "Please Enter Valid Email Address")
            return false
            
        }
        else if(emailIdTF!.text!.isValidEmail() == false){
            showAlert(title: "", msg: "Please Enter Valid Email Address")
            return false
            
        }
        
        if passwordTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Please Enter Valid password")
            return false
        }
        else if (passwordTF.text?.count)! < 8 || (passwordTF.text?.count)! > 16
        {
            showAlert(title: "" , msg: "Password should contain minimum 8 and maximum 16 characters")
            return false
        }
        
        
        if checkButton.isSelected == false{
            showAlert(title: "", msg: "Please accept Terms and conditions")
            return false
        }
        
        return true
    }
    
    @IBAction func checkBtnTapped(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
    }
    
    @IBAction func termsConBtnTapped(_ sender: Any) {
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "TermsCondViewController") as! TermsCondViewController
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetail = try? JSONDecoder().decode(response.self, from: data!)
        // organizationdetail?.organization?.orginationURL
        vc.urlString =  organizationdetail?.organization?.orginationURL
        self.present(vc, animated: true, completion: nil)
        //        self.navigationController?.present
        //        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func addProfilePicBtn(_ sender: Any) {
        
        Analytics.logEvent("Sign_Up_Upload Photo", parameters:nil)
        
        let alert:UIAlertController=UIAlertController(title: "Choose Option", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        imagePicker.delegate = self
        //  imagePicker.allowsEditing = true
        
        //Display AlertSheet For iPad
        
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX - 45, y: self.view.bounds.midY - 15 + 500, width: 100, height: 0)
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            self .present(imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertView()
            alert.title = "Warning"
            alert.message = "You don't have camera"
            alert.addButton(withTitle: "OK")
            alert.show()
        }
    }
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
        //            profilePicBtnn.setImage(image, for: .normal)
        //        }
        //        dismiss(animated: true, completion: nil)
        
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            
            
            let cropController = CropViewController(croppingStyle: croppingStyle, image: image)
            cropController.delegate = self
            if croppingStyle == .circular {
                picker.pushViewController(cropController, animated: true)
            }
            else { //otherwise dismiss, and then present from the main controller
                picker.dismiss(animated: true, completion: {
                    self.present(cropController, animated: true, completion: nil)
                })
            }
            
            
            
            
            
            
        }
        // pickedImage = true
        dismiss(animated: true, completion: nil)
    }
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        
        profilePicBtnn.setImage(image, for: .normal)
        //     pickedImage = true
        if cropViewController.croppingStyle != .circular {
            cropViewController.dismiss(animated: true, completion: nil)
        }
        else {
            
            cropViewController.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
        
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        return (organizationdetails?.countriesInfo!.count)!
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        return "\(String(describing: (organizationdetails?.countriesInfo![row].countryCode!)!))" + " " + "\(String(describing: (organizationdetails?.countriesInfo![row].countryName!)!))"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        let cid = organizationdetails?.countriesInfo![row].countryCode!
        countryCode.text = String(describing: cid!)
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController){
        print("picker cancel.")
    }
    
    @IBAction func passwordVisibleHideBtnTapped(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        /***If eye non - visible (defalut) secured***/
        passwordTF.isSecureTextEntry = !sender.isSelected
        
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        Analytics.logEvent("Sign_Up_Cancel_Clicked", parameters:nil)
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == firstNameTF || textField == lastNameTF
        {
            if range.location < 30{
                let allowedCharacters = CharacterSet.letters
                let characterSet = CharacterSet(charactersIn: string)
                return allowedCharacters.isSuperset(of: characterSet)
            }
            else
            {
                return range.location < 30
                
            }
        }
            //        else if(textField == mobileNumTF)
            //        {
            //            return range.location < 14
            //        }
        else if (textField == mobileNumTF)
        {
            let newPosition = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
            
            if range.length > 0 {
                return true
            }
            if range.location >= 17 {
                return false
            }
            if string == "" {
                return false
            }
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: " ", with: "")
            
            // Verify entered text is a numeric value
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
            
            //Put / after 2 digit
            if range.location == 0 {
                originalText?.append("(")
                textField.text = originalText
            }
            if range.location == 4 {
                originalText?.append(")")
                textField.text = originalText
            }
            if range.location == 8 {
                originalText?.append("-")
                textField.text = originalText
            }
            return true
        }
        else if (textField == passwordTF)
        {
            return range.location < 16
        }
        
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder();
        return true;
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
}

