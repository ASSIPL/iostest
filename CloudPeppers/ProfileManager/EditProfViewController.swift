//
//  EditProfViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 18/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase

class EditProfViewController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate {
    
    private var croppingStyle = CropViewCroppingStyle.default
    private var croppedRect = CGRect.zero
    private var croppedAngle = 0
    
    @IBOutlet weak var addProfilePicBtnn: UIButton!
    @IBOutlet weak var mobileNumTF: UITextField!
    @IBOutlet weak var disView: UIView!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    var pickedImage:Bool?
    
    var imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        disView.displayViewShadow()
        updateButton.changeButtonCornerRadius()
//        firstNameTF.setBottomBorder()
//        lastNameTF.setBottomBorder()
//        mobileNumTF.setBottomBorder()
//        emailTF.setBottomBorder()
        
        
        firstNameTF.useUnderline()
        lastNameTF.useUnderline()
        mobileNumTF.useUnderline()
        emailTF.useUnderline()
        //   mobileNumTF.backgroundColor = UIColor.lightGray
        //  emailTF.backgroundColor = UIColor.lightGray
        
        
        mobileNumTF.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        emailTF.textColor = #colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
        
        mobileNumTF.isEnabled = false
        emailTF.isEnabled = false
        
        addProfilePicBtnn.layer.cornerRadius = addProfilePicBtnn.frame.height/2
        addProfilePicBtnn.clipsToBounds = true
        addProfilePicBtnn.layer.borderColor = UIColor(hexString: "#3B3D3D").cgColor
        addProfilePicBtnn.layer.borderWidth = 1
        
        imagePicker.delegate = self
        
        if let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) {
            
            firstNameTF.text = profileData.firstName
            lastNameTF.text = profileData.lastName
      //      mobileNumTF.text = "\(profileData.countryCode!)" + " " +  (profileData.mobileNo?.getPhoneNumberFromString())!
            
            mobileNumTF.text =  "\(String(describing: (profileData.countryCode)!))" + (profileData.mobileNo?.getPhoneNumberFromString())! 
            //  addressTF.text = profileData.address
            emailTF.text = profileData.email
            
            if profileData.profileImage != nil &&  profileData.profileImage?.count != 0
            {
                
                let imageview = UIImageView()
                
                
                
                
                imageview.sd_setImage(with: URL(string: (profileData.profileImage)!), placeholderImage: UIImage(named: "placeHolderProfile"))
                
                self.addProfilePicBtnn.setImage(imageview.image, for: .normal)
                
                
                //                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { // Change `2.0` to the desired number of seconds.
                //                    let url = URL(string: profileData.profileImage!)
                //                    let data = try? Data(contentsOf: url!)
                //                    self.addProfilePicBtnn.setImage(UIImage(data: data!), for: .normal)
                //                }
                
                //    self.addProfilePicBtnn.setImage(UIImage(data: profileData.profilePic!), for: .normal)
                
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Analytics.logEvent("EditProfile_Screen_Enter", parameters:nil)

        navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("EditProfile_Screen_Exit", parameters:nil)

    }
    // Do any additional setup after loading the view.
    @IBAction func updateBtnTapped(_ sender: Any) {
        
        if isAccountHasValidDetails() == true{
            
            Analytics.logEvent("EditProfile_Screen_Update_clicked", parameters:nil)
            
            guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                let profileData = try? JSONDecoder().decode(profile.self, from: data) else
            {
                return
            }
            
            
            let parameter:NSMutableDictionary = NSMutableDictionary()
            
            parameter.setValue(profileData.userId, forKey: "userId")
            let profileImage = addProfilePicBtnn.image(for: UIControl.State.normal)
            
            if profileImage != UIImage.init(named: "placeHolderProfile")
            {
                let imageData = profileImage!.pngData()
                let datastring =   imageData!.base64EncodedString()
                if pickedImage == true
                {
                    parameter.setValue(datastring, forKey: "profileImage")
                }
                
            }
            
            parameter.setValue(firstNameTF.text, forKey: "firstName")
            parameter.setValue(lastNameTF.text, forKey: "lastName")
            parameter.setValue(mobileNumTF.text?.getStringFromPhoneNumber(), forKey: "mobileNo")
            parameter.setValue(emailTF.text, forKey: "email")
            
            parameter.setValue("iOS", forKey: "platform")
            
            parameter.setValue(UserDefaults.standard.string(forKey: "fcmToken"), forKey: "fcmId")
            
            
            //   print(parameter)
            APIServices.postDataToServer(url: API.updateParentProfile, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
                if status == true{
                    print(response)
                
                    let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                    
                    let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
                    let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
                
                    
                    let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
                    let msgAttrString = NSMutableAttributedString(string: "Profile Updated Successfully", attributes: msgFont)
                    
                    alert.setValue(titAttrString, forKey: "attributedTitle")
                    alert.setValue(msgAttrString, forKey: "attributedMessage")

                    let saveAction = UIAlertAction(title: "Ok", style: .default, handler: { alert -> Void in
                        
                        let revealViewController:SWRevealViewController = self.revealViewController()
                        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let desController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        let newFrontViewController = UINavigationController.init(rootViewController:desController)
                        revealViewController.pushFrontViewController(newFrontViewController, animated: true)
                        
                     //   self.navigationController?.popViewController(animated: true)
                        
                      //  self.dismiss(animated: true, completion: nil)
                        
                    })
                    // alertController.addAction(cancelAction)
                    alert.addAction(saveAction)
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    /*
                    
                    let alertController = UIAlertController(title: Constants.alertTitle, message: "Profile Updated Successfully", preferredStyle: .alert)
                    
                    
                    
                    
                    let saveAction = UIAlertAction(title: "Ok", style: .default, handler: { alert -> Void in
                        
                        self.navigationController?.popViewController(animated: true)
                        
                        self.dismiss(animated: true, completion: nil)
                        
                    })
                   // alertController.addAction(cancelAction)
                    alertController.addAction(saveAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    */
                    
                    
                    
                    var derivedData = response as! [String : Any]
                    
                    print(derivedData)
                    if let theJSONData = try? JSONSerialization.data(withJSONObject: derivedData["response"] as Any , options:[])
                    {
                        do{
                            
                            let loginRespone = try JSONDecoder().decode(Array<profile>.self, from: theJSONData)
                            
                            if let data = try? JSONEncoder().encode(loginRespone[0]) {
                                UserDefaults.standard.set(data, forKey: "profile_details")
                                
                            }
                            
                            
                            
                            //                        if let profileInfo = try? JSONDecoder().decode(profile.self, from: theJSONData) {
                            //                            print(profileInfo)
                            //
                            //                            if profileInfo.profileImage != nil &&  profileInfo.profileImage?.count != 0
                            //                            {
                            //                                let url = URL(string: profileInfo.profileImage!)
                            //                                //   profileInfo.profilePic = try? Data(contentsOf: url!)
                            //
                            //
                            //                            }
                            //
                            //                            if let data = try? JSONEncoder().encode(profileInfo) {
                            //                                UserDefaults.standard.set(data, forKey: "profile_details")
                            //                            }
                            //
                            //                        }
                        }catch let jsonErr {
                            
                            
                            print("Error serializing json:", jsonErr)
                            
                        }
                        
                    }
                    
                    UserDefaults.standard.set(self.mobileNumTF.text?.getStringFromPhoneNumber(), forKey: "mobileNumber")
                    
                    
                    
                }
                
            })
            
        }
        
    }
    
    
    func isAccountHasValidDetails()  -> Bool {
        
        if firstNameTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Please enter First Name")
            return false
        }else if (firstNameTF.text?.count)! < 3 || (firstNameTF.text?.count)! > 30{
            showAlert(title: "" , msg: "First Name should contain minimum 3 & maximum 30 characters")
            return false
        }
        
        if lastNameTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Please enter Last Name")
            return false
        }
        else if (lastNameTF.text?.count)! < 2 || (lastNameTF.text?.count)! > 30 {
            showAlert(title: "", msg: "Last Name should contain minimum 2 & maximum 30 characters")
            return false
        }
        
        
        if mobileNumTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Please enter Mobile Number")
            return false
            
        }
        else if (mobileNumTF.text?.count)! < 10 || (mobileNumTF.text?.count)! > 17
        {
            showAlert(title: "", msg: "Enter Valid Mobile Number")
            return false
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == firstNameTF || textField == lastNameTF
        {
            if range.location < 30{
                let allowedCharacters = CharacterSet.letters
                let characterSet = CharacterSet(charactersIn: string)
                return allowedCharacters.isSuperset(of: characterSet)
            }
            else
            {
                return range.location < 30
                
            }
        }
            //        else if(textField == mobileNumTF)
            //        {
            //            return range.location < 14
            //        }
            
        else if (textField == mobileNumTF)
        {
            let newPosition = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
            
            if range.length > 0 {
                return true
            }
            if range.location >= 17 {
                return false
            }
            if string == "" {
                return false
            }
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: " ", with: "")
            
            // Verify entered text is a numeric value
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
            
            //Put / after 2 digit
            if range.location == 0 {
                originalText?.append("(")
                textField.text = originalText
            }
            if range.location == 4 {
                originalText?.append(")")
                textField.text = originalText
            }
            if range.location == 8 {
                originalText?.append("-")
                textField.text = originalText
            }
            return true
        }
        
        return true
    }
    
    @IBAction func profileImgTapped(_ sender: Any) {
        
        Analytics.logEvent("EditProfile_Screen_ImageProfile_Clicked", parameters:nil)
        let alert:UIAlertController=UIAlertController(title: "Choose Option", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        imagePicker.delegate = self
       // imagePicker.allowsEditing = true
        
        //Display Action Sheet Alert For iPad
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX - 45, y: self.view.bounds.midY - 15 + 500, width: 100, height: 0)
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            self .present(imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertView()
            alert.title = "Warning"
            alert.message = "You don't have camera"
            alert.addButton(withTitle: "OK")
            alert.show()
        }
    }
    
    
    func openGallary(){
        
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            
            
            let cropController = CropViewController(croppingStyle: croppingStyle, image: image)
            cropController.delegate = self
            if croppingStyle == .circular {
                picker.pushViewController(cropController, animated: true)
            }
            else { //otherwise dismiss, and then present from the main controller
                picker.dismiss(animated: true, completion: {
                    self.present(cropController, animated: true, completion: nil)
                })
            }
            
            
            
            
            
            
        }
        // pickedImage = true
        dismiss(animated: true, completion: nil)
        
//        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
//            addProfilePicBtnn.setImage(image, for: .normal)
//        }
//        pickedImage = true
//        dismiss(animated: true, completion: nil)
    }
    
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
      
        addProfilePicBtnn.setImage(image, for: .normal)
         pickedImage = true
        if cropViewController.croppingStyle != .circular {
            cropViewController.dismiss(animated: true, completion: nil)
        }
        else {
            
            cropViewController.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        dismiss(animated: true, completion: nil)
        print("picker cancel.")
    }
    
    @IBAction func changePasswordTapped(_ sender: Any) {
        Analytics.logEvent("EditProfile_Screen_ChnagePassword_clicked", parameters:nil)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        
        
        let revealViewController:SWRevealViewController = self.revealViewController()
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let newFrontViewController = UINavigationController.init(rootViewController:desController)
        revealViewController.pushFrontViewController(newFrontViewController, animated: true)
        
//        navigationController?.popViewController(animated: true)
//        dismiss(animated: true, completion: nil)
    }
    
}

