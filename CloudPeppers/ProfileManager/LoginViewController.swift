//
//  LoginViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 14/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging


protocol customDelegate1: class {
    func didSelectCountry(_ result:String)
    
    
}



class LoginViewController: UIViewController, UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate,customDelegate1{
    
    // var isAllSelected: Bool = false
    
    
    var comparisionArray:profile?
    
    
    @IBOutlet var mobileExtension: UITextField!
    let pickerView = UIPickerView()
    @IBOutlet var companyLogo: UIImageView!
    @IBOutlet weak var displayView: UIView!
    @IBOutlet weak var newUserButton: UIButton!
    @IBOutlet weak var checkBoxBtnTap: UIButton!
    @IBOutlet weak var emaidAddTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        newUserButton.underline()
        let tokens = Messaging.messaging().fcmToken
        print("FCM token: \(tokens ?? "")")
        let defaults = UserDefaults.standard
        defaults.set(tokens, forKey: "fcmToken")
        
        passwordTF.clearsOnInsertion = false
        passwordTF.clearsOnBeginEditing = false
        
        
        
        if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type" || UserDefaults.standard.string(forKey: "roleKey") == "thing.type"
        {
            
            newUserButton.isHidden = true
        }
      //  passwordTF.clearsOnBeginEditing = false
        
        mobileExtension.setBottomBorder()
        passwordTF.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        mobileExtension.delegate = self
        emaidAddTF.delegate = self
        displayView.displayViewShadow()
           mobileExtension.addTarget(self, action: #selector(myTargetFunction), for: .touchDown)
        emaidAddTF.useUnderline()
        passwordTF.useUnderline()
        
//          emaidAddTF.setBottomBorder()
//        passwordTF.setBottomBorder()
        newUserButton.changeButtonCornerRadius()
        
        pickerView.delegate = self
        pickerView.reloadAllComponents()
      //  mobileExtension.inputView = pickerView
        
        if let id = (UserDefaults.standard.string(forKey: "rememberme_id")) {
            emaidAddTF.text = id
            
        }
        if let password = (UserDefaults.standard.string(forKey: "rememberme_password")) {
            passwordTF.text = password
        }
        
        if let extensionmobile = (UserDefaults.standard.string(forKey: "rememberme_mobie")),extensionmobile != "" {
            mobileExtension.text = extensionmobile
        }
      
        
        guard  let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data,
            let organizationdetails = try? JSONDecoder().decode(response.self, from: data)
            else
        {
            return
        }
        
        self.companyLogo.sd_setImage(with: URL(string: (organizationdetails.organization?.splashImage)!), placeholderImage: UIImage(named: "newcoolpadlogo"))
        // Do any additional setup after loading the view.
    }
    
    func didSelectCountry(_ result: String) {
        mobileExtension.text = result
    }
    
    @objc func myTargetFunction() {
        print("myTargetFunction")
        
        passwordTF.resignFirstResponder()
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CountryPickViewController") as! CountryPickViewController
        vc.delegate = self
        self.present(vc, animated:true, completion:nil)
        //self.present(vc, animated:true, completion:nil)
    }
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == mobileExtension
//        {
//            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc = mainStoryboard.instantiateViewController(withIdentifier: "CountryPickViewController") as! CountryPickViewController
//            vc.delegate = self
//            self.present(vc, animated:true, completion:nil)
//        }
//    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
       
        
    }
    
    
    
    @IBAction func passwordVisibleHideBtn(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        /***If eye non - visible (defalut) secured***/
        passwordTF.isSecureTextEntry = !sender.isSelected
       
        
    }
    @IBAction func newUserBtnTapped(_ sender: Any) {
        
        Analytics.logEvent("Login_New_User_Clicked", parameters:nil)
        print("New User ")
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        
         Analytics.logEvent("Login_Forgot_Pw_Clicked", parameters:nil)
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
        //  self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated:true, completion:nil)
        
        //        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        //      //  self.navigationController?.pushViewController(vc, animated: true)
        //          self.present(vc, animated:true, completion:nil)
        
    }
    
    @IBAction func checkBoxxTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        //        let image = isAllSelected ? "UnCheck" : "CheckBox"
        //        isAllSelected = !isAllSelected
        //        sender.setImage(UIImage(named: image), for: .normal)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         Analytics.logEvent("LoginEntered", parameters:nil)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("LoginExit", parameters:nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
        
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
    
        return (organizationdetails?.countriesInfo!.count)!
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        
        return "\(String(describing: (organizationdetails?.countriesInfo![row].countryCode!)!))" + " " + "\(String(describing: (organizationdetails?.countriesInfo![row].countryName!)!))"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        let cid = organizationdetails?.countriesInfo![row].countryCode!
        mobileExtension.text = String(describing: cid!)
    }
    
    @IBAction func loginToHomeScreen(_ sender: Any) {
        
         Analytics.logEvent("Login_Arrow_Clicked", parameters:nil)
        
        if emaidAddTF.text?.isEmpty == true{
            showAlert(title: "", msg: "Please enter valid mobile number")
            return
        }else
            
            if passwordTF.text?.isEmpty == true{
                showAlert(title: "", msg: "Incorrect password")
                return
            }
            else if (passwordTF.text?.count)! < 8 || (passwordTF.text?.count)! > 16
            {
                showAlert(title: "", msg: "Password should contain minimum 8 and maximum 16 characters")
                return
        }
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        print(emaidAddTF.text!.components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined())
        
        print(emaidAddTF.text!.getStringFromPhoneNumber())
        
        parameter.setValue(emaidAddTF.text!.getStringFromPhoneNumber(), forKey: "mobileNo")
        parameter.setValue(passwordTF.text, forKey: "password")
       parameter.setValue(UserDefaults.standard.string(forKey: "fcmToken"), forKey: "fcmId")
     parameter.setValue(UserDefaults.standard.string(forKey: "endpointArnForSNS"), forKey: "snsARN")
        
        
        
    //    parameter.setValue(UserDefaults.standard.string(forKey: "roleKey"), forKey: "login_type")
         parameter.setValue("", forKey: "login_type")
       
        
           parameter.setValue(UIHelper.getUUID(), forKey: "imei")
        
        
        parameter.setValue(mobileExtension.text, forKey: "countryCode")
         parameter.setValue("iOS", forKey: "platform")
        
        print(parameter)
        
        
        APIServices.postDataToServer(url: API.login_URL, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                if self.checkBoxBtnTap.isSelected == true{
                    Analytics.logEvent("Login_Remember_Me_Checked", parameters:nil)

                    UserDefaults.standard.set(self.emaidAddTF.text, forKey: "rememberme_id")
                    UserDefaults.standard.set(self.passwordTF.text, forKey: "rememberme_password")
                      UserDefaults.standard.set(self.mobileExtension.text, forKey: "rememberme_mobie")
                    
                }
                else{
                    UserDefaults.standard.set("", forKey: "rememberme_id")
                    UserDefaults.standard.set("", forKey: "rememberme_password")
                     UserDefaults.standard.set("", forKey: "rememberme_mobie")
                }
                
                print(response)
                
                var derivedData = response as! [String : Any]
                
                print(derivedData)
                if let theJSONData = try? JSONSerialization.data(withJSONObject: derivedData["response"] as Any , options:[])
                {
                    do{
                        
                        let loginRespone = try JSONDecoder().decode(profile.self, from: theJSONData)
                        
                             UserDefaults.standard.set(loginRespone.jwtToken, forKey: "jwtToken")
                        
                        if let data = try? JSONEncoder().encode(loginRespone) {
                                                            UserDefaults.standard.set(data, forKey: "profile_details")
                            
                            
                          
                            
                        }
                    if let  paymentData = try? JSONEncoder().encode(loginRespone.userPlans) {
                              UserDefaults.standard.set(paymentData, forKey: "Paymentdata")
                        }
                        
                        self.comparisionArray = loginRespone
                        print(self.comparisionArray)
                        
//                        if let profileInfo = try? JSONDecoder().decode(profile.self, from: theJSONData) {
//                            print(profileInfo)
//
//                            if profileInfo.profileImage != nil &&  profileInfo.profileImage?.count != 0
//                            {
//                                let url = URL(string: profileInfo.profileImage!)
//                                //   profileInfo.profilePic = try? Data(contentsOf: url!)
//
//
//                            }
//
//                            if let data = try? JSONEncoder().encode(profileInfo) {
//                                UserDefaults.standard.set(data, forKey: "profile_details")
//                            }
//
//                        }
                        
                        
                        
                        
                        
                        
                    }catch let jsonErr {
                        
                        
                        print("Error serializing json:", jsonErr)
                        
                    }
                    
                }
                
                
                if self.comparisionArray?.login_type == "kid.type"
                {
                    self.showAlert(title: "", msg: "You cannot login in iOS platform with Kid credentials")
                }else{
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                    self.present(nextViewController, animated:true, completion:nil)
                }
                
                
                
            }
        })
        
    }
    
    func getWidth(text: String) -> CGFloat {
        let txtField = UITextField(frame: .zero)
        txtField.text = text
        txtField.sizeToFit()
        return txtField.frame.size.width
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        if mobileExtension.text != nil {
        //            let text = textField.text! as NSString
        //            let finalString = text.replacingCharacters(in: range, with: string)
        //            textField.frame.size.width = getWidth(text: finalString)
        //        }
        
        if (textField == emaidAddTF)
        {
            let newPosition = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
            
            if range.length > 0 {
                return true
            }
            if range.location >= 17 {
                return false
            }
            if string == "" {
                return false
            }
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: " ", with: "")
            
            // Verify entered text is a numeric value
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
            
            //Put / after 2 digit
            if range.location == 0 {
                originalText?.append("(")
                textField.text = originalText
            }
            if range.location == 4 {
                originalText?.append(")")
                textField.text = originalText
            }
            if range.location == 8 {
                originalText?.append("-")
                textField.text = originalText
            }
            return true
        }else if textField == passwordTF
        {
            return range.location < 16
        }
        
    
        
        
        
        return true
    }
    
}


@IBDesignable extension UITextField{
    
    @IBInspectable var setLeftIcon: UIImage {
        
        get{
            return UIImage.init(named: "")!
        }
        set {
            let padding = 8
            let size = 20
            let outerView = UIView(frame: CGRect(x: 0, y: 0, width: size + padding + padding, height: size) )
            let iconView  = UIImageView(frame: CGRect(x: padding, y: 0, width: size, height: size))
            iconView.image = newValue
            outerView.addSubview(iconView)
            leftView = outerView
            leftViewMode = .always
        }
    }
}

extension UIButton {
    func centerTextAndImage(spacing: CGFloat) {
        let insetAmount = spacing / 2
        imageEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: -insetAmount)
        contentEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: insetAmount)
    }
}

extension UIButton {
    func underline() {
        guard let text = self.titleLabel?.text else { return }
        
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        
        self.setAttributedTitle(attributedString, for: .normal)
    }
}


