//
//  ResetPasswordVC.swift
//  CloudPeppers
//
//  Created by Allvy on 28/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase

class ResetPasswordVC: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,customDelegate1 {
    
    @IBOutlet var logo: UIImageView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var disView: UIView!
    @IBOutlet weak var emailIDTF: UITextField!
    @IBOutlet weak var mobileExtension: UITextField!
    let pickerView = UIPickerView()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Analytics.logEvent("ResetPasswordEnter", parameters:nil)
        if let extensionmobile = (UserDefaults.standard.string(forKey: "rememberme_mobie")),extensionmobile != "" {
            mobileExtension.text = extensionmobile
        }
        
        mobileExtension.setBottomBorder()
        
        mobileExtension.delegate = self
        emailIDTF.delegate = self
       
        disView.displayViewShadow()
//        emailIDTF.setBottomBorder()
        emailIDTF.useUnderline()
        mobileExtension.useUnderline()
        submitButton.changeButtonCornerRadius()
        
        pickerView.delegate = self
        pickerView.reloadAllComponents()
      //  mobileExtension.inputView = pickerView
        
        guard  let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data,
            let organizationdetails = try? JSONDecoder().decode(response.self, from: data)
            else
        {
            return
        }
        
        print(organizationdetails.organization?.splashImage)
        mobileExtension.addTarget(self, action: #selector(myTargetFunction), for: .touchDown)
        
        self.logo.sd_setImage(with: URL(string: (organizationdetails.organization?.splashImage)!), placeholderImage: UIImage(named: "newcoolpadlogo"))
        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func myTargetFunction() {
        print("myTargetFunction")
        
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CountryPickViewController") as! CountryPickViewController
        vc.delegate = self
        self.present(vc, animated:true, completion:nil)
        //self.present(vc, animated:true, completion:nil)
    }
    func didSelectCountry(_ result: String) {
        mobileExtension.text = result
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("ResetPasswordExit", parameters:nil)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
        
        
    }
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        if isAccountHasValidDetails() 
        {
          //  mobileNumber
            
//            guard let mobileNumber = (UserDefaults.standard.string(forKey: "mobileNumber")) else {
//
//                return
//            }
//
//
            let parameter:NSMutableDictionary = NSMutableDictionary()
            
            parameter.setValue(emailIDTF.text!.getStringFromPhoneNumber(), forKey: "mobileNo")
            parameter.setValue(mobileExtension.text, forKey: "countryCode")
            
            
            APIServices.postDataToServer(url: API.resetPasswordOTP, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
                if status == true{
                    
                    UserDefaults.standard.set(self.emailIDTF.text, forKey: "mobileNumber")
                    
                    UserDefaults.standard.set(self.mobileExtension.text, forKey: "countryCode")
                    
                    let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
                    
                    //  self.navigationController?.pushViewController(vc, animated: true)
                    self.present(vc, animated:true, completion:nil)
                    
                    
                    
                }
            })
            
           
            
         
        }
        
    }
    
    func isAccountHasValidDetails()  -> Bool {
        
        if emailIDTF.text?.isEmpty == true{
            showAlert(title: "", msg: "Please enter valid mobile number")
            return false
        }
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
        
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        return (organizationdetails?.countriesInfo!.count)!
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        
        return "\(String(describing: (organizationdetails?.countriesInfo![row].countryCode!)!))" + " " + "\(String(describing: (organizationdetails?.countriesInfo![row].countryName!)!))"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        let cid = organizationdetails?.countriesInfo![row].countryCode!
        mobileExtension.text = String(describing: cid!)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == emailIDTF)
        {
            let newPosition = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
            
            if range.length > 0 {
                return true
            }
            if range.location >= 17 {
                return false
            }
            if string == "" {
                return false
            }
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: " ", with: "")
            
            // Verify entered text is a numeric value
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
            
            //Put / after 2 digit
            if range.location == 0 {
                originalText?.append("(")
                textField.text = originalText
            }
            if range.location == 4 {
                originalText?.append(")")
                textField.text = originalText
            }
            if range.location == 8 {
                originalText?.append("-")
                textField.text = originalText
            }
            return true
        }
        
        
        return true
    }
    
    
    
}
