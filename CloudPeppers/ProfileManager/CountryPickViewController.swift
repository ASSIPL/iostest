//
//  CountryPickViewController.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 06/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class CountryPickViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
   
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet weak var titleLabel: UILabel!
    var searchActive : Bool = false
    
    @IBOutlet var tableView: UITableView!
      var delegate: customDelegate1?
    
     var countriesInfo:[countriesInfo]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       tableView.delegate = self
       tableView.dataSource = self
        
        searchBar.delegate = self
        
      titleLabel.text = "Coolpad FamilyLabs"
        
        tableView.layer.cornerRadius = 5
        tableView.clipsToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func dismissAction(_ sender: UIButton) {
        
          dismiss(animated: true, completion: nil)
        
        
        
    }
    
    
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = false
        //   callsTableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        
        searchBar.text = nil
        searchBar.resignFirstResponder()
        tableView.reloadData()
        
        self.searchBar.showsCancelButton = false
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        if searchText == "" || searchText.isEmpty
        {
            self.searchActive = false
        }else
        {
            self.searchActive = true;
            self.searchBar.showsCancelButton = true
            
            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
            
            countriesInfo = organizationdetails?.countriesInfo?.filter{ $0.countryName!.localizedStandardContains(searchText) || $0.countryCode!.localizedStandardContains(searchText) }
            
        }
        
       
        tableView.reloadData()
        
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
        
        if(searchActive){
            return countriesInfo?.count ?? 0
            
        }
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        return (organizationdetails?.countriesInfo!.count)!
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as UITableViewCell
        cell.textLabel?.numberOfLines = 0;
        cell.textLabel?.lineBreakMode = .byWordWrapping
//        cell.layer.cornerRadius = 10
//        cell.clipsToBounds = true
//        cell.layer.borderWidth = 1
//        cell.layer.borderColor = #colorLiteral(red: 0, green: 0.7152789235, blue: 0.7682691216, alpha: 1)
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
          if(searchActive){
            
            cell.textLabel?.text =   "\(String(describing: (countriesInfo![indexPath.row].countryCode!)))" + " " + "\(String(describing: (countriesInfo![indexPath.row].countryName!)))"
        }else
          {
              cell.textLabel?.text =   "\(String(describing: (organizationdetails?.countriesInfo![indexPath.row].countryCode!)!))" + " " + "\(String(describing: (organizationdetails?.countriesInfo![indexPath.row].countryName!)!))"
        }
    
      
        
        return cell
    }
    
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
         if(searchActive){
            self.delegate?.didSelectCountry("\(String(describing: (countriesInfo![indexPath.row].countryCode!)))")
               dismiss(animated: true, completion: nil)
        }else
         {
            self.delegate?.didSelectCountry("\(String(describing: (organizationdetails?.countriesInfo![indexPath.row].countryCode!)!))")
               dismiss(animated: true, completion: nil)
        }
        
       
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
