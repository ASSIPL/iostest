//
//  VerificationViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 24/04/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase

class VerificationViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var firstTF: UITextField!
    
    @IBOutlet weak var thirdTF: UITextField!
    
    @IBOutlet weak var fourthTF: UITextField!
    
    @IBOutlet weak var secondTF: UITextField!
    
    
    var profiledata:profile?
    override func viewDidLoad() {
        super.viewDidLoad()
        
         Analytics.logEvent("VerificationEntered", parameters:nil)
        firstTF.delegate = self
        secondTF.delegate = self
        thirdTF.delegate = self
        fourthTF.delegate = self
        verifyButton.changeButtonCornerRadius()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("VerificationExit", parameters:nil)
    }
    @IBAction func backAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//         var otpString = "\(String(describing: firstTF.text!))" +  "\(String(describing: secondTF.text!))" + "\(String(describing: thirdTF.text!))" + "\(String(describing: fourthTF.text!))"
//
    //   string = "6"
      if (range.length == 0)
            
           // (((textField.text?.count)!) < 1 ) && (string.count > 0)
        {
            if textField == firstTF {
                secondTF.becomeFirstResponder()
            }
            if textField == secondTF
            {
                thirdTF.becomeFirstResponder()
            }
            if textField == thirdTF
            {
                fourthTF.becomeFirstResponder()
            }
            if textField == fourthTF
            {
                fourthTF.becomeFirstResponder()
            }
            textField.text = string
            return false
        }
            
        else if (range.length == 1)
            //(((textField.text?.count)!) >= 1 ) && (string.count == 0)
        {
            if textField == secondTF {
                firstTF.becomeFirstResponder()
            }
            if textField == thirdTF
            {
                secondTF.becomeFirstResponder()
            }
            if textField == fourthTF
            {
                thirdTF.becomeFirstResponder()
            }
            if textField == firstTF
            {
                firstTF.becomeFirstResponder()
            }
            textField.text = ""
            return false
        }
        else if ((textField.text?.count)!) >= 1
        {
            textField.text = string
            return false
        }
        return true
    }
    
    
    @IBAction func verifyButton(_ sender: Any) {
        
        Analytics.logEvent("Verific_Code_Verify_Clicked", parameters:nil)
        
        if firstTF.text?.isEmpty == true || secondTF.text?.isEmpty  == true  || thirdTF.text?.isEmpty  == true  || fourthTF.text?.isEmpty  == true
        {
            showAlert(title: "", msg: "Please enter Verification code")
            
            return
        }
        
        let otpString = "\(String(describing: firstTF.text!))" +  "\(String(describing: secondTF.text!))" + "\(String(describing: thirdTF.text!))" + "\(String(describing: fourthTF.text!))"
        
        
        print(otpString)
        
        guard let mobileNumber = (UserDefaults.standard.string(forKey: "mobileNumber")) else {
            
            return
        }
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(mobileNumber, forKey: "mobileNo")
        parameter.setValue(otpString, forKey: "otp")
        
        APIServices.postDataToServer(url: API.verifyOTP, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                
                
                var derivedData = response as! [String : Any]
                
                print(derivedData)
                if let theJSONData = try? JSONSerialization.data(withJSONObject: derivedData["response"] as Any , options:[])
                {
                    do{
                        if let profileInfo = try? JSONDecoder().decode(profile.self, from: theJSONData) {
                            
                            print(profileInfo)
                            
                               UserDefaults.standard.set(profileInfo.jwtToken, forKey: "jwtToken")
                            
                            if let data = try? JSONEncoder().encode(profileInfo) {
                                UserDefaults.standard.set(data, forKey: "profile_details")
                                
                            }
                            
                            
                            print( UserDefaults.standard.string(forKey: "jwtToken"))
                        }
                    }
                }
               
                
             
                
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                self.present(nextViewController, animated:true, completion:nil)
            }else
            {
                
                UserDefaults.standard.removeObject(forKey: "profile_details")
                
            }
        })
        
        
    }
    
    @IBAction func resendotp(_ sender: Any) {
        
        Analytics.logEvent("Verific_Code_Resend_Clicked", parameters:nil)
        
        guard let mobileNumber = (UserDefaults.standard.string(forKey: "mobileNumber")) else {
            
            return
        }
        guard let countryCode = (UserDefaults.standard.string(forKey: "countryCode")) else {
            
            return
        }
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(mobileNumber, forKey: "mobileNo")
         parameter.setValue(countryCode, forKey: "countryCode")
        
        print(parameter)
        APIServices.postDataToServer(url: API.reSendOTP, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                self.showAlert(title: "", msg: "OTP Resent Successfully")
                
            }
        })
        
    }
    
}
