//
//  ChangePasswordVC.swift
//  CloudPeppers
//
//  Created by Allvy on 25/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import CoreData
import Firebase

class ChangePasswordVC: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var oldPasswordTF: UITextField!
    
    @IBOutlet weak var disView: UIView!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var confirmPassTF: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        confirmPassTF.clearsOnInsertion = false
        confirmPassTF.clearsOnBeginEditing = false
        
        newPasswordTF.clearsOnInsertion = false
        newPasswordTF.clearsOnBeginEditing = false
        
        oldPasswordTF.clearsOnInsertion = false
        oldPasswordTF.clearsOnBeginEditing = false
        
        newPasswordTF.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        confirmPassTF.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        oldPasswordTF.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        
        disView.displayViewShadow()
        submitButton.changeButtonCornerRadius()
//        oldPasswordTF.setBottomBorder()
//        newPasswordTF.setBottomBorder()
//        confirmPassTF.setBottomBorder()
        
        
                oldPasswordTF.useUnderline()
                newPasswordTF.useUnderline()
                confirmPassTF.useUnderline()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         Analytics.logEvent("ChangePasswordEnter", parameters:nil)
        oldPasswordTF.delegate = self
        newPasswordTF.delegate = self
        confirmPassTF.delegate = self
          let revealViewController:SWRevealViewController = self.revealViewController()
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("ChangePasswordExit", parameters:nil)
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func oldPasswordVisible(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        /***If eye non - visible (defalut) secured***/
        oldPasswordTF.isSecureTextEntry = !sender.isSelected
    }
    
    
    @IBAction func newPasswordVisible(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        /***If eye non - visible (defalut) secured***/
        newPasswordTF.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func confirmPasswordVisible(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        /***If eye non - visible (defalut) secured***/
        confirmPassTF.isSecureTextEntry = !sender.isSelected
    }
    
    
    
    
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        
         Analytics.logEvent("ChangePassword_Screen_Submit_Clicked", parameters:nil)
        if isAccountHasValidDetails() == true
        {
           
             let revealViewController:SWRevealViewController = self.revealViewController()

            guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                let profileData = try? JSONDecoder().decode(profile.self, from: data) else
            {
                return
            }
            
            
            let parameter:NSMutableDictionary = NSMutableDictionary()
            parameter.setValue(profileData.userId, forKey: "userId")
            parameter.setValue(oldPasswordTF.text, forKey: "oldPassword")
            parameter.setValue(newPasswordTF.text, forKey: "newPassword")
            
            
            
            APIServices.postDataToServer(url: API.changePassword, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
                if status == true{
                    
                //    let alertController = UIAlertController(title: Constants.alertTitle, message: "Password updated successfully", preferredStyle: .alert)
                    
                    
                    let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                    
                    let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
                    let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
                    
                    let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
                    let msgAttrString = NSMutableAttributedString(string: "Password updated successfully", attributes: msgFont)
                    
                    alert.setValue(titAttrString, forKey: "attributedTitle")
                    alert.setValue(msgAttrString, forKey: "attributedMessage")
                    
                    
//                    if let password = (UserDefaults.standard.string(forKey: "rememberme_password")) ,password != "" {
//                        UserDefaults.standard.set(self.newPasswordTF.text, forKey: "rememberme_password")
//                    }
//                    else
//                    {
//                        UserDefaults.standard.set("", forKey: "rememberme_password")
//                    }
                    
                    
                       UserDefaults.standard.set("", forKey: "rememberme_password")

                  

                    
                    let settingsAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                        if #available(iOS 10.0, *) {
                            self.deleteAllRecords()
                           
                            let revealViewController:SWRevealViewController = self.revealViewController()
                            UserDefaults.standard.removeObject(forKey: "profile_details")
                            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let desController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                            let newFrontViewController = UINavigationController.init(rootViewController:desController)
                            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
                        //    self.navigationController?.popViewController(animated: true)
                             self.dismiss(animated: true, completion: nil)
                        } else {
                            // Fallback on earlier versions
                        }
                    }
                    
                    
                    alert.addAction(settingsAction)
                    self.present(alert, animated: true, completion: nil)
                    print(response)
                    
                 
                }
            })
            
        }
    }
    
    func deleteAllRecords() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "PushNotifications")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }

    func isAccountHasValidDetails()  -> Bool {
        
        if oldPasswordTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Incorrect password")
            return false
        }
        else if (oldPasswordTF.text?.count)! < 8 || (oldPasswordTF.text?.count)! > 16
        {
            showAlert(title: "", msg: "Password should contain minimum 8 and maximum 16 characters")
            return false
        }
        if oldPasswordTF.text == newPasswordTF.text
        {
            showAlert(title: "" , msg: "new password must different from your previous password")
            return false
        }
        if newPasswordTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Incorrect password")
            return false
        }
        else if (newPasswordTF.text?.count)! < 8 || (newPasswordTF.text?.count)! > 16
        {
            showAlert(title: "", msg: "Password should contain minimum 8 and maximum 16 characters")
            return false
        }
        if confirmPassTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Incorrect password")
            return false
        }
        else if (confirmPassTF.text?.count)! < 8 || (confirmPassTF.text?.count)! > 16
        {
            showAlert(title: "" , msg: "Password should contain minimum 8 and maximum 16 characters")
            return false
        }

        if newPasswordTF.text == confirmPassTF.text
        {
            return true
        }
        else
        {
           showAlert(title: "" , msg: "confirm password should match with new password")
            return false
        }
       
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if (textField == oldPasswordTF) || (textField == newPasswordTF) || (textField == confirmPassTF)
        {
            return range.location < 16
        }
        
        return true
    }
}
