//
//  ForgotPasswordVC.swift
//  CloudPeppers
//
//  Created by Allvy on 24/04/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase

class ForgotPasswordVC: UIViewController,UITextFieldDelegate  {
    
    
    
    @IBOutlet weak var resendButton: UIButton!
    
    
    @IBOutlet weak var newPasswordEye: UIButton!
    
    @IBOutlet weak var confirmPasswordEye: UIButton!
    @IBOutlet var informationlabel: UILabel!
    
    @IBOutlet weak var firstTF: UITextField!
    
    @IBOutlet weak var secondTF: UITextField!
    @IBOutlet weak var thirdTF: UITextField!
    @IBOutlet weak var fourthTF: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var forgotSuccessView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    
    @IBOutlet var verificationCodeView: UIView!
    
    
    
    
    
    override func viewDidLoad() {
        
        submitButton.changeButtonCornerRadius()
        loginButton.changeButtonCornerRadius()
        super.viewDidLoad()
        
         Analytics.logEvent("ForgotPasswordEnter", parameters:nil)
        firstTF.delegate = self
        secondTF.delegate = self
        thirdTF.delegate = self
        fourthTF.delegate = self
        
        newPasswordTF.delegate = self
        confirmPasswordTF.delegate = self
        
        newPasswordTF.isSecureTextEntry = true
        confirmPasswordTF.isSecureTextEntry = true
        
        newPasswordTF.useUnderline()
        confirmPasswordTF.useUnderline()
        
        
      
        submitButton.changeButtonCornerRadius()
        
        guard  let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data,
            let organizationdetails = try? JSONDecoder().decode(response.self, from: data)
            else
        {
            return
        }
        
//        addBorderTF(textfield: firstTF)
//        addBorderTF(textfield: secondTF)
//        addBorderTF(textfield: thirdTF)
//        addBorderTF(textfield: fourthTF)
        
        forgotSuccessView.isHidden = true
        
       
        
    }
    
    
    @IBAction func newPasswordTap(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        /***If eye non - visible (defalut) secured***/
        newPasswordTF.isSecureTextEntry = !sender.isSelected
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("ForgotPasswordExit", parameters:nil)
    }

    
    @IBAction func confirmPasswordTap(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        /***If eye non - visible (defalut) secured***/
        confirmPasswordTF.isSecureTextEntry = !sender.isSelected
    }
    
    
    
    @IBAction func resentButtonTapped(_ sender: UIButton) {
        
        guard let mobileNumber = (UserDefaults.standard.string(forKey: "mobileNumber")) else {
            
            return
        }
        guard let countryCode = (UserDefaults.standard.string(forKey: "countryCode")) else {
            
            return
        }
       
        
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(mobileNumber, forKey: "mobileNo")
        parameter.setValue(countryCode, forKey: "countryCode")
        
        
        
        APIServices.postDataToServer(url: API.reSendOTP, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                self.showAlert(title: "", msg: "OTP Resent Successfully")
                
            }
        })
        
    }
        
        
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        Analytics.logEvent("Forgot_Pw_Submit_Clicked", parameters:nil)
        if  isAccountHasValidDetails() == true
        {
            if firstTF.text?.isEmpty == true || secondTF.text?.isEmpty  == true  || thirdTF.text?.isEmpty  == true  || fourthTF.text?.isEmpty  == true
            {
                showAlert(title: "", msg: "Please enter Verification code")
                
                return
            }
            
            let otpString = "\(String(describing: firstTF.text!))" +  "\(String(describing: secondTF.text!))" + "\(String(describing: thirdTF.text!))" + "\(String(describing: fourthTF.text!))"
            
            guard let mobileNumber = (UserDefaults.standard.string(forKey: "mobileNumber")) else {
                
                return
            }
            
            let parameter:NSMutableDictionary = NSMutableDictionary()
            parameter.setValue(mobileNumber.getStringFromPhoneNumber(), forKey: "mobileNo")
            parameter.setValue(otpString, forKey: "otp")
            parameter.setValue(newPasswordTF.text, forKey: "password")
            
            print(parameter)
            
            
            APIServices.postDataToServer(url: API.resetPassword, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
                if status == true{
                    self.informationlabel.isHidden = true
                    self.verificationCodeView.isHidden = true
                    self.forgotSuccessView.isHidden = false
                    self.newPasswordEye.isHidden = true
                    self.confirmPasswordEye.isHidden = true
                    self.newPasswordTF.isHidden = true
                    self.confirmPasswordTF.isHidden = true
                    self.resendButton.isHidden = true
                    
                    
                    let alertController = UIAlertController(title: "", message: "Your Password has been successfully changed", preferredStyle: .alert)
                    
                    
//                    if let password = (UserDefaults.standard.string(forKey: "rememberme_password")) ,password != "" {
//                        UserDefaults.standard.set(self.newPasswordTF.text, forKey: "rememberme_password")
//                    }
//                    else
//                    {
//                        UserDefaults.standard.set("", forKey: "rememberme_password")
//                    }

                 //UserDefaults.standard.set(self.newPasswordTF.text, forKey: "rememberme_password")
                    UserDefaults.standard.set("", forKey: "rememberme_password")
                 
                    
                    
                    let settingsAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                        if #available(iOS 10.0, *) {
                          //  self.navigationController?.popViewController(animated: true)
                          //  self.dismiss(animated: true, completion: nil)
                        } else {
                            // Fallback on earlier versions
                        }
                    }
                    
                    
                    alertController.addAction(settingsAction)
                    self.present(alertController, animated: true, completion: nil)
                    print(response)
                    
                    
                }
            })
            
            
            
            
            
           
           
        }
        
        
    }
    
    @IBAction func dismisButton(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == newPasswordTF) || (textField == confirmPasswordTF)
        {
            return range.location < 16
        }
        
        
        if (((textField.text?.count)!) < 1 ) && (string.count > 0)
        {
            if textField == firstTF {
                secondTF.becomeFirstResponder()
            }
            if textField == secondTF
            {
                thirdTF.becomeFirstResponder()
            }
            if textField == thirdTF
            {
                fourthTF.becomeFirstResponder()
            }
            if textField == fourthTF
            {
                fourthTF.becomeFirstResponder()
            }
            textField.text = string
            return false
        }
            
        else if (((textField.text?.count)!) >= 1 ) && (string.count == 0)
        {
            if textField == secondTF {
                firstTF.becomeFirstResponder()
            }
            if textField == thirdTF
            {
                secondTF.becomeFirstResponder()
            }
            if textField == fourthTF
            {
                thirdTF.becomeFirstResponder()
            }
            if textField == firstTF
            {
                firstTF.becomeFirstResponder()
            }
            textField.text = ""
            return false
        }
        else if ((textField.text?.count)!) >= 1
        {
            textField.text = string
            return false
        }
        return true
    }
    
    func addBorderTF(textfield:UITextField)
    {
        textfield.layer.cornerRadius = 10
        textfield.clipsToBounds = true
        textfield.layer.borderColor = #colorLiteral(red: 0.6360199331, green: 0.5160852983, blue: 1, alpha: 1)
        textfield.layer.borderWidth = 1.5
        
    }
    
    func isAccountHasValidDetails()  -> Bool {
        
        if firstTF.text?.isEmpty == true || secondTF.text?.isEmpty  == true  || thirdTF.text?.isEmpty  == true  || fourthTF.text?.isEmpty  == true
        {
            showAlert(title: "", msg: "Please enter SMS Verification code")
            
            return false
        }
        
        if newPasswordTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Incorrect password")
            return false
        }
        else  if confirmPasswordTF.text?.isEmpty == true
        {
            showAlert(title: "", msg: "Enter Confirm Password")
            return false
        }
            
        else if ((newPasswordTF.text?.count)! < 8 || (newPasswordTF.text?.count)! > 16) || ((confirmPasswordTF.text?.count)! < 8 || (confirmPasswordTF.text?.count)! > 16)
        {
            showAlert(title: "" , msg: "Password  should contain minimum 8 and maximum 16 characters")
            return false
        }
        
        if newPasswordTF.text != confirmPasswordTF.text
        {
            self.showAlert(title: "", msg: "confirm password should match with New password")
            return false
        }
        
        return true
    }
    
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(nextViewController, animated:true, completion:nil)
    }
    
}
