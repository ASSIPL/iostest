//
//  SplashscreenViewController.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 23/04/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD
import Firebase

class SplashscreenViewController: UIViewController {
    
    @IBOutlet weak var coolpadLinkImage: UIImageView!
    @IBOutlet var poweredByLabel: UILabel!
    @IBOutlet var logoImageView: UIImageView!
    @IBOutlet var version: UILabel!
    let text = "Powered By coolpad"
    var imageview : [UIImageView]?
    var imageviewsaved :  UIImageView?
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
       
        
//      //  poweredByLabel.text = text
//        self.poweredByLabel.textColor =  UIColor.white
//        let underlineAttriString = NSMutableAttributedString(string: text)
//        let range1 = (text as NSString).range(of: "coolpad")
//        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
//        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Montserrat-Bold", size: 17)!, range: range1)
//        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value:Constants.titleColor, range: range1)
//        poweredByLabel.attributedText = underlineAttriString
//        poweredByLabel.isUserInteractionEnabled = true
//        poweredByLabel.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(tapLabel(gesture:))))
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedMe))
        coolpadLinkImage.addGestureRecognizer(tap)
        coolpadLinkImage.isUserInteractionEnabled = true
        
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        
        if data == nil
        {
            
            SVProgressHUD.show()
            HttpWrapper.gets(with: API.getOrganizationInfo, parameters: nil, headers: nil, completionHandler: { (response, error)  in
                print(response)
                
                SVProgressHUD.dismiss()
                guard let data = response else { return }
                do {
                    
                    let loginRespone = try JSONDecoder().decode(ApiStatus.self, from: data)
                    let loginStatus = loginRespone.status
                    if  loginStatus  ==  true
                    {
                        
                        if let data = try? JSONEncoder().encode(loginRespone.response) {
                            UserDefaults.standard.set(data, forKey: "organizationdetails")
                        }
                        
                        
                        SharedData.data.organizationdetails  = loginRespone.response
                        
                        
                        for each in (SharedData.data.organizationdetails?.roles)!
                        {
                            print(each.imageUrl)
                            self.imageviewsaved?.sd_setImage(with: URL(string: each.imageUrl!),placeholderImage: UIImage(named: "cool"))
                            
                        }
                    }
                    
                    _ = UIApplication.shared.delegate as! AppDelegate
                    let id = "LoginViewController"
                    let vc = UIStoryboard.getViewController(storyboardName: "Main", storyboardId: id) as? LoginViewController
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = vc
                        appDelegate.window?.makeKeyAndVisible()
                    }
                    
                    self.logoImageView.sd_setImage(with: URL(string: (SharedData.data.organizationdetails?.organization?.logo)!), placeholderImage: UIImage(named: "cool"))
                    //    self.version.text = SharedData.data.organizationdetails?.organization?.versionId
                    
                    self.version.text =  "1.0.26"
                    
                } catch let jsonErr {
                    
                    
                    
                    print("Error serializing json:", jsonErr)
                    
                }
                
                
            }){ (error) in
                
                print(error)
                if Reachability.isConnectedToNetwork(){
                    print("Internet Connection Available!")
                }else{
                    self.showAlert(title: "", msg: "No network connection")
                }
            }
            
            
        }else
        {
            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
            
            self.logoImageView.sd_setImage(with: URL(string: (organizationdetails?.organization?.logo)!), placeholderImage: UIImage(named: "cool"))
            
            print(organizationdetails?.organization?.logo)
            //self.logoImageView.sd_setImage(with: URL(string: (organizationdetails?.organization?.splashImage)!), placeholderImage: UIImage(named: "SplashLogoImage"))
            
            //   self.version.text =  "Version" + " " + "\(String(describing: (organizationdetails?.organization?.versionId!)!))"
            self.version.text =  "1.0.26"
            
            if UserDefaults.standard.value(forKey: "profile_details") != nil
            {
                
                _ = UIApplication.shared.delegate as! AppDelegate
                let id = "SWRevealViewController"
                let vc = UIStoryboard.getViewController(storyboardName: "Main", storyboardId: id)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = vc
                    appDelegate.window?.makeKeyAndVisible()
                }
                
            }else
            {
                
                _ = UIApplication.shared.delegate as! AppDelegate
                
                
                let pickedOptionsValue =  UserDefaults.standard.string(forKey: "pickedOptions")
                
                
                //                if pickedOptionsValue == "true"
                //                {
                //                    id = "LoginViewController"
                //                    vc = UIStoryboard.getViewController(storyboardName: "Main", storyboardId: id)
                //
                //
                //                }else
                //                {
                //
                //                    id = "UserOptionViewController"
                //                    vc = UIStoryboard.getViewController(storyboardName: "Main", storyboardId: id)
                //
                // }
                
                if (UserDefaults.standard.value(forKey: "isFirstLaunchs") as? Bool) != nil {
                    
                    let id = "LoginViewController"
                    let  vc = UIStoryboard.getViewController(storyboardName: "Main", storyboardId: id)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = vc
                        appDelegate.window?.makeKeyAndVisible()
                    }
                    
                    
                    
                    
                    
                    return
                    
                    
                }else
                {
                    
                    //                    let id = "UserOptionViewController"
                    //                    _ = UIStoryboard.getViewController(storyboardName: "Main", storyboardId: id)
                    
                    let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let desController = mainStoryboard.instantiateViewController(withIdentifier: "TutorialViewController") as! TutorialViewController
                    //   vc = UIStoryboard.getViewController(storyboardName: "Main", storyboardId: id) as? TutorialViewController
                    UserDefaults.standard.set(false, forKey: "isFirstLaunchs")
                    UserDefaults.standard.synchronize()
                    desController.fromFirstTime = true
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = desController
                        appDelegate.window?.makeKeyAndVisible()
                    }
                    
                }
                
                
            }
            
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Analytics.logEvent("Splash_Entered", parameters:nil)
       
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("Splash_Exit", parameters:nil)
    }
    
    @objc func tappedMe()
    {
        // let termsRange = (text as NSString).range(of: "coolpad")
        // comment for now
        //let privacyRange = (text as NSString).range(of: "Privacy Policy")
        
        //        if gesture.didTapAttributedTextInLabel(label: powerBy, inRange: termsRange) {
        //            print("Tapped terms")
        //
        
        
        //            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        //            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        //
        //            let urlString = organizationdetails?.organization?.orginationURL
        //
        //            guard let url = URL(string: urlString!) else { return }
        //
        //            let svc = SFSafariViewController(url: url)
        //            present(svc, animated: true, completion: nil)
        //UIApplication.shared.open(url)
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "TermsCondViewController") as! TermsCondViewController
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        vc.urlString = organizationdetails?.organization?.orginationURL
        self.present(vc, animated: true, completion: nil)
        //
        // }
        //        } else if gesture.didTapAttributedTextInLabel(label: poweredByLabel, inRange: privacyRange) {
        //            print("Tapped privacy")
        //        else {
        //            print("Tapped none")
        //        }
        
    }
    
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        let termsRange = (text as NSString).range(of: "coolpad")
        // comment for now
        //let privacyRange = (text as NSString).range(of: "Privacy Policy")
        
        if gesture.didTapAttributedTextInLabel(label: poweredByLabel, inRange: termsRange) {
            print("Tapped terms")
            
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "TermsCondViewController") as! TermsCondViewController
            
            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
            
            vc.urlString = organizationdetails?.organization?.orginationURL
            self.present(vc, animated: true, completion: nil)
            
        }
            //        } else if gesture.didTapAttributedTextInLabel(label: poweredByLabel, inRange: privacyRange) {
            //            print("Tapped privacy")
        else {
            print("Tapped none")
        }
    }
    
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        //let textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
        //(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        //let locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
        // locationOfTouchInLabel.y - textContainerOffset.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}
extension UIStoryboard {
    
    class func getStoryboard(storyboardName: String?) -> UIStoryboard {
        if let name = storyboardName {
            return UIStoryboard(name: name, bundle: nil)
        }
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    class func getViewController(storyboardName: String?, storyboardId: String?) -> UIViewController {
        guard let id = storyboardId, let name = storyboardName else {
            return UIViewController()
        }
        return getStoryboard(storyboardName: name).instantiateViewController(withIdentifier: id)
    }
    
}
