//
//  PermissionRequestView.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 24/04/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import SVProgressHUD
import MessageUI
import IQKeyboardManagerSwift
import Firebase

class SetRulesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate,UITextViewDelegate,customDelegate {
     @IBOutlet weak var locationPointButton: UIButton!
    
    @IBOutlet weak var editViewHeightConstant: NSLayoutConstraint!
    
       var toyouApprove :[responses]?
    
    @IBOutlet var lockBgViewObj: UIView!
    @IBOutlet weak var lockViewObj: UIView!
    @IBOutlet weak var lockUnlockDescLbl: UILabel!
    @IBOutlet weak var lockCancelBtn: UIButton!
    
    @IBOutlet weak var messageViewBackground: UIView!
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var widthConstant: NSLayoutConstraint!
    @IBOutlet weak var heightConstant1: NSLayoutConstraint!
    
    
    @IBOutlet var batterySymbol: UIImageView!
    
    @IBOutlet var permissionTableView: UITableView!
    
    
    @IBOutlet var heightConstant: NSLayoutConstraint!
    @IBOutlet var messageTextField: UITextView!
    
    @IBOutlet var deleteButton: UIButton!
    
    @IBOutlet var editButton: UIButton!
    
    @IBOutlet var messageView: UIView!
    @IBOutlet var sendRequestButton: UIButton!
    @IBOutlet var selectPermissionButton: UIButton!
    @IBOutlet var profileHeaderName: UILabel!
    @IBOutlet var profileName: UILabel!
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var batteryStatus: UILabel!
    @IBOutlet var mobileNumber: UILabel!
    @IBOutlet var mailId: UILabel!
    
    
 
    
    var imageString:String?
    var memberId:Int?
    var userId:Int?
    var chooseoptions:Bool?
      var lockUnlockRowId : Int?
    
    @IBOutlet var topheight: NSLayoutConstraint!
    
    var permissionlist:Permissionlist?
    var getMemberDetails:responses?
    var membersGeoList:MembersGeoList?
    
    var featureList:[FeatureList]?

    var fromNotificationTap:String?
    
    var getMemberDetailsbyId:Responsedata?
    var seletedarray = [String]()
    var seletedarrayCompair1 = [String]()
    var seletedarrayCompair2 = [String]()
    
    @IBOutlet var viewCornortop: UIView!
    
    var menuNameArray:Array = [String]()
    var iconsArray:Array = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        if let data = UserDefaults.standard.value(forKey: "Paymentdata") as? Data,
            let profileData = try? JSONDecoder().decode(UserPlans.self, from: data)
        {
            
            featureList = profileData.featureList
            
            if (featureList?.contains(where: { name in name.featureKey == "key.lockAndUnlock"}))!
            {
                var FeatureList1 = FeatureList()
                FeatureList1.featureKey = "Lock Device"
                
                var FeatureList2 = FeatureList()
                FeatureList2.featureKey = "Unlock Device"
                
                
                featureList?.append(FeatureList2)
                
                featureList?.append(FeatureList1)
                
            }
            
          
            
            
//            print(featureList)
            
            
            if (featureList?.contains(where: { name in name.featureKey == "key.battery"}))!
            {
                

                self.batteryStatus.isHidden = false
                self.batterySymbol.isHidden = false
                
                
            }else
            {
                
                self.batterySymbol.isHidden = true
                self.batteryStatus.isHidden = true
                
            }
            
            
//            if UserPlans?.remainDays ?? 0 <= 0
//            {
//
//            }
            
            
         
            
            //  return
            
        }
        
        
        //  messageView.dropShadow()
        messageView.layer.cornerRadius = 20
        messageView.clipsToBounds = true
        messageViewBackground.dropShadow()
        cancelButton.changeButtonCornerRadius()
        sendButton.changeButtonCornerRadius()
        sendRequestButton.changeButtonCornerRadius()
        
        
        self.lockBgViewObj.center.x = self.view.center.x
        self.lockBgViewObj.center.y = self.view.center.y
        lockBgViewObj.frame = self.view.frame
        
        lockBgViewObj.isHidden = true
        lockBgViewObj.layer.cornerRadius = 20
        lockBgViewObj.clipsToBounds = true
        lockBgViewObj.layer.masksToBounds = true
        // lockViewObj.dropShadow()
        
        lockViewObj.layer.cornerRadius = 20
        lockViewObj.layer.borderWidth = 0.5
        lockViewObj.layer.borderColor = UIColor.white.cgColor
        lockViewObj.layer.masksToBounds = true
        lockViewObj.clipsToBounds = true
        
        
        
        profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        profileImage.clipsToBounds = true
        self.profileImage.layer.masksToBounds = true
        profileImage.layer.borderColor = #colorLiteral(red: 0, green: 0.7152789235, blue: 0.7682691216, alpha: 1)
        profileImage.layer.borderWidth = 2.0
        
        menuNameArray = ["Set Geofence","Apps & Games","Lock Device","Daily Time Limits"]
        //   seletedarray.append("Location")
        viewCornortop.roundedTop()
        //   iconsArray = [#imageLiteral(resourceName: "FamilyLabs"),#imageLiteral(resourceName: "Apps & Games"),#imageLiteral(resourceName: "Lock Device"),#imageLiteral(resourceName: "Daily Time Limits")]
        permissionTableView.dataSource = self
        permissionTableView.delegate = self
        
        
        
        self.messageView.center.x = self.view.center.x
        self.messageView.center.y = self.view.center.y
        
        messageView.frame = self.view.frame
        messageView.isHidden = true
        
           messageTextField.delegate = self
        
        sendRequestButton.isHidden = true
        heightConstant.constant = 0
        
             self.title = "Manage"
        
        
        
        if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type" || UserDefaults.standard.string(forKey: "roleKey") == "thing.type"
        {
            deleteButton.isHidden = true
            editButton.isHidden = true
        }
        
     
        
        if fromNotificationTap == "true"
        {
            
            
            callgetMemberApi1111()
            
            
             callgetMemberApi()
            return
        }
        
       
   
        
        
        profileName.text =  "\(String(describing: (getMemberDetails?.firstName ?? "")!))" +  " " +  "\(String(describing: (getMemberDetails?.lastName ?? "")!))"
    
        
       
        
        if getMemberDetails?.profileImage != nil && getMemberDetails?.profileImage != ""
        {
            profileImage.sd_setImage(with: URL(string: (getMemberDetails?.profileImage!)!), placeholderImage: UIImage(named: "placeHolderProfile"))
        }
        
        
      
        
        if getMemberDetails?.batteryStatus == ""
        {
            batteryStatus.text = "0%"
        }else
        {
            if getMemberDetails!.batteryStatus != nil
            {
                batteryStatus.text = String(describing:  getMemberDetails?.batteryStatus ?? "") +  "%"
                
            }
           
            print(batteryStatus.text)
        }
        
        mobileNumber.text =  "\((getMemberDetails?.countryCode ?? ""))" + " " + (getMemberDetails?.mobileNo?.getPhoneNumberFromString() ?? "")
        //  mobileNumber.text = getMemberDetails?.mobileNo?.getPhoneNumberFromString()
        mailId.text = getMemberDetails?.email
           callgetMemberApi()
     
        
        // Do any additional setup after loading the view.
    }
    
    
    func callgetMemberApi1111()
    {
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        let addMemberUrl = API.getFromYourToYou + "?userId=\(String(describing: profileData.userId!))"
        print(addMemberUrl)
        
        
          SVProgressHUD.show()
        HttpWrapper.gets(with: addMemberUrl, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            print(response)
             SVProgressHUD.dismiss()
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatuspermissionRequest.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    
                    
                    
                    
                    print(loginRespone.response?.toyouApprove)
                    
                    self.toyouApprove  = loginRespone.response?.toyouApprove
                    
                    
                    for each in self.toyouApprove!
                    {
                        
                        
                        print(self.memberId)
                        
                           print(each.memberId)
                        if each.userId == self.memberId
                        {
                            self.getMemberDetails = each
                        }
                        
                    }
                    
                    
                    
                    
                    
                    self.profileName.text =  "\(String(describing: (self.getMemberDetails?.firstName ?? "")!))" +  " " +  "\(String(describing: (self.getMemberDetails?.lastName ?? "")!))"
                    
                    if self.getMemberDetails?.profileImage != nil && self.getMemberDetails?.profileImage != ""
                    {
                        self.profileImage.sd_setImage(with: URL(string: (self.getMemberDetails?.profileImage!)!), placeholderImage: UIImage(named: "placeHolderProfile"))
                    }
                    
                    
                    
                    
                    if self.getMemberDetails?.batteryStatus == ""
                    {
                        self.batteryStatus.text = "0%"
                    }else
                    {
                        if self.getMemberDetails!.batteryStatus != nil
                        {
                            self.batteryStatus.text = String(describing:  self.getMemberDetails?.batteryStatus ?? "") +  "%"
                            
                        }
                        
                        print(self.batteryStatus.text)
                    }
                    
                    self.mobileNumber.text =  "\((self.getMemberDetails?.countryCode ?? ""))" + " " + (self.getMemberDetails?.mobileNo?.getPhoneNumberFromString() ?? "")
                    //  mobileNumber.text = getMemberDetails?.mobileNo?.getPhoneNumberFromString()
                    self.mailId.text = self.getMemberDetails?.email
                    
                    
                    
                    
                    
                  
                }
                else
                    
                {
                   SVProgressHUD.dismiss()
                    
                }
                
            }catch let jsonErr {
                
                
               SVProgressHUD.dismiss()
                print("Error serializing json:", jsonErr)
                
            }
        }){ (error) in
            
             SVProgressHUD.dismiss()
            SharedData.data.getMemberDetails=nil
        
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
            print(error)
        }
    }
    
    func didSelectData(_ result: responses) {
    
    profileName.text =  "\(String(describing: (result.firstName ?? "")))" +  " " +  "\(String(describing: (result.lastName ?? "")))"
    
    getMemberDetails = result
    
    }
    func callgetMemberApi()
    {
        
        guard let memberId = memberId else
            
        {
            
            return
            
        }
        
        //     http://localhost:8080/getMemberPermissionToYouFromYou?userId=2&memberId=189&fromOrTo=toyou&approveOrReject=approve
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        userId = profileData.userId
        
        let datas = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: datas!)
        let  url = API.getMemberPermissionToYouFromYou + "?userId=\(String(describing: profileData.userId!))" + "&memberId=\(String(describing: memberId))" + "&fromOrTo=toyou" + "&approveOrReject=approve"
        print(url)
        
        seletedarray.removeAll()
        seletedarrayCompair1.removeAll()
        seletedarrayCompair2.removeAll()
        
        
        SVProgressHUD.show()
        HttpWrapper.posts(with: url, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            print(response)
            
            SVProgressHUD.dismiss()
            guard let data = response else { return }
            do {
                let loginRespone = try JSONDecoder().decode(ApiStatuspermission.self, from: data)
                let loginStatus = loginRespone.status
                
                if loginStatus!{
                    self.permissionlist = loginRespone.response
                    
                    if self.permissionlist?.ask?.count ?? 0 == 0
                    {
                        self.showAlert(title: "", msg: "No Permissions found")
                    }
                    
                    for each in self.permissionlist?.ask ?? []
                    {
                        if self.seletedarray.contains(each.roleType ?? "") {
                            self.seletedarray.remove(at: self.seletedarray.index(of: (each.roleType ?? "")) ?? 0)
                        } else {
                            self.seletedarray.append(each.roleType ?? "")
                        }
                        
                        
                        print(each.roleType)
                    }
                    
                    
                    print(self.seletedarray)
                    
                    for each in (self.permissionlist?.ask)!
                    {
                        if each.roleType == "key.dailyTimeLimits" || each.roleType == "key.restrictedTimes" ||  each.roleType == "Unlock Device"  || each.roleType == "Lock Device"
                            
                        {
                            if self.seletedarray.contains("key.lockAndUnlock") {
                                
                            } else {
                                self.seletedarray.append("key.lockAndUnlock")
                            }
                        }else
                        {
                            
                        }
                    }
                    
                    
                    if self.permissionlist?.ask?.count ?? 0 > 0
                    {
                        
                        self.batteryStatus.isHidden = false
                        self.batterySymbol.isHidden = false
                        
                        
                    }else
                    {
                        
                        self.batterySymbol.isHidden = true
                        self.batteryStatus.isHidden = true
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                    for each in (self.permissionlist?.ask)!
                    {
                        //print(each.roleType)
                        if each.roleType == "key.battery"
                        {
                            
                            
                            
                            self.batteryStatus.isHidden = false
                            self.batterySymbol.isHidden = false
                            break
                        }else
                        {
                            self.batteryStatus.isHidden = true
                            self.batterySymbol.isHidden = true
                        }
                        
                        
                    }
                    
                    
                    if let data = UserDefaults.standard.value(forKey: "Paymentdata") as? Data,
                        let profileData = try? JSONDecoder().decode(UserPlans.self, from: data)
                    {
                        if (self.featureList?.contains(where: { name in name.featureKey == "key.battery"}))!
                        {
                            
                            
                            
                            for each in (self.permissionlist?.ask)!
                            {
                                //print(each.roleType)
                                if each.roleType == "key.battery"
                                {
                                    
                                    
                                    
                                    self.batteryStatus.isHidden = false
                                    self.batterySymbol.isHidden = false
                                    break
                                }else
                                {
                                    self.batteryStatus.isHidden = true
                                    self.batterySymbol.isHidden = true
                                }
                                
                                
                            }
                            
                            
                            
                            
//                            self.batteryStatus.isHidden = false
//                            self.batterySymbol.isHidden = false
                            
                            
                        }else
                        {
                            
                            self.batterySymbol.isHidden = true
                            self.batteryStatus.isHidden = true
                            
                        }
                    }else
                    {
                        self.batteryStatus.isHidden = true
                        self.batterySymbol.isHidden = true
                        
                        
                    }
                    
                    
                    
                    
                    
                    
                    

                    
//                    if (self.featureList?.contains(where: { name in name.featureKey == "key.battery"}))!
//                    {
//
//                        self.batteryStatus.isHidden = false
//                        self.batterySymbol.isHidden = false
//
//
//                    }else
//                    {
//
//                        self.batterySymbol.isHidden = true
//                        self.batteryStatus.isHidden = true
//
//                    }
                    
                    
                    
                    for each in (organizationdetails?.rulez)!
                    {
                        
                        if self.seletedarrayCompair1.contains(each.rulesKey!) {
                            self.seletedarrayCompair1.remove(at: self.seletedarrayCompair1.index(of: (each.rulesKey!))!)
                        } else {
                            self.seletedarrayCompair1.append(each.rulesKey!)
                        }
                        
                        
                        print(each.name)
                    }
                    
                    for each in (self.permissionlist?.ask)!
                    {
                        if self.seletedarrayCompair2.contains(each.roleType!) {
                            self.seletedarrayCompair2.remove(at: self.seletedarrayCompair2.index(of: (each.roleType!))!)
                        } else {
                            
                            
                            if each.roleType == "Unlock Device" || each.roleType == "Lock Device"
                            {
                                if self.seletedarrayCompair2.contains("key.lockAndUnlock") {
                                    
                                } else {
                                    self.seletedarrayCompair2.append("key.lockAndUnlock")
                                }
                                
                                
                            }else
                            {
                                self.seletedarrayCompair2.append(each.roleType!)
                            }
                            
                        }
                        
                        
                    }
                    
                    for each in (self.permissionlist?.ask)!
                    {
                        
                        
                        
                    }
                    
                    
                    
                    print(self.seletedarrayCompair1)
                    print(self.seletedarrayCompair2)
                    
                    
                    if self.seletedarrayCompair1.containsSameElements(as: self.seletedarrayCompair2)
                    {
                        self.editViewHeightConstant.constant = 10
                        
                        self.selectPermissionButton.isHidden = true
                        
                    }else
                    {
                        self.editViewHeightConstant.constant = 65
                        self.selectPermissionButton.isHidden = false
                    }
                    
                    
                     DispatchQueue.main.async {
                         self.permissionTableView.reloadData()
                    }
                    
                   
                    print(self.permissionlist)
                }
            }
            catch let jsonErr {
                  SVProgressHUD.dismiss()
                print(jsonErr)
                
            }
            
        }){ (error) in
              SVProgressHUD.dismiss()
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
    }
    
    func updateUi(sender: Responsedata?)
    {
        
        self.profileName.text =  "\(String(describing: (sender?.firstName!)!))"
        batteryStatus.text = (sender?.batteryStatus)! + "%"
        mobileNumber.text = sender?.mobileNo
        mailId.text = sender?.email
        
        
        // profileImage.sd_setImage(with: URL(string: sender?.profileImage!, placeholderImage: UIImage(named: "placeHolderProfile"))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         Analytics.logEvent("SetrulesEntered", parameters:nil)
      //  navigationController?.setNavigationBarHidden(true, animated: animated)
        
        UserDefaults.standard.set(nil, forKey: "notificationTap")
    }
    
    override func  viewWillDisappear(_ animated: Bool) {
        
        
      //  profileName.text =  "\(String(describing: (getMemberDetails?.firstName ?? "")!))" +  " " +  "\(String(describing: (getMemberDetails?.lastName ?? "")!))"
         Analytics.logEvent("SetrulesExit", parameters:nil)
        
        UserDefaults.standard.set(nil, forKey: "notificationTap")
        
        UserDefaults.standard.set(nil, forKey: "approveOrReject")
        UserDefaults.standard.set(nil, forKey: "approveOrReject")
        
        UserDefaults.standard.set(nil, forKey: "approve")
        UserDefaults.standard.set(nil, forKey: "approveOrReject")
        
        
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    @IBAction func chatButtonAction(_ sender: UIButton) {
        
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        // let vc = mainStoryboard.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
        
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        vc.name = profileName.text
        vc.memberId = userId
        vc.getMemberDetails = getMemberDetails
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
        
    }
    
    
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if chooseoptions == true
        {
            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
            
            return (organizationdetails?.rulez?.count)!
        }
        
        
        
        
        
        
        return  self.permissionlist?.ask?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PermissionTableViewCell") as! PermissionTableViewCell
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        
        
        
        //        if indexPath.row == 0
        //        {
        //            cell.tickButton.setImage(UIImage(named: "rightArrow"), for: .normal)
        //           // cell.tickButton.addTarget(self, action: #selector(tapOnAction), for: .touchUpInside)
        //
        //        }else
        //        {
        //            if #available(iOS 10.0, *) {
        //                cell.backgroundColor = UIColor(displayP3Red: 231.0/255.0, green: 234.0/255.0, blue: 237.9/255.0, alpha: 1)
        //            } else {
        //                // Fallback on earlier versions
        //            }
        //            cell.tickButton.setImage(UIImage(named: ""), for: .normal)
        //            cell.tickButton.isHidden = true
        //
        //        }
        if chooseoptions == true
        {
            cell.tickButton.tag = indexPath.row
            cell.tickButton.isHidden = false
            cell.tickButton.isEnabled = true
            cell.tickButton.setImage(UIImage(named: "UnCheck"), for: .normal)
            cell.tickButton.addTarget(self, action: #selector(panicAction), for: .touchUpInside)
            
            cell.permissionText.text = organizationdetails?.rulez?[indexPath.row].name
            cell.permissionimageView.image = UIImage(named: (organizationdetails?.rulez?[indexPath.row].rulesKey)!)
            if organizationdetails?.rulez?[indexPath.row].rulesKey == "key.lockAndUnlock"
            {
                cell.permissionimageView.image = #imageLiteral(resourceName: "key.lockAndUnlock")
            }
            if organizationdetails?.rulez?[indexPath.row].name == "key.battery"
            {
                
                cell.permissionText?.text = "Battery(%)"
                
            }
            //                        if self.seletedarray.contains("Unlock Device") || self.seletedarray.contains("lock Device") || self.seletedarray.contains("Lock/Unlock Device")
            //                        {
            //                            cell.tickButton.setImage(UIImage(named:"CheckBox"),for: .normal)
            //                        }
            
            
            
            
            
            
            cell.tickButton.setImage(UIImage(named: self.seletedarray.contains((organizationdetails?.rulez?[indexPath.row].rulesKey)!) ?  "checknew" : "UnCheck"), for: .normal)
            
            
            for each in (organizationdetails?.rulez)!
            {
                print(each.rulesKey)
                print(seletedarray)
            }
            
            //            if self.seletedarray.contains("Unlock Device") || self.seletedarray.contains("lock Device")
            //            {
            //                cell.tickButton.setImage(UIImage(named:"CheckBox"),for: .normal)
            //            }
            
            
             return cell
            
        }else
        {
            
            
            print(self.permissionlist?.ask?[indexPath.row].roleType)
            cell.permissionimageView.image = UIImage(named: (self.permissionlist?.ask?[indexPath.row].roleType)!)
          //  cell.permissionText.text = self.permissionlist?.ask?[indexPath.row].roleType
            
            for each in   (organizationdetails?.rulez)!
            {
                if each.rulesKey == self.permissionlist?.ask?[indexPath.row].roleType
               {
                cell.permissionText.text  = each.name
                
                
                
                }
                
                if let data = UserDefaults.standard.value(forKey: "Paymentdata") as? Data,
                    let profileData = try? JSONDecoder().decode(UserPlans.self, from: data)
                {
                if (featureList?.contains(where: { name in name.featureKey == "key.battery"}))!
                    
                {
                    if self.permissionlist?.ask?[indexPath.row].roleType == "key.battery"
                    {
                        
                        if each.rulesKey == "key.battery"
                        {
                            //  print(self.permissionlist?.ask?[indexPath.row].roleType)
                            
                            if getMemberDetails?.batteryStatus != nil
                            {
                                cell.permissionText.text = (each.name)! + " " + "(\(String(describing: (getMemberDetails?.batteryStatus!)!)))" + "%"
                            }
                            
                        }
                        
                        print(each.name)
                        
                        
                    }
                }
                }
                
                
                
                
                if self.permissionlist?.ask?[indexPath.row].roleType == "Lock Device" || self.permissionlist?.ask?[indexPath.row].roleType == "Unlock Device"
                {
                    cell.permissionimageView.image = #imageLiteral(resourceName: "key.lockAndUnlock")
                       cell.permissionText.text  = self.permissionlist?.ask?[indexPath.row].roleType

               }
            }
            
     
           
//            if self.permissionlist?.ask?[indexPath.row].roleType == "Unlock Device"
//            {
//                cell.permissionimageView.image = #imageLiteral(resourceName: "Lock Device")
//
//            }
            
           
            
            if self.permissionlist?.ask?[indexPath.row].roleType == "key.battery"
                
            {
                cell.tickButton.setImage(UIImage(named: ""), for: .normal)
            }else
            {
                cell.tickButton.setImage(UIImage(named: "rightArrow"), for: .normal)
            }
            
        }
        
        return cell
        
    }
    
    @objc func panicAction(sender: UIButton!) {
        
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        
        
        
        if self.seletedarray.contains((organizationdetails?.rulez?[sender.tag].rulesKey)!) {
            self.seletedarray.remove(at: self.seletedarray.index(of: (organizationdetails?.rulez?[sender.tag].rulesKey)!)!)
        } else {
            self.seletedarray.append((organizationdetails?.rulez?[sender.tag].rulesKey)!)
        }
        
        permissionTableView.reloadData()
        
    }
    
    @IBAction func editMemberProfile(_ sender: Any) {
        
        
        Analytics.logEvent("Set_Rules_Edit_Icon_Clicked", parameters:nil)
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddFamilyMemberVC") as! AddFamilyMemberVC
        vc.getMemberDetails = getMemberDetails
        vc.delegate = self
        vc.isEditing = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func deleteMember(_ sender: Any) {
        Analytics.logEvent("Set_Rules_Delete_Icon_Clicked", parameters:nil)
        
        //   let alertController = UIAlertController(title: Constants.alertTitle, message: "Are you sure you want to delete?", preferredStyle: .alert)
        
        
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        
        let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
        let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
        
        let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
        let msgAttrString = NSMutableAttributedString(string: "Are you sure you want to delete?", attributes: msgFont)
        
        alert.setValue(titAttrString, forKey: "attributedTitle")
        alert.setValue(msgAttrString, forKey: "attributedMessage")
        
        
        let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("YES", comment: ""), style: .default) { (UIAlertAction) in
            
            
            Analytics.logEvent("Set_Rules_Delete_Popup_Yes_Clicked", parameters:nil)

            //   ?userId=43&mobileNo=1234567891this feat
            
            
            guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                let profileData = try? JSONDecoder().decode(profile.self, from: data) else
            {
                
                return
                
            }
            
            
            let url = API.deleteMember + "?userId=" + String(describing: (profileData.userId!)) + "&memberId=\(String(describing: self.memberId!))"
            
            
            //"&mobileNo=" +  (self.getMemberDetails?.mobileNo)!
            
            let parameter:NSMutableDictionary = NSMutableDictionary()
            APIServices.postDataToServer(url:url, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
                
                
                
                if status == true{
                    
                    self.navigationController?.popViewController(animated: true)
                    print(response)
                    
                }
            })
            
        }
        
        let cancelAction = UIKit.UIAlertAction(title: NSLocalizedString("NO", comment: ""), style: .cancel) { (UIAlertAction) in
             Analytics.logEvent("Set_Rules_Delete_Popup_No_Clicked", parameters:nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(settingsAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    @objc func tapOnAction(sender: UIButton!) {
        
        if chooseoptions == true
        {
            
            return
        }
        permissionTableView.reloadData()
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SetGeoFenceViewController") as! SetGeoFenceViewController
        vc.memberId = memberId
        vc.userId = userId
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func sendRequestAction(_ sender: Any) {
        //        chooseoptions = false
        //       //sendRequestButton.isHidden = true
        //        permissionTableView.reloadData()
        //        selectPermissionButton.isSelected = false
      
        //
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        
        if seletedarray.isEmpty || seletedarray.count == 0
        {
            showAlert(title: "", msg: "Please select atleast one permission")
            return
        }
        
        let RequestList:NSMutableDictionary = NSMutableDictionary()
        
        RequestList.setValue(userId, forKey: "userId")
        RequestList.setValue(memberId, forKey: "memberId")
        
        let permissionRequestList:NSMutableArray = NSMutableArray()
        
        for each in (organizationdetails?.rulez)!
        {
            let innerRequestList:NSMutableDictionary = NSMutableDictionary()
            print(seletedarray)
            
            if self.seletedarray.contains(each.rulesKey!)
            {
                innerRequestList.setValue(each.rulesKey, forKey: "roleType")
                innerRequestList.setValue(each.id, forKey: "roleId")
                
                for eachs in ((self.permissionlist?.ask)!)
                {
                    
                    
                    if  eachs.roleType == each.rulesKey!
                    {
                        innerRequestList.setValue(eachs.permissionId, forKey: "permissionId")
                    }
                    
                    
                    if each.rulesKey == "key.lockAndUnlock"
                    {
                        if eachs.roleType == "Lock Device" || eachs.roleType == "Unlock Device"
                        {
                            innerRequestList.setValue(eachs.permissionId, forKey: "permissionId")
                        }
                    }
                    
                    
                }
                
                
                //  if self.seletedarray.contains(self.permissionlist?.ask.)
                
                // if self.permissionlist?.ask
                //    self.seletedarray
                
                innerRequestList.setValue("true", forKey: "flag")
                innerRequestList.setValue("true", forKey: "ask")
                innerRequestList.setValue("false", forKey: "give")
                innerRequestList.setValue("approve", forKey: "approveOrReject")
                
                
            }else
            {
                
                for eachs in ((self.permissionlist?.ask)!)
                {
                    
                    
                    if  eachs.roleType == each.rulesKey!
                    {
                        innerRequestList.setValue(eachs.permissionId, forKey: "permissionId")
                    }
                    
                    
                    if each.rulesKey == "key.lockAndUnlock"
                    {
                        if eachs.roleType == "Lock Device" || eachs.roleType == "Unlock Device"
                        {
                            innerRequestList.setValue(eachs.permissionId, forKey: "permissionId")
                        }
                    }
                    
                    
                }
                innerRequestList.setValue(each.rulesKey, forKey: "roleType")
                innerRequestList.setValue(each.id, forKey: "roleId")
                innerRequestList.setValue("true", forKey: "flag")
                innerRequestList.setValue("true", forKey: "ask")
                innerRequestList.setValue("false", forKey: "give")
                innerRequestList.setValue("Reject", forKey: "approveOrReject")
            }
            
            
            
            permissionRequestList.add(innerRequestList)
            
        }
        
        RequestList.setValue(permissionRequestList, forKey: "permissionRequestList")
          SVProgressHUD.show()
        print(RequestList)
        
        APIServices.postDataToServer(url: API.editPermission, parameters: RequestList, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                  SVProgressHUD.dismiss()
                
                let respons = response as? [String:Any]
                
                   let statusCode = respons?["statusCode"] as?  Int
                
                let alertController = UIAlertController(title: "", message: respons?["message"] as? String, preferredStyle: .alert)
                
                
                let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                    if statusCode == 400
                    {
                        self.selectPermissionButton.isSelected = false
                        self.widthConstant.constant = 40
                        self.heightConstant1.constant = 40
                        self.chooseoptions = false
                        self.heightConstant.constant = 0
                        self.sendRequestButton.isHidden = true
                        self.permissionTableView.reloadData()
                        
                    }else
                    {
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    
                    
                }
                
                alertController.addAction(settingsAction)
                self.present(alertController, animated: true, completion: nil)
                
                
                
            }else
            {
                  SVProgressHUD.dismiss()
                
                
            }
        })
        
        
        
        
        
        
        
    }
    
    
    @IBAction func callButtonAction(_ sender: Any) {
        
        
        Analytics.logEvent("Set_Rules_Call_Icon_Clicked", parameters:nil)
        let mobileNos = getMemberDetails?.mobileNo?.getStringFromPhoneNumber()
        //   print(getMemberDetails?.mobileNo?.getStringFromPhoneNumber())
        
        let url: NSURL = URL(string: "TEL://\(mobileNos ?? ""))")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        
    }
    
    
    @IBAction func messageButtonAction(_ sender: Any) {
        
        
        Analytics.logEvent("Set_Rules_SMS_Icon_Clicked", parameters:nil)
//        messageView.isHidden = false
//        self.view.addSubview(messageView)
//        
//        messageTextField.layer.borderWidth = 1.5
//        messageTextField.layer.borderColor = UIColor(red: 53/255, green: 154/255, blue: 210/255, alpha: 1.0).cgColor
//        messageTextField.layer.cornerRadius = 10
//        messageTextField.layer.masksToBounds = true
//
//        
//        messageTextField.text = nil
        //
        //        let messageVC = MFMessageComposeViewController()
        //        messageVC.body = "Enter a message details here";
        //        messageVC.recipients = [mobileNumber.text] as? [String]
        //        messageVC.messageComposeDelegate = self
        //        self.present(messageVC, animated: true, completion: nil)
        
        //        jsonObject.addProperty("mobileNo", smsnumber);
        //        jsonObject.addProperty("msg", smstext);
        
        
        
        
        
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        // let vc = mainStoryboard.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
        
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        vc.name = profileName.text
        vc.memberId = userId
        vc.getMemberDetails = getMemberDetails
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
        
    }
    
    
    @IBAction func messageCancel(_ sender: Any) {
        
        Analytics.logEvent("Set_Rules_SMS_Popup_Cancel_Clicked", parameters:nil)
         IQKeyboardManager.shared.layoutIfNeededOnUpdate = true
        IQKeyboardManager.shared.resignFirstResponder()
        
        
        messageView.isHidden = true
    }
    
    
    
    @IBAction func messageSendAction(_ sender: Any) {
        Analytics.logEvent("Set_Rules_SMS_Popup_Send_Clicked", parameters:nil)
        
      
        
        if messageTextField.text!.isEmpty || messageTextField.text == ""
        {
            self.showAlert(title: "", msg: "Please enter message")
            
            return
        }
        
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(mobileNumber.text, forKey: "mobileNo")
        parameter.setValue(messageTextField.text, forKey: "msg")
        print(API.sendingSMS)
        print(parameter)
        
        APIServices.postDataToServer(url: API.sendingSMS, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                self.messageView.isHidden = true
                
                print(response)
                
                let  respons = response as? [String:Any]
                
                self.showAlert(title: "", msg: respons?["message"] as! String)
                
            }else
            {
                
                
                
            }
        })
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
    
    @IBAction func selectPermissionButtonAction(_ sender: UIButton) {
        
        //sender.isSelected = !sender.isSelected
        
        if !sender.isSelected
            
        {
            
            seletedarray.removeAll()
           widthConstant.constant = 45
        heightConstant1.constant = 45
            chooseoptions = true
            sendRequestButton.isHidden = false
           heightConstant.constant = 40
            
           //   topheight.constant = 20
            
            if self.permissionlist?.ask?.count ?? 0 == 0
            {
                   sender.isSelected = !sender.isSelected
                permissionTableView.reloadData()
                return
            }
            
            for each in (self.permissionlist?.ask)!
            {
                if self.seletedarray.contains(each.roleType!) {
                    self.seletedarray.remove(at: self.seletedarray.index(of: (each.roleType!))!)
                } else {
                    self.seletedarray.append(each.roleType!)
                }
            }
            
        
            for each in (self.permissionlist?.ask)!
            {
                
                
                print(each.roleType)
                
                
                if each.roleType == "key.dailyTimeLimits" || each.roleType == "key.restrictedTimes" ||  each.roleType == "Unlock Device"  || each.roleType == "Lock Device"
                    
                {
                    
                    
                   
                        if self.seletedarray.contains("key.lockAndUnlock") {
                            
                        } else {
                            self.seletedarray.append("key.lockAndUnlock")
                        }
                    
                   
                }else
                {
//                     if self.seletedarray.contains("key.lockAndUnlock")
//                     {
//                      self.seletedarray.remove(at: self.seletedarray.index(of: "key.lockAndUnlock")!)
//                    }
//
                    
                    
                }
            }
            
//            for each in (self.permissionlist?.ask)!
//            {
//                if self.seletedarray.contains(each.roleType!) {
//
//                } else {
//                    self.seletedarray.append(each.roleType!)
//                }
//            }
//
             permissionTableView.reloadData()
//            let topIndex = IndexPath(row: 0, section: 0)
//            permissionTableView.scrollToRow(at: topIndex, at: .top, animated: true)
//            permissionTableView.setContentOffset(CGPoint.zero, animated: true)
//
            
        }else
        {
           widthConstant.constant = 45
        heightConstant1.constant = 45
            chooseoptions = false
        heightConstant.constant =  40
            sendRequestButton.isHidden = true
          //  topheight.constant = 50
            permissionTableView.layoutIfNeeded()
            permissionTableView.updateConstraintsIfNeeded()
            permissionTableView.reloadData()
            
            if self.permissionlist?.ask?.count ?? 0 > 0
            {
                let topIndex = IndexPath(row: 0, section: 0)
                permissionTableView.scrollToRow(at: topIndex, at: .top, animated: true)
                permissionTableView.setContentOffset(CGPoint.zero, animated: true)
               permissionTableView.scrollToNearestSelectedRow(at: .top, animated: true)
                
            }

        }
        
        sender.isSelected = !sender.isSelected
        
    }
    
    @IBAction func reportsButtonTapped(_ sender: Any) {
        
        
                if getMemberDetails?.platform == "iOS"
                {

                     self.showCustomAlertWith(message: "", descMsg: "\(String(describing: profileName.text!)) is using iOS device, you can not access this feature", itemimage: #imageLiteral(resourceName: "AboutUs"), actions: nil)

                    return

                }

        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ReportsViewController") as! ReportsViewController
          vc.memberId = memberId
        vc.permissionlist = permissionlist
        
        
        vc.memberName = profileName.text
        //        vc.getMemberDetails = getMemberDetails
        //        vc.delegate = self
        //        vc.isEditing = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        print("selected")
        
        
        Analytics.logEvent("Set_Rules_(Permission Type)_Clicked", parameters:nil)

        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
        if chooseoptions == true
        {
//            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
//            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
            

            if self.seletedarray.contains((organizationdetails?.rulez?[indexPath.row].rulesKey)!) {
                self.seletedarray.remove(at: self.seletedarray.index(of: (organizationdetails?.rulez?[indexPath.row].rulesKey)!)!)
            } else {
                self.seletedarray.append((organizationdetails?.rulez?[indexPath.row].rulesKey)!)
            }
            
            for each in seletedarray
            {
                if each == "key.dailyTimeLimits" || each == "key.restrictedTimes"
                {
                    if self.seletedarray.contains("key.lockAndUnlock") {
                        
                    } else {
                        self.seletedarray.append("key.lockAndUnlock")
                    }
                }else
                {
                    if self.seletedarray.contains("Lock Device")
                    {
                        self.seletedarray.remove(at: self.seletedarray.index(of: "Lock Device")!)
                    }
                    
                    if self.seletedarray.contains("Unlock Device")
                    {
                        self.seletedarray.remove(at: self.seletedarray.index(of: "Unlock Device")!)
                    }
                }
            }
            
            
            permissionTableView.reloadData()
            return
            
        }
        
        
//        if featureList?.contains(where: self.permissionlist?.ask?[indexPath.row].roleType)
//        {
//
//        }
//        
//        if self.featureList?.contains(where: self.permissionlist?.ask?[indexPath.row].roleType) {
//            
//        } else {
//           
//        }
//        
        
//        if self.permissionlist?.ask?[indexPath.row].roleType == featureList!
//        {
//
//        }
        
        
        
        if let data = UserDefaults.standard.value(forKey: "Paymentdata") as? Data,
            let profileData = try? JSONDecoder().decode(UserPlans.self, from: data)
        {
            
            
            
            
            
            
            if profileData.remainDays ?? 0 <= 0
            {
                self.showCustomAlertWith(message: "", descMsg:"expire", itemimage: #imageLiteral(resourceName: "AboutUs"), actions: nil)
                
            }else
            {
                
                
                
                if   (featureList?.contains(where: { name in name.featureKey == self.permissionlist?.ask?[indexPath.row].roleType}))!
                    
                {
                    
                    
                    
                    print("match")
                }else
                {
                    print("not match")
                    
             
                    self.showCustomAlertWith(message: "", descMsg:"Please select a payment plan to access this feature.", itemimage: #imageLiteral(resourceName: "AboutUs"), actions: nil)
                    return
            
//
//                    for each in (featureList!)
//                    {
//
//                        print(each.featureKey)
//                        if each.featureKey == "key.lockAndUnlock"
//                        {
//
//                        }else
//                        {
//                            self.showCustomAlertWith(message: "", descMsg:"To avail this feature, please take subscription.", itemimage: #imageLiteral(resourceName: "AboutUs"), actions: nil)
//
//
//                         //   return
//                        }
//                    }
            
                    
                    
                    
                    
//                     if   (featureList?.contains(where: { name in name.featureKey == "key.lockAndUnlock" }))!
//                    
//                     {
//                        
//                    }else
//                     {
//                        self.showCustomAlertWith(message: "", descMsg:"To avail this feature, please take subscription.", itemimage: #imageLiteral(resourceName: "AboutUs"), actions: nil)
//                        
//                        
//                        return
//                    }
                    
                   
                }
                
                
                
                
                
               
                
            }
            
            
            
            //  return
            
        }else
        {
        self.showCustomAlertWith(message: "", descMsg:"Please select a payment plan to access this feature.", itemimage: #imageLiteral(resourceName: "AboutUs"), actions: nil)
            return
            
        }
        
        
        
        
        
        
        
        
        
        
       
        
        if  self.permissionlist?.ask?[indexPath.row].roleType == "key.geo"
        {
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "SetGeoFenceViewController") as! SetGeoFenceViewController
            vc.memberId = memberId
            vc.userId = userId
            vc.membersGeoList = membersGeoList
            
            for each in   (organizationdetails?.rulez)!
            {
                if each.rulesKey == "key.geo"
                {
                 vc.titleName   = each.name
                    
                    
                    
                }
            }
        //    vc.titleName =
            
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        if  self.permissionlist?.ask?[indexPath.row].roleType == "key.alarmAndReminders"
        {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "AlarmsAndRemindersView") as! AlarmsAndRemindersView
            vc.memberId = memberId
            vc.userId = userId
            vc.getMemberDetailsbyId = getMemberDetailsbyId
            
            for each in   (organizationdetails?.rulez)!
            {
                if each.rulesKey == "key.alarmAndReminders"
                {
                    vc.titleName   = each.name
                    
                    
                    
                }
            }
            //            AlarmsAndRemindersView
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        if  self.permissionlist?.ask?[indexPath.row].roleType == "key.battery"
        {
            
            return
        }
        
        
        
        if getMemberDetails?.platform == "iOS"
        {
            
             self.showCustomAlertWith(message: "", descMsg: "\(String(describing: profileName.text!)) is using iOS device, you can not access this feature", itemimage: #imageLiteral(resourceName: "AboutUs"), actions: nil)
        //    self.showAlert(title: "", msg: "\(String(describing: profileName.text!)) is using iOS device, you can not access this feature")
            
            
            return
            
        }
        
        
        if  self.permissionlist?.ask?[indexPath.row].roleType == "key.dailyTimeLimits"
        {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "DailyTimeLimitsVC") as! DailyTimeLimitsVC
            vc.memberId = memberId
            vc.userId = userId
            
            for each in   (organizationdetails?.rulez)!
            {
                if each.rulesKey == "key.dailyTimeLimits"
                {
                    vc.titleName   = each.name
                    
                    
                    
                }
            }
            //   vc.getMemberDetailsbyId = getMemberDetailsbyId
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        if  self.permissionlist?.ask?[indexPath.row].roleType == "key.restrictedTimes"
        {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "RestrictedTimeLimitsVC") as! RestrictedTimeLimitsVC
            vc.memberId = memberId
            vc.userId = userId
            for each in   (organizationdetails?.rulez)!
            {
                if each.rulesKey == "key.restrictedTimes"
                {
                    vc.titleName   = each.name
                    
                    
                    
                }
            }
            // vc.getMemberDetailsbyId = getMemberDetailsbyId
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        if  self.permissionlist?.ask?[indexPath.row].roleType == "key.appsAndGames"
        {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "MainAppsViewController") as! MainAppsViewController
            vc.memberId = memberId
            vc.userId = userId
            
            for each in   (organizationdetails?.rulez)!
            {
                if each.rulesKey == "key.appsAndGames"
                {
                    vc.titleName   = each.name
                    
                    
                    
                }
            }
            // vc.getMemberDetailsbyId = getMemberDetailsbyId
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        if  self.permissionlist?.ask?[indexPath.row].roleType == "key.callAndSms"
        {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "MainCallsViewController") as! MainCallsViewController
            vc.memberId = memberId
            vc.userId = userId
            vc.loginType = getMemberDetails?.login_type
            
            for each in   (organizationdetails?.rulez)!
            {
                if each.rulesKey == "key.callAndSms"
                {
                    vc.titleName   = each.name
                    
                    
                    
                }
            }
            // vc.getMemberDetailsbyId = getMemberDetailsbyId
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        if  self.permissionlist?.ask?[indexPath.row].roleType == "key.web.filter"
        {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "MainWebFilterViewController") as! MainWebFilterViewController
            vc.memberId = memberId
            vc.userId = userId
            vc.loginType = getMemberDetails?.login_type
            
            for each in   (organizationdetails?.rulez)!
            {
                if each.rulesKey == "key.web.filter"
                {
                    vc.titleName   = each.name
                    
                    
                    
                }
            }
            // vc.getMemberDetailsbyId = getMemberDetailsbyId
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        
        
        //http://35.165.108.250:8080/CloudPeppers-V2_new_qa/unlockMemberDevice?memberId=26&mobileStatus=Unlock
        
        
        if  self.permissionlist?.ask?[indexPath.row].roleType == "Lock Device" ||  self.permissionlist?.ask?[indexPath.row].roleType == "Unlock Device"
        {
            lockUnlockRowId = indexPath.row
          //  var alertController = UIAlertController()
            var alert = UIAlertController()
            
            if   self.permissionlist?.ask?[indexPath.row].roleType == "Lock Device"
            {
                /*
                 alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                 
                 let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
                 let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
                 
                 let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
                 let msgAttrString = NSMutableAttributedString(string: "Do you want to lock \(profileName.text!)'s device?", attributes: msgFont)
                 
                 alert.setValue(titAttrString, forKey: "attributedTitle")
                 alert.setValue(msgAttrString, forKey: "attributedMessage")
                 */
                
                let actionDic : [String: () -> Void] = [ "YES" : { (
                    Analytics.logEvent("Set_Rules_Device_Lock_Popup_Yes_Click", parameters:nil),
                    self.lockUnlockMethod(),
                    self.lockBgViewObj.isHidden = false,
                    self.view.addSubview(self.lockBgViewObj)
                    
                    ) }, "NO" : { (
                        Analytics.logEvent("Set_Rules_Device_Lock_Popup_No_Click", parameters:nil)
                       
                        
                        ) }]
                
                self.showCustomAlertWith(
                    message: "",
                    descMsg: "Do you want to lock \(profileName.text!)'s device?",
                    itemimage: nil,
                    actions: actionDic)
                
            }
            
            if   self.permissionlist?.ask?[indexPath.row].roleType == "Unlock Device"
            {
                /*
                 alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                 let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
                 let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
                 
                 let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
                 let msgAttrString = NSMutableAttributedString(string: "Do you want to unlock \(profileName.text!)'s device?", attributes: msgFont)
                 
                 alert.setValue(titAttrString, forKey: "attributedTitle")
                 alert.setValue(msgAttrString, forKey: "attributedMessage")
                 */
                let actionDic : [String: () -> Void] = [ "YES" : { (
                    
                    Analytics.logEvent("Set_Rules_Device_UnLock_Popup_Yes_Click", parameters:nil),
                    self.lockUnlockMethod()
                    
                    ) }, "NO" : { (
                        Analytics.logEvent("Set_Rules_Device_UnLock_Popup_No_Click", parameters:nil)
                        
                        ) }]
                
                self.showCustomAlertWith(
                    message: "",
                    descMsg: "Do you want to unlock \(profileName.text!)'s device?",
                    itemimage: nil,
                    actions: actionDic)
                
            }
            
            
            /*
            
            let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                
                
                
                guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                    let profileData = try? JSONDecoder().decode(profile.self, from: data) else
                {
                    
                    return
                    
                }
                self.userId = profileData.userId
                
                
                var  url:String?
                
                
                if   self.permissionlist?.ask?[indexPath.row].roleType == "Lock Device"
                {
                    url = API.unlockMemberDevice + "?memberId=\(self.memberId!)" + "&userId=\(String(describing: (self.userId)!))" + "&lock_status=true"
                }
                
                if   self.permissionlist?.ask?[indexPath.row].roleType == "Unlock Device"
                {
                    url = API.unlockMemberDevice + "?memberId=\(self.memberId!)" + "&userId=\(String(describing: (self.userId)!))" + "&lock_status=false"
                }
                
                
                print(url)
                
                // let para = NSMutableDictionary()
                
                APIServices.getDataFromServer(url: url!, parameters:nil, controller : self,completionHandler: { (status,response) -> Void in
                    if status == true{
                        
                        
                        if   self.permissionlist?.ask?[indexPath.row].roleType == "Lock Device"
                        {
                            
                            self.showAlert(title: "", msg: "Device locked successfully")
                        }
                        if   self.permissionlist?.ask?[indexPath.row].roleType == "Unlock Device"
                        {
                            self.showAlert(title: "", msg: "Device unlocked successfully")
                        }
                        self.callgetMemberApi()
                    }
                })
                
                
                
                
            }
            
            let cancelAction = UIKit.UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { (UIAlertAction) in
            }
            alert.addAction(cancelAction)
            alert.addAction(settingsAction)
            self.present(alert, animated: true, completion: nil)
            
            
            return
 */
        }
        
        
        
        
        
        
        //        if indexPath.row == 0
        //        {
        //            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //            let vc = mainStoryboard.instantiateViewController(withIdentifier: "SetGeoFenceViewController") as! SetGeoFenceViewController
        //            vc.memberId = memberId
        //            vc.userId = userId
        //
        //            self.navigationController?.pushViewController(vc, animated: true)
        //        }
        //        if indexPath.row == 1
        //        {
        //            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //            let vc = mainStoryboard.instantiateViewController(withIdentifier: "AlarmsAndRemindersView") as! AlarmsAndRemindersView
        //            vc.memberId = memberId
        //            vc.userId = userId
        //            vc.getMemberDetailsbyId = getMemberDetailsbyId
        ////            AlarmsAndRemindersView
        //            self.navigationController?.pushViewController(vc, animated: true)
        //        }
    }
    
    @IBAction func locationIconTapped(_ sender: UIButton) {
        
        //        if getMemberDetails?.platform == "iOS"
        //        {
        //
        //            self.showCustomAlertWith(message: "", descMsg: "\(String(describing: profileName.text!)) is using iOS device, you can not access this feature", itemimage: #imageLiteral(resourceName: "AboutUs"), actions: nil)
        //
        //            return
        //
        //        }
        
//        sender.isSelected = !sender.isSelected
//
//        if sender.isSelected == true
//        {
//            profileName.text  = "\(String(describing: (getMemberDetails?.firstName ?? "")!))"
//        }
//        else
//        {
//         profileName.text =  "\(String(describing: (getMemberDetails?.firstName ?? "")!))" +  " " +  "\(String(describing: (getMemberDetails?.lastName ?? "")!))"
//        }
        
        
      
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "LocationHistoryVC") as! LocationHistoryVC
        vc.memberId = memberId
        vc.permissionlist = permissionlist
        
       //   profileName.text =  "\(String(describing: (getMemberDetails?.firstName ?? "")!))"
        vc.memberName = profileName.text
        //        vc.getMemberDetails = getMemberDetails
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func lockUnlockMethod()
    {
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        self.userId = profileData.userId
        
        
        var  url:String?
        
        if   self.permissionlist?.ask?[self.lockUnlockRowId!].roleType == "Lock Device"
        {
            url = API.unlockMemberDevice + "?memberId=\(self.memberId!)" + "&userId=\(String(describing: (self.userId)!))" + "&lock_status=true"
        }
        
        if   self.permissionlist?.ask?[self.lockUnlockRowId!].roleType == "Unlock Device"
        {
            url = API.unlockMemberDevice + "?memberId=\(self.memberId!)" + "&userId=\(String(describing: (self.userId)!))" + "&lock_status=false"
        }
        
        
        print(url!)
        
        // let para = NSMutableDictionary()
        
        APIServices.getDataFromServer(url: url!, parameters:nil, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                if   self.permissionlist?.ask?[self.lockUnlockRowId!].roleType == "Lock Device"
                {
                    // self.showAlert(title: "", msg: "Device locked successfully")
                    
                    self.lockBgViewObj.isHidden = false
                    self.view.addSubview(self.lockBgViewObj)
                    self.lockUnlockDescLbl.text = "Device locked successfully"
                    
                }
                if   self.permissionlist?.ask?[self.lockUnlockRowId!].roleType == "Unlock Device"
                {
                    //self.showAlert(title: "", msg: "Device unlocked successfully")
                    
                    self.lockBgViewObj.isHidden = false
                    self.view.addSubview(self.lockBgViewObj)
                    self.lockUnlockDescLbl.text = "Device unlocked successfully"
                }
                self.callgetMemberApi()
            }
        })
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func lockCancelBtnActn(_ sender: Any) {
        self.lockBgViewObj.isHidden = true
        
    }
}
extension UIView{
    
    func roundedTop(){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.topRight , .topLeft],
                                     cornerRadii: CGSize(width: 15, height: 15))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
    
}

extension Array where Element: Comparable {
    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }
}
