//
//  MainAppsViewController.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 30/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase

class MainAppsViewController: UIViewController {
    
    
    @IBOutlet var blockedView: UIView!
    @IBOutlet var blockedNumbers: UILabel!
    @IBOutlet var blockedText: UILabel!
    @IBOutlet var allowedView: UIView!
    @IBOutlet var allowedText: UILabel!
    @IBOutlet var allowedNumber: UILabel!
    @IBOutlet var totalView: UIView!
    @IBOutlet var totalNumber: UILabel!
    @IBOutlet var totalText: UILabel!
    var titleName:String?
    @IBOutlet weak var contentView: UIView!
    enum TabIndex : Int {
        case firstChildTab = 0
        case secondChildTab = 1
        case ThirdViewController = 2
    }
    var memberId:Int?
    var userId:Int?
    
    var currentViewController: UIViewController?
    lazy var firstChildTabVC: UIViewController? = {
        let firstChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewControllerId") as? FirstTabViewController
        firstChildTabVC?.memberId = memberId
        firstChildTabVC?.userId = userId
        
        
        return firstChildTabVC
    }()
    lazy var secondChildTabVC : UIViewController? = {
        let secondChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewControllerId") as? SecondTabViewController
        
        secondChildTabVC?.memberId = memberId
        secondChildTabVC?.userId = userId
        
        return secondChildTabVC
    }()
    
    lazy var thirdViewdTabVC : UIViewController? = {
        let ThirdViewController = self.storyboard?.instantiateViewController(withIdentifier: "ThirdViewController")
        
        return ThirdViewController
    }()
    override func viewDidLoad() {
        
       // blockedView.layer.cornerRadius = 5
       // blockedView.clipsToBounds = true
        
        //        allowedView.layer.cornerRadius = 5
        //        allowedView.clipsToBounds = true
        
//        totalView.layer.cornerRadius = 5
//        totalView.clipsToBounds = true
        
        super.viewDidLoad()
        displayCurrentTab(1)
        b1(UIButton.self)
        self.title = titleName
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("callApi"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification1(notification:)), name: Notification.Name("callApi1"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification2(notification:)), name: Notification.Name("callApi2"), object: nil)
        
        
        // Do any additional setup after loading the view.
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        callgetMemberApi()
        
    }
    
    @objc func methodOfReceivedNotification1(notification: Notification) {
        b2(UIButton.self)
        // callgetMemberApi()
        
    }
    
    @objc func methodOfReceivedNotification2(notification: Notification) {
        b1(UIButton.self)
        // callgetMemberApi()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Analytics.logEvent("Apps_Games_Entered", parameters:nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("Apps_Games_Exited", parameters:nil)

    }
    
   
    
    @IBAction func b1(_ sender: Any) {
        
        Analytics.logEvent("Apps_Games_Blocked_Tab_Clicked", parameters:nil)
        displayCurrentTab(1)
        
        callgetMemberApi()
//        blockedView.backgroundColor =  UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1)
//
//        blockedView.layer.borderColor = UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1).cgColor
        blockedView.backgroundColor =  #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        blockedView.layer.borderColor = #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        blockedView.layer.borderWidth = 1.0
        allowedView.backgroundColor = UIColor.white
        allowedView.layer.borderColor = UIColor.gray.cgColor
        allowedView.layer.borderWidth = 1.0
        totalView.backgroundColor = UIColor.white
        
        totalView.layer.borderColor = UIColor.gray.cgColor
        totalView.layer.borderWidth = 1.0
        
        blockedNumbers.textColor = UIColor.white
        blockedText.textColor = UIColor.white
        
        
        totalNumber.textColor = UIColor.black
        totalText.textColor = UIColor.black
        allowedNumber.textColor = UIColor.black
        allowedText.textColor = UIColor.black
        
        if let vc = viewControllerForSelectedSegmentIndex(2)
        {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
        
        if let vc = viewControllerForSelectedSegmentIndex(3)
        {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
    }
    func callgetMemberApi()
    {
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        print(memberId)
        
        
        let getGamesAndAppsUrl = API.getGamesAndApps + "?userId=\(String(describing: profileData.userId!))" + "&memberId=\(memberId!)"
        print(getGamesAndAppsUrl)
        HttpWrapper.gets(with: getGamesAndAppsUrl, parameters: nil, headers: nil, completionHandler: { (response, error)  in
             print(response)
            
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatusAppsGames.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    SharedData.data.responseappsGames = loginRespone.response
                    print(SharedData.data.responseappsGames)
                    
                    NotificationCenter.default.post(name: Notification.Name("BlockedApps"), object: nil)
                    
                    NotificationCenter.default.post(name: Notification.Name("Allowedapps"), object: nil)
                    
                    NotificationCenter.default.post(name: Notification.Name("TotalApps"), object: nil)
                    
                    
                    if (SharedData.data.responseappsGames?.blocked?.count)! > 0 || !(SharedData.data.responseappsGames?.blocked!.isEmpty)!
                    {
                        self.blockedNumbers.text =  String(describing: (SharedData.data.responseappsGames?.blocked?.count)!)
                    }else
                    {
                        self.blockedNumbers.text = "0"
                    }
                    if  (SharedData.data.responseappsGames?.approve?.count)! > 0
                    {
                        self.allowedNumber.text = String(describing: (SharedData.data.responseappsGames?.approve?.count)!)
                    }else
                    {
                        self.allowedNumber.text = "0"
                    }
                    
                    
                    self.totalNumber.text = String(describing: SharedData.data.responseappsGames!.blocked!.count + SharedData.data.responseappsGames!.approve!.count)
                    
                    // self.familycollectionView.reloadData()
                }
                else
                    
                {
                    
                }
                
            }catch let jsonErr {
                
                
                print("Error serializing json:", jsonErr)
                
            }
        }){ (error) in
            
            print(error)
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
    }
    @IBAction func b2(_ sender: Any) {
        
        Analytics.logEvent("Apps_Games_Allowed_Tab_Clicked", parameters:nil)
        displayCurrentTab(2)
        
        callgetMemberApi()
        
        blockedView.backgroundColor = UIColor.white
        
        blockedView.layer.borderColor = UIColor.gray.cgColor
        blockedView.layer.borderWidth = 1.0
        
//        allowedView.backgroundColor = UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1)
//
//        allowedView.layer.borderColor = UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1).cgColor
        
        allowedView.backgroundColor =  #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        allowedView.layer.borderColor = #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        allowedView.layer.borderWidth = 1.0
        
        totalView.backgroundColor = UIColor.white
        totalView.layer.borderColor = UIColor.gray.cgColor
        totalView.layer.borderWidth = 1.0
        
        
        allowedNumber.textColor = UIColor.white
        allowedText.textColor = UIColor.white
        
        
        totalNumber.textColor = UIColor.black
        totalText.textColor = UIColor.black
        
        
        
        blockedNumbers.textColor = UIColor.black
        blockedText.textColor = UIColor.black
        if let vc = viewControllerForSelectedSegmentIndex(1)
        {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
        
        if let vc = viewControllerForSelectedSegmentIndex(3)
        {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
    }
    
    @IBAction func b3(_ sender: Any) {
        
        Analytics.logEvent("Apps_Games_Total_Tab_Clicked", parameters:nil)
        displayCurrentTab(3)
        callgetMemberApi()
        blockedView.backgroundColor = UIColor.white
        blockedView.layer.borderColor = UIColor.gray.cgColor
        blockedView.layer.borderWidth = 1.0
        allowedView.backgroundColor = UIColor.white
        
        allowedView.layer.borderColor = UIColor.gray.cgColor
        allowedView.layer.borderWidth = 1.0
        totalView.backgroundColor =  #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        totalView.layer.borderColor = #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
//        totalView.backgroundColor =  UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1)
//
//        totalView.layer.borderColor = UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1).cgColor
        totalView.layer.borderWidth = 1.0
        
        totalNumber.textColor = UIColor.white
        totalText.textColor = UIColor.white
        
        allowedNumber.textColor = UIColor.black
        allowedText.textColor = UIColor.black
        blockedNumbers.textColor = UIColor.black
        blockedText.textColor = UIColor.black
        
        if let vc = viewControllerForSelectedSegmentIndex(2)
        {
            // vc.removeFromParent()
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
            //contentView.willRemoveSubview(vc)
        }
        
        if let vc = viewControllerForSelectedSegmentIndex(1)
        {
            vc.removeFromParent()
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
        
        
    }
    
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            
            
            
            self.addChild(vc)
            vc.didMove(toParent: self)
            
            vc.view.frame = self.contentView.bounds
            self.contentView.addSubview(vc.view)
            
            self.currentViewController = vc
        }
    }
    
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case 1:
            vc = firstChildTabVC
        case 2 :
            vc = secondChildTabVC
        default:
            vc = thirdViewdTabVC
        }
        
        return vc
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
