//
//  SecondTabViewController.swift
//  UISegmentedControlAsTabbarDemo
//
//  Created by Ahmed Abdurrahman on 9/16/15.
//  Copyright © 2015 A. Abdurrahman. All rights reserved.
//
import UIKit
import SDWebImage
import Alamofire
import SVProgressHUD
import Firebase

class SecondTabViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UISearchBarDelegate {
    var selectedArray = [String]()
    var memberId:Int?
    var userId:Int?
    
     var allowed:[approve]?
    
    var searchActive : Bool = false
    @IBOutlet var searchBar: UISearchBar!
    
    @IBOutlet weak var blockButton: UIButton!
    var responseappsGames:responseappsGames?
    
    @IBOutlet var familycollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
    //    blockButton.dropShadow()
        
        blockButton.changeButtonCornerRadius()
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        familycollectionView.register(UINib(nibName: "AppsandGamesCollectionCell", bundle: nil),
                                      forCellWithReuseIdentifier: "AppsandGamesCollectionCell")
        familycollectionView.delegate = self
        familycollectionView.dataSource = self
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("Allowedapps"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        
        searchBar.text = nil
        searchBar.resignFirstResponder()
        familycollectionView.reloadData()
        
        self.searchBar.showsCancelButton = false
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.searchActive = true;
        self.searchBar.showsCancelButton = true
        
        
        allowed = SharedData.data.responseappsGames?.approve?.filter{ $0.appName!.localizedStandardContains(searchText) }
        
        familycollectionView.reloadData()
        
    }
    
    
    @IBAction func blockAction(_ sender: Any) {
        
        Analytics.logEvent("Apps_Games_Block_Btn_Clicked", parameters:nil)

        if selectedArray.isEmpty || selectedArray.count == 0
        {
            showAlert(title: "", msg: "Please select at least one app to block")
            return
        }
        
        
        
     //   let alertController = UIAlertController(title: Constants.alertTitle, message: "Are you sure want to block the selected app(s)?", preferredStyle: .alert)
        
        
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        
        let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
        let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
        
        let titAttrString = NSMutableAttributedString(string: "Apps & Games", attributes: titFont)
        let msgAttrString = NSMutableAttributedString(string: "Are you sure you want to block the selected app(s)?", attributes: msgFont)
        
        alert.setValue(titAttrString, forKey: "attributedTitle")
        alert.setValue(msgAttrString, forKey: "attributedMessage")
        
        let cancelAction = UIAlertAction(title: "NO", style: .cancel, handler: { (action : UIAlertAction!) -> Void in })
        
        let saveAction = UIAlertAction(title: "YES", style: .default, handler: { alert -> Void in
            
            
            let permissionRequestList:NSMutableArray = NSMutableArray()
            //  let RequestList:NSMutableDictionary = NSMutableDictionary()
            for each in (SharedData.data.responseappsGames?.approve)!
            {
                let innerRequestList:NSMutableDictionary = NSMutableDictionary()
                
                if self.selectedArray.contains(each.appName!)
                {
                    innerRequestList.setValue(each.appsAndGamesId, forKey: "appsAndGamesId")
                    innerRequestList.setValue(each.appName, forKey: "appName")
                    
                    innerRequestList.setValue(each.memberId, forKey: "memberId")
                    innerRequestList.setValue(each.packeage, forKey: "packeage")
                    
                    innerRequestList.setValue(each.kidOrMember, forKey: "kidOrMember")
                    
                    innerRequestList.setValue("block", forKey: "appStatus")
                    
                                    if  each.appImg != nil
                                    {
                    innerRequestList.setValue(each.appImg, forKey: "appImg")
                    
                    
                    //                    let image = UIImageView()
                    //                    image.sd_setImage(with: URL(string: each.appImg!)!)
                    //
                    //
                    //                    let imageData:Data =  image.image!.pngData()!
                    //                    let base64String = imageData.base64EncodedString()
                    //                    innerRequestList.setValue(base64String, forKey: "appImg")
                                    }
                    
                    
                    
                    permissionRequestList.add(innerRequestList)
                    
                }
            }
            
            let parameters: [[String: Any]] = permissionRequestList as! [[String : Any]]
            var request = URLRequest(url: URL(string: API.saveAppsAndGames)!)
            request.httpMethod = "POST"
            
            
            print(parameters)
            let id = UserDefaults.standard.string(forKey: "jwtToken")
            
            if id != nil && id != ""
            {
                let token = "Bearer" + " "  + "\(UserDefaults.standard.string(forKey: "jwtToken")!)"
                
                print(token)
                request.setValue(token, forHTTPHeaderField: "Authorization")
            }
            
            
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let dataToSync = parameters
            request.httpBody = try! JSONSerialization.data(withJSONObject: dataToSync)
            SVProgressHUD.show()
            
            Alamofire.request(request).responseJSON { (response) in
                
                switch response.result{
                case .success:
                    let statusCode: Int = (response.response?.statusCode)!
                    switch statusCode{
                    case 200:
                        SVProgressHUD.dismiss()
                        guard let data = response.data else { return }
                        do {
                            let loginRespone =  try JSONDecoder().decode(ApiStatus1.self, from: data)
                            
                            let loginStatus = loginRespone.status
                            
                            
                            if loginStatus!
                            {
                                NotificationCenter.default.post(name: Notification.Name("callApi"), object: nil)
                                
                                NotificationCenter.default.post(name: Notification.Name("callApi2"), object: nil)
                                //                            let vc = MainAppsViewController()
                                //
                                //                            vc.memberId = self.memberId
                                //                            vc.userId = self.userId
                                //                               vc.callgetMemberApi()
                                
                                let alertController = UIAlertController(title: "", message: loginRespone.message!, preferredStyle: .alert)
                                
                                
                                let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                                    
                                }
                                
                                alertController.addAction(settingsAction)
                                self.present(alertController, animated: true, completion: nil)
                                
                            }
                            else
                            {
                                let alertController = UIAlertController(title: "", message: loginRespone.message!, preferredStyle: .alert)
                                let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel) { (UIAlertAction) in
                                }
                                alertController.addAction(settingsAction)
                                self.present(alertController, animated: true, completion: nil)
                            }
                            
                            
                        }catch
                        {
                            print(error)
                            
                            SVProgressHUD.dismiss()
                        }
                        
                        
                        //
                        //
                        
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure:
                    SVProgressHUD.dismiss()
                    break
                }
            }
            
            
        })
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        self.present(alert, animated: true, completion: nil)
        
    
        
        
        
        
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        selectedArray.removeAll()
        familycollectionView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("Second VC will appear")
        familycollectionView.reloadData()
        
        // callgetMemberApi()
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(searchActive){
            return allowed?.count ?? 0
            
        }
        if SharedData.data.responseappsGames?.approve?.count == 0
        {
            blockButton.isHidden = true
            
        }else{
            blockButton.isHidden = false
            
        }
        
        return SharedData.data.responseappsGames?.approve?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AppsandGamesCollectionCell",for: indexPath) as! AppsandGamesCollectionCell
        
          if(searchActive){
            cell.appName.text = allowed?[indexPath.row].appName
            
            if allowed?[indexPath.row].appImg != nil
            {
                cell.appImage.sd_setImage(with: URL(string: (allowed?[indexPath.row].appImg)!), placeholderImage: UIImage(named: "AboutUs"))
            }else
            {
                cell.appImage.image = UIImage(named: "AboutUs")
                
            }
            cell.contentView.layer.cornerRadius = 15
            cell.contentView.layer.borderWidth = 0.5
            cell.contentView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            
            if self.selectedArray.contains((allowed?[indexPath.row].appName)!)
                
            {
                cell.tickMark.isHidden = false
                cell.tickMark.image = #imageLiteral(resourceName: "CheckBox")
                
                
            }else
            {
                
                cell.tickMark.image = #imageLiteral(resourceName: "UnCheck")
                cell.tickMark.isHidden = true
                
            }
            
            
        }else
          {
            if SharedData.data.responseappsGames?.approve?[indexPath.row].appImg != nil
            {
                cell.appImage.sd_setImage(with: URL(string: (SharedData.data.responseappsGames?.approve?[indexPath.row].appImg)!), placeholderImage: UIImage(named: "AboutUs"))
            }else
            {
                cell.appImage.image = UIImage(named: "AboutUs")
            }
            cell.appName.text = SharedData.data.responseappsGames?.approve?[indexPath.row].appName
            cell.contentView.layer.cornerRadius = 15
            cell.contentView.layer.borderWidth = 0.5
            cell.contentView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            
            if self.selectedArray.contains((SharedData.data.responseappsGames?.approve?[indexPath.row].appName)!)
                
            {
                cell.tickMark.isHidden = false
                cell.tickMark.image = #imageLiteral(resourceName: "CheckBox")
                
            }else
            {
                
                cell.tickMark.image = #imageLiteral(resourceName: "UnCheck")
                cell.tickMark.isHidden = true
            }
            
        }
        
     
        
     
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
         if(searchActive){
            if self.selectedArray.contains((allowed?[indexPath.row].appName)!) {
                self.selectedArray.remove(at: self.selectedArray.index(of: (allowed?[indexPath.row].appName)!)!)
            } else {
                self.selectedArray.append((allowed?[indexPath.row].appName)!)
            }
        }else
         {
        if self.selectedArray.contains((SharedData.data.responseappsGames?.approve?[indexPath.row].appName)!) {
            self.selectedArray.remove(at: self.selectedArray.index(of: (SharedData.data.responseappsGames?.approve?[indexPath.row].appName)!)!)
        } else {
            self.selectedArray.append((SharedData.data.responseappsGames?.approve?[indexPath.row].appName)!)
        }
        }
        familycollectionView.reloadData()
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let viewWidth = UIScreen.main.bounds.size.width
        
        if collectionView == familycollectionView || collectionView == familycollectionView {
            var width = (viewWidth - 20) / 4
            if UIDevice.current.userInterfaceIdiom == .pad {
                width = (viewWidth - 30) / 6
            }
            return CGSize(width: width, height: width)
        } else  {
            return CGSize(width: 60, height: 60)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("Second VC will disappear")
    }
}
