//
//  ThirdViewController.swift
//  UISegmentedControlAsTabbarDemo
//
//  Created by Ashok Reddy G on 28/05/19.
//  Copyright © 2019 A. Abdurrahman. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UISearchBarDelegate {
    @IBOutlet var familycollectionView: UICollectionView!
    var searchActive : Bool = false
    @IBOutlet var searchBar: UISearchBar!
    // var approve1:[[approve]]?
    var blocked:[approve]?
    var approve1:[approve]?
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        //        approve1?.append((SharedData.data.responseappsGames?.approve)!)
        //        approve1?.append((SharedData.data.responseappsGames?.blocked)!)
        //
        //        for each in (SharedData.data.responseappsGames?.approve)!
        //        {
        //            approve2?.append(each)
        //        }
        //
        //        for each in (SharedData.data.responseappsGames?.blocked)!
        //        {
        //            approve2?.append(each)
        //        }
        
        familycollectionView.register(UINib(nibName: "AppsandGamesCollectionCell", bundle: nil),
                                      forCellWithReuseIdentifier: "AppsandGamesCollectionCell")
        familycollectionView.delegate = self
        familycollectionView.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("TotalApps"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = false
      //  familycollectionView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        familycollectionView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        
        searchBar.text = nil
        searchBar.resignFirstResponder()
        familycollectionView.reloadData()
        
        self.searchBar.showsCancelButton = false
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.searchActive = true;
        self.searchBar.showsCancelButton = true
        
        
        blocked = approve1?.filter{ $0.appName!.localizedStandardContains(searchText) }
        
        familycollectionView.reloadData()
        
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        var approve2 = [approve]()
        
        
        for each in (SharedData.data.responseappsGames?.approve)!
        {
            approve2.append(each)
            
            print(approve2.count)
            
        }
        
        for each in (SharedData.data.responseappsGames?.blocked)!
        {
            approve2.append(each)
        }
        
        approve1 = approve2
        familycollectionView.reloadData()
        
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(searchActive){
            return blocked?.count ?? 0
            
        }
        
        return approve1?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AppsandGamesCollectionCell",for: indexPath) as! AppsandGamesCollectionCell
        
        
        if(searchActive){
            
            cell.appName.text = blocked?[indexPath.row].appName
            if approve1?[indexPath.row].appImg != nil
            {
                cell.appImage.sd_setImage(with: URL(string: (blocked?[indexPath.row].appImg)!), placeholderImage: UIImage(named: "AboutUs"))
            }else
            {
                cell.appImage.image = UIImage(named: "AboutUs")
            }
            
            
        }else
        {
            cell.appName.text = approve1?[indexPath.row].appName
            if approve1?[indexPath.row].appImg != nil
            {
                cell.appImage.sd_setImage(with: URL(string: (approve1?[indexPath.row].appImg)!), placeholderImage: UIImage(named: "AboutUs"))
            }else
            {
                cell.appImage.image = UIImage(named: "AboutUs")
            }
            
        }
        
        
        
        cell.tickMark.isHidden = true
        
        
        cell.contentView.layer.cornerRadius = 15
        cell.contentView.layer.borderWidth = 0.5
        cell.contentView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let viewWidth = UIScreen.main.bounds.size.width
        
        if collectionView == familycollectionView || collectionView == familycollectionView {
            var width = (viewWidth - 20) / 4
            if UIDevice.current.userInterfaceIdiom == .pad {
                width = (viewWidth - 30) / 6
            }
            return CGSize(width: width, height: width)
        } else  {
            return CGSize(width: 60, height: 60)
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
