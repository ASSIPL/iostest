//
//  AlarmsAndRemindersView.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 03/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//
//dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
import UIKit
import Firebase

class AlarmsAndRemindersView: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var gradientImageView: UIImageView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet var alarmReminderTableview: UITableView!
    var titleName:String?
    
    var getMemberDetailsbyId:Responsedata?
    
    var starters = [String]()
    var dishes = [String]()
    var deserts = [String]()
    var memberId:Int?
    var userId:Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        alarmReminderTableview.tableFooterView = UIView()
        
        
        self.title = titleName
        userId = profileData.userId
        addButton.changeButtonCornerRadius()
        backgroundImage.changeImageViewCornerRadius()
        starters  = ["sdfsdf","sdfsadg"]
        dishes = ["sdfsdf","sdfsadg"]
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        callApiAlarmsReminders()
         Analytics.logEvent("AlarmsAndRemindersViewEntered", parameters:nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("AlarmsAndRemindersViewExit", parameters:nil)
    }
    
    func callApiAlarmsReminders()
    {
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        let addMemberUrl = API.getMemberDetailsByMemberId + "?memberId=\(String(describing: memberId!))"  + "&userId=\(String(describing: (profileData.userId)!))"
        print(addMemberUrl)
        HttpWrapper.gets(with: addMemberUrl, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            // print(response)
            
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatusgetMemberDeatils.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    self.getMemberDetailsbyId  = loginRespone.response
                    if self.getMemberDetailsbyId != nil
                    {
                        self.alarmReminderTableview.dataSource = self
                        self.alarmReminderTableview.delegate = self
                        
                        self.alarmReminderTableview.reloadData()
                        // self.updateUi(sender: self.getMemberDetailsbyId)
                    }
                    
                }
                else
                    
                {
                    //   SharedData.data.getMemberDetails  = nil
                }
                
            }catch let jsonErr {
                //  SharedData.data.getMemberDetails=nil
                
                print("Error serializing json:", jsonErr)
                
            }
        }){ (error) in
            
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
            
            print(error)
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if getMemberDetailsbyId?.alarmsInfo?.count == 0 && getMemberDetailsbyId?.remindersInfo?.count == 0
        {
            
            let noDataLabel: UILabel = UILabel(frame:CGRect(x: 0, y: 0, width: self.alarmReminderTableview.frame.width, height: self.alarmReminderTableview.frame.height))
            noDataLabel.backgroundColor = .white
            noDataLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            alarmReminderTableview.separatorStyle = .none
            noDataLabel.text = "No Alarms/Reminders found"
            noDataLabel.textAlignment = NSTextAlignment.center
            self.alarmReminderTableview.backgroundView = noDataLabel
            return 1
        }
        else
        {
            
            self.alarmReminderTableview.backgroundView = nil
            
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return getMemberDetailsbyId?.alarmsInfo?.count ?? 0
        }
        else if section == 1 {
            return  getMemberDetailsbyId?.remindersInfo?.count ?? 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Alarms"
        }
        else if section == 1
        {
            return "Reminders"
        }
        return ""
        
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.contentView.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            headerView.layer.cornerRadius = 5
            headerView.clipsToBounds = true
            headerView.textLabel?.textColor = .white
            headerView.textLabel?.font = UIFont(name: "Montserrat-Regular", size: 17)
            
        }
    }
    //    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    //        return 61.0
    //    }
    //    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
    //
    //
    //        if section == 1
    //        {
    //            let header = view as? UITableViewHeaderFooterView
    //            header?.textLabel?.font = UIFont(name:"Courier", size:15)
    //            header?.textLabel?.textColor = UIColor.white
    //            header?.textLabel?.textAlignment = .center
    //            header?.backgroundView?.backgroundColor = #colorLiteral(red: 0.3849651016, green: 0.4546981623, blue: 0.9686274529, alpha: 1)
    //
    //        }
    //        else
    //        {
    //            let header = view as? UITableViewHeaderFooterView
    //            header?.textLabel?.font = UIFont(name:"Courier", size:15)
    //            header?.textLabel?.textColor = UIColor.white
    //            header?.textLabel?.textAlignment = .center
    //            header?.backgroundView?.backgroundColor = #colorLiteral(red: 0.3849651016, green: 0.4546981623, blue: 0.9686274529, alpha: 1)
    //        }
    //
    //
    //    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AlarmTableViewCell", for: indexPath) as? AlarmTableViewCell
            
            
            let array = getMemberDetailsbyId?.alarmsInfo?[indexPath.row].days?.components(separatedBy: ",")
            
            if array?.contains("Monday") ?? false
            {
                cell!.monday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.monday.setTitleColor(UIColor.white, for: .normal)
                cell!.monday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.monday.borderWidth = 0.5
                
            }else
            {
                cell!.monday.backgroundColor = UIColor.white
                cell!.monday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
                cell!.monday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.monday.borderWidth = 0.5
            }
            if array?.contains("Tuesday") ?? false
            {
                cell!.thuesday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.thuesday.setTitleColor(UIColor.white, for: .normal)
                cell!.thuesday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.thuesday.borderWidth = 0.5
            }else
            {
                cell!.thuesday.backgroundColor = UIColor.white
                cell!.thuesday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
                cell!.thuesday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.thuesday.borderWidth = 0.5
            }
            
            if array?.contains("Wednesday") ?? false
            {
                
                cell!.wednesday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.wednesday.setTitleColor(UIColor.white, for: .normal)
                cell!.wednesday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.wednesday.borderWidth = 0.5
            }else
            {
                cell!.wednesday.backgroundColor = UIColor.white
                cell!.wednesday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
                cell!.wednesday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.wednesday.borderWidth = 0.5
            }
            
            if array?.contains("Thursday") ?? false
            {
                cell!.thursday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.thursday.setTitleColor(UIColor.white, for: .normal)
                cell!.thursday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.thursday.borderWidth = 0.5
            }else
            {
                cell!.thursday.backgroundColor = UIColor.white
                cell!.thursday.setTitleColor(#colorLiteral(red: 0, green: 0.7152789235, blue: 0.7682691216, alpha: 1), for: .normal)
                cell!.thursday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.thursday.borderWidth = 0.5
            }
            
            if array?.contains("Friday") ?? false
            {
                cell!.friday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.friday.setTitleColor(UIColor.white, for: .normal)
                cell!.friday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.friday.borderWidth = 0.5
            }else
            {
                cell!.friday.backgroundColor = UIColor.white
                cell!.friday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
                cell!.friday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.friday.borderWidth = 0.5
            }
            
            if array?.contains("Saturday") ?? false
            {
                cell!.saturday.backgroundColor = #colorLiteral(red: 0, green: 0.7152789235, blue: 0.7682691216, alpha: 1)
                cell!.saturday.setTitleColor(UIColor.white, for: .normal)
                cell!.saturday.borderColor = #colorLiteral(red: 0, green: 0.7152789235, blue: 0.7682691216, alpha: 1)
                cell!.saturday.borderWidth = 0.5
            }else
            {
                cell!.saturday.backgroundColor = UIColor.white
                cell!.saturday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
                cell!.saturday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.saturday.borderWidth = 0.5
            }
            
            if array?.contains("Sunday") ?? false
            {
                cell!.sunday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.sunday.setTitleColor(UIColor.white, for: .normal)
                cell!.sunday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.sunday.borderWidth = 0.5
            }else
            {
                cell!.sunday.backgroundColor = UIColor.white
                cell!.sunday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
                cell!.sunday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.sunday.borderWidth = 0.5
            }
            
            cell!.alarmNameLabel.text = getMemberDetailsbyId?.alarmsInfo?[indexPath.row].title
            var alarmTime:String?
            alarmTime = getMemberDetailsbyId?.alarmsInfo?[indexPath.row].time
            
            
            let dateAsString = alarmTime
            let df = DateFormatter()
            df.dateFormat = "HH:mm:ss"
            
            let date = df.date(from: dateAsString!)
            df.dateFormat = "hh:mm a"
            let time12 = df.string(from: date!)
            
            
            
            cell?.timeLabel.text = time12
            
            cell!.alarmDescripLabel.text = getMemberDetailsbyId?.alarmsInfo?[indexPath.row].description
            cell?.deleteButton.tag = indexPath.row
            cell?.deleteButton.addTarget(self, action: #selector(deleteAlarm), for: .touchUpInside)
            
            //     cell!.description.text = getMemberDetailsbyId?.alarmsInfo?[indexPath.row].title
            
            return cell!
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReminderTableViewCell", for: indexPath) as? ReminderTableViewCell
            
            cell!.reminderName.text = getMemberDetailsbyId?.remindersInfo?[indexPath.row].title
            
            cell!.reminderDescription.text = getMemberDetailsbyId?.remindersInfo?[indexPath.row].description
            
            var reminderDate:String?
            reminderDate = getMemberDetailsbyId?.remindersInfo?[indexPath.row].reminderDate
            
            print(reminderDate!)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date1 = dateFormatter.date(from: reminderDate!)
            
            dateFormatter.dateFormat = "MM-dd-yyyy"
            let goodDate = dateFormatter.string(from: date1!)
            
            
            cell!.dateRemainder.text = goodDate
            
            var reminderTime:String?
            reminderTime = getMemberDetailsbyId?.remindersInfo?[indexPath.row].time
            
            
            
            
            
            let dateAsString = reminderTime
            let df = DateFormatter()
            df.dateFormat = "HH:mm:ss"
            
            let date = df.date(from: dateAsString!)
            df.dateFormat = "hh:mm a"
            let time12 = df.string(from: date!)
            print(time12)
            
            cell!.timeRemainder.text = time12
            cell?.deleteButton.tag = indexPath.row
            cell?.deleteButton.addTarget(self, action: #selector(deleteRemainder), for: .touchUpInside)
            
            return cell!
            
        }
        
        
        return UITableViewCell()
    }
    
    @objc func deleteAlarm(sender: UIButton!) {
        Analytics.logEvent("Alarms_Reminders_Screen_DeleteAlarm_Clicked", parameters:nil)
        
        //     let alertController = UIAlertController(title: Constants.alertTitle, message: "Do you want to remove \(String(describing: (getMemberDetailsbyId?.alarmsInfo?[sender.tag].title!)!)) Alarm", preferredStyle: .alert)
        
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        
        let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
        let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
        
        let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
        let msgAttrString = NSMutableAttributedString(string: "Do you want to remove \(String(describing: (getMemberDetailsbyId?.alarmsInfo?[sender.tag].title!)!)) Alarm ?", attributes: msgFont)
        
        alert.setValue(titAttrString, forKey: "attributedTitle")
        alert.setValue(msgAttrString, forKey: "attributedMessage")
        
        
        
        let cancelAction = UIAlertAction(title: "NO", style: .cancel, handler: { (action : UIAlertAction!) -> Void in })
        
        let saveAction = UIAlertAction(title: "YES", style: .default, handler: { alert -> Void in
            let deleteAlarm = API.deleteAlarm + "?alaramSerialKey=\(String(describing: (self.getMemberDetailsbyId?.alarmsInfo?[sender.tag].alaramKey!)!))"
            print(deleteAlarm)
            
            HttpWrapper.gets(with: deleteAlarm, parameters: nil, headers: nil, completionHandler: { (response, error)  in
                //  print(response)
                guard let data = response else { return }
                do {
                    
                    let loginRespone = try JSONDecoder().decode(ApiStatus1.self, from: data)
                    let loginStatus = loginRespone.status
                    if  loginStatus  ==  true
                    {
                        self.callApiAlarmsReminders()
                        self.showAlert(title: "", msg: "Deleted Successfully")
                    }
                    
                }catch let jsonErr {
                    
                    print("Error serializing json:", jsonErr)
                }
            }){ (error) in
                
                if Reachability.isConnectedToNetwork(){
                    print("Internet Connection Available!")
                }else{
                    self.showAlert(title: "", msg: "No network connection")
                }
                print(error)
            }
            
            
        })
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @objc func deleteRemainder(sender: UIButton!) {
        
        Analytics.logEvent("Alarms_Reminders_Screen_DeleteReminbers_Clicked", parameters:nil)
        //    let alertController = UIAlertController(title: Constants.alertTitle, message: "Do you want to remove \(String(describing: (getMemberDetailsbyId?.remindersInfo?[sender.tag].title!)!)) reminder", preferredStyle: .alert)
        
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        
        let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
        let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
        
        let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
        let msgAttrString = NSMutableAttributedString(string: "Do you want to remove \(String(describing: (getMemberDetailsbyId?.remindersInfo?[sender.tag].title!)!)) reminder ?", attributes: msgFont)
        
        alert.setValue(titAttrString, forKey: "attributedTitle")
        alert.setValue(msgAttrString, forKey: "attributedMessage")
        
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: { (action : UIAlertAction!) -> Void in })
        
        let saveAction = UIAlertAction(title: "Yes", style: .default, handler: { alert -> Void in
            
            let deleteAlarm = API.deleteReminder + "?remainderId=\(String(describing: (self.getMemberDetailsbyId?.remindersInfo?[sender.tag].remainderId!)!))"
            print(deleteAlarm)
            
            HttpWrapper.gets(with: deleteAlarm, parameters: nil, headers: nil, completionHandler: { (response, error)  in
                print(response)
                
                
                guard let data = response else { return }
                do {
                    
                    let loginRespone = try JSONDecoder().decode(ApiStatus1.self, from: data)
                    let loginStatus = loginRespone.status
                    if  loginStatus  ==  true
                    {
                        self.showAlert(title: "", msg: "Deleted Successfully")
                        self.callApiAlarmsReminders()
                    }
                    
                }catch let jsonErr {
                    
                    print("Error serializing json:", jsonErr)
                }
            }){ (error) in
                
                if Reachability.isConnectedToNetwork(){
                    print("Internet Connection Available!")
                }else{
                    self.showAlert(title: "", msg: "No network connection")
                }
                print(error)
            }
            
        })
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
        if section == 0 {
            return ((getMemberDetailsbyId?.alarmsInfo?.count)! > 0) ? 30 : 0
        }
        else if section == 1
        {
            return ((getMemberDetailsbyId?.remindersInfo?.count)! > 0) ? 30 : 0
        }
        
        return 0
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SetReminderViewController") as! SetReminderViewController
        vc.memberId = memberId
        vc.userId = userId
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension UIImageView
{
    func changeImageViewCornerRadius()
    {
        self.layer.cornerRadius = 20
        self.clipsToBounds = true
        
    }
    
}
