//
//  NotificationsViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 03/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import EventKit
import CoreData
import SVProgressHUD
import Firebase
import Alamofire

class NotificationsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var selectAllLabel: UILabel!
    
    @IBOutlet var selectAllButton: UIButton!
    
    
    
    @IBOutlet var deleteButton: UIButton!
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    @IBOutlet var notificationTableView: UITableView!
    
    var notifiationResponse:NotifiationResponse?
    
    var allNotificationsrr = [allNotifications]()
    
    
    
    var pushNotifications:[PushNotifications] = []
    
    var notificationText:Array = [String]()
    var time:Array = [String]()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var seletedarray = [allNotifications]()
    
    var eventStore: EKEventStore!
    var reminders: [EKReminder]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.callgetMemberApi), name: NSNotification.Name(rawValue: "nameOfNotification"), object: nil)
        
        navigationController?.navigationBar.barTintColor = Constants.colour
        self.navigationItem.title = "Notifications"
        self.changeFontForViewController()
        
        
        getPushNotificationData()
        
        callgetMemberApi()
        
        appDelegate.notificationBargeCount = ""
        notificationTableView.dataSource = self
        notificationTableView.delegate = self
        
        
        notificationText = ["Member Permission Request","Member Permission Request","Member Permission Request"]
        time = ["15 mins","15 mins","15 mins"]
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("NotificationViewExit", parameters:nil)
        
        
        UserDefaults.standard.set(nil, forKey: "fromnotificationTap")
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        Analytics.logEvent("NotificationViewEntered", parameters:nil)
    }
    
    
    func getPushNotificationData()
    {
        
        allNotificationsrr.removeAll()
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PushNotifications")
        do {
            
            pushNotifications =  try context.fetch(fetchRequest) as! [PushNotifications]
            
            
            
            if pushNotifications.count == 0
            {
                
            }else
            {
                
                for each in pushNotifications
                {
                    var model1 = allNotifications()
                    
                    model1.notificationText = each.body
                    
                    print(each.timeStamp)
                    
                    // print(Int(each.timeStamp!))
                    
                    
                    model1.createdDate = Int(each.timeStamp ?? "")
                    // model1.createdDate = NSDate().timeIntervalSince1970
                    
                    
                    allNotificationsrr.append(model1)
                    
                }
                
            }
            
        }catch
        {
            
        }
        
    }
    
    
    
    
    @IBAction func DeleteButton(_ sender: UIButton) {
        
        if allNotificationsrr.count < 1
        {
            
            
            return
        }
        
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        
        let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
        let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
        
        let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
        let msgAttrString = NSMutableAttributedString(string: "Are you sure want to delete?", attributes: msgFont)
        
        alert.setValue(titAttrString, forKey: "attributedTitle")
        alert.setValue(msgAttrString, forKey: "attributedMessage")
        
        
        let cancelAction = UIAlertAction(title: "NO", style: .cancel, handler: { (action : UIAlertAction!) -> Void in })
        
        let saveAction = UIAlertAction(title: "YES", style: .default, handler: { alert -> Void in
            
            var Notificationids = [Int]()
            
            
            Notificationids.removeAll()
            
            
            
            
            
            for each in self.allNotificationsrr
            {
                //
                //
                
                if each.userId  == nil
                {
                    var pushNotifications:[PushNotifications] = []
                    //            let fetchRequest: NSFetchRequest<PushNotifications> = PushNotifications.fetchRequest()
                    //            fetchRequest.predicate = NSPredicate(format: "title == %d", each)
                    
                    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                    
                    let fetchrequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PushNotifications")
                    
                    //   self.selectAllButton.isSelected = false
                    //      self.selectAllButton.setImage(UIImage(named: "UnCheck"), for: .normal)
                    
                    fetchrequest.predicate = NSPredicate(format: "timeStamp == %@",String(describing: (each.createdDate)!))
                    
                    let objects = try! context.fetch(fetchrequest)
                    for obj in objects {
                        context.delete(obj as! NSManagedObject)
                        
                    }
                    
                    do {
                        
                        //                        print(fetchrequest)
                        //                        pushNotifications = try context.fetch(fetchrequest) as! [PushNotifications]
                        //
                        //                        context.delete(pushNotifications[0])
                        //
                        try context.save()
                    } catch _ {
                        // error handling
                    }
                    
                    
                    
                }else
                {
                    Notificationids.append(each.notificationId!)
                }
                
                
                
                if Notificationids.count > 0
                {
                    
                    
                    let parameter:NSMutableArray = NSMutableArray()
                    
                    parameter.addObjects(from: Notificationids)
                    
                    
                    var request = URLRequest(url: URL(string: API.deleteNotificationById)!)
                    request.httpMethod = "POST"
                    let id = UserDefaults.standard.string(forKey: "jwtToken")
                    
                    if id != nil && id != ""
                    {
                        let token = "Bearer" + " "  + "\(UserDefaults.standard.string(forKey: "jwtToken")!)"
                        
                        print(token)
                        request.setValue(token, forHTTPHeaderField: "Authorization")
                    }
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    let dataToSync = parameter
                    request.httpBody = try! JSONSerialization.data(withJSONObject: dataToSync)
                    
                    
                    Alamofire.request(request).responseJSON { (response) in
                        switch response.result{
                        case .success:
                            let statusCode: Int = (response.response?.statusCode)!
                            guard let data = response.data else { return }
                            switch statusCode{
                            case 200:
                                do {
                                    let loginRespone =  try JSONDecoder().decode(ApiStatus12.self, from: data)
                                    let loginStatus = loginRespone.status
                                    
                                    print(loginRespone.status)
                                    if loginStatus ?? false
                                    {
                                        
                                        
                                        self.seletedarray.removeAll()
                                        DispatchQueue.main.async() {
                                            
                                         //   self.selectAllButton.isSelected = false
                                          //  self.selectAllButton.setImage(UIImage(named: "UnCheck"), for: .normal)
                                            
                                            self.getPushNotificationData()
                                            self.callgetMemberApi()
                                            self.notificationTableView.reloadData()
                                        }
                                    }
                                    
                                    
                                }catch
                                {
                                    print(error)
                                    DispatchQueue.main.async() {
                                        
                                        self.getPushNotificationData()
                                        self.callgetMemberApi()
                                        self.notificationTableView.reloadData()
                                    }
                                    
                                }
                                
                                
                                break
                            default:
                                
                                break
                            }
                            break
                        case .failure:
                            
                            break
                        }}
                    
                    
                    //            DispatchQueue.main.async() {
                    //
                    //                self.getPushNotificationData()
                    //                self.notificationTableView.reloadData()
                    //            }
                    //
                    //            let request = NSBatchDeleteRequest(fetchRequest: fetchrequest as! NSFetchRequest<NSFetchRequestResult>)
                    //
                    //            do {
                    //
                    //
                    //                try context.execute(request)
                    //                try context.save()
                    //
                    //                DispatchQueue.main.async() {
                    //
                    //                                    self.getPushNotificationData()
                    //                                    self.notificationTableView.reloadData()
                    //                                }
                    //            } catch {
                    //                print ("There was an error")
                    //            }
                    //        }
                    
                    
                    
                    
                    
                    
                }
                
            }
            
            
            
            DispatchQueue.main.async() {
                
                self.getPushNotificationData()
                self.callgetMemberApi()
                self.notificationTableView.reloadData()
            }
            
            
        })
        
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        self.present(alert, animated: true, completion: nil)
        
        
        
    }
    
    func deleteNotificationID(notificationID:Int)
    {
        
        
        var Notificationids = [Int]()
        let parameter:NSMutableArray = NSMutableArray()
        
        Notificationids.append(notificationID)
        
        parameter.addObjects(from: Notificationids)
        
        //     parameter.addObjects(from: notificationID)
        
        
        var request = URLRequest(url: URL(string: API.deleteNotificationById)!)
        request.httpMethod = "POST"
        let id = UserDefaults.standard.string(forKey: "jwtToken")
        
        if id != nil && id != ""
        {
            let token = "Bearer" + " "  + "\(UserDefaults.standard.string(forKey: "jwtToken")!)"
            
            print(token)
            request.setValue(token, forHTTPHeaderField: "Authorization")
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let dataToSync = parameter
        request.httpBody = try! JSONSerialization.data(withJSONObject: dataToSync)
        
        
        Alamofire.request(request).responseJSON { (response) in
            switch response.result{
            case .success:
                let statusCode: Int = (response.response?.statusCode)!
                guard let data = response.data else { return }
                switch statusCode{
                case 200:
                    do {
                        let loginRespone =  try JSONDecoder().decode(ApiStatus12.self, from: data)
                        let loginStatus = loginRespone.status
                        
                        print(loginRespone.status)
                        if loginStatus ?? false
                        {
                            
                            
                            self.seletedarray.removeAll()
                            DispatchQueue.main.async() {
                                
                              //  self.selectAllButton.isSelected = false
                              //  self.selectAllButton.setImage(UIImage(named: "UnCheck"), for: .normal)
                                
                                self.getPushNotificationData()
                                self.callgetMemberApi()
                                self.notificationTableView.reloadData()
                            }
                        }
                        
                        
                    }catch
                    {
                        print(error)
                        DispatchQueue.main.async() {
                            
                            self.getPushNotificationData()
                            self.callgetMemberApi()
                            self.notificationTableView.reloadData()
                        }
                        
                    }
                    
                    
                    break
                default:
                    
                    break
                }
                break
            case .failure:
                
                break
            }}
        
    }
    
    
    @objc func callgetMemberApi()
    {
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        SVProgressHUD.show()
        let addMemberUrl = API.getAllNotificationsBYMemberId + "?userId=\(String(describing: profileData.userId!))"
        print(addMemberUrl)
        HttpWrapper.gets(with: addMemberUrl, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            // print(response)
            SVProgressHUD.dismiss()
            guard let data = response else { return }
            do {
                
                
                let loginRespone = try JSONDecoder().decode(ApiStatusNotifications.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    self.notifiationResponse = loginRespone.response
                    if (self.notifiationResponse?.allNotifications?.count)! > 0
                    {
                        for each in ((self.notifiationResponse?.allNotifications)!)
                        {
                            
                            self.allNotificationsrr.append(each)
                        }
                    }
                    
                    print(self.allNotificationsrr.count)
                    
                    
                    
                    
                    
                    
                    // self.notifiationResponse?.allNotifications?.reversed()
                    if (self.notifiationResponse?.remainderList?.count)! > 0
                    {
                        for each in (self.notifiationResponse?.remainderList)!
                        {
                            
                            let date = each.reminderDate
                            print(date)
                            
                            let time = each.time
                            print(time)
                            
                            let timetotal = "\(String(describing: date!))" + " " + time!
                            
                            
                            
                            self.addEventToCalendar(title: each.title!, description: each.description, startDate: self.convertDateFormater(date: timetotal).toDate()!, endDate:   self.convertDateFormater(date: timetotal).toDate()!)
                            
                            //                    self.addEventToCalendar(title: each.title!, description: "Remember or die!", startDate: "08-05-2019 10:30".toDate()!, endDate:  "08-05-2019 10:30".toDate()!)
                            
                            
                            
                        }
                        
                        
                    }
                    
                    
                    
                    
                    
                    self.notificationTableView.reloadData()
                }
                else
                    
                {
                    
                    self.notificationTableView.reloadData()
                    
                }
                
            }catch let jsonErr {
                SVProgressHUD.dismiss()
                self.notificationTableView.reloadData()
                print("Error serializing json:", jsonErr)
                
            }
        }){ (error) in
            
            SVProgressHUD.dismiss()
            self.notificationTableView.reloadData()
            
            print(error)
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
    }
    func convertDateFormater(date: String) -> String
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        return  dateFormatter.string(from: date!)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let numOfSection: NSInteger =  allNotificationsrr.count
        
        if numOfSection == 0
        {
            
            let noDataLabel: UILabel = UILabel(frame:CGRect(x: 0, y: 0, width: self.notificationTableView.frame.width, height: self.notificationTableView.frame.height))
            noDataLabel.backgroundColor = .white
            noDataLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            notificationTableView.separatorStyle = .none
            noDataLabel.text = "No notifications found"
            noDataLabel.textAlignment = NSTextAlignment.center
            self.notificationTableView.backgroundView = noDataLabel
            return 1
        }
        else
        {
            
            self.notificationTableView.backgroundView = nil
            
            return 1
        }
    }
    
    
    @IBAction func selectAllButton(_ sender: UIButton) {
        if sender.isSelected
        {
            self.seletedarray.removeAll()
            notificationTableView.reloadData()
            
        }else
        {
            
            self.seletedarray.removeAll()
            //   self.seletedarray.append((allNotificationsrr.reversed()[sender.tag]))
            
            for each in allNotificationsrr.reversed()
            {
                self.seletedarray.append(each)
            }
            
            
            
            
            
            
            
            notificationTableView.reloadData()
        }
        
        sender.isSelected = !sender.isSelected
        
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(notifiationResponse?.allNotifications?.count ?? 0)
        
        if allNotificationsrr.reversed().count > 0
        {
            deleteButton.isHidden = false
           // selectAllButton.isHidden = false
          //  selectAllLabel.isHidden = false
        }else
        {
            deleteButton.isHidden = true
          //  selectAllButton.isHidden = true
           // selectAllLabel.isHidden = true
        }
        //self.notifiationResponse?.allNotifications?.reversed()
        return allNotificationsrr.reversed().count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell") as! NotificationTableViewCell
        // cell.sideImageView.image = #imageLiteral(resourceName: "minus")
        cell.notificationLabel.text = allNotificationsrr.reversed()[indexPath.row].notificationText
        
        cell.deleteButton.tag = indexPath.row
        
        //   cell.deleteButton.addTarget(self, action: #selector(panicAction), for: .touchUpInside)
        
        if   seletedarray.contains(where: { name in name.createdDate == (allNotificationsrr.reversed()[indexPath.row].createdDate)})
            
        {
            cell.deleteButton.setImage(UIImage(named:"checknew"), for: .normal)
        }else
        {
            cell.deleteButton.setImage(UIImage(named:"UnCheck"), for: .normal)
        }
        
        
        //  cell.deleteButton.setImage(UIImage(named: self.seletedarray[indexPath.row].createdDate!.contains((allNotificationsrr.reversed()[indexPath.row].createdDate)!) ?  "CheckBox" : "UnCheck"), for: .normal)
        
        if allNotificationsrr.reversed()[indexPath.row].createdDate != nil
        {
            let unixTimeStamp: Double = Double((allNotificationsrr.reversed()[indexPath.row].createdDate)!) / 1000.0
            //        let date = NSDate(timeIntervalSince1970:unixTimeStamp)
            //        print(date)
            cell.timeLabel.text = getDateFromTimeStamp(timeStamp : unixTimeStamp)
        }
        
        // print(getDateFromTimeStamp(timeStamp : unixTimeStamp))
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmPermissionsVC") as! ConfirmPermissionsVC
        
        
        vc.fromNotification = "true"
        //        if notifiationResponse?.allNotifications![indexPath.row].memberName == nil ||  notifiationResponse?.allNotifications![indexPath.row].memberName == ""
        //        {
        //           return
        //        }
        
        if allNotificationsrr.reversed()[indexPath.row].notificationId != nil
        {
            vc.notificationId = allNotificationsrr.reversed()[indexPath.row].notificationId
            vc.allNotifications = allNotificationsrr.reversed()[indexPath.row]
            
            
            
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle:   UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            
            
            if allNotificationsrr.reversed()[indexPath.row].notificationId  == nil
            {
                
                
                var pushNotifications:[PushNotifications] = []
                
                let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                
                let fetchrequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PushNotifications")
                
                fetchrequest.predicate = NSPredicate(format: "timeStamp == %@",String(describing: (allNotificationsrr.reversed()[indexPath.row].createdDate)!))
                
                let objects = try! context.fetch(fetchrequest)
                for obj in objects {
                    context.delete(obj as! NSManagedObject)
                    
                }
                
                do {
                    
                    try context.save()
                } catch _ {
                    
                    
                    
                    
                }
            }else
            {
                let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                
                let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
                let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
                
                let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
                let msgAttrString = NSMutableAttributedString(string: "Are you sure want to delete?", attributes: msgFont)
                
                alert.setValue(titAttrString, forKey: "attributedTitle")
                alert.setValue(msgAttrString, forKey: "attributedMessage")
                
                
                let cancelAction = UIAlertAction(title: "NO", style: .cancel, handler: { (action : UIAlertAction!) -> Void in })
                
                let saveAction = UIAlertAction(title: "YES", style: .default, handler: { alert -> Void in
                    
                    self.deleteNotificationID(notificationID: (self.allNotificationsrr.reversed()[indexPath.row].notificationId)!)
                    
                })
                
                alert.addAction(cancelAction)
                alert.addAction(saveAction)
                self.present(alert, animated: true, completion: nil)
                
                
            }
            
            
            DispatchQueue.main.async() {
                
                self.getPushNotificationData()
                self.callgetMemberApi()
                self.notificationTableView.reloadData()
            }
            
            
        }
    }
    
    @objc func panicAction(sender: UIButton!) {
        
        
        
        
        //  if seletedarray[sender.tag].createdDate!.
        
        if seletedarray.contains(where: { name in name.createdDate == (allNotificationsrr.reversed()[sender.tag].createdDate)})
        {
            
            
            // seletedarray.remove(at:seletedarray.firstIndex{$0 === (allNotificationsrr.reversed()[sender.tag].createdDate)})
            
            
            seletedarray.remove(at:seletedarray.index(where: { $0.createdDate == (allNotificationsrr.reversed()[sender.tag].createdDate)})!)
            
            
            
            
        }else
        {
            self.seletedarray.append((allNotificationsrr.reversed()[sender.tag]))
        }
        
        
        if seletedarray.count == 0
        {
          //  selectAllButton.isSelected = false
            
        }
        
        
        if seletedarray.count == allNotificationsrr.reversed().count
        {
          //  selectAllButton.isSelected = true
            
        }else
        {
          //  selectAllButton.isSelected = false
            
        }
        
        
        //        if self.seletedarray[sender.tag].contains((allNotificationsrr.reversed()[sender.tag].createdDate)!) {
        //
        //
        //           // self.seletedarray.remove(at: seletedarray.index(after: 1))
        //
        //
        //         //   self.seletedarray.remove(at: self.seletedarray.index(of: (allNotificationsrr.reversed()[sender.tag]))!)
        //
        //   // self.seletedarray.remove(at: self.seletedarray.index(of: (allNotificationsrr.reversed()[sender.tag]))!)
        //        } else {
        //            self.seletedarray.append((allNotificationsrr.reversed()[sender.tag]))
        //        }
        //
        notificationTableView.reloadData()
        
    }
    
    
    func addEventToCalendar(title: String, description: String?, startDate: Date, endDate: Date, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = description
                
                event.calendar = eventStore.defaultCalendarForNewEvents
                event.addAlarm(EKAlarm(relativeOffset: -5.0))
                do {
                    try eventStore.save(event, span: .thisEvent)
                    let alarm = EKAlarm(relativeOffset: 30)
                    
                    // let alarm = EKAlarm(absoluteDate: startDate)
                    event.addAlarm(alarm)
                } catch let e as NSError {
                    completion?(false, e)
                    return
                }
                completion?(true, nil)
            } else {
                completion?(false, error as NSError?)
            }
        })
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func getDateFromTimeStamp(timeStamp : Double) -> String {
        
        let date = NSDate(timeIntervalSince1970: timeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        //  dayTimePeriodFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        dayTimePeriodFormatter.dateFormat = "MM-dd-yyyy hh:mm a"

        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
}

extension UITableView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        // messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}
extension Date {
    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
