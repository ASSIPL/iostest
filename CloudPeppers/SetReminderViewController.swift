//
//  SetReminderViewController.swift
//  Uidesign
//
//  Created by Allvy on 06/05/19.
//  Copyright © 2019 Ashok Gudipati. All rights reserved.
//

import UIKit
import Firebase

class SetReminderViewController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource{
    
    @IBOutlet weak var repeatViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var timePicker: UIDatePicker!
    
    @IBOutlet weak var lineLabel: UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var displayDateLabel: UILabel!
    
    @IBOutlet weak var displayDateTF: UITextField!
    
    @IBOutlet weak var mondayButton: UIButton!
    
    @IBOutlet weak var sundayButton: UIButton!
    @IBOutlet weak var saturdayButton: UIButton!
    @IBOutlet weak var fridayButton: UIButton!
    @IBOutlet weak var thurdayButton: UIButton!
    @IBOutlet weak var tuesdayButton: UIButton!
    @IBOutlet weak var wednesdayButton: UIButton!
    @IBOutlet weak var descriptionTextField: UITextField!
    
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var typeTextField: UITextField!
    
    @IBOutlet weak var calenderButton: UIButton!
    
    @IBOutlet weak var calenderTextField: UITextField!
    var titleName:String?
    
    @IBOutlet weak var repeatLabel: UILabel!
    
    var dateString:String?
    var repeatornot:Bool?
    var timeString:String?
    var type:String?
    var weekarray = [String]()
    var pickerData: [String] = [String]()
    
    var memberId:Int?
    var userId:Int?
    
    let datePicker = UIDatePicker()
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        repeatornot = true
        
      Analytics.logEvent("SetReminderViewEntered", parameters:nil)
        
        
        addButton.changeButtonCornerRadius()
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        let data1 = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data1!)
        
        for each in   (organizationdetails?.rulez)!
        {
            if each.rulesKey == "key.alarmAndReminders"
            {
                titleName   = each.name
                
                
                
            }
        }
        
        userId = profileData.userId
        
        self.title = titleName
        
        let myPicker = UIPickerView()
        typeTextField.inputView = myPicker
        myPicker.delegate = self
        myPicker.dataSource = self
        pickerData = ["Alarm", "Reminder"]
        typeTextField.text = "Alarm"
        self.calenderButton.isHidden = true
        
        
        
        //       print(timePicker.date)
        //          print(timePicker.timeZone)
        //
        //        print(type)
        //
        //        print(timeString)
        
        showDatePicker()
        titleTextField.useUnderline()
        descriptionTextField.useUnderline()
        
        buttonUnSelected(button: mondayButton)
        buttonUnSelected(button: tuesdayButton)
        buttonUnSelected(button: wednesdayButton)
        buttonUnSelected(button: thurdayButton)
        buttonUnSelected(button: fridayButton)
        buttonUnSelected(button: saturdayButton)
        buttonUnSelected(button: sundayButton)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("SetReminderViewExit", parameters:nil)
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
         toolbar.tintColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7254901961, alpha: 1)
        displayDateTF.inputAccessoryView = toolbar
        displayDateTF.inputView = datePicker
        
        
        
        let now = Date();
        datePicker.minimumDate = now
        
    }
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        //   formatter.dateFormat = "MM-dd-yyyy"
        
        formatter.dateFormat = "yyyy-MM-dd"
        dateString = formatter.string(from: datePicker.date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let date = dateFormatter.string(from: datePicker.date)
        
        
        displayDateLabel.text = date
        
        self.view.endEditing(true)
        
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        typeTextField.text = pickerData[row]
        if pickerData[row] == "Reminder"
        {
            Analytics.logEvent("AddingAlarms_Reminders_NewReminders_Cliicked", parameters:nil)
            self.heightConstraint.constant = 0
            self.lineLabel.isHidden = true
          //  self.checkButton.isHidden = true
            self.repeatLabel.isHidden = true
            self.repeatViewHeight.constant = 0
            self.displayDateLabel.isHidden = false
            
            calenderButton.isHidden = false
            
            calenderTextField.isHidden = false
            
        }
        else
        {
            
            Analytics.logEvent("AddingAlarms_Reminders_NewAlarm_Cliicked", parameters:nil)
            calenderButton.isHidden = true
            
            calenderTextField.isHidden = true
            
            self.displayDateLabel.isHidden = true
            
            self.heightConstraint.constant = 39.5
            self.lineLabel.isHidden = false
        //    self.checkButton.isHidden = false
            self.repeatLabel.isHidden = false
            self.repeatViewHeight.constant = 55
        }
        
        
        
        self.view.endEditing(true) 
    }
    
    
    @IBAction func calenderButtonTapped(_ sender: UIButton) {
    }
    @IBOutlet weak var weekDisView: UIView!
    
    
    
    @IBAction func mondayButtonTapped(_ sender: UIButton) {
        Analytics.logEvent("DailyTimeLimits_Screen_DayPicked", parameters:nil)
        
        let monString = "Monday"
        
        if mondayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(monString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(monString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: monString)!)
                
            }
            
        }
        sender.isSelected = !sender.isSelected
        
    }
    
    @IBAction func tuesdayButtonTapped(_ sender: UIButton) {
        
        
        let tueString = "Tuesday"
        
        if tuesdayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(tueString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(tueString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: tueString)!)
                
            }
            
        }
        sender.isSelected = !sender.isSelected
        
    }
    @IBAction func wednesdayButtonTapped(_ sender: UIButton) {
        
        let wedString = "Wednesday"
        
        if wednesdayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(wedString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(wedString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: wedString)!)
                
            }
        }
        sender.isSelected = !sender.isSelected
    }
    @IBAction func thursdayButtonTapped(_ sender: UIButton) {
        
        let thurString = "Thursday"
        
        if thurdayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(thurString)
        }
        else
        {
            buttonUnSelected(button: sender)
            
            if (weekarray.contains(thurString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: thurString)!)
                
            }
        }
        sender.isSelected = !sender.isSelected
    }
    @IBAction func fridayButtonTapped(_ sender: UIButton) {
        
        let friString = "Friday"
        
        if fridayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(friString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(friString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: friString)!)
                
            }
        }
        sender.isSelected = !sender.isSelected
    }
    @IBAction func saturdayButtonTapped(_ sender: UIButton) {
        
        let satString = "Saturday"
        
        if saturdayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(satString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(satString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: satString)!)
                
            }
        }
        sender.isSelected = !sender.isSelected
    }
    @IBAction func sundayButtonTapped(_ sender: UIButton) {
        
        let sunString = "Sunday"
        
        if sundayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(sunString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(sunString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: sunString)!)
                
            }
        }
        sender.isSelected = !sender.isSelected
    }
    
    func buttonSelected(button:UIButton)
    {
        button.layer.cornerRadius = button.frame.height/2
        button.clipsToBounds = true
        button.layer.borderWidth = 0.5
        button.layer.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
        button.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
        button.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .selected)
    }
    func buttonUnSelected(button:UIButton)
    {
        button.layer.cornerRadius = button.frame.height/2
        button.clipsToBounds = true
        button.layer.borderWidth = 0.5
        button.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        button.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
    }
    
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        
        
        
        if typeTextField.text == "Type"
        {
            
            showAlert(title: "", msg: "Please select type")
            return
        }
        
        
        
        
        
        
        if typeTextField.text == "Alarm"
        {
            
            if timeString == nil
            {
                showAlert(title: "", msg: "Please select time")
                return
            }
            
            if weekarray.isEmpty
            {
                showAlert(title: "", msg: "Please select atleast one day")
                return
            }
            if titleTextField.text!.isEmpty
            {
                showAlert(title: "", msg: "Title textfield should not be empty")
                return
            }
            
            
            
            
            let parameter:NSMutableDictionary = NSMutableDictionary()
            //          parameter.setValue(typeTextField.text, forKey: "type")
            parameter.setValue(repeatornot, forKey: "repeatOrNot")
            parameter.setValue(weekarray.joined(separator: ", "), forKey: "days")
            parameter.setValue(titleTextField.text, forKey: "title")
            parameter.setValue(descriptionTextField.text, forKey: "description")
            parameter.setValue(timeString, forKey: "time")
            parameter.setValue(userId, forKey: "userId")
            parameter.setValue(memberId, forKey: "memberId")
            
            print(parameter)
            
            print(API.saveAlarm)
            
            
            APIServices.postDataToServer(url: API.saveAlarm, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
                if status == true{
                    
                    print(response)
                    //      let alertController = UIAlertController(title: Constants.alertTitle, message: "Alarm added successfully", preferredStyle: .alert)
                    
                    
                    let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                    
                    let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
                    let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
                    
                    let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
                    let msgAttrString = NSMutableAttributedString(string: "\(String(describing: self.titleTextField.text!)) Alarm added successfully", attributes: msgFont)
                    
                    alert.setValue(titAttrString, forKey: "attributedTitle")
                    alert.setValue(msgAttrString, forKey: "attributedMessage")
                    
                    
                    
                    //      "Do you want to remove \(String(describing: (getMemberDetailsbyId?.alarmsInfo?[sender.tag].title!)!)) Alarm"
                    
                    
                    let settingsAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                        if #available(iOS 10.0, *) {
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            // Fallback on earlier versions
                        }
                    }
                    
                    
                    alert.addAction(settingsAction)
                    self.present(alert, animated: true, completion: nil)
                    print(response)
                    
                }
            })
        }
        else
        {
            if (displayDateLabel.text?.isEmpty)!
            {
                showAlert(title: "", msg: "Please select Date")
                return
            }
            if timeString == nil
            {
                showAlert(title: "", msg: "Please select time")
                return
            }
            if titleTextField.text!.isEmpty
            {
                showAlert(title: "", msg: "Please enter title")
                return
            }
            
            
            let parameter:NSMutableDictionary = NSMutableDictionary()
            // parameter.setValue(typeTextField.text, forKey: "type")
            parameter.setValue(dateString, forKey: "reminderDate")
            parameter.setValue(titleTextField.text, forKey: "title")
            parameter.setValue(descriptionTextField.text, forKey: "description")
            parameter.setValue(timeString, forKey: "time")
            parameter.setValue(userId, forKey: "userId")
            parameter.setValue(memberId, forKey: "memberId")
            
            APIServices.postDataToServer(url: API.saveReminder, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
                
                
                
                
                if status == true{
                    
                    
                    //     let alertController = UIAlertController(title: Constants.alertTitle, message: "Reminder added successfully", preferredStyle: .alert)
                    
                    
                    let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                    
                    let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
                    let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
                    
                    let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
                    let msgAttrString = NSMutableAttributedString(string: " \(String(describing: self.titleTextField.text!)) Reminder added successfully", attributes: msgFont)
                    
                    alert.setValue(titAttrString, forKey: "attributedTitle")
                    alert.setValue(msgAttrString, forKey: "attributedMessage")
                    
                    
                    let settingsAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                        if #available(iOS 10.0, *) {
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            // Fallback on earlier versions
                        }
                    }
                    
                    
                    alert.addAction(settingsAction)
                    self.present(alert, animated: true, completion: nil)
                    print(response)
                    
                    
                    print(response)
                    
                }
            })
        }
        
        
        
    }
    
    
    
    @IBAction func timePickerTapped(_ sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "HH:mm:ss"
        let timeData = dateFormatter.string(from: sender.date)
        timeString = timeData
        
        
        print(timeString!)
    }
    
    
}

extension UITextField {
    
    //    func useUnderline() {
    //        let border = CALayer()
    //        let borderWidth = CGFloat(1.0)
    //        border.borderColor = UIColor.lightGray.cgColor
    //        border.frame = CGRect(origin: CGPoint(x: 0,y :self.frame.size.height - borderWidth), size: CGSize(width: self.frame.size.width * 2, height: self.frame.size.height))
    //        border.borderWidth = borderWidth
    //        self.layer.addSublayer(border)
    //        self.layer.masksToBounds = true
    //    }
}
