//
//  ContactViewController.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 25/09/19.
//  Copyright © 2019 Allvy. All rights reserved.
//


import UIKit
import Contacts
import ContactsUI


class ContactViewController: UIViewController,CNContactViewControllerDelegate,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate{
    @IBOutlet var searchBar: UISearchBar!
    var searchActive : Bool = false
    var delegate: customMobileNumberDelegate?
    
    @IBOutlet var tableView: UITableView!
    var contacts1 = [CNContact]()
    
    let cellID = "cellID"
    
    var contacts = [Contact]()
    var contactsWithSections = [[Contact]]()
    
    var contacts12 = [Contact]()
    
    var countriesInfo = [[Contact]]()
    
    
    
    
    
    let collation = UILocalizedIndexedCollation.current() // create a locale collation object, by which we can get section index titles of current locale. (locale = local contry/language)
    var sectionTitles = [String]()
    
    
    var sectionTitles1 = [String]()
    
    
    private func fetchContacts(){
        
        let store = CNContactStore()
        
        store.requestAccess(for: (.contacts)) { (granted, err) in
            if let err = err{
                print("Failed to request access",err)
                return
            }
            
            if granted {
                print("Access granted")
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
                let fetchRequest = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                fetchRequest.sortOrder = CNContactSortOrder.userDefault
                
                
                
                do {
                    try store.enumerateContacts(with: fetchRequest, usingBlock: { ( contact, error) -> Void in
                        
                        guard let phoneNumber = contact.phoneNumbers.first?.value.stringValue else {return}
                        self.contacts.append(Contact(givenName: contact.givenName, familyName: contact.familyName, mobile: phoneNumber))
                        
                        self.contacts.orderedSet
                    })
                    
                    for index in self.contacts.indices{
                        
                        print(self.contacts[index].givenName)
                        print(self.contacts[index].familyName)
                        print(self.contacts[index].mobile)
                    }
                    
                    self.setUpCollation()
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
                
                
            }else{
                print("Access denied")
            }
        }
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = false
        //   callsTableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        
        searchBar.text = nil
        searchBar.resignFirstResponder()
        tableView.reloadData()
        
        self.searchBar.showsCancelButton = false
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        if searchText == "" || searchText.isEmpty
        {
            self.searchActive = false
        }else
        {
            self.searchActive = true;
            self.searchBar.showsCancelButton = true
            
            contacts12 = contacts.filter{ $0.familyName.localizedStandardContains(searchText) || $0.mobile.localizedStandardContains(searchText) || $0.givenName.localizedStandardContains(searchText)}
            setUpCollation()
            
            print(countriesInfo.count)
            
            //            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
            //            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
            //
            //            countriesInfo = organizationdetails?.countriesInfo?.filter{ $0.countryName!.localizedStandardContains(searchText) || $0.countryCode!.localizedStandardContains(searchText) }
            //
            //        }
            
        }
        //
        
        tableView.reloadData()
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Contacts"
        
        searchBar.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        fetchContacts()
        
    }
    @objc func setUpCollation(){
        
        if(searchActive){
            
            let (arrayContacts, arrayTitles) = collation.partitionObjects(array: self.contacts12, collationStringSelector: #selector(getter: Contact.givenName))
            self.countriesInfo = arrayContacts as! [[Contact]]
            self.sectionTitles1 = arrayTitles
            
            print(countriesInfo.count)
            print(sectionTitles1.count)
            
        }else
        {
            let (arrayContacts, arrayTitles) = collation.partitionObjects(array: self.contacts, collationStringSelector: #selector(getter: Contact.givenName))
            self.contactsWithSections = arrayContacts as! [[Contact]]
            self.sectionTitles = arrayTitles
            
            print(contactsWithSections.count)
            print(sectionTitles.count)
        }
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if(searchActive){
            print(sectionTitles1.count)
            return sectionTitles1.count
            
        }
        return sectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive){
            print(countriesInfo.count)
            return countriesInfo[section].count
            
        }
        return contactsWithSections[section].count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //   let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath)
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "cellID")
        //        let cell = ContactsCell(style: .subtitle, reuseIdentifier: cellID)
        //
        //        cell.link = self // custom delegation
        
        if(searchActive)
        {
            let contact = countriesInfo[indexPath.section][indexPath.row]
            cell.selectionStyle = .default
            cell.textLabel?.text = contact.givenName + " " + contact.familyName
            cell.detailTextLabel?.text = contact.mobile
        }else
        {
            let contact = contactsWithSections[indexPath.section][indexPath.row]
            cell.selectionStyle = .default
            cell.textLabel?.text = contact.givenName + " " + contact.familyName
            cell.detailTextLabel?.text = contact.mobile
            
        }
        
        
        return cell
    }
    
    //  delegate
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(searchActive)
        {
            let contact = countriesInfo[indexPath.section][indexPath.row]
            delegate?.didSelectMobileNumber(contact.mobile)
            
            self.navigationController?.popViewController(animated: true)
        }else
        {
            let contact = contactsWithSections[indexPath.section][indexPath.row]
            print(contact.mobile)
            delegate?.didSelectMobileNumber(contact.mobile)
            
            self.navigationController?.popViewController(animated: true)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(searchActive)
        {
            return sectionTitles1[section]
        }
        return sectionTitles[section]
    }
    
    //Changing color for the Letters in the section titles
    func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
        if let headerTitle = view as? UITableViewHeaderFooterView {
            headerTitle.textLabel?.textColor = UIColor.red
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if(searchActive)
        {
            return sectionTitles1
        }
        return sectionTitles
    }
    
    func getContacts(){
        
        let contactStore = CNContactStore()
        let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactImageDataAvailableKey, CNContactThumbnailImageDataKey]
        let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
        request.sortOrder = CNContactSortOrder.givenName
        
        do {
            try contactStore.enumerateContacts(with: request) {
                (contact, stop) in
                self.contacts1.append(contact)
            }
        }
        catch {
            print("unable to fetch contacts")
        }
        
    }
    
    func getNameFromContacts(number: String) -> String {
        var contactFetched : CNContact
        var contactName = ""
        if contacts1.count > 0 {
            
            let numberToBeCompared = number.components(separatedBy:CharacterSet.decimalDigits.inverted).joined(separator: "")
            for c in contacts1 {
                for n in c.phoneNumbers {
                    if let numberRetrived = n.value as? CNPhoneNumber {
                        let numberRetrivedFixed = numberRetrived.stringValue.components(separatedBy:CharacterSet.decimalDigits.inverted).joined(separator: "")
                        if numberRetrivedFixed.elementsEqual(numberToBeCompared){
                            contactName = c.givenName
                            // OR get the contact --> c
                            contactFetched = c
                            
                        }
                    }
                }
            }
            
            return contactName
            
        } else {
            return ""
        }
    }
    
    func getContactFromCNContact() -> [CNContact] {
        
        let contactStore = CNContactStore()
        let keysToFetch = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName),CNContactPhoneNumbersKey,CNContactGivenNameKey,CNContactEmailAddressesKey,CNContactFamilyNameKey,CNContactEmailAddressesKey, CNContactPhoneNumbersKey, CNContactImageDataAvailableKey, CNContactThumbnailImageDataKey] as [Any]
        
        //    let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactImageDataAvailableKey, CNContactThumbnailImageDataKey]
        
        //Get all the containers
        
        
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results: [CNContact] = []
        
        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
                
            } catch {
                print("Error fetching results for container")
            }
        }
        
        return results
    }
    
}

@objc class Contact : NSObject {
    @objc var givenName: String!
    @objc var familyName: String!
    @objc var mobile: String!
    
    init(givenName: String, familyName: String, mobile: String) {
        self.givenName = givenName
        self.familyName = familyName
        self.mobile = mobile
    }
}


extension UILocalizedIndexedCollation {
    //func for partition array in sections
    func partitionObjects(array:[AnyObject], collationStringSelector:Selector) -> ([AnyObject], [String]) {
        var unsortedSections = [[AnyObject]]()
        
        //1. Create a array to hold the data for each section
        for _ in self.sectionTitles {
            unsortedSections.append([]) //appending an empty array
        }
        //2. Put each objects into a section
        for item in array {
            let index:Int = self.section(for: item, collationStringSelector:collationStringSelector)
            unsortedSections[index].append(item)
        }
        //3. sorting the array of each sections
        var sectionTitles = [String]()
        var sections = [AnyObject]()
        for index in 0 ..< unsortedSections.count { if unsortedSections[index].count > 0 {
            sectionTitles.append(self.sectionTitles[index])
            sections.append(self.sortedArray(from: unsortedSections[index], collationStringSelector: collationStringSelector) as AnyObject)
            }
        }
        return (sections, sectionTitles)
    }
}
extension RangeReplaceableCollection where Element: Hashable {
    var orderedSet: Self {
        var set = Set<Element>()
        return filter { set.insert($0).inserted }
    }
    mutating func removeDuplicates() {
        var set = Set<Element>()
        removeAll { !set.insert($0).inserted }
    }
}

