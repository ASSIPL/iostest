//
//  PageViewController.swift
//  PageControl
//
//  Created by Andrew Seeley on 2/2/17.
//  Copyright © 2017 Seemu. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    var index:Int?
    var userPlans:UserPlans?
  
     var responsePlans:[ResponsePlans]?
    
    var pageControl = UIPageControl()
    
    // MARK: UIPageViewControllerDataSource
    
    var orderedViewControllers: [UIViewController]?
        
        //= {
        
        
        
        
      //  return [self.newVc(viewController: "FeatureWiseVC"),
            //    self.newVc(viewController: "MonthlyPlanVC"),self.newVc(viewController: "BasePlusVC")]
  //  }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        
        self.dataSource = self
        self.delegate = self
        
           self.title = "Coolpad FamilyLabs"
        
        let imageDataDict:[String: Any] = [
            "notificationId":index ?? 0]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeTap"), object: nil,userInfo: imageDataDict)
//        for each in SharedData.data.responsePlans!
//        {
//            orderedViewControllers?.append(self.newVc(viewController: "FeatureWiseVC"))
//        }
//
    orderedViewControllers = {

//            if "FeatureWiseVC" == "FeatureWiseVC"
//            {
//
//                return [self.newVc(viewController: "FeatureWiseVC")
//,self.newVc(viewController: "BasePlusVC")]
//        }
        
          var orderedViewControllers1: [UIViewController]? = []
//
        for each in SharedData.data.responsePlans!
        {
            orderedViewControllers1?.append(self.newVc(viewController: "FeatureWiseVC"))
            
           // orderedViewControllers1?.append(self.newVc(viewController: "FeatureWiseVC"))
        }


    //return orderedViewControllers

        return orderedViewControllers1
        
        }()
        
        // This sets up the first view that will show up on our page control
        if let firstViewController = orderedViewControllers?[index ?? 0] {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
        
        configurePageControl()
        
        // Do any additional setup after loading the view.
    }

    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        
        self.view.addSubview(pageControl)
        // The total number of pages that are available is based on how many available colors we have.
        //  pageControl = UIPageControl(frame: CGRect(x: 0,y: UIScreen.main.bounds.maxY - 80,width: UIScreen.main.bounds.width,height: 90))
        
        
        pageControl.isUserInteractionEnabled = false
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        
        pageControl.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        pageControl.widthAnchor.constraint(equalToConstant: self.view.frame.width - 20).isActive = true
        pageControl.heightAnchor.constraint(equalToConstant: 50).isActive = true
        pageControl.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -20).isActive = true
        

      //  pageControl = UIPageControl(frame: CGRect(x: 0,y: UIScreen.main.bounds.maxY - 60,width: UIScreen.main.bounds.width,height: 50))
        
        self.pageControl.numberOfPages = orderedViewControllers?.count ?? 0
        self.pageControl.currentPage = index ?? 0
        self.pageControl.tintColor = #colorLiteral(red: 0, green: 0.7152789235, blue: 0.7682691216, alpha: 1)
        self.pageControl.pageIndicatorTintColor = #colorLiteral(red: 0.5725490196, green: 0.8470588235, blue: 0.8666666667, alpha: 1)
        self.pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0, green: 0.7152789235, blue: 0.7682691216, alpha: 1)
        pageControl.transform = CGAffineTransform(scaleX: 3, y: 3)
    //    self.view.addSubview(pageControl)
    }
    
    func newVc(viewController: String) -> UIViewController {
        
        let vc =  (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: viewController) as? FeatureWiseVC)!
        vc.planeIndex = index
         vc.userPlans = userPlans
    
        return vc
    }
    
    
    // MARK: Delegate methords
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = orderedViewControllers?.firstIndex(of: pageContentViewController)! ?? 0
        
        print(self.pageControl.currentPage)
        
                let imageDataDict:[String: Any] = [
                    "notificationId":self.pageControl.currentPage]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeTap"), object: nil,userInfo: imageDataDict)
    }
    
    // MARK: Data source functions.
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers?.firstIndex(of: viewController) else {
            return nil
        }
        
        
       print(viewControllerIndex)
//        let imageDataDict:[String: Any] = [
//            "notificationId":viewControllerIndex]
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeTap"), object: nil,userInfo: imageDataDict)
      
        let previousIndex = viewControllerIndex - 1
        
     
        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
          //  return orderedViewControllers?.last
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
             return nil
        }
        
        guard orderedViewControllers?.count ?? 0 > previousIndex else {
            return nil
        }
        
        return orderedViewControllers?[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers?.firstIndex(of: viewController) else {
            return nil
        }
        
        print(viewControllerIndex)
      
       let nextIndex = viewControllerIndex + 1
//
//        let imageDataDict:[String: Any] = [
//            "notificationId":nextIndex]
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeTap"), object: nil,userInfo: imageDataDict)
//
//
        let orderedViewControllersCount = orderedViewControllers?.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard orderedViewControllersCount != nextIndex else {
          //  return orderedViewControllers?.first
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
             return nil
        }
        
        guard orderedViewControllersCount ?? 0 > nextIndex else {
            return nil
        }
        
        return orderedViewControllers?[nextIndex]
    }
    

}
