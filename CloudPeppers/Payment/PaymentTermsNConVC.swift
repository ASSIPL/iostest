//
//  TermsCondViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 22/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD
import  Firebase

class PaymentTermsNConVC: UIViewController,WKNavigationDelegate {
    
    var urlString:String?
    var fromaboutUs:String?
    
    @IBOutlet var webView: WKWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if urlString != nil
        {
            let  url = URL(string: urlString!)
            
            webView.navigationDelegate = self
            webView.load(URLRequest(url: url!))
        }else
        {
            let url = URL(string: API.paymentTemrsNCon)
            
            webView.navigationDelegate = self
            webView.load(URLRequest(url: url!))
        }
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        Analytics.logEvent("DailyTimeLimitsViewEntered", parameters:nil)
        //   self.navigationController?.view.tintColor = UIColor.black
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("DailyTimeLimitsViewExit", parameters:nil)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        // hide/stop indicator
        SVProgressHUD.dismiss()
        
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        //show your activity
        // SVProgressHUD.show()
        
        if Reachability.isConnectedToNetwork(){
            SVProgressHUD.show()
        }else{
            self.showAlert(title: "", msg: "No network connection")
        }
    }
    
    
}
