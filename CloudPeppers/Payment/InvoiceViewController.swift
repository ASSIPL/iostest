//
//  InvoiceViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 05/09/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class InvoiceViewController: UIViewController {

    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var planPriceLbl: UILabel!
    
    @IBOutlet weak var totalPriceLbl: UILabel!
    @IBOutlet weak var itemPriceLbl: UILabel!
    
    @IBOutlet weak var planExpireAlertLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var transactionIDLbl: UILabel!
    @IBOutlet weak var planNameLbl: UILabel!
    @IBOutlet weak var personNameLabel: UILabel!
    
    
    @IBOutlet weak var detailPlanNameLbl: UILabel!
    
    var responsePayment:ResponsePayment?
    override func viewDidLoad() {
        super.viewDidLoad()
        

        continueButton.changeButtonCornerRadius()

        // Do any additional setup after loading the view.
        
        planNameLbl.text = responsePayment?.userPlans?.planName
        transactionIDLbl.text = responsePayment?.orderId
        dateLbl.text = String(describing:(responsePayment?.createdDate!))
        planExpireAlertLbl.text = "Your plan expires in" + " " + String(describing:(responsePayment?.userPlans?.remainDays!)!)  + " " + "days"
        print(responsePayment?.userPlans?.fullPrice)
        
        var strswift = (responsePayment?.userPlans?.fullPrice)!
        var double : Double = NSString(string: strswift).doubleValue
        print(double)
        
        let text = String(format: "%.2f", arguments: [double])
        
        
        planPriceLbl.text = "$" + "" + text
        itemPriceLbl.text = "$" + "" + text
        totalPriceLbl.text = "$" + "" + text
        detailPlanNameLbl.text = responsePayment?.userPlans?.planName
        
        let unixTimeStamp: Double = Double((responsePayment?.createdDate!)!) / 1000.0
        
     //   cell.dateLabel.text = getDateFromTimeStamp(timeStamp : unixTimeStamp)
       
        dateLbl.text = getDateFromTimeStamp(timeStamp : unixTimeStamp)
        
        if let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) {
        
            personNameLabel.text =  "\(profileData.firstName!)" + " " + "\(profileData.lastName!)"
        
        }
        
    
        
        //Screenshot
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()! )
        var sourceImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIImageWriteToSavedPhotosAlbum(sourceImage!,nil,nil,nil)
        
        
    }
  

    
    func getDateFromTimeStamp(timeStamp : Double) -> String {
        
        let date = NSDate(timeIntervalSince1970: timeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        //  dayTimePeriodFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        dayTimePeriodFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        
        //    "yyyy-MM-dd HH:mm:ss"
        
        // UnComment below to get only time
        //  dayTimePeriodFormatter.dateFormat = "hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
    
  
    @IBAction func continueButtonTapped(_ sender: Any) {
        
        let revealViewController:SWRevealViewController = self.revealViewController()
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let newFrontViewController = UINavigationController.init(rootViewController:desController)
        revealViewController.pushFrontViewController(newFrontViewController, animated: true)
    }
    
}

