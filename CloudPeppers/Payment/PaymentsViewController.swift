//
//  PaymentsViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 25/04/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import SVProgressHUD
import Firebase

class PaymentsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var changePlanButton: UIButton!
    @IBOutlet weak var renewButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet var planeLabel: UILabel!
    
    @IBOutlet weak var paymentTableView: UITableView!
    
    @IBOutlet weak var btnMenuButton: UIBarButtonItem!
    
    
    var numberingArray:Array = [String]()
    var plansArray:Array = [String]()
    var plansDescArray:Array = [String]()
    var priceArray:Array = [String]()
    var priceDescArray:Array = [String]()
    var arrowImage:Array = [UIImage]()
    
    var userPlans:UserPlans?
    
    override func viewDidLoad() {
        super.viewDidLoad()
           // UserDefaults.standard.removeObject(forKey: "Paymentdata")
        renewButton.isHidden = true
        changePlanButton.isHidden = true
        
        renewButton.changeButtonCornerRadius()
        changePlanButton.changeButtonCornerRadius()
        
        if  SharedData.data.responsePlans?.count ?? 0 > 0
       {
        for each in  SharedData.data.responsePlans ?? []
        {
            //   arrowImage = [#imageLiteral(resourceName: "arrowmarkpay"),#imageLiteral(resourceName: "arrowmarkpay"),#imageLiteral(resourceName: "arrowmarkpay")]
            self.arrowImage.append(#imageLiteral(resourceName: "arrowmarkpay"))
        }
        
        }
        
        
//        
//        UIGraphicsBeginImageContext(bottomView.frame.size)
//        bottomView.layer.render(in: UIGraphicsGetCurrentContext()! )
//        var sourceImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        UIImageWriteToSavedPhotosAlbum(sourceImage!,nil,nil,nil)
//        
        
        
      if let data = UserDefaults.standard.value(forKey: "Paymentdata") as? Data,
        let profileData = try? JSONDecoder().decode(UserPlans.self, from: data)
        {
            
            print(userPlans)
          
            userPlans = profileData
            
            if userPlans?.remainDays ?? 0 <= 0
            {
                
            }else
            {
               
                planeLabel.text = "Your plan will renew in \(String(describing: (userPlans?.remainDays)!)) days"
                
                if userPlans?.remainDays ?? 0 <= 5
                {
                    renewButton.isHidden = false
                    changePlanButton.isHidden = false
                }
            }
            
            
            
            
            print(userPlans?.planKey)
            
          //  return
            
        }
        
        
      
        SVProgressHUD.show()
       
        HttpWrapper.gets(with: API.getAllPlans , parameters: nil, headers: nil, completionHandler: { (response, error)  in
            print(response)
            
             SVProgressHUD.dismiss()
            
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatusPlans.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    
                    
                SharedData.data.responsePlans = loginRespone.response
                    print(SharedData.data.responsePlans)
                    
                    self.arrowImage.removeAll()
                    if  SharedData.data.responsePlans?.count ?? 0 > 0
                    {
                        for each in  SharedData.data.responsePlans!
                        {
                            //   arrowImage = [#imageLiteral(resourceName: "arrowmarkpay"),#imageLiteral(resourceName: "arrowmarkpay"),#imageLiteral(resourceName: "arrowmarkpay")]
                            self.arrowImage.append(#imageLiteral(resourceName: "arrowmarkpay"))
                        }
                        
                    }
                   
                    self.paymentTableView.reloadData()
                    // self.familycollectionView.reloadData()
                }
                else
                    
                {
                    
                }
                
            }catch let jsonErr {
                  SVProgressHUD.dismiss()
                
                print("Error serializing json:", jsonErr)
                
            }
        }){ (error) in
              SVProgressHUD.dismiss()
            print(error)
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
        
        
        
        
        

        numberingArray = ["1","2","3"]
        plansArray = ["Feature wise monthly plan","Monthly plan","Base Plus Subscription"]
        plansDescArray = ["Choose features which you want?","You can access fetures for a month","Buy below 5 features $1.50 as base plan"]
        priceArray = ["$0.05","$10.99","$1.50"]
        priceDescArray = ["per feature","",""]
       // arrowImage = [#imageLiteral(resourceName: "arrowmarkpay"),#imageLiteral(resourceName: "arrowmarkpay"),#imageLiteral(resourceName: "arrowmarkpay")]
            
        navigationController?.navigationBar.barTintColor = Constants.colour
        self.navigationItem.title = "Payments"
        self.changeFontForViewController()
    
        btnMenuButton.target = revealViewController()
        btnMenuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        
        self.bottomView.layer.cornerRadius = 10
        self.bottomView.clipsToBounds = true
        
        // Do any additional setup after loading the view.
    }
    


//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let numOfSection: NSInteger =   SharedData.data.responsePlans?.count ?? 0
        
        if numOfSection == 0
        {
            
            let noDataLabel: UILabel = UILabel(frame:CGRect(x: 0, y: 0, width: self.paymentTableView.frame.width, height: self.paymentTableView.frame.height))
            noDataLabel.backgroundColor = .white
            noDataLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            paymentTableView.separatorStyle = .none
            noDataLabel.text = "No plans found"
            noDataLabel.textAlignment = NSTextAlignment.center
            self.paymentTableView.backgroundView = noDataLabel
            return 1
        }
        else
        {
            
            self.paymentTableView.backgroundView = nil
            
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  SharedData.data.responsePlans?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "payment") as! PaymentMainTableViewCell
        
        
        cell.serialNumberingLab.text = String(describing: indexPath.row + 1)
        
        
            
            
           // sti[indexPath.row]
        cell.upperLabel.text = SharedData.data.responsePlans?[indexPath.row].planName
        cell.lowerLabel.text = SharedData.data.responsePlans?[indexPath.row].planDescription1
        
        
        if SharedData.data.responsePlans?[indexPath.row].planKey == "key.featurewiseplan"
        {
            cell.priceLabel.text = "$" + (SharedData.data.responsePlans?[indexPath.row].priceForFeature)!
            cell.priceFeatureLabel.text = "per feature"
        }
        
        if SharedData.data.responsePlans?[indexPath.row].planKey == "key.monthly"
        {
            cell.priceLabel.text = "$" + (SharedData.data.responsePlans?[indexPath.row].fullPrice)!
             cell.priceFeatureLabel.text = ""
          //  cell.priceFeatureLabel.text = priceDescArray[indexPath.row]
        }
        
        if SharedData.data.responsePlans?[indexPath.row].planKey == "key.baseplus"
        {
            let pi = (SharedData.data.responsePlans?[indexPath.row].basePlanOfferPrice)!
            
            let text = String(format: "%.2f", arguments: [pi])
            
            cell.priceLabel.text = "$" + text
            
             cell.priceFeatureLabel.text = ""
            
          
            //cell.priceFeatureLabel.text = priceDescArray[indexPath.row]
        }
        
        
        if let data = UserDefaults.standard.value(forKey: "Paymentdata") as? Data,
            let profileData = try? JSONDecoder().decode(UserPlans.self, from: data)
        {
            
            
            
            if profileData.remainDays ?? 0 <= 0
            {
                cell.tickMark.isHidden = true
                cell.priceLabel.isHidden = false
                cell.priceFeatureLabel.isHidden = false
            }else
            {
                
                
                print(profileData.planKey)
                if SharedData.data.responsePlans?[indexPath.row].planKey == profileData.planKey &&  SharedData.data.responsePlans?[indexPath.row].planName == profileData.planName
                {
                    // cell.backView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                    
                    cell.tickMark.isHidden = false
                    cell.priceLabel.isHidden = true
                    cell.priceFeatureLabel.isHidden = true
                    //   cell.tickMark.ish
                }else
                {
                    cell.tickMark.isHidden = true
                    cell.priceLabel.isHidden = false
                    cell.priceFeatureLabel.isHidden = false
                }
                
            }
            
            
            
            //  return
            
        }else
        {
            cell.tickMark.isHidden = true
            cell.priceLabel.isHidden = false
            cell.priceFeatureLabel.isHidden = false
        }
        
        
        
        
        
        
      
        
        
      
        cell.arrowImage.image = arrowImage[indexPath.row]
        
       // cell.backView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Analytics.logEvent("PlanArrow_Screen_Enter", parameters:nil)
        
        if userPlans?.remainDays ?? 0 <= 0
        {
            
        }else
            
            
            
        {
            
            if let data = UserDefaults.standard.value(forKey: "Paymentdata") as? Data,
                let profileData = try? JSONDecoder().decode(UserPlans.self, from: data)
            {
                if SharedData.data.responsePlans?[indexPath.row].planKey == userPlans?.planKey &&  SharedData.data.responsePlans?[indexPath.row].planName == profileData.planName
                {
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MyPlanViewController") as? MyPlanViewController
                    vc?.userPlans = userPlans
                    
                    self.navigationController?.pushViewController(vc!, animated: true)
                    return
                }
                
                
            }
            
            
        }
        
        
//        if SharedData.data.responsePlans?[indexPath.row].planKey == "key.monthly"
//        {
//
//        }
//        if SharedData.data.responsePlans?[indexPath.row].planKey == "key.baseplus"
//        {
//
//        }
//
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PageViewController") as? PageViewController
        vc?.userPlans = userPlans
        vc?.index = indexPath.row
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    override func viewWillAppear(_ animated: Bool) {
        Analytics.logEvent("Payment_Screen_Enter", parameters:nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("Payment_Screen_Exit", parameters:nil)
    }
    
    @IBAction func changePlanButton(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PageViewController") as? PageViewController
        vc?.userPlans = userPlans
        vc?.index =  0
        self.navigationController?.pushViewController(vc!, animated: true)
        
        
    }
    
    @IBAction func renewButtonTapped(_ sender: Any) {
        
        
    //    userPlans?.planName
        
        
      let filterdata =  SharedData.data.responsePlans?.filter{$0.planName == userPlans?.planName}
        
        
        if filterdata?.count ?? 0 > 0
        {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MyPlanViewController") as? MyPlanViewController
            vc?.userPlans = userPlans
            
            self.navigationController?.pushViewController(vc!, animated: true)
        }else
        {
            self.showAlert(title: "", msg: "You cannot renew this plan as it's deactivated by Coolpad FamilyLabs admin")
        }
        
        
        
        
      
//        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CardInfoViewController") as? CardInfoViewController
//        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
}

