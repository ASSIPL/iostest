//
//  BasePlusVC.swift
//  CloudPeppers
//
//  Created by Allvy on 20/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class BasePlusVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var basePlusBGView: UIView!
    
    @IBOutlet weak var basePlusLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var upperTableView: UITableView!
    
    @IBOutlet weak var lowerTableView: UITableView!
    
    var featureNameArray:Array = [String]()
    var featureNameArray1:Array = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        basePlusBGView.dropShadow()
        
        basePlusLabel.layer.cornerRadius = 20
        basePlusLabel.clipsToBounds = true
        basePlusLabel.layer.borderColor = #colorLiteral(red: 0, green: 0.7152789235, blue: 0.7682691216, alpha: 1)
        basePlusLabel.layer.borderWidth = 1.5
        submitButton.changeButtonCornerRadius()
        
        
featureNameArray = ["Add Member","Daily Time Limits","Restricted Times","Apps&Games","Calls & SMS"]
        
        featureNameArray1 = ["Add Member","Daily Time Limits","Restricted Times","Apps&Games","Calls & SMS","Alarms & Reminders","Set Geofence"]
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == upperTableView
        {
            return featureNameArray.count
        }
        else
        {
            return featureNameArray1.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == upperTableView
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BasePlusUpperTableViewCell") as! BasePlusUpperTableViewCell
            cell.featureNameLabel.text = featureNameArray[indexPath.row]
            cell.infoButton.addTarget(self, action: #selector(infoButtonTapped), for: .touchUpInside)
            
            
            return cell
        }
        else
        {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "BasePlusLowerTableViewCell") as! BasePlusLowerTableViewCell
            cell1.checkboxButton.addTarget(self, action: #selector(checkButtonTapped), for: .touchUpInside)
            cell1.featureLabel.text = featureNameArray1[indexPath.row]
            cell1.infoButton.addTarget(self, action: #selector(infoButtonTapped1), for: .touchUpInside)
            
            return cell1
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == upperTableView
        {
           return 55
        }
        else
        {
            return 55
        }
    }

    @IBAction func infoButtonTapped(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        
        
        
    }
    
    @IBAction func checkButtonTapped(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        
        
        
    }
    @IBAction func infoButtonTapped1(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
 
    }
    @IBAction func submitButtonTapped(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CardInfoViewController") as? CardInfoViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
}
