//
//  ReceiptViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 21/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase


class ReceiptViewController: UIViewController {

    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var displayViewForScreenshot: UIView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var orderIDLabel: UILabel!
    @IBOutlet weak var planNameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet var thanksMessageLabel: UILabel!
    var message:String?
    @IBOutlet var price: UILabel!
    
    @IBOutlet var month: UILabel!
    
    @IBOutlet var planDis: UILabel!
    var responsePayment:ResponsePayment?
    @IBOutlet weak var lowerLabel: UILabel!
    @IBOutlet weak var downloadView: UIView!
  //  @IBOutlet weak var lowerView: UIView!
    @IBOutlet weak var upperView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Confirmation"
        
        doneButton.changeButtonCornerRadius()
        
        let logoutBarButtonItem = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(BackButton))
        
        
      //  let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "BackArrow"), style:.done, target: self, action: #selector(BackButton))
        
        
        
        self.navigationItem.leftBarButtonItem  = logoutBarButtonItem


        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
        self.downloadView.addGestureRecognizer(gesture)
        
        upperView.layer.cornerRadius = 5
        upperView.clipsToBounds = true
        
        lowerLabel.layer.cornerRadius = 5
        lowerLabel.clipsToBounds = true
        
        downloadView.layer.cornerRadius = 20
        downloadView.clipsToBounds = true
        
        
        thanksMessageLabel.text = message
        
      
        
        
      
        var strswift = (responsePayment?.userPlans?.fullPrice)!
        var double : Double = NSString(string: strswift).doubleValue
        
        let text = String(format: "%.2f", arguments: [double])
        
        print(text)
       
        

        
     
         // price.text = "$"  + String(describing:Double((responsePayment?.userPlans?.fullPrice)!)!)
        
         price.text = "$"  + text
        
     //   price.text = "0"
        
//        (String(describing: (reponsedata?.firstName!)!))
        
        
    //    price.text = "$" +   String(describing: Double((responsePayment?.userPlans?.fullPrice)!)/100)
            
            
            //Double((responsePayment?.userPlans?.fullPrice)!)/100)
            
            
            
          
        
        month.text = "/month"
        // Do any additional setup after loading the view.
        
        
        
        planNameLabel.text = responsePayment?.userPlans?.planName
        orderIDLabel.text = responsePayment?.orderId
        dateLabel.text = String(describing:(responsePayment?.createdDate!))

        print(responsePayment?.userPlans?.fullPrice)
        

        
        
     
        
        let unixTimeStamp: Double = Double((responsePayment?.createdDate!)!) / 1000.0
        
        //   cell.dateLabel.text = getDateFromTimeStamp(timeStamp : unixTimeStamp)
        
        dateLabel.text = getDateFromTimeStamp(timeStamp : unixTimeStamp)
        
        if let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) {
            
            nameLabel.text =  "\(profileData.firstName!)" + " " + "\(profileData.lastName!)"
            
        }
        
        // Local notification
    
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       Analytics.logEvent("Payment_Success_Screen_Enter", parameters:nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("Payment_Success_Screen_Exit", parameters:nil)
    }
    
    func getDateFromTimeStamp(timeStamp : Double) -> String {
        
        let date = NSDate(timeIntervalSince1970: timeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        //  dayTimePeriodFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        dayTimePeriodFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        
        //    "yyyy-MM-dd HH:mm:ss"
        
        // UnComment below to get only time
        //  dayTimePeriodFormatter.dateFormat = "hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
    
    

    
    @objc func BackButton(){
        
        let revealViewController:SWRevealViewController = self.revealViewController()
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let newFrontViewController = UINavigationController.init(rootViewController:desController)
        revealViewController.pushFrontViewController(newFrontViewController, animated: true)
       
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @objc func checkAction(sender : UITapGestureRecognizer) {
        
        Analytics.logEvent("Payment_Success_Download_Btn_Clicked", parameters:nil)
        
        //Screenshot
        UIGraphicsBeginImageContext(displayViewForScreenshot.frame.size)
        displayViewForScreenshot.layer.render(in: UIGraphicsGetCurrentContext()! )
        var sourceImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIImageWriteToSavedPhotosAlbum(sourceImage!,nil,nil,nil)
        
        
        
        
        
        
        localNotiAlert()
        
        
        
    }
    
    
    func localNotiAlert()
    {
        
      
        
        
        let content = UNMutableNotificationContent()
        
        
        
        //adding title, subtitle, body and badge
        
        content.title = ""
        
      //  content.subtitle = "iOS Development is fun"
        
        content.body = "Payment Receipt successfully saved in photos."
        
     //   content.badge = 1
        
        
        
        //getting the notification trigger
        
        //it will be called after 5 seconds
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        
        
        //getting the notification request
        
        let request = UNNotificationRequest(identifier: "SimplifiedIOSNotification", content: content, trigger: trigger)
        
        
        
        //adding the notification to notification center
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
        
        

        
        
//
//        let content = UNMutableNotificationContent()
//
//
//
//        //adding title, subtitle, body and badge
//
//        content.title = "Coolpad FamilyLabs"
//
//
//        content.body = "Payment receipt successfully saved in Gallery"
//
//        content.badge = 1
//
//
//
//        //getting the notification trigger
//
//        //it will be called after 5 seconds
//
//        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
//
//
//
//        //getting the notification request
//
//        let request = UNNotificationRequest(identifier: "SimplifiedIOSNotification", content: content, trigger: trigger)
//
//
//
//        NSNotificationCenter.current().delegate = self
//
//
//        //adding the notification to notification center
//
//        NSNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        Analytics.logEvent("Payment_Success_Done_Btn_Clicked", parameters:nil)
        
        let revealViewController:SWRevealViewController = self.revealViewController()
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let newFrontViewController = UINavigationController.init(rootViewController:desController)
        revealViewController.pushFrontViewController(newFrontViewController, animated: true)
        
    }
    
}
