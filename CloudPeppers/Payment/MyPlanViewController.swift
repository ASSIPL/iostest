//
//  MyPlanViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 26/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase

class MyPlanViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var renewButton: UIButton!
    @IBOutlet weak var planFeatureDesc: UILabel!
    @IBOutlet weak var myPlanHeaderLabel: UILabel!
    
    @IBOutlet weak var planNameLabel: UILabel!
    @IBOutlet weak var myPlanTV: UITableView!
    @IBOutlet weak var planPriceLabel: UILabel!
    @IBOutlet weak var daysLabel: UILabel!
     var userPlans:UserPlans?
    var fromRenew:String?
    @IBOutlet weak var myPlanBGView: UIView!
    
    var featureNameArray:Array = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        renewButton.isHidden = true
        renewButton.changeButtonCornerRadius()
        
        
        myPlanBGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        myPlanBGView.layer.shadowColor = UIColor.gray.cgColor
        myPlanBGView.layer.shadowOpacity = 1
        myPlanBGView.layer.shadowOffset = CGSize.zero
        myPlanBGView.layer.shadowRadius = 5
        myPlanBGView.layer.cornerRadius = 10
        myPlanTV.tableFooterView = UIView()
        
        
      //  myPlanBGView.dropShadow()
        
        planNameLabel.text = userPlans?.planName
        
        if let data = UserDefaults.standard.value(forKey: "Paymentdata") as? Data,
            let profileData = try? JSONDecoder().decode(UserPlans.self, from: data)
        {
            
            print(profileData.fullPrice!)
            var strswift = profileData.fullPrice!
            var double : Double = NSString(string: strswift).doubleValue
            print(double)
            
              let text = String(format: "%.2f", arguments: [double])
            print(text)
   
            planPriceLabel.text = "$" + text
            daysLabel.text = String(describing: profileData.remainDays!) + " " + "days"
            
            print(userPlans?.remainDays)
            
            if userPlans?.remainDays ?? 0 <= 0
            {
                
            }else
            {
                
             
                
                if userPlans?.remainDays ?? 0 <= 5
                {
                    renewButton.isHidden = false
                  
                }
            }
            
        }
        

        
        planNameLabel.layer.cornerRadius = 20
        planNameLabel.clipsToBounds = true
        planNameLabel.layer.borderColor = #colorLiteral(red: 0, green: 0.7152789235, blue: 0.7682691216, alpha: 1)
        planNameLabel.layer.borderWidth = 1.5
        
        myPlanHeaderLabel.layer.cornerRadius = 20
        myPlanHeaderLabel.clipsToBounds = true
        myPlanHeaderLabel.layer.borderColor = #colorLiteral(red: 0, green: 0.7152789235, blue: 0.7682691216, alpha: 1)
        myPlanHeaderLabel.layer.borderWidth = 1.5
        
        print(userPlans?.planName)
        
        
        if userPlans?.planName == "Monthly Plan"
        {
            planFeatureDesc.text = "You can access all features for a month"
        }
        else{
            planFeatureDesc.text = "You can access selected features for a month"
            
        }
        
        
        featureNameArray = ["Add Member","Daily Time Limits","Restricted Times","Apps&Games","Calls & SMS","Alarms & Reminders","Set Geofence"]

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Analytics.logEvent("MyPlan_Screen_Enter", parameters:nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("MyPlan_Screen_Exit", parameters:nil)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userPlans?.featureList?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyPlanTableViewCell") as! MyPlanTableViewCell
        
            cell.infoButton.isHidden = false
        
        
         cell.infoButton.tag = indexPath.row
        cell.featureNameLabel.text = userPlans?.featureList?[indexPath.row].featureName
        cell.infoButton.addTarget(self, action: #selector(infoButtonTapped), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }

    @IBAction func infoButtonTapped(_ sender: UIButton) {
        
      //  sender.isSelected = !sender.isSelected
        
        
       //    self.showAlert(title: "", msg:userPlans?.featureList?[sender.tag].featureInfo ?? "")
        
        self.showCustomAlertWith(message: "", descMsg: userPlans?.featureList?[sender.tag].featureInfo ?? "", itemimage: #imageLiteral(resourceName: "AboutUs") , actions: nil)
    
        
    }
    
    @IBAction func renewButtonTapped(_ sender: Any) {
        
        
        let filterdata =  SharedData.data.responsePlans?.filter{$0.planName == userPlans?.planName}
      
        if filterdata?.count ?? 0 > 0
        {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CardInfoViewController") as? CardInfoViewController
            vc?.userPlans = userPlans
            vc?.planName = userPlans?.planName
            vc?.planKey = userPlans?.planKey
            var strswift = userPlans?.fullPrice!
            var double : Double = NSString(string: strswift!).doubleValue
            print(double)
            
            let text = String(format: "%.2f", arguments: [double])
            
            vc?.fullPrice = double
            vc?.reNew = "true"
            
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }else
        {
            self.showAlert(title: "", msg: "You cannot renew this plan as it's deactivated by Coolpad FamilyLabs admin")
        }
        
        
        
        
        
        
//        [11:20] Feroze Mohammed
//        You cannot renew this plan as it's deactivated by Coolpad FamilyLabs admin.

        
        
   
//        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CardInfoViewController") as? CardInfoViewController
//        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
}
