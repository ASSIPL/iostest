//
//  MonthlyPlanVC.swift
//  CloudPeppers
//
//  Created by Allvy on 20/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class MonthlyPlanVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var heightSubmit: NSLayoutConstraint!
    
    @IBOutlet weak var monthlyBGView: UIView!
    
    @IBOutlet weak var monthlyLabel: UILabel!
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var monthlyTV: UITableView!
    
    
    var featureNameArray:Array = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        monthlyBGView.dropShadow()
        
        monthlyLabel.layer.cornerRadius = 20
        monthlyLabel.clipsToBounds = true
        monthlyLabel.layer.borderColor = #colorLiteral(red: 0, green: 0.7152789235, blue: 0.7682691216, alpha: 1)
        monthlyLabel.layer.borderWidth = 1.5
        submitButton.changeButtonCornerRadius()
        
        featureNameArray = ["Add Member","Daily Time Limits","Restricted Times","Apps&Games","Calls & SMS","Alarms & Reminders","Set Geofence"]
     
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return featureNameArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MonthlyWiseTableViewCell") as! MonthlyWiseTableViewCell
        cell.featureLabel.text = featureNameArray[indexPath.row]
        cell.infoButton.addTarget(self, action: #selector(infoButtonTapped), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    @IBAction func infoButtonTapped(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        
        
        
    }


    @IBAction func submitButtonTapped(_ sender: Any) {
    }
    
    
}
