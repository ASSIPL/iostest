//
//  FeatureWiseVC.swift
//  CloudPeppers
//
//  Created by Allvy on 20/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase

class FeatureWiseVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UIPopoverPresentationControllerDelegate {
    var userPlans:UserPlans?
    @IBOutlet var topcontantBuylabel: NSLayoutConstraint!
    
    @IBOutlet weak var buyLabel: UILabel!
    
    @IBOutlet weak var totalAmountDisLabel: UILabel!
    @IBOutlet weak var minPriceLabel: UILabel!
    
    @IBOutlet weak var buyViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var buyView: UIView!
    
    @IBOutlet weak var minPriceHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var labelTableViewLinkHeight: NSLayoutConstraint!
    
    @IBOutlet var heightSubmitButton: NSLayoutConstraint!
    @IBOutlet weak var priceHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var priceDisplayLabel: UILabel!
    
    
    @IBOutlet weak var featureTableView: UITableView!
    @IBOutlet weak var featureBGView: UIView!
    @IBOutlet weak var featureHeadLineLabel: UILabel!
    
    @IBOutlet weak var submitButton: UIButton!
    var planeIndex:Int?
    
    var seletedarray = [customerPlansList]()
    
    var seletedarray1 = [customerPlansList]()
    
    var featureNameArray:Array = [String]()
    var priceArray:Array = [String]()
    var featureNameArray1:Array = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        totalAmountDisLabel.text = ""
        
        //   buyLabel.text = "Buy($0.00)"
        
        featureBGView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        featureBGView.layer.shadowColor = UIColor.gray.cgColor
        featureBGView.layer.shadowOpacity = 1
        featureBGView.layer.shadowOffset = CGSize.zero
        featureBGView.layer.shadowRadius = 5
        featureBGView.layer.cornerRadius = 10
        
        //featureBGView.dropShadow()
        
        featureHeadLineLabel.layer.cornerRadius = 20
        featureHeadLineLabel.clipsToBounds = true
        featureHeadLineLabel.layer.borderColor = #colorLiteral(red: 0, green: 0.7152789235, blue: 0.7682691216, alpha: 1)
        featureHeadLineLabel.layer.borderWidth = 1.5
        //  submitButton.changeButtonCornerRadius()
        buyView.layer.cornerRadius = 20
        buyView.clipsToBounds = true
        
        
        self.title = "Payment plans"
        
        priceArray = ["$0.05","$0.05","$0.05","$0.05","$0.05","$0.05","$0.05"]
        
        featureNameArray1 = ["Add Member","Daily Time Limits","Restricted Times","Apps&Games","Calls & SMS","Alarms & Reminders","Set Geofence"]
        
        
        self.featureTableView.tableFooterView = UIView(frame: .zero)
        self.featureTableView.tableFooterView?.isHidden = true
        featureTableView.sectionHeaderHeight = UITableView.automaticDimension
        featureTableView.estimatedSectionHeaderHeight = 44
        
        //******** Feature wise monthly plan *******
        //  featureHeadLineLabel.text = "Feature wise monthly plan"
        // priceHeightConstant.constant = 0
        
        
        //********  Monthly plan *******
        /*
         featureHeadLineLabel.text = "Monthly plan"
         priceHeightConstant.constant = 32
         priceDisplayLabel.text = "$10.99"
         */
        
        
        //******** Base plus subscription *******
        
        //  featureHeadLineLabel.text = "Base plus subscription"
        
    
        featureHeadLineLabel.text =   SharedData.data.responsePlans?[planeIndex ?? 0].planName
        
        if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.monthly"
        {
            priceDisplayLabel.text = "$" + (SharedData.data.responsePlans?[planeIndex ?? 0].fullPrice)!
            priceHeightConstant.constant = 40
            minPriceHeightConstant.constant = 0
            totalAmountDisLabel.text = ""
            
            topcontantBuylabel.constant = 10
            
            // buyLabel.text = "Buy"
            
            
            for each in (SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA)!
            {
                seletedarray.append(each)
            }
            
        }else
        {
              topcontantBuylabel.constant = 4
            totalAmountDisLabel.text = ""
            priceHeightConstant.constant = 0
            minPriceHeightConstant.constant = 10
        }
        
        if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.baseplus"
        {
            for each in (SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA)!
            {
                seletedarray.append(each)
            }
            

            let pi = (SharedData.data.responsePlans?[planeIndex ?? 0].basePlanOfferPrice)!
            
            let text = String(format: "%.2f", arguments: [pi])
            
            totalAmountDisLabel.text = "(" + "" + "$"  + text + ")"
   
        }
 
        if userPlans?.remainDays ?? 0 <= 0
        {
            buyViewHeightConstant.constant = 42
        }else
        {
            
           if  userPlans?.remainDays ?? 0 <= 5
           {
             buyViewHeightConstant.constant = 42
            }else
           {
                buyViewHeightConstant.constant = 0
            }
        
            
            
        }

        featureTableView.reloadData()

        NotificationCenter.default.addObserver(self, selector: #selector(self.ChangeTap), name: NSNotification.Name(rawValue: "ChangeTap"), object: nil)
        
        
        featureNameArray = ["Add Member","Daily Time Limits","Restricted Times","Apps&Games","Calls & SMS","Alarms & Reminders","Set Geofence"]
        // Do any additional setup after loading the view.
        
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.buyAction))
        self.buyView.addGestureRecognizer(gesture)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Analytics.logEvent("PaymentPlans_Screen_Enter", parameters:nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
         Analytics.logEvent("PaymentPlans_Screen_Exit", parameters:nil)
    }
    
    @objc func ChangeTap(_ notification: NSNotification)
        
    {
        
        print(notification.userInfo)
        
        if let dict = notification.userInfo as NSDictionary? {
            if let id = dict["notificationId"] as? Int{
                
                seletedarray.removeAll()
                seletedarray1.removeAll()
                print(id)
                
                
                planeIndex = id
                featureHeadLineLabel.text =   SharedData.data.responsePlans?[planeIndex ?? 0].planName
                
                if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.monthly"
                {
                    priceDisplayLabel.text = "$" + (SharedData.data.responsePlans?[planeIndex ?? 0].fullPrice)!
                    
                    
                    for each in (SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA)!
                    {
                        seletedarray.append(each)
                    }
                    
                      topcontantBuylabel.constant = 10
                    minPriceHeightConstant.constant = 0
                    priceHeightConstant.constant = 40
                    totalAmountDisLabel.text = ""
                    
                    //     buyLabel.text = "Buy"
                }else
                {
                    totalAmountDisLabel.text = ""
                    minPriceHeightConstant.constant = 10
                    priceHeightConstant.constant = 0
                        topcontantBuylabel.constant = 4
                }
                
                if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.baseplus"
                {
                    for each in (SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA)!
                    {
                        seletedarray.append(each)
                    }
                    print(seletedarray)
                    
                    let pi = (SharedData.data.responsePlans?[planeIndex ?? 0].basePlanOfferPrice)!
                    
                    let text = String(format: "%.2f", arguments: [pi])
                    
                    totalAmountDisLabel.text = "(" + "" + "$"  + text + ")"
                }

                self.featureTableView.reloadData()
  
            }
            
        }
  
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupB?.count ?? 0 > 0
        {
            return 2
        }
        
        
        return 1
    }
    //    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //        return SharedData.data.responsePlans?[planeIndex ?? 0].customerPlansList!.count ?? 0
    //    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
        if section == 0 {
            
            print(SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?.count ?? 0)
            return  SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?.count ?? 0
        }
        else if section == 1 {
            
            print(SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupB?.count ?? 0)
            return  SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupB?.count ?? 0
        }
        return 0
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpperFeatureTableViewCell") as! UpperFeatureTableViewCell
            
            //  cell.infoButton.isHidden = true
            
            //    SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "Monthly Plan"
            
            if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.featurewiseplan"
            {
                
                //******** Feature wise monthly plan *******
                
                print(seletedarray)
                print(SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[indexPath.row].featureName)
                cell.leftSideImage.addTarget(self, action: #selector(checkButtonTapped), for: .touchUpInside)
                cell.leftSideImage.tag = indexPath.row
                cell.featureLabel.text = SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[indexPath.row].featureName
                cell.infoButton.addTarget(self, action: #selector(infoButtonTapped), for: .touchUpInside)
                
                
                cell.infoButton.tag = indexPath.row
                cell.priceLabel.text = "$" + (SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[indexPath.row].priceForFeature)!
                cell.priceWidthConstant.constant = 50
                
                
                
                if seletedarray.contains(where: { name in name.featureKey == (SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[indexPath.row].featureKey)})
                    
                {
                    
                    cell.leftSideImage.setImage(UIImage(named:"checknew"), for: .normal)
                }else
                {
                    cell.leftSideImage.setImage(UIImage(named:"UnCheck"), for: .normal)
                }
                cell.leftSideImage.isEnabled = true
                
                
                
                //  cell.leftSideImage.setImage(UIImage(named: self.seletedarray.contains(where: (SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[indexPath.row].featureName)!) ?  "checknew" : "UnCheck"), for: .normal)
                
                
                //  cell.leftSideImage.setImage(#imageLiteral(resourceName: "uncheckbox"), for: .normal)
                return cell
                
            }
            //
            //            if   SharedData.data.responsePlans?[planeIndex ?? 0].planName == "Feature wise monthly plan"
            //            {
            //
            //                //******** Feature wise monthly plan *******
            //
            //                let cell = tableView.dequeueReusableCell(withIdentifier: "UpperFeatureTableViewCell") as! UpperFeatureTableViewCell
            //                cell.leftSideImage.addTarget(self, action: #selector(checkButtonTapped), for: .touchUpInside)
            //                cell.featureLabel.text =  SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[indexPath.row].featureName
            //                cell.infoButton.addTarget(self, action: #selector(infoButtonTapped), for: .touchUpInside)
            //                cell.priceLabel.text = SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[indexPath.row].priceForFeature
            //
            //
            //
            //            }
            
            
            
            if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.monthly"
            {
                
                
                //********  Monthly plan *******
                
                
                //   let cell = tableView.dequeueReusableCell(withIdentifier: "UpperFeatureTableViewCell") as! UpperFeatureTableViewCell
                
                cell.infoButton.tag = indexPath.row
                cell.leftSideImage.setImage(#imageLiteral(resourceName: "yestickmark"), for: .normal)
                cell.featureLabel.text = SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[indexPath.row].featureName
                cell.infoButton.addTarget(self, action: #selector(infoButtonTapped), for: .touchUpInside)
                cell.priceWidthConstant.constant = 0
                
                cell.leftSideImage.isEnabled = false
                
                return cell
                
                
                
                
            }
            if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.baseplus"
            {
                
                
                
                //******** Base plus subscription *******
                
                
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "UpperFeatureTableViewCell") as! UpperFeatureTableViewCell
                cell.infoButton.tag = indexPath.row
                cell.leftSideImage.setImage(#imageLiteral(resourceName: "yestickmark"), for: .normal)
                cell.featureLabel.text = SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[indexPath.row].featureName
                cell.infoButton.addTarget(self, action: #selector(infoButtonTapped), for: .touchUpInside)
                cell.priceLabel.text = "$" + (SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[indexPath.row].priceForFeature)!
                cell.priceWidthConstant.constant = 50
                cell.leftSideImage.isEnabled = false
                return cell
                
            }
            
            return cell
            
            
        }
        else
        {
            
            //****For Feature wise & Monthly plan Hide this section
            
            
            
            //******** Base plus subscription *******
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LowerFeatureTableViewCell") as! LowerFeatureTableViewCell
            //   cell.infoButton.isHidden = true
            cell.leftSideButton.addTarget(self, action: #selector(checkButtonTapped1), for: .touchUpInside)
            cell.leftSideButton.tag = indexPath.row
            
            cell.infoButton.tag  = indexPath.row
            cell.featureLabel.text =  SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupB?[indexPath.row].featureName
            cell.infoButton.addTarget(self, action: #selector(infoButtonTapped1), for: .touchUpInside)
            cell.priceLabel.text =  "$" + (SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupB?[indexPath.row].priceForFeature)!
            cell.priceWidthConstant.constant = 50
            if seletedarray1.contains(where: { name in name.featureKey == (SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupB?[indexPath.row].featureKey)})
                
            {
                
                cell.leftSideButton.setImage(UIImage(named:"checknew"), for: .normal)
            }else
            {
                cell.leftSideButton.setImage(UIImage(named:"UnCheck"), for: .normal)
            }
            cell.leftSideButton.isEnabled = true
            
            
            
            return cell
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0
        {
            return 55
        }
        else
        {
            //******** Base plus subscription *******
            
            return 55
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 65
    }
    
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        if section == 0
        {
            let label = UILabel()
            label.backgroundColor = UIColor.white
            label.textColor = #colorLiteral(red: 0.3490196078, green: 0.3490196078, blue: 0.3490196078, alpha: 1)
            label.lineBreakMode = .byWordWrapping
            label.numberOfLines = 0
            label.textAlignment = .center
            label.font = UIFont(name:"Montserrat-Bold", size: 16)
            label.text = SharedData.data.responsePlans?[planeIndex ?? 0].planDescription1
            // label.text = "You can access all features for a month"
            // label.text = "Buy below 5 features $1.50 as base plan"
            
            return label
        }
        else{
            let label = UILabel()
            label.backgroundColor = UIColor.white
            label.textColor = #colorLiteral(red: 0.3490196078, green: 0.3490196078, blue: 0.3490196078, alpha: 1)
            label.lineBreakMode = .byWordWrapping
            label.numberOfLines = 0
            label.textAlignment = .center
            label.font = UIFont(name:"Montserrat-Bold", size: 16)
            label.text = SharedData.data.responsePlans?[planeIndex ?? 0].planDescription2
            return label
        }
        
    }
    
    
    
    @IBAction func checkButtonTapped1(_ sender: UIButton) {
        
        
        
        if seletedarray1.contains(where: { name in name.featureKey == ((SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupB?[sender.tag].featureKey))})
        {
            
            
            // seletedarray.remove(at:seletedarray.firstIndex{$0 === (allNotificationsrr.reversed()[sender.tag].createdDate)})
            
            
            seletedarray1.remove(at:seletedarray1.index(where: { $0.featureKey == ((SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupB?[sender.tag].featureKey))})!)
            
            
            
            
        }else
        {
            self.seletedarray1.append((SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupB?[sender.tag])!)
        }
        
        
        if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.baseplus"
            
        {
            
            
            
            if seletedarray1.count > 0
                
            {
                
                let fullprice = Double(String(format:"%.2f",seletedarray1.reduce(0, { $0 + Double($1.priceForFeature!)! ?? 0})))
                
                
                
                let va = (SharedData.data.responsePlans?[planeIndex ?? 0].basePlanOfferPrice)! + fullprice!
                
                let text = String(format: "%.2f", arguments: [va])
                
                totalAmountDisLabel.text =   "(" + "$" + "\(text)" + ")"
                
                
                //buyLabel.text = "Buy" + "(" + "$" + "\(text)" + ")"
            }
            
            
            
        }
        
        
        //        print(self.seletedarray1.count)
        //        for each in self.seletedarray1
        //        {
        //
        //        }
        //
        
        DispatchQueue.main.async {
            self.featureTableView.reloadData()
        }
        // sender.isSelected = !sender.isSelected
        
        
    }
    
    @IBAction func checkButtonTapped(_ sender: UIButton) {
        
        
        
        
        if seletedarray.contains(where: { name in name.featureKey == ((SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[sender.tag].featureKey))})
        {
            
            
            // seletedarray.remove(at:seletedarray.firstIndex{$0 === (allNotificationsrr.reversed()[sender.tag].createdDate)})
            
            
            seletedarray.remove(at:seletedarray.index(where: { $0.featureKey == ((SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[sender.tag].featureKey))})!)
            
            
            
            
        }else
        {
            self.seletedarray.append((SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[sender.tag])!)
        }
        
        
        if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.featurewiseplan"
            
        {
            
            if seletedarray.count > 0
            {
                let va = Double(String(format:"%.2f",seletedarray.reduce(0, { $0 + Double($1.priceForFeature!)! ?? 0})))
                
                let text = String(format: "%.2f", arguments: [va!])
                
                totalAmountDisLabel.text = "(" + "$" + "\(text)" + ")"
                
            }else
            {
                totalAmountDisLabel.text = ""
            }
            
            
            
        }
        
        //        if self.seletedarray.contains((organizationdetails?.rulez?[indexPath.row].rulesKey)!) {
        //            self.seletedarray.remove(at: self.seletedarray.index(of: (organizationdetails?.rulez?[indexPath.row].rulesKey)!)!)
        //        } else {
        //            self.seletedarray.append((organizationdetails?.rulez?[indexPath.row].rulesKey)!)
        //        }
        //
        DispatchQueue.main.async {
            self.featureTableView.reloadData()
        }
        print(seletedarray.count)
        
        
        
        // sender.isSelected = !sender.isSelected
        
        
        
        
    }
    @IBAction func infoButtonTapped(_ sender: UIButton) {
        
        
        
        
        
        if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.baseplus"
        {
            
            
            //    self.showAlert(title: "", msg: SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[sender.tag].featureInfo ?? "")
            
            self.showCustomAlertWith(message: "", descMsg: SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[sender.tag].featureInfo ?? "", itemimage: #imageLiteral(resourceName: "AboutUs") , actions: nil)
            
            return
        }
        
        
        
        if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.monthly"
        {
            
            
            //  self.showAlert(title: "", msg: SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[sender.tag].featureInfo ?? "")
            
            self.showCustomAlertWith(message: "", descMsg: SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[sender.tag].featureInfo ?? "", itemimage: #imageLiteral(resourceName: "AboutUs") , actions: nil)
            
            return
        }
        
        
        if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.featurewiseplan"
        {
            
            
            //     self.showAlert(title: "", msg: SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[sender.tag].featureInfo ?? "")
            
            self.showCustomAlertWith(message: "", descMsg: SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?[sender.tag].featureInfo ?? "", itemimage: #imageLiteral(resourceName: "AboutUs") , actions: nil)
            
            return
        }
        
        
        
        
        
        //
        //        if seletedarray1.contains(where: { name in name.featureKey == ((SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupB?[sender.tag].featureKey))})
        //        {
        //
        //
        //            // seletedarray.remove(at:seletedarray.firstIndex{$0 === (allNotificationsrr.reversed()[sender.tag].createdDate)})
        //
        //
        //            seletedarray1.remove(at:seletedarray.index(where: { $0.featureKey == ((SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupB?[sender.tag].featureKey))})!)
        //
        //
        //
        //
        //        }else
        //        {
        //            self.seletedarray1.append((SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupB?[sender.tag])!)
        //        }
        //        if self.seletedarray.contains((organizationdetails?.rulez?[indexPath.row].rulesKey)!) {
        //            self.seletedarray.remove(at: self.seletedarray.index(of: (organizationdetails?.rulez?[indexPath.row].rulesKey)!)!)
        //        } else {
        //            self.seletedarray.append((organizationdetails?.rulez?[indexPath.row].rulesKey)!)
        //        }
        //
        //        DispatchQueue.main.async {
        //            self.featureTableView.reloadData()
        //        }
        //
        //
        //
        
        
        
    }
    @IBAction func infoButtonTapped1(_ sender: UIButton) {
        
        //   sender.isSelected = !sender.isSelected
        
        if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.baseplus"
        {
            
            
            //   self.showAlert(title: "", msg: SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupB?[sender.tag].featureInfo ?? "")
            
            
            self.showCustomAlertWith(message: "", descMsg: SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupB?[sender.tag].featureInfo ?? "", itemimage: #imageLiteral(resourceName: "AboutUs") , actions: nil)
            
            return
        }
        
        
    }
    
    @objc func buyAction(sender : UITapGestureRecognizer) {
        
        
         Analytics.logEvent("PaymentPlans_Screen_Submit_Btn_Clicked", parameters:nil)
        
        print(seletedarray.count)

        //print(seletedarray.reduce(0, { $0 + $1.createdDate! ?? 0}))
        //  let jsdhfjsdhf =     seletedarray.reduce(0, { $0 + Int($1.priceForFeature!)! ?? 0})
        // print(self.seletedarray.flatMap{Int($0.priceForFeature!)}.reduce(0, +))
        
        
        //   print(jsdhfjsdhf)
        //    arr.reduce(0, { ($0 + $1)})
        print(String(format:"%.2f",seletedarray.reduce(0, { $0 + Double($1.priceForFeature!)! ?? 0})))
        
        print(seletedarray.reduce(0, { $0 + Double($1.priceForFeature!)! ?? 0}))
        
        
        
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CardInfoViewController") as? CardInfoViewController
        vc?.seletedarray = seletedarray
        
        vc?.seletedarray1 = seletedarray1
        
        vc?.planName = featureHeadLineLabel.text
        vc?.planKey = SharedData.data.responsePlans?[planeIndex ?? 0].planKey
        
        
        if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.baseplus"
        {
            
            
            if seletedarray.contains(where: { name in name.featureKey == "key.addmember"})
                
            {
   
                
            }else
                
            {
                
                //  self.showAlert(title: "", msg: "Please select add member to proceed as this is a mandatory feature.")
                
                if seletedarray1.contains(where: { name in name.featureKey == "key.addmember"})
                {
                    
                    
                    
                }else
                {
                    self.showCustomAlertWith(message: "", descMsg: "Please select add member to proceed as this is a mandatory feature.", itemimage: #imageLiteral(resourceName: "danger") , actions: nil)
                    
                    return
                }
                
             
                
                
                
            }
            
            
            
            if (SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?.contains(where: { name in name.featureKey == "key.lockAndUnlock"}))!
            {
                
                
                
            }else
            {
                
                
                if (SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupB?.contains(where: { name in name.featureKey == "key.lockAndUnlock"}))!
                {
                    
                    
                    
                    if seletedarray1.contains(where: { name in name.featureKey == "key.lockAndUnlock"})
                    {
                        
                    }else
                    {
                        
                        if seletedarray.contains(where: { name in name.featureKey == "key.dailyTimeLimits"}) || seletedarray.contains(where: { name in name.featureKey == "key.restrictedTimes"})
                        {
                            self.showCustomAlertWith(message: "", descMsg: "Daily Time Limits/Restricted Times has dependency with Lock/Unlock screen feature. So please select Lock/Unlock screen feature too.", itemimage: #imageLiteral(resourceName: "danger") , actions: nil)
                            
                            
                            //  self.showAlert(title: "", msg: "Daily Time Limits/Restricted Times has dependency with Lock/Unlock screen feature. So please select Lock/Unlock screen feature too.")
                            return
                        }
                        
                        
                        
                        if seletedarray1.contains(where: { name in name.featureKey == "key.dailyTimeLimits"}) || seletedarray1.contains(where: { name in name.featureKey == "key.restrictedTimes"})
                        {
                            self.showCustomAlertWith(message: "", descMsg: "Daily Time Limits/Restricted Times has dependency with Lock/Unlock screen feature. So please select Lock/Unlock screen feature too.", itemimage: #imageLiteral(resourceName: "danger") , actions: nil)
                            
                            // self.showAlert(title: "", msg: "Daily Time Limits/Restricted Times has dependency with Lock/Unlock screen feature. So please select Lock/Unlock screen feature too.")
                            return
                        }
                        
                        
                        
                        
                    }
                    
                }
   
            }
            
            let fullprice = Double(String(format:"%.2f",seletedarray1.reduce(0, { $0 + Double($1.priceForFeature!)! ?? 0})))
            
            let va = (SharedData.data.responsePlans?[planeIndex ?? 0].basePlanOfferPrice)! + fullprice!
            
            
            if va < 5.00
            {
                // self.showAlert(title: "", msg: "please add more features to continue")
                self.showCustomAlertWith(message: "", descMsg: "Please add more features to continue ", itemimage: #imageLiteral(resourceName: "danger") , actions: nil)
                return
            }
            

            print(Double(fullprice!))
            
            
            
            
            vc?.fullPrice = (SharedData.data.responsePlans?[planeIndex ?? 0].basePlanOfferPrice)! + fullprice!
            
            
        }
        
        if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.monthly"
        {
 
            vc?.fullPrice = Double((SharedData.data.responsePlans?[planeIndex ?? 0].fullPrice)!)
  
        }
        
        
        if SharedData.data.responsePlans?[planeIndex ?? 0].planKey == "key.featurewiseplan"
        {
            
            if seletedarray.count == 0
            {
                //  self.showAlert(title: "", msg: "Please select one feature")
                
                self.showCustomAlertWith(message: "", descMsg:"Please add more features to continue ", itemimage: #imageLiteral(resourceName: "danger") , actions: nil)
                return
            }
            
            
            
            
            if seletedarray.contains(where: { name in name.featureKey == "key.addmember"})
                
            {
 
            }else
                
            {
                
                // self.showAlert(title: "", msg: "Please select add member to proceed as this is a mandatory feature.")
                self.showCustomAlertWith(message: "", descMsg: "Please select add member to proceed as this is a mandatory feature.", itemimage: #imageLiteral(resourceName: "danger") , actions: nil)
                
                return
                
                
                
            }
            

            
            if (SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?.contains(where: { name in name.featureKey == "key.lockAndUnlock"}))!
            {
                if seletedarray.contains(where: { name in name.featureKey == "key.lockAndUnlock"})
                {
                    
                }else
                {
                    if seletedarray.contains(where: { name in name.featureKey == "key.dailyTimeLimits"}) || seletedarray.contains(where: { name in name.featureKey == "key.restrictedTimes"})
                    {
                        //  self.showAlert(title: "", msg: "Daily Time Limits/Restricted Times has dependency with Lock/Unlock screen feature. So please select Lock/Unlock screen feature too.")
                        
                        self.showCustomAlertWith(message: "", descMsg: "Daily Time Limits/Restricted Times has dependency with Lock/Unlock screen feature. So please select Lock/Unlock screen feature too.", itemimage: #imageLiteral(resourceName: "danger") , actions: nil)
                        return
                    }
                }
                
                
                
                
            }
            
            let va = Double(String(format:"%.2f",seletedarray.reduce(0, { $0 + Double($1.priceForFeature!)! ?? 0})))
            
            if va! < 5.00
            {
                //  self.showAlert(title: "", msg: "Please select more features")
                self.showCustomAlertWith(message: "", descMsg: "Please add more features to continue", itemimage: #imageLiteral(resourceName: "danger") , actions: nil)
                
            }
            
            //      if SharedData.data.responsePlans?[planeIndex ?? 0].basePluseGroupA?.contains(where: )
            
            
            
            
            
            vc?.fullPrice = Double(String(format:"%.2f",seletedarray.reduce(0, { $0 + Double($1.priceForFeature!)! ?? 0})))
            
            
        }

        //Double("%.2f",radix: seletedarray.reduce(0, { $0 + Double($1.priceForFeature!)! ?? 0}))
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "feature"
        {
            let popOverViewController = segue.destination
            popOverViewController.popoverPresentationController?.delegate = self
        }
        
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}

