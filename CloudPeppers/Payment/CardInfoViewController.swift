//
//  CardInfoViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 21/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import SVProgressHUD
import IQKeyboardManagerSwift
import Firebase



class CardInfoViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var cancelButton: UIButton!
    var userPlans:UserPlans?
    
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var displayContentLabel: UILabel!
    
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var displayPriceLabel: UILabel!
    @IBOutlet weak var yearTF: UITextField!
    var seletedarray:[customerPlansList]?
    
    var seletedarray1:[customerPlansList]?
    var fullPrice:Double?
    var planKey:String?
    var reNew:String?
    
    var finalPrice:String?
    
    var planName:String?
    
    var autoRenewal:String?
    
    
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var monthTF: UITextField!
    @IBOutlet weak var securityCodeTF: UITextField!
    @IBOutlet weak var cardNumberTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print(fullPrice)
        
        autoRenewal = "false"
        if reNew == "true"
        {
            
            changeButton.isHidden = true
            
        }else
        {
            changeButton.isHidden = false
        }
        
        securityCodeTF.isSecureTextEntry = true
        
        self.title = planName
        
        cardNumberTF.delegate = self
        securityCodeTF.delegate = self
        monthTF.delegate = self
        yearTF.delegate = self
        nameTF.delegate = self
        
        cardNumberTF.textfieldBorder()
        securityCodeTF.textfieldBorder()
        monthTF.textfieldBorder()
        yearTF.textfieldBorder()
        nameTF.textfieldBorder()
        
        continueButton.changeButtonCornerRadius()
        cancelButton.changeButtonCornerRadius()
        changeButton.changeButtonCornerRadius()
        
        displayPriceLabel.layer.cornerRadius = 5
        displayPriceLabel.clipsToBounds = true
        
        //        print(fullPrice!.rounded(places: 4))
        //        print(String(describing: fullPrice!.rounded))
        
        //   let pi = String(describing: fullPrice!.rounded(places: 4))
        let pi1 = fullPrice
        
        let text = String(format: "%.2f", arguments: [pi1!])
        
        finalPrice = text
        
        
        //  displayPriceLabel.text = "$" + String(describing: fullPrice!.rounded(places: 4)) + "" + "0"
        
        displayPriceLabel.text = "$" + text
        print(displayPriceLabel.text)
        IQKeyboardManager.shared.layoutIfNeededOnUpdate = true
        IQKeyboardManager.shared.canAdjustAdditionalSafeAreaInsets = true
        
        
        if planKey == "key.monthly"
        {
            displayContentLabel.text = "You can access all features for a month"
        }
        else{
            displayContentLabel.text = "You can access selected features for a month"
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func AutoRenewalButton(_ sender: UISwitch) {
        
        
        if sender.isOn
        {
            autoRenewal = "true"
        }else
        {
            autoRenewal = "false"
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        Analytics.logEvent("Card_Details_Screen_Enter", parameters:nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("Card_Details_Screen_Exit", parameters:nil)
    }
    
    @IBAction func continueTapped(_ sender: Any) {
        Analytics.logEvent("Card_Details_Continue_Btn_Clicked", parameters:nil)
        
        isAccountHasValidDetails()
        
        //        if isAccountHasValidDetails() == true
        //        {
        //            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReceiptViewController") as? ReceiptViewController
        //            self.navigationController?.pushViewController(vc!, animated: true)
        //        }
        //
        
        
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == cardNumberTF
        {
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if range.location == 19 && isBackSpace != -92{
                return false
            }
            
            if range.length == 1 {
                if (range.location == 5 || range.location == 10 || range.location == 15) {
                    let text = textField.text ?? ""
                    textField.text = text.substring(to: text.index(before: text.endIndex))
                }
                return true
            }
            
            if (range.location == 4 || range.location == 9 || range.location == 14) {
                textField.text = String(format: "%@ ", textField.text ?? "")
            }
            
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: " ", with: "")
            
            //Verify entered text is a numeric value
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
        }
        else if textField == securityCodeTF
        {
            if range.length > 0 {
                return true
            }
            if string == "" {
                return false
            }
            if range.location > 2 {
                return false
            }
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: "", with: "")
            
            //Verify entered text is a numeric value
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
            
            
            return true
        }
            
            
        else if textField == securityCodeTF
        {
            if range.location > 2 {
                return false
            }
        }
            
            
            
        else if textField == monthTF
        {
            if range.length > 0 {
                return true
            }
            if string == "" {
                return false
            }
            if range.location > 1 {
                return false
            }
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: "", with: "")
            
            //Verify entered text is a numeric value
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
            
            
            return true
        }
            
            
        else if textField == monthTF
        {
            if range.location > 1 {
                return false
            }
        }
            
            
        else if textField == yearTF
        {
            if range.length > 0 {
                return true
            }
            if string == "" {
                return false
            }
            if range.location > 3 {
                return false
            }
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: "", with: "")
            
            //Verify entered text is a numeric value
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
            
            
            return true
        }
            
            
        else if textField == yearTF
        {
            if range.location > 3 {
                return false
            }
        }
        
        
        
        return true
    }
    
    
    
    
    func isAccountHasValidDetails()  -> Bool
    {
        let cardNameValidation = (nameTF.text?.trimmingCharacters(in: .whitespaces))
        
        
        if cardNumberTF.text?.count == 0 || (cardNumberTF.text?.count)! < 19
        {
            showAlert(title: "", msg: "Please enter valid card number.")
            return false
        }
        if securityCodeTF.text?.count == 0 || (securityCodeTF.text?.count)! < 3
        {
            showAlert(title: "", msg: "Please enter valid security code.")
            return false
        }
        if monthTF.text?.count == 0 || (monthTF.text?.count)! < 2
        {
            AlertFun.ShowAlert(title: "", message:"Please enter valid month." , in: self)
            return false
        }
        else if Int(monthTF.text!)! > 12
        {
            AlertFun.ShowAlert(title: "", message:"Please enter valid month." , in: self)
            return false
        }
        if yearTF.text?.count == 0 || (yearTF.text?.count)! < 4
        {
            AlertFun.ShowAlert(title: "", message:"Please enter valid year." , in: self)
            return false
        }
        else if Int(yearTF.text!)! < 2019
        {
            AlertFun.ShowAlert(title: "", message:"Please enter valid year." , in: self)
            return false
        }
        
        if  nameTF?.text?.count == 0
        {
            AlertFun.ShowAlert(title: "", message:"Please enter card holder name." , in: self)
            return false
        }
        if checkButton.isSelected == false{
            showAlert(title: "", msg: "Please accept Terms and conditions")
            return false
        }
        
        let creditCardNum : String?
        let expYear : String?
        let expMonth : String?
        let cardHolderName : String?
        let cvv : String?
        
        expMonth = monthTF.text?.trimmingCharacters(in: .whitespaces)
        expYear = yearTF.text?.trimmingCharacters(in: .whitespaces)
        
        print(expMonth)
        print(expYear)
        
        creditCardNum = cardNumberTF.text?.replacingOccurrences(of: " ", with: "")
        
        
        cvv = securityCodeTF.text?.trimmingCharacters(in: .whitespaces)
        cardHolderName = nameTF.text
        
        
        let amountsIncents = fullPrice! * 100
        
        let amountsIncents1 = Int(amountsIncents)
        
        var ApiType = "QAghgh"
        
        if ApiType == "QA"
        {
            ApiType = "Live"
        }
        else
        {
            ApiType = "QA"
            
        }
        
        var parameters : [String: Any] = [:]
        
        
        print(String(describing: amountsIncents1))
        // "imei":"351540061857488"
        parameters["imei"] = "0DCB978D-3502-4700-9105-E4F15447EDCB"
        parameters["cardNo"] = creditCardNum
        parameters["cvv"] = cvv
        parameters["expMonth"] = expMonth
        parameters["expyear"] = expYear
        parameters["apiType"] = ApiType
        parameters["amount"] = amountsIncents1
        parameters["nameOftheCard"] = cardHolderName
        parameters["flag"] = autoRenewal
        parameters["insertedDate"] = String(describing: Int(NSDate().timeIntervalSince1970 * 1000))
        parameters["discription"] = "Hdfc Card payment"
        
        //   parameter.setValue("true", forKey: "isTemrsAccepted")
        
        
        print(parameters)
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: parameters as Any, options: .prettyPrinted)
            
            print(jsonData)
            
            let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) as! String
            
            
            
            
            let result =  jsonString.trimmingCharacters(in: .whitespaces)
            print(result)
            
            let key = "fdsfdsfdsfdsfkljklfdslkdsjfkldsfjdslkfjsdl"
            
            let iv = "fdsfdslkfjdslkfdsjfklds"
            
            
            
            let cryptLib = CryptLib()
            
            let encryptionString = cryptLib.encryptPlainTextRandomIV(withPlainText: jsonString, key: key)
            print(encryptionString)
            
            let decryptString = cryptLib.decryptCipherTextRandomIV(withCipherText: encryptionString, key: key)
            print(decryptString)
            
            
            HandlePayment(withkey: (encryptionString! as String))
            
            
            
        } catch _ {
            // print ("UH OOO")
        }
        return true
    }
    
    
    
    
    
    
    
    func HandlePayment(withkey:String){
        
        
        
        
        
        
        
        var parameters : [String: Any] = [:]
        
        //  parameters["totalPrice"] = String(describing: fullPrice)
        
        
        if reNew == "true"
        {
            guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                let profileData = try? JSONDecoder().decode(profile.self, from: data) else
            {
                
                return
                
            }
            
            parameters["cardDetails"] = withkey
            parameters["userId"] = profileData.userId
            parameters["planName"] = planName
            parameters["planKey"] = planKey
            parameters["fullPrice"] = fullPrice
            
            
            
            parameters["expired"] = userPlans?.expDate
            
            print(userPlans?.expDate)
            
            let unixTimeStamp: Double = Double((userPlans?.expDate)!) / 1000.0
            //        let date = NSDate(timeIntervalSince1970:unixTimeStamp)
            //        print(date)
            
            print(getDateFromTimeStamp(timeStamp : unixTimeStamp))
            
            SVProgressHUD.show()
            print(parameters)
            
            HttpWrapper.posts(with: API.renewalPlan, parameters: parameters, headers: nil, completionHandler: {(response, error)  in
                
                SVProgressHUD.dismiss()
                
                guard let data = response else { return }
                do {
                    let loginRespone = try JSONDecoder().decode(ApiStatusPayment.self, from: data)
                    let loginStatus = loginRespone.status
                    
                    if loginStatus!{
                        Analytics.logEvent("Card_Details_Payment_Successful", parameters:nil)
                        
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReceiptViewController") as? ReceiptViewController
                        
                        
                        vc?.responsePayment = loginRespone.response
                        
                        print(loginRespone.response)
                        vc?.message = loginRespone.message
                        if let  paymentData = try? JSONEncoder().encode(loginRespone.response?.userPlans) {
                            UserDefaults.standard.set(paymentData, forKey: "Paymentdata")
                        }
                        self.navigationController?.pushViewController(vc!, animated: true)
                        
                        
                    }else
                    {
                        Analytics.logEvent("Card_Details_Payment_Failed", parameters:nil)
                        
                        self.showAlert(title: "", msg: loginRespone.message ?? "")
                    }
                }
                catch let jsonErr {
                    
                    SVProgressHUD.dismiss()
                    print(jsonErr)
                    SVProgressHUD.dismiss()
                    
                    self.showAlert(title: "", msg: "Invalid Card Details")
                    
                }
                
                print(response)
                
                
            }
                
            ){ (error) in
                SVProgressHUD.dismiss()
                
            }
            
            //  innerRequestList.setValue(finalPrice, forKey: "fullPrice")
            
            //  innerRequestList.setValue(profileData.userId ,forKey: "userId")
            
            
        }else
        {
            
            
            let permissionRequestList:NSMutableArray = NSMutableArray()
            
            for each in seletedarray!
            {
                
                let innerRequestList:NSMutableDictionary = NSMutableDictionary()
                guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                    let profileData = try? JSONDecoder().decode(profile.self, from: data) else
                {
                    
                    return
                    
                }
                innerRequestList.setValue(each.customerPlansId ,forKey: "customerPlansId")
                innerRequestList.setValue(profileData.userId ,forKey: "userId")
                innerRequestList.setValue(each.customerId, forKey: "customerId")
                innerRequestList.setValue(finalPrice, forKey: "fullPrice")
                print(finalPrice)
                
                
                innerRequestList.setValue(each.planSequenceId, forKey: "planSequenceId")
                innerRequestList.setValue(each.planName, forKey: "planName")
                innerRequestList.setValue(each.typeOfPlan, forKey: "typeOfPlan")
                innerRequestList.setValue(each.planKey, forKey: "planKey")
                innerRequestList.setValue(each.featureName, forKey: "featureName")
                innerRequestList.setValue(each.featureKey, forKey: "featureKey")
                innerRequestList.setValue(each.featureInfo, forKey: "featureInfo")
                
                innerRequestList.setValue(each.featureStatus, forKey: "featureStatus")
                innerRequestList.setValue(each.priceForFeature, forKey: "priceForFeature")
                innerRequestList.setValue(each.planDescription1, forKey: "planDescription1")
                
                innerRequestList.setValue(each.basePlanOfferPrice, forKey: "basePlanOfferPrice")
                
                if planKey == "key.baseplus"
                {
                    innerRequestList.setValue("true", forKey: "groupA")
                }
                
                innerRequestList.setValue(each.planDescription2, forKey: "planDescription2")
                innerRequestList.setValue(each.addedFor, forKey: "addedFor")
                
                
                permissionRequestList.add(innerRequestList)
                print(permissionRequestList)
                
                print()
            }
            
            
            if planKey == "key.baseplus"
            {
                
                if seletedarray1?.count ?? 0 > 0
                {
                    for each in seletedarray1!
                    {
                        
                        let innerRequestList:NSMutableDictionary = NSMutableDictionary()
                        
                        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
                        {
                            
                            return
                            
                        }
                        
                        innerRequestList.setValue(profileData.userId ,forKey: "userId")
                        innerRequestList.setValue(finalPrice, forKey: "fullPrice")
                        
                        
                        innerRequestList.setValue(each.customerPlansId, forKey: "customerPlansId")
                        innerRequestList.setValue(each.customerId, forKey: "customerId")
                        // innerRequestList.setValue(each. forKey: "userId")
                        innerRequestList.setValue(each.planSequenceId, forKey: "planSequenceId")
                        innerRequestList.setValue(each.planName, forKey: "planName")
                        innerRequestList.setValue(each.typeOfPlan, forKey: "typeOfPlan")
                        innerRequestList.setValue(each.featureName, forKey: "featureName")
                        innerRequestList.setValue(each.featureKey, forKey: "featureKey")
                        innerRequestList.setValue(each.planKey, forKey: "planKey")
                        innerRequestList.setValue(each.featureInfo, forKey: "featureInfo")
                        
                        
                        
                        innerRequestList.setValue(each.featureStatus, forKey: "featureStatus")
                        innerRequestList.setValue(each.priceForFeature, forKey: "priceForFeature")
                        innerRequestList.setValue(each.planDescription1, forKey: "planDescription1")
                        innerRequestList.setValue(each.basePlanOfferPrice, forKey: "basePlanOfferPrice")
                        
                        if planKey == "key.baseplus"
                        {
                            innerRequestList.setValue("true", forKey: "groupB")
                        }
                        
                        innerRequestList.setValue(each.planDescription2, forKey: "planDescription2")
                        innerRequestList.setValue(each.addedFor, forKey: "addedFor")
                        
                        
                        permissionRequestList.add(innerRequestList)
                        
                        print()
                    }
                }
                
                
                
            }
            
            
            
            
            
            
            
            
            
            parameters["userSelectedPlanList"] = permissionRequestList
            
            
            
            
            parameters["cardDetails"] = withkey
            
            
            print(parameters)
            SVProgressHUD.show()
            
            HttpWrapper.posts(with: API.saveUserSelectedPlan, parameters: parameters, headers: nil, completionHandler: {(response, error)  in
                
                SVProgressHUD.dismiss()
                
                guard let data = response else { return }
                do {
                    let loginRespone = try JSONDecoder().decode(ApiStatusPayment.self, from: data)
                    let loginStatus = loginRespone.status
                    
                    if loginStatus!{
                        Analytics.logEvent("Card_Details_Payment_Successful", parameters:nil)
                        
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReceiptViewController") as? ReceiptViewController
                        
                        
                        vc?.responsePayment = loginRespone.response
                        
                        print(loginRespone.response)
                        vc?.message = loginRespone.message
                        if let  paymentData = try? JSONEncoder().encode(loginRespone.response?.userPlans) {
                            UserDefaults.standard.set(paymentData, forKey: "Paymentdata")
                        }
                        self.navigationController?.pushViewController(vc!, animated: true)
                        
                        
                    }else
                    {
                        Analytics.logEvent("Card_Details_Payment_Failed", parameters:nil)
                        
                        self.showAlert(title: "", msg: loginRespone.message ?? "")
                    }
                }
                catch let jsonErr {
                    
                    SVProgressHUD.dismiss()
                    print(jsonErr)
                    SVProgressHUD.dismiss()
                    
                    self.showAlert(title: "", msg: "Invalid Card Details")
                    
                }
                
                print(response)
                
                
            }
                
            ){ (error) in
                SVProgressHUD.dismiss()
                
            }
        }
        
        
    }
    
    func getDateFromTimeStamp(timeStamp : Double) -> String {
        
        let date = NSDate(timeIntervalSince1970: timeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        //  dayTimePeriodFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        dayTimePeriodFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
        
        //    "yyyy-MM-dd HH:mm:ss"
        
        // UnComment below to get only time
        //  dayTimePeriodFormatter.dateFormat = "hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
    
    
    @IBAction func checkButtonTapped(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func termsButtonTapped(_ sender: Any) {
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "PaymentTermsNConVC") as! PaymentTermsNConVC
        
        let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetail = try? JSONDecoder().decode(response.self, from: data!)
        // organizationdetail?.organization?.orginationURL
        // vc.urlString =  organizationdetail?.organization?.orginationURL
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func changePlanButtonTapped(_ sender: Any) {
        Analytics.logEvent("Card_Details_ChangePlan_Btn_Clicked", parameters:nil)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cancelButtonTapped(_ sender: Any) {
        Analytics.logEvent("Card_Details_Cancel_Btn_Clicked", parameters:nil)
        self.navigationController?.popViewController(animated: true)
    }
}
extension UITextField {
    func textfieldBorder()
    {
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        self.layer.borderWidth = 1
        self.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(places:Double) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

