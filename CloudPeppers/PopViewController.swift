//
//  PopViewController.swift
//  CustomPopUp
//
//  Created by APPLE on 7/18/19.
//  Copyright © 2019 APPLE. All rights reserved.
//

import UIKit

class PopViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var customViewObj: UIView!
    @IBOutlet weak var dismissBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var permissionTableObj: UITableView!
    @IBOutlet weak var displyCntLbl: UILabel!
    
    @IBOutlet weak var okayBtn: UIButton!
    var invalidPermissionShareArray : Array = [String]()
    var sharValeStr : String!
    
    var statusCodeValue : Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if statusCodeValue == 301 {
            okayBtn.isHidden = false
            dismissBtn.isHidden = true
            nextBtn.isHidden = true
        }
        else
        {
            okayBtn.isHidden = true
            dismissBtn.isHidden = false
            nextBtn.isHidden = false
        }
         CustomDesignViews()
        displyCntLbl.text = sharValeStr!
        
        self.permissionTableObj.delegate = self
        self.permissionTableObj.dataSource = self
        self.permissionTableObj.reloadData()
        
    }
     // MARK: - Custom Methods
    func CustomDesignViews()  {
        customViewObj.layer.cornerRadius = 20
        customViewObj.layer.borderWidth = 0.1
        customViewObj.layer.masksToBounds = true
        
        dismissBtn.layer.cornerRadius = 20
        dismissBtn.layer.borderWidth = 0.1
        dismissBtn.layer.masksToBounds = true
        
        nextBtn.layer.cornerRadius = 20
        nextBtn.layer.borderWidth = 0.1
        nextBtn.layer.masksToBounds = true
        
        okayBtn.layer.cornerRadius = 20
        okayBtn.layer.borderWidth = 0.1
        okayBtn.layer.masksToBounds = true
    }

     // MARK: - Table view Delagete and DataSource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invalidPermissionShareArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PopUpTableViewCell", for: indexPath) as! PopUpTableViewCell
        cell.limitsLbl.text! = invalidPermissionShareArray[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: - Button Events
    @IBAction func dismissBtnActn(_ sender: Any) {
        self.dismiss(animated: true, completion: {});
        
    }
    @IBAction func nextBtnActn(_ sender: Any) {
         self.dismiss(animated: true, completion: {});
    }
    
    @IBAction func okayBtnActn(_ sender: Any) {
        self.dismiss(animated: true, completion: {});
    }
    
}
