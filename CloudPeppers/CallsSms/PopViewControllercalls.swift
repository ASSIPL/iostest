//
//  PopViewControllercalls.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 20/08/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
protocol customDelegate3: class {
    func callApi()
    
     func enableBarButton()
    
    
    
}

class PopViewControllercalls:UIViewController,UITextFieldDelegate,customDelegate1 {
    
    @IBOutlet var countryFiledWidthContant: NSLayoutConstraint!
    
    @IBOutlet var contentLabel: UILabel!
    
    

    @IBOutlet var name: UITextField!
    
    @IBOutlet var mobileNumber: UITextField!
    
    
    var fromareacode:String?
    
    @IBOutlet var countryCode: UITextField!
    
   
    var delegate2: customDelegate3?
  
    var allowedorblock:String?
    var memberId:Int?
    var userId:Int?
    var loginType:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if fromareacode == "true"
        {
           countryFiledWidthContant.constant = 0
            contentLabel.text = "Add Area Code"
            name.placeholder = "Area Name"
             mobileNumber.placeholder = "Area Code"
           // mobileNumber.keyboardType = .default
        }
        
        allowedorblock = "block"
        name.useUnderline()
        mobileNumber.useUnderline()
        name.delegate = self
        mobileNumber.delegate = self
        
        countryCode.useUnderline()
         countryCode.delegate = self
        
       
        countryCode.addTarget(self, action: #selector(myTargetFunction), for: .allTouchEvents)
        print(memberId)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        self.navigationController?.navigationBar.isTranslucent = false
        
         self.navigationController?.navigationBar.tintColor = UIColor.red
      //self.navigationController.navigationBar setBarTintColor:
        
        // Do any additional setup after loading the view.
    }
    
    @objc func myTargetFunction() {
        
        
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CountryPickViewController") as! CountryPickViewController
        vc.delegate = self
        self.present(vc, animated:true, completion:nil)
        //self.present(vc, animated:true, completion:nil)
    }
    func didSelectCountry(_ result: String) {
        countryCode.text = result
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        
        
        
       
        if textField == countryCode
        {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "CountryPickViewController") as! CountryPickViewController
            vc.delegate = self
            self.present(vc, animated:true, completion:nil)
        }
        
    
    
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == name
        {
            
           
            
            if range.location < 30{
                let allowedCharacters = CharacterSet.letters
                let characterSet = CharacterSet(charactersIn: string)
                return allowedCharacters.isSuperset(of: characterSet)
            }
            else
            {
                return range.location < 30
                
            }
            
        }
        
    
    if (textField == mobileNumber)
        {
            
            if fromareacode == "true"
            {
                let newPosition = textField.endOfDocument
                textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                
                if range.length > 0 {
                    return true
                }
                if range.location >= 3{
                    return false
                }
                if string == "" {
                    return false
                }
                var originalText = textField.text
                let replacementText = string.replacingOccurrences(of: " ", with: "")
                
                // Verify entered text is a numeric value
                if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                    return false
                }
                
                
                  return true
            }
            let newPosition = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
            
            if range.length > 0 {
                return true
            }
            if range.location >= 17 {
                return false
            }
            if string == "" {
                return false
            }
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: " ", with: "")
            
            // Verify entered text is a numeric value
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
            
            //Put / after 2 digit
            if range.location == 0 {
                originalText?.append("(")
                textField.text = originalText
            }
            if range.location == 4 {
                originalText?.append(")")
                textField.text = originalText
            }
            if range.location == 8 {
                originalText?.append("-")
                textField.text = originalText
            }
            return true
        }
        
        
    
        
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        if  textField == name
//        {
//            mobileNumber.becomeFirstResponder()
//        }
//        
//        if  textField == mobileNumber
//        {
//            mobileNumber.resignFirstResponder()
//        }
        return false
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        
        delegate2?.enableBarButton()
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
    @IBAction func segmentController(_ sender: UISegmentedControl) {
        
        // if sender.selectedSegmentIndex
        if sender.selectedSegmentIndex == 0 {
            
            allowedorblock = "block"
        }
        else {
            allowedorblock = "allow"
            
        }
        
    }
    
    @IBAction func add(_ sender: Any) {
        
        if name.text!.isEmpty || name.text == ""
        {
            self.showAlert(title: "", msg: "Invalid Name")
            
            return
        }
        
        if mobileNumber.text!.isEmpty || mobileNumber.text == ""
        {
            
           if  fromareacode == "true"
           {
              self.showAlert(title: "", msg: "Invalid Area Code")
            }else
           {
              self.showAlert(title: "", msg: "Invalid Number")
            }
            
            
          
            
            return
        }
        
        if fromareacode == "true"
        {
            
            if mobileNumber.text!.count < 3
            {
                 self.showAlert(title: "", msg: "Area code should 3 digits")
                return
            }
        }
        
        
          let parameter1:NSMutableDictionary = NSMutableDictionary()
        
        parameter1.setValue(self.memberId, forKey: "memberId")
        
        let permissionRequestList:NSMutableArray = NSMutableArray()
      
      let innerRequestList:NSMutableDictionary = NSMutableDictionary()
        
    
        
        innerRequestList.setValue(name.text, forKey: "contactName")
        
        if fromareacode == "true"
        {
             innerRequestList.setValue("\(String(describing: mobileNumber.text!))" , forKey: "contactNumber")
            
            
            innerRequestList.setValue("false", forKey: "contactType")
        }else
        {
             innerRequestList.setValue("\(String(describing: countryCode.text!))" + "\(String(describing: mobileNumber.text!.getStringFromPhoneNumber()))" , forKey: "contactNumber")
            
             innerRequestList.setValue("true", forKey: "contactType")
        }
       
        innerRequestList.setValue(self.userId, forKey: "userId")
        innerRequestList.setValue(self.memberId, forKey: "memberId")
        innerRequestList.setValue(self.loginType, forKey: "typeOfUser")
        innerRequestList.setValue(allowedorblock, forKey: "contactStatus")
        
       permissionRequestList.add(innerRequestList)
        
          parameter1.setValue(permissionRequestList, forKey: "contacts")
        
//        for each in (SharedData.data.responsecalls?.blocked)!
//        {
//            let innerRequestList:NSMutableDictionary = NSMutableDictionary()
//
//            if self.selectedArray.contains(each.contactNumber!)
//            {
//                innerRequestList.setValue(each.contactName, forKey: "contactName")
//                innerRequestList.setValue(each.contactNumber, forKey: "contactNumber")
//                innerRequestList.setValue("allow", forKey: "contactStatus")
//                innerRequestList.setValue(each.contactId, forKey: "contactId")
//                innerRequestList.setValue(self.memberId, forKey: "memberId")
//                permissionRequestList.add(innerRequestList)
//
//            }else
//            {
//                innerRequestList.setValue(each.contactName, forKey: "contactName")
//                innerRequestList.setValue(each.contactNumber, forKey: "contactNumber")
//                innerRequestList.setValue("block", forKey: "contactStatus")
//                innerRequestList.setValue(each.contactId, forKey: "contactId")
//                innerRequestList.setValue(self.memberId, forKey: "memberId")
//                permissionRequestList.add(innerRequestList)
//            }
//        }
//
        
//        let parameter:NSMutableDictionary = NSMutableDictionary()
//
//        parameter.setValue(name.text, forKey: "title")
//        parameter.setValue(mobileNumber.text?.lowercased(), forKey: "url")
//        parameter.setValue(self.userId, forKey: "userId")
//        parameter.setValue(self.memberId, forKey: "memberId")
//        parameter.setValue(self.loginType, forKey: "typeOfUser")
//        parameter.setValue(allowedorblock, forKey: "allowdOrBloked")
//
//
        
        print(parameter1)
        APIServices.postDataToServer(url: API.saveContacts, parameters: parameter1, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                self.view.removeFromSuperview()
                self.removeFromParent()
                self.delegate2?.callApi()
                print(response)
                //                self.urlName.text = nil
                //                self.url.text = nil
                //                self.callgetMemberApi()
                let  respons = response as? [String:Any]
                
                self.showAlert(title: "", msg: respons?["message"] as! String)
                
            }else
            {
                
                
                
            }
        })
    }
    
    
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


