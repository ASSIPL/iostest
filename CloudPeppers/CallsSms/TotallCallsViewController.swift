
import UIKit
import Firebase

class TotallCallsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {
    @IBOutlet var callsTableView: UITableView!
    
    //var approve1:[[allowed]]?
    var searchActive : Bool = false
    var blocked:[allowed]?
    
    @IBOutlet var searchBar: UISearchBar!
    var approve1:[allowed]?
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        callsTableView.register(UINib(nibName: "CallAndSmaTableViewCell", bundle: nil), forCellReuseIdentifier: "CallAndSmaTableViewCell")
        callsTableView.dataSource = self
        callsTableView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("Totalcalls"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = false
        //  callsTableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        callsTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        
        searchBar.text = nil
        searchBar.resignFirstResponder()
        callsTableView.reloadData()
        
        self.searchBar.showsCancelButton = false
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.searchActive = true;
        self.searchBar.showsCancelButton = true
        
        
        blocked = approve1?.filter{ $0.contactName!.localizedStandardContains(searchText) }
        
        callsTableView.reloadData()
        
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        var approve2 = [allowed]()
        
        
        for each in (SharedData.data.responsecalls?.allowed)!
        {
            approve2.append(each)
            
            print(approve2.count)
            
        }
        
        for each in (SharedData.data.responsecalls?.blocked)!
        {
            approve2.append(each)
        }
        
        approve1 = approve2
        callsTableView.reloadData()
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive){
            return blocked?.count ?? 0
            
        }
        return approve1?.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger =  approve1?.count ?? 0
        if(searchActive){
            numOfSection = blocked?.count ?? 0
        }
        
        
        if numOfSection == 0
        {
            
            let noDataLabel: UILabel = UILabel(frame:CGRect(x: 0, y: 0, width: self.callsTableView.frame.width, height: self.callsTableView.frame.height))
            noDataLabel.backgroundColor = .white
            noDataLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            callsTableView.separatorStyle = .none
            noDataLabel.text = "No Contact(s) found"
            noDataLabel.textAlignment = NSTextAlignment.center
            self.callsTableView.backgroundView = noDataLabel
            return 1
        }
        else
        {
            
            self.callsTableView.backgroundView = nil
            
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CallAndSmaTableViewCell", for: indexPath) as! CallAndSmaTableViewCell
        
        
        if(searchActive){
            cell.phoneNumber.text = blocked?[indexPath.row].contactName
            cell.subNumber.text =  blocked?[indexPath.row].contactNumber
            
            cell.buttonWidthLayout.constant = 0
            cell.selectButton.isHidden = true
            
        }else
        {
            cell.phoneNumber.text = approve1?[indexPath.row].contactName
            cell.subNumber.text =  approve1?[indexPath.row].contactNumber
            
            cell.buttonWidthLayout.constant = 0
            cell.selectButton.isHidden = true
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 75.0
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
