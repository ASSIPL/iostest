//
//  CallAndSmaTableViewCell.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 17/06/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class CallAndSmaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var buttonWidthLayout: NSLayoutConstraint!
    
    @IBOutlet weak var disView: RoundUIView!
    @IBOutlet var selectButton: UIButton!
    @IBOutlet var phoneNumber: UILabel!
    @IBOutlet var subNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
      //  disView.dropShadow()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
