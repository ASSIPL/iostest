//
//  MainCallsViewController.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 17/06/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift

protocol Deletescontacts: class {
    func didSelectdate(_ result: Int?)
}


class MainCallsViewController: UIViewController,customDelegate3,UIPopoverPresentationControllerDelegate,Deletescontacts{
   
    
      var popUpVC  = PopViewControllercalls()
    
    @IBOutlet var blockedView: UIView!
    @IBOutlet var blockedNumbers: UILabel!
    @IBOutlet var blockedText: UILabel!
    @IBOutlet var allowedView: UIView!
    @IBOutlet var allowedText: UILabel!
    @IBOutlet var allowedNumber: UILabel!
    @IBOutlet var totalView: UIView!
    @IBOutlet var totalNumber: UILabel!
    @IBOutlet var totalText: UILabel!
    var titleName:String?
    @IBOutlet weak var contentView: UIView!
    enum TabIndex : Int {
        case firstChildTab = 0
        case secondChildTab = 1
        case ThirdViewController = 2
    }
    var loginType:String?
    var memberId:Int?
    var userId:Int?
    
    var currentViewController: UIViewController?
    lazy var firstChildTabVC: UIViewController? = {
        let firstChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "BlockedCallsView") as? BlockedCallsView
        firstChildTabVC?.memberId = memberId
        firstChildTabVC?.userId = userId
        
        
        return firstChildTabVC
    }()
    lazy var secondChildTabVC : UIViewController? = {
        let secondChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "UnblockedCallsView") as? UnblockedCallsView
        
        secondChildTabVC?.memberId = memberId
        secondChildTabVC?.userId = userId
        
        return secondChildTabVC
    }()
    
    lazy var thirdViewdTabVC : UIViewController? = {
        let ThirdViewController = self.storyboard?.instantiateViewController(withIdentifier: "TotallCallsViewController") as? TotallCallsViewController
        
        return ThirdViewController
    }()
   var rightButtonItem1:UIBarButtonItem?
   
    
    
    override func viewDidLoad() {
        
      
        
        rightButtonItem1 = UIBarButtonItem.init(image: #imageLiteral(resourceName: "whitePlus"), style: .done, target: self, action: #selector(rightButtonAction(sender:)))
        self.navigationItem.rightBarButtonItem = rightButtonItem1
        
        //    blockedView.layer.cornerRadius = 5
        //   blockedView.clipsToBounds = true
        
        //        allowedView.layer.cornerRadius = 5
        //        allowedView.clipsToBounds = true
        
        //   totalView.layer.cornerRadius = 5
        //    totalView.clipsToBounds = true
        
        super.viewDidLoad()
        displayCurrentTab(1)
        b1(UIButton.self)
        self.title = titleName
        self.navigationController?.navigationBar.isTranslucent = false
        IQKeyboardManager.shared.layoutIfNeededOnUpdate = true
        IQKeyboardManager.shared.canAdjustAdditionalSafeAreaInsets = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("internalApi"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification1(notification:)), name: Notification.Name("callApi1"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification2(notification:)), name: Notification.Name("callApi2"), object: nil)
        
        // Do any additional setup after loading the view.
        
        
    }
    func callApi() {
        
         rightButtonItem1?.isEnabled = true
        callgetMemberApi()
    }
    
    
   func  enableBarButton()
   {
     rightButtonItem1?.isEnabled = true
    }
    
    func didSelectdate(_ result: Int?) {
        
        popUpVC = storyboard?.instantiateViewController(withIdentifier: "PopViewControllercalls") as! PopViewControllercalls
        popUpVC.memberId = memberId
        
        popUpVC.userId = userId
        
        popUpVC.loginType = loginType
        popUpVC.delegate2 = self
        
        rightButtonItem1?.isEnabled = false
        if result == 0
        {
            
            popUpVC.fromareacode = "false"
            
        }
        if result == 1
        {
            
            
            popUpVC.fromareacode = "true"
        }
        
        
        self.view.addSubview(popUpVC.view)
        self.addChild(popUpVC)
        
        
    
    }
    
    @objc func rightButtonAction(sender: UIBarButtonItem)
    {
        
    
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "popViewController") as! popViewController
        vc.modalPresentationStyle = .popover
        let popover: UIPopoverPresentationController = vc.popoverPresentationController!
        popover.barButtonItem = sender
        vc.preferredContentSize = CGSize(width: 160,height: 100)
        popover.delegate = self
        popover.sourceView = self.view
        vc.delegate3 = self
        vc.fromContacts = "true"
    
        present(vc, animated: true, completion:nil)
        
        
        
    }
    
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        // Force popover style
        return UIModalPresentationStyle.none
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pop" {
            let popoverViewController = segue.destination
            popoverViewController.modalPresentationStyle = .popover
            popoverViewController.presentationController?.delegate = self
            
            
            
            if let barButtonFrame = rightButtonItem1?.view {
                // etc...
                
//                popover?.sourceView = barButtonFrame
//                popover?.sourceRect = barButtonFrame.bounds
                
                popoverViewController.popoverPresentationController?.sourceView = barButtonFrame
                popoverViewController.popoverPresentationController?.sourceRect  = CGRect(x: 0, y: 0, width: (barButtonFrame.frame.size.width), height:  (barButtonFrame.frame.size.height))
                
                
            }
            
//            popoverViewController.popoverPresentationController?.sourceView = rightButtonItem1?.customView
//            popoverViewController.popoverPresentationController?.sourceRect  = CGRect(x: 0, y: 0, width: (rightButtonItem1?.customView?.frame.size.width)!, height:  (rightButtonItem1?.customView?.frame.size.height)!)
        }
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        callgetMemberApi()
        
    }
    
    @objc func methodOfReceivedNotification1(notification: Notification) {
        b2(UIButton.self)
        // callgetMemberApi()
        
    }
    
    @objc func methodOfReceivedNotification2(notification: Notification) {
        b1(UIButton.self)
        // callgetMemberApi()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Analytics.logEvent("Calls_Sms_Screen_Enter", parameters:nil)

    }
    override func viewWillDisappear(_ animated: Bool) {
        
        Analytics.logEvent("Calls_Sms_Screen_Exit", parameters:nil)

    }
    
    @IBAction func b1(_ sender: Any) {
        
         Analytics.logEvent("Calls_Sms_Screen_Blocked_Tab_Clicked", parameters:nil)
        displayCurrentTab(1)
        
        callgetMemberApi()
//        blockedView.backgroundColor =  UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1)
//
//        blockedView.layer.borderColor = UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1).cgColor
        blockedView.backgroundColor =  #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        blockedView.layer.borderColor = #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        blockedView.layer.borderWidth = 1.0
        allowedView.backgroundColor = UIColor.white
        allowedView.layer.borderColor = UIColor.gray.cgColor
        allowedView.layer.borderWidth = 1.0
        totalView.backgroundColor = UIColor.white
        
        totalView.layer.borderColor = UIColor.gray.cgColor
        totalView.layer.borderWidth = 1.0
        
        blockedNumbers.textColor = UIColor.white
        blockedText.textColor = UIColor.white
        
        
        totalNumber.textColor = UIColor.black
        totalText.textColor = UIColor.black
        allowedNumber.textColor = UIColor.black
        allowedText.textColor = UIColor.black
        
        if let vc = viewControllerForSelectedSegmentIndex(2)
        {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
        
        if let vc = viewControllerForSelectedSegmentIndex(3)
        {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
    }
    func callgetMemberApi()
    {
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        print(memberId)
        
        
        let getGamesAndAppsUrl = API.getallContacts + "?userId=\(String(describing: profileData.userId!))" + "&memberId=\(memberId!)"
        print(getGamesAndAppsUrl)
        HttpWrapper.gets(with: getGamesAndAppsUrl, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            print(response)
            
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatusCalls.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    SharedData.data.responsecalls = loginRespone.response
                    
                    NotificationCenter.default.post(name: Notification.Name("Blockedcalls"), object: nil)
                    
                    NotificationCenter.default.post(name: Notification.Name("Allowedcalls"), object: nil)
                    
                    NotificationCenter.default.post(name: Notification.Name("Totalcalls"), object: nil)
                    
                    
                    if (SharedData.data.responsecalls?.blocked?.count)! > 0 || !(SharedData.data.responsecalls?.blocked!.isEmpty)!
                    {
                        self.blockedNumbers.text =  String(describing: (SharedData.data.responsecalls?.blocked?.count)!)
                    }else
                    {
                        self.blockedNumbers.text = "0"
                    }
                    if  (SharedData.data.responsecalls?.allowed?.count)! > 0
                    {
                        self.allowedNumber.text = String(describing: (SharedData.data.responsecalls?.allowed?.count)!)
                    }else
                    {
                        self.allowedNumber.text = "0"
                    }
                    
                    
                    self.totalNumber.text = String(describing: SharedData.data.responsecalls!.blocked!.count + SharedData.data.responsecalls!.allowed!.count)
                    
                    // self.familycollectionView.reloadData()
                }
                else
                    
                {
                    
                }
                
            }catch let jsonErr {
                
                
                print("Error serializing json:", jsonErr)
                
            }
        }){ (error) in
            
            print(error)
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
    }
    @IBAction func b2(_ sender: Any) {
         Analytics.logEvent("Calls_Sms_Screen_UnBlocked_Tab_Clicked", parameters:nil)
        displayCurrentTab(2)
        
        callgetMemberApi()
        
        blockedView.backgroundColor = UIColor.white
        
        blockedView.layer.borderColor = UIColor.gray.cgColor
        blockedView.layer.borderWidth = 1.0
        
//        allowedView.backgroundColor = UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1)
//
//        allowedView.layer.borderColor = UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1).cgColor
        allowedView.backgroundColor =  #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        allowedView.layer.borderColor = #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        allowedView.layer.borderWidth = 1.0
        
        totalView.backgroundColor = UIColor.white
        totalView.layer.borderColor = UIColor.gray.cgColor
        totalView.layer.borderWidth = 1.0
        
        
        allowedNumber.textColor = UIColor.white
        allowedText.textColor = UIColor.white
        
        
        totalNumber.textColor = UIColor.black
        totalText.textColor = UIColor.black
        
        
        
        blockedNumbers.textColor = UIColor.black
        blockedText.textColor = UIColor.black
        if let vc = viewControllerForSelectedSegmentIndex(1)
        {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
        
        if let vc = viewControllerForSelectedSegmentIndex(3)
        {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
    }
    
    @IBAction func b3(_ sender: Any) {
         Analytics.logEvent("Calls_Sms_Screen_TotalCalls_Tab_Clicked", parameters:nil)
        displayCurrentTab(3)
        callgetMemberApi()
        blockedView.backgroundColor = UIColor.white
        blockedView.layer.borderColor = UIColor.gray.cgColor
        blockedView.layer.borderWidth = 1.0
        allowedView.backgroundColor = UIColor.white
        
        allowedView.layer.borderColor = UIColor.gray.cgColor
        allowedView.layer.borderWidth = 1.0
//        totalView.backgroundColor =  UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1)
//
//        totalView.layer.borderColor = UIColor(displayP3Red: 244.0/255.0, green: 107.0/255.0, blue: 48.0/255.0, alpha: 1).cgColor
        totalView.backgroundColor =  #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        totalView.layer.borderColor = #colorLiteral(red: 0.9176470588, green: 0.337254902, blue: 0.3137254902, alpha: 1)
        totalView.layer.borderWidth = 1.0
        
        totalNumber.textColor = UIColor.white
        totalText.textColor = UIColor.white
        
        allowedNumber.textColor = UIColor.black
        allowedText.textColor = UIColor.black
        blockedNumbers.textColor = UIColor.black
        blockedText.textColor = UIColor.black
        
        if let vc = viewControllerForSelectedSegmentIndex(2)
        {
            // vc.removeFromParent()
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
            //contentView.willRemoveSubview(vc)
        }
        
        if let vc = viewControllerForSelectedSegmentIndex(1)
        {
            vc.removeFromParent()
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
        
        
    }
    
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            
            
            
            self.addChild(vc)
            vc.didMove(toParent: self)
            
            vc.view.frame = self.contentView.bounds
            self.contentView.addSubview(vc.view)
            
            self.currentViewController = vc
        }
    }
    
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case 1:
            vc = firstChildTabVC
        case 2 :
            vc = secondChildTabVC
        default:
            vc = thirdViewdTabVC
        }
        
        return vc
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}
extension UIBarButtonItem {
    var view: UIView? {
        return perform(#selector(getter: UIViewController.view)).takeRetainedValue() as? UIView
    }
}
