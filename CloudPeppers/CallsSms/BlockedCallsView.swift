
import UIKit
import Alamofire
import Firebase
class BlockedCallsView: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {
    var selectedArray = [String]()
    var memberId:Int?
    var userId:Int?
    //  var responseappsGames:responseappsGames?
    var approve:[[approve]]?
    var blocked:[allowed]?
    var searchActive : Bool = false
    
    @IBOutlet var searchBar: UISearchBar!
    
    @IBOutlet weak var unblockButton: UIButton!
    @IBOutlet var callsTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        
        callsTableView.register(UINib(nibName: "CallAndSmaTableViewCell", bundle: nil), forCellReuseIdentifier: "CallAndSmaTableViewCell")
        callsTableView.dataSource = self
        callsTableView.delegate = self
        unblockButton.changeButtonCornerRadius()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("Blockedcalls"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = false
        //   callsTableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        callsTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        
        searchBar.text = nil
        searchBar.resignFirstResponder()
        callsTableView.reloadData()
        
        self.searchBar.showsCancelButton = false
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.searchActive = true;
        self.searchBar.showsCancelButton = true
        
        
        blocked = SharedData.data.responsecalls?.blocked?.filter{ $0.contactName!.localizedStandardContains(searchText) }
        
        callsTableView.reloadData()
        
    }
    
    
    @IBAction func UnBlockAction(_ sender: Any) {
        Analytics.logEvent("Calls_Sms_Screen_UnBlockButton_Clicked", parameters:nil)
        if selectedArray.isEmpty || selectedArray.count == 0
        {
            showAlert(title: "", msg: "Please select at least one contact to unblock")
            return
        }
        
        
        
        //    let alertController = UIAlertController(title: Constants.alertTitle, message: "Are you sure want to unblock the selected contact(s)?", preferredStyle: .alert)
        
        
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        
        let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
        let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
        
        let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
        let msgAttrString = NSMutableAttributedString(string: "Are you sure want to unblock the selected contact(s)?", attributes: msgFont)
        
        alert.setValue(titAttrString, forKey: "attributedTitle")
        alert.setValue(msgAttrString, forKey: "attributedMessage")
        
        
        let cancelAction = UIAlertAction(title: "NO", style: .cancel, handler: { (action : UIAlertAction!) -> Void in })
        
        let saveAction = UIAlertAction(title: "YES", style: .default, handler: { alert -> Void in
            
            //  let parameter:NSMutableDictionary = NSMutableDictionary()
            
            //  parameter.setValue(self.memberId, forKey: "memberId")
            
            let permissionRequestList:NSMutableArray = NSMutableArray()
            
            for each in (SharedData.data.responsecalls?.blocked)!
            {
                let innerRequestList:NSMutableDictionary = NSMutableDictionary()
                
                if self.selectedArray.contains(each.contactNumber!)
                {
                    innerRequestList.setValue(each.contactName, forKey: "contactName")
                    innerRequestList.setValue(each.contactNumber, forKey: "contactNumber")
                    innerRequestList.setValue("allow", forKey: "contactStatus")
                    innerRequestList.setValue(each.contactId, forKey: "contactId")
                    innerRequestList.setValue(self.memberId, forKey: "memberId")
                    permissionRequestList.add(innerRequestList)
                    
                }else
                {
                    innerRequestList.setValue(each.contactName, forKey: "contactName")
                    innerRequestList.setValue(each.contactNumber, forKey: "contactNumber")
                    innerRequestList.setValue("block", forKey: "contactStatus")
                    innerRequestList.setValue(each.contactId, forKey: "contactId")
                    innerRequestList.setValue(self.memberId, forKey: "memberId")
                    permissionRequestList.add(innerRequestList)
                }
            }
            
            //   parameter.setValue(permissionRequestList, forKeyPath: "contacts")
            //       print(parameter)
            let parameters: [[String: Any]] = permissionRequestList as! [[String : Any]]
            var request = URLRequest(url: URL(string: API.uploadcontacts)!)
            request.httpMethod = "POST"
            let id = UserDefaults.standard.string(forKey: "jwtToken")
            
            if id != nil && id != ""
            {
                let token = "Bearer" + " "  + "\(UserDefaults.standard.string(forKey: "jwtToken")!)"
                
                print(token)
                request.setValue(token, forHTTPHeaderField: "Authorization")
            }
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let dataToSync = parameters
            request.httpBody = try! JSONSerialization.data(withJSONObject: dataToSync)
            
            Alamofire.request(request).responseJSON { (response) in
                
                switch response.result{
                case .success:
                    let statusCode: Int = (response.response?.statusCode)!
                    switch statusCode{
                    case 200:
                        
                        guard let data = response.data else { return }
                        do {
                            let loginRespone =  try JSONDecoder().decode(ApiStatus1.self, from: data)
                            
                            let loginStatus = loginRespone.status
                            
                            
                            if loginStatus!
                            {
                                
                                NotificationCenter.default.post(name: Notification.Name("internalApi"), object: nil)
                                
                                
                                
                                NotificationCenter.default.post(name: Notification.Name("callApi1"), object: nil)
                                
                                //                            let vc = MainAppsViewController()
                                //
                                //                            vc.memberId = self.memberId
                                //                            vc.userId = self.userId
                                //                             vc.callgetMemberApi()
                                
                                let alertController = UIAlertController(title: "", message: loginRespone.message!, preferredStyle: .alert)
                                let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                                }
                                
                                alertController.addAction(settingsAction)
                                self.present(alertController, animated: true, completion: nil)
                                
                            }
                            else
                            {
                                
                                let alertController = UIAlertController(title: "", message: loginRespone.message!, preferredStyle: .alert)
                                let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel) { (UIAlertAction) in
                                }
                                alertController.addAction(settingsAction)
                                self.present(alertController, animated: true, completion: nil)
                            }
                            
                        }catch
                        {
                            print(error)
                            
                            
                        }
                        
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure:
                    
                    break
                }
            }
            
            
            
        })
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        self.present(alert, animated: true, completion: nil)
        
        
        
        
    }
    
    
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        selectedArray.removeAll()
        callsTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        callsTableView.reloadData()
        
        print("First VC will appear")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive){
            return blocked?.count ?? 0
            
        }
        if SharedData.data.responsecalls?.blocked?.count == 0
        {
            unblockButton.isHidden = true
            
        }else{
            unblockButton.isHidden = false
            
        }
        return SharedData.data.responsecalls?.blocked?.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSection: NSInteger =  SharedData.data.responsecalls?.blocked?.count ?? 0
        if(searchActive){
            numOfSection = blocked?.count ?? 0
        }
        
        
        if numOfSection == 0
        {
            
            let noDataLabel: UILabel = UILabel(frame:CGRect(x: 0, y: 0, width: self.callsTableView.frame.width, height: self.callsTableView.frame.height))
            noDataLabel.backgroundColor = .white
            noDataLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            callsTableView.separatorStyle = .none
            noDataLabel.text = "No Contact(s) found"
            noDataLabel.textAlignment = NSTextAlignment.center
            self.callsTableView.backgroundView = noDataLabel
            return 1
        }
        else
        {
            
            self.callsTableView.backgroundView = nil
            
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CallAndSmaTableViewCell", for: indexPath) as! CallAndSmaTableViewCell
        
        
        
        if(searchActive){
            // return allowed?.count ?? 0
            
            cell.phoneNumber.text = blocked?[indexPath.row].contactName
            cell.subNumber.text = blocked?[indexPath.row].contactNumber
            cell.selectButton.tag = indexPath.row
            
            if selectedArray.count > 0
            {
                if self.selectedArray.contains((blocked?[indexPath.row].contactNumber)!)
                {
                    cell.selectButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
                }else
                {
                    cell.selectButton.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
                }
            }
            
            
        }else
        {
            cell.phoneNumber.text = SharedData.data.responsecalls?.blocked?[indexPath.row].contactName
            cell.subNumber.text = SharedData.data.responsecalls?.blocked?[indexPath.row].contactNumber
            cell.selectButton.tag = indexPath.row
            if self.selectedArray.contains((SharedData.data.responsecalls?.blocked?[indexPath.row].contactNumber)!)
                
            {
                
                cell.selectButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
                
                
            }else
            {
                
                cell.selectButton.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
                
                
            }
        }
        
        
        cell.selectButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        return cell
    }
    @objc func buttonAction(sender: UIButton!) {
        
        
        if(searchActive){
            
            
            if self.selectedArray.contains((blocked?[sender.tag].contactNumber)!) {
                self.selectedArray.remove(at: self.selectedArray.index(of: (blocked?[sender.tag].contactNumber)!)!)
            } else {
                self.selectedArray.append((blocked?[sender.tag].contactNumber)!)
            }
            
            
        }else
        {
            if self.selectedArray.contains((SharedData.data.responsecalls?.blocked?[sender.tag].contactNumber)!) {
                self.selectedArray.remove(at: self.selectedArray.index(of: (SharedData.data.responsecalls?.blocked?[sender.tag].contactNumber)!)!)
            } else {
                self.selectedArray.append((SharedData.data.responsecalls?.blocked?[sender.tag].contactNumber)!)
            }
            
        }
        
        
        callsTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 75.0
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
