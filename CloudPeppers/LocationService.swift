//
//  LocationService.swift
//
//
//  Created by Ashok Reddy G on 25/04/19.
//  Copyright © 2018 Ashok Reddy G. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

public class LocationService: NSObject,CLLocationManagerDelegate {
    
    
    public static var sharedInstance = LocationService()
    var locationManager = CLLocationManager()
    var lastLocation:CLLocation?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var locationTimer = Timer()
     var locationTimer1 = Timer()
    let defaults = UserDefaults.standard
    override init() {
        super.init()
        
        //        self.locationManager.requestAlwaysAuthorization()
        //        self.locationManager.startUpdatingLocation()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined,.restricted,.denied:
                print("notDetermined")
                
                return
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                // getLocation()
            }
        } else {
            
            return
        }
        
        getLocation()
        //_ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(getLocation), userInfo: nil, repeats: true)
        
    }
    
    @objc func locationUpdate(){
        
        locationTimer = Timer.scheduledTimer(timeInterval: 40.0, target: self, selector: #selector(updateCoordinates), userInfo: nil, repeats: true)
        
        //  LocationService.sharedInstance.updateCoordinates()
        
        
    }
    
    
    @objc func updateCoordinatesEveryThirtyMinutes(){
        
        locationTimer1 = Timer.scheduledTimer(timeInterval: 1800.0, target: self, selector: #selector(updateCoordinatesEveryThirtyMinute), userInfo: nil, repeats: true)
        
        //  LocationService.sharedInstance.updateCoordinates()
        
        
    }
    
    @objc func StopLocationtimer()
    {
        locationTimer.invalidate()
        
    }
    
    @objc func StopLocationtimer1()
    {
        
        locationTimer1.invalidate()
    }
    
    @objc func getLocation()
    {
        
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.distanceFilter = 10
        // self.locationManager.desiredAccuracy = 15
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.pausesLocationUpdatesAutomatically = false
        self.locationManager.startUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
        self.locationManager.delegate = self
        self.locationManager.allowDeferredLocationUpdates(untilTraveled: 10, timeout: 5)
    }
    //    public func locationManager(_ manager: CLLocationManager, didFinishDeferredUpdatesWithError error: Error?)
    //    {
    //        let title = "Some title"
    //        let message = "body message"
    //
    //        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    //        let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
    //
    //        alert.addAction(action)
    //
    //        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    //        //self.locationManager.distanceFilter = 10
    //      //  self.locationManager.allowDeferredLocationUpdates(untilTraveled: 10, timeout: 60)
    //
    //        //  self.locationManager.desiredAccuracy = 15
    //      //  let userLocation :CLLocation = locations[0] as CLLocation
    //        let location = locationManager.location?.coordinate
    //      //  print(userLocation.timestamp)
    //     //   print(userLocation.horizontalAccuracy)
    //        appDelegate.latitude = (location?.latitude)!
    //        appDelegate.longtitude = (location?.longitude)!
    //
    //
    //        updateCustomerCoordinates()
    //
    //}
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // let location = locations.last
        self.locationManager.distanceFilter = 10
        self.locationManager.allowDeferredLocationUpdates(untilTraveled: 10, timeout: 10)
        
        //  self.locationManager.desiredAccuracy = 15
        let userLocation :CLLocation = locations[0] as CLLocation
        let location = locationManager.location?.coordinate
        print(userLocation.timestamp)
        print(userLocation.horizontalAccuracy)
        appDelegate.latitude = (location?.latitude)!
        appDelegate.longtitude = (location?.longitude)!
        
        UserDefaults.standard.set(appDelegate.latitude, forKey: "latitude")
        UserDefaults.standard.set(appDelegate.longtitude, forKey: "longtitude")
        
        
        if UIApplication.shared.applicationState == .active {
        } else {
            //App is in BG/ Killed or suspended state
            //send location to server
            // create a New Region with current fetched location
            let location = locations.last
            lastLocation = location
            
            //Make region and again the same cycle continues.
            self.createRegion(location: lastLocation)
        }
        
        
        //               let title = "Some title"
        //                let message = "body message"
        //
        //                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //                let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
        //
        //                alert.addAction(action)
        //
        //                UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        
        
        if profileData.userId != nil
        {
            //  updateCoordinates()
        }
        
    }
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location updated")
        print("error:: \(error.localizedDescription)")
    }
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    func createRegion(location:CLLocation?) {
        
        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            let coordinate = CLLocationCoordinate2DMake((location?.coordinate.latitude)!, (location?.coordinate.longitude)!)
            let regionRadius = 50.0
            
            let region = CLCircularRegion(center: CLLocationCoordinate2D(
                latitude: coordinate.latitude,
                longitude: coordinate.longitude),
                                          radius: regionRadius,
                                          identifier: "aabb")
            
            region.notifyOnExit = true
            region.notifyOnEntry = true
            
            //Send your fetched location to server
            
            //Stop your location manager for updating location and start regionMonitoring
            // self.locationManager.stopUpdatingLocation()
            self.locationManager.startMonitoring(for: region)
        }
        else {
            print("System can't track regions")
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("Entered Region")
        locationManager.startUpdatingLocation()
    }
    
    public func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("Exited Region")
        
        //  locationManager.stopMonitoring(for: region)
        
        //Start location manager and fetch current location
        locationManager.startUpdatingLocation()
    }
    
    @objc func updateCoordinates()
    {
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        if profileData.userId != nil
        {
            var params: [String:Any] = [:]
            params["userId"] =  profileData.userId
            params["latitude"]  =  UserDefaults.standard.string(forKey: "latitude")
            params["longitude"] =  UserDefaults.standard.string(forKey: "longtitude")
            params["isRequireMembersLocation"]  = "no"
            params["batteryStatus"] =  batteryLevel
            params["geoHistoryFlag"] = false
            HttpWrapper.post(with: API.saveGeocoordinate , parameters: params, headers: nil, completionHandler: { (response) in
                
                print(response)
                print(response["status"] as Any)
                print(response["responds"] as Any)
                
            }){ (error) in
               
            }
        }
        
    }
    
    @objc func updateCoordinatesEveryThirtyMinute()
    {
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        if profileData.userId != nil
        {
            var params: [String:Any] = [:]
            params["userId"] =  profileData.userId
            params["latitude"]  =  UserDefaults.standard.string(forKey: "latitude")
            params["longitude"] =  UserDefaults.standard.string(forKey: "longtitude")
            params["isRequireMembersLocation"]  = "no"
            params["batteryStatus"] =  batteryLevel
            params["geoHistoryFlag"] = true
            
            HttpWrapper.post(with: API.saveGeocoordinate , parameters: params, headers: nil, completionHandler: { (response) in
                
                print(response)
                print(response["status"] as Any)
                print(response["responds"] as Any)
                
            }){ (error) in
                
            }
        }
        
    }
    
    
    
    
    var batteryLevel: Int {
        return Int(round(UIDevice.current.batteryLevel * 100))
    }
    func getCurrentTimeZone() -> String{
        
        return String (TimeZone.current.identifier)
        
    }
    func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    
}
