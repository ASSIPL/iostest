//
//  BottomCollectionViewCell.swift
//  CloudPeppers
//
//  Created by Allvy on 19/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class BottomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var profileBgView: UIView!
    @IBOutlet weak var bgViewWidth: NSLayoutConstraint!
    @IBOutlet weak var bgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var relationLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    override var isHighlighted: Bool {
        didSet {
//            if isHighlighted {
//                UIView.animate(withDuration: 0.9, delay: 0, options: .curveEaseOut, animations: {
//                    self.transform = CGAffineTransform(scaleX: 5.0, y: 5.0)
//                }, completion: nil)
//            } else {
//                UIView.animate(withDuration: 0.9, delay: 0, options: .curveEaseOut, animations: {
//                    self.transform = CGAffineTransform(scaleX: 1, y: 1)
//                }, completion: nil)
//            }
        }
    }
     
    
    
}
