//
//  PermissionCollectionViewCell.swift
//  CloudPeppers
//
//  Created by Allvy on 26/04/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class PermissionCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var displayImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var relationLabel: UILabel!
}
