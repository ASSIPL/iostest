//
//  DailyTimeLimitsVC.swift
//  CloudPeppers
//
//  Created by Allvy on 28/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Firebase

class DailyTimeLimitsVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    
    @IBOutlet weak var dailyTimeLimitsTV: UITableView!
    @IBOutlet weak var weeksView: UIView!
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var maxNumberOfHoursView: UIView!
    @IBOutlet weak var mondayButton: UIButton!
    @IBOutlet weak var sundayButton: UIButton!
    @IBOutlet weak var saturdayButton: UIButton!
    @IBOutlet weak var fridayButton: UIButton!
    @IBOutlet weak var thurdayButton: UIButton!
    @IBOutlet weak var tuesdayButton: UIButton!
    @IBOutlet weak var wednesdayButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    var titleName:String?
    
    var timeString:String?
    var weekarray = [String]()
    var numberOfMinutes:Int?
    var getMemberDetailsbyId:[DailyTimedata]?
    var memberId:Int?
    var userId:Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addButton.changeButtonCornerRadius()
        buttonUnSelected(button: mondayButton)
        buttonUnSelected(button: tuesdayButton)
        buttonUnSelected(button: wednesdayButton)
        buttonUnSelected(button: thurdayButton)
        buttonUnSelected(button: fridayButton)
        buttonUnSelected(button: saturdayButton)
        buttonUnSelected(button: sundayButton)
        maxNumberOfHoursView.dropShadow()
        self.title = titleName
        callApidailyTimeLimit()
        timePicker.countDownDuration = 0.0
        // timePicker.minuteInterval = 0
        
        timeString = "00:01"
        
        let time = timeString!
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm"
        let value =  time.components(separatedBy: ":")
        print(Int(value[0])! * 60)
        print(Int(value[1])!)
        numberOfMinutes = (Int(value[0])! * 60) + Int(value[1])!
        
        
        var dateComp : NSDateComponents = NSDateComponents()
        dateComp.hour = 0
        dateComp.minute = 1
        dateComp.timeZone = NSTimeZone.system
        var calendar : NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier(rawValue: NSGregorianCalendar))!
        var date : NSDate = calendar.date(from: dateComp as DateComponents)! as NSDate
        
        timePicker.setDate(date as Date, animated: true)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        Analytics.logEvent("DailyTimeLimitsViewEntered", parameters:nil)
        if getMemberDetailsbyId?.isEmpty == true
        {
            // dailyTimeLimitsTV.isHidden = true
        }
        else
        {
            dailyTimeLimitsTV.isHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("DailyTimeLimitsViewExit", parameters:nil)
    }
    
    
    @IBAction func mondayButtonTapped(_ sender: UIButton) {
        
        let monString = "Monday"
        
        if mondayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(monString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(monString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: monString)!)
                
            }
            
        }
        sender.isSelected = !sender.isSelected
        
    }
    
    @IBAction func tuesdayButtonTapped(_ sender: UIButton) {
        
        let tueString = "Tuesday"
        
        if tuesdayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(tueString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(tueString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: tueString)!)
                
            }
            
        }
        sender.isSelected = !sender.isSelected
        
    }
    @IBAction func wednesdayButtonTapped(_ sender: UIButton) {
        
        let wedString = "Wednesday"
        
        if wednesdayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(wedString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(wedString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: wedString)!)
                
            }
        }
        sender.isSelected = !sender.isSelected
    }
    @IBAction func thursdayButtonTapped(_ sender: UIButton) {
        
        let thurString = "Thursday"
        
        if thurdayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(thurString)
        }
        else
        {
            buttonUnSelected(button: sender)
            
            if (weekarray.contains(thurString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: thurString)!)
                
            }
        }
        sender.isSelected = !sender.isSelected
    }
    @IBAction func fridayButtonTapped(_ sender: UIButton) {
        
        let friString = "Friday"
        
        if fridayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(friString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(friString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: friString)!)
                
            }
        }
        sender.isSelected = !sender.isSelected
    }
    @IBAction func saturdayButtonTapped(_ sender: UIButton) {
        
        let satString = "Saturday"
        
        if saturdayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(satString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(satString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: satString)!)
                
            }
        }
        sender.isSelected = !sender.isSelected
    }
    @IBAction func sundayButtonTapped(_ sender: UIButton) {
        
        let sunString = "Sunday"
        
        if sundayButton.isSelected == false
        {
            buttonSelected(button: sender)
            weekarray.append(sunString)
        }
        else
        {
            buttonUnSelected(button: sender)
            if (weekarray.contains(sunString))
            {
                self.weekarray.remove(at: self.weekarray.index(of: sunString)!)
                
            }
        }
        sender.isSelected = !sender.isSelected
    }
    
    func buttonSelected(button:UIButton)
    {
        button.layer.cornerRadius = button.frame.height/2
        button.clipsToBounds = true
        button.layer.borderWidth = 0.5
        button.layer.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
        button.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
        button.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .selected)
    }
    func buttonUnSelected(button:UIButton)
    {
        button.layer.cornerRadius = button.frame.height/2
        button.clipsToBounds = true
        button.layer.borderWidth = 0.5
        button.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        button.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
    }
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        Analytics.logEvent("DailyTimeLimits_Screen_AddButton_clicked", parameters:nil)
        
        if timeString == nil
        {
            showAlert(title: "", msg: "Please select time")
            return
        }
        
        //        if weekarray.isEmpty
        //        {
        //            showAlert(title: Constants.alertTitle, msg: "Please select Atleast one day")
        //            return
        //        }
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(weekarray.joined(separator: ", "), forKey: "day")
        parameter.setValue(numberOfMinutes, forKey: "dailyTimeInMinutes")
        parameter.setValue(userId, forKey: "userId")
        parameter.setValue(memberId, forKey: "memberId")
        
        print(parameter)
        
        APIServices.postDataToServer(url: API.saveDailyTimeLimits, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            if status == true{
                
                //
                //                if self.weekarray.contains("monday")
                //                {
                //                    self.buttonUnSelected(button: self.self.mondayButton)
                //                }else
                //                {
                //                    //  self.buttonUnSelected(button: self.self.mondayButton)
                //                }
                
                self.weekarray.removeAll()
                self.mondayButton.isSelected = false
                self.tuesdayButton.isSelected = false
                self.wednesdayButton.isSelected = false
                self.thurdayButton.isSelected = false
                self.fridayButton.isSelected = false
                
                self.saturdayButton.isSelected = false
                
                self.sundayButton.isSelected = false
                
                self.buttonUnSelected(button: self.mondayButton)
                self.buttonUnSelected(button: self.tuesdayButton)
                self.buttonUnSelected(button: self.wednesdayButton)
                self.buttonUnSelected(button: self.thurdayButton)
                self.buttonUnSelected(button: self.fridayButton)
                self.buttonUnSelected(button: self.saturdayButton)
                self.buttonUnSelected(button: self.sundayButton)
                
                
                self.callApidailyTimeLimit()
                
                
                //      let alertController = UIAlertController(title: Constants.alertTitle, message: "Daily Time Limit added successfully", preferredStyle: .alert)
                
                
                let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                
                let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
                let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
                
                let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
                let msgAttrString = NSMutableAttributedString(string: "Daily Time Limit added successfully", attributes: msgFont)
                
                alert.setValue(titAttrString, forKey: "attributedTitle")
                alert.setValue(msgAttrString, forKey: "attributedMessage")
                
                
                let settingsAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel) { (UIAlertAction) in
                    if #available(iOS 10.0, *) {
                        //     self.navigationController?.popViewController(animated: true)
                    } else {
                        // Fallback on earlier versions
                    }
                }
                
                
                alert.addAction(settingsAction)
                self.present(alert, animated: true, completion: nil)
                print(response)
                
            }
        })
        
    }
    
    //Display Data In TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        let numOfSection: NSInteger =  getMemberDetailsbyId?.count ?? 0
        
        if numOfSection == 0
        {
            
            let noDataLabel: UILabel = UILabel(frame:CGRect(x: 0, y: 0, width: self.dailyTimeLimitsTV.frame.width, height: self.dailyTimeLimitsTV.frame.height))
            noDataLabel.backgroundColor = .white
            noDataLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            dailyTimeLimitsTV.separatorStyle = .none
            noDataLabel.text = "No daily time limits found"
            noDataLabel.textAlignment = NSTextAlignment.center
            self.dailyTimeLimitsTV.backgroundView = noDataLabel
            return 1
        }
        else
        {
            
            self.dailyTimeLimitsTV.backgroundView = nil
            
            return 1
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return getMemberDetailsbyId?.count ?? 0
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DailyTimeLimitsTableViewCell", for: indexPath) as? DailyTimeLimitsTableViewCell
        
        print(getMemberDetailsbyId?.count)
        
        if (cell == nil)
        {
            
            return cell!
        }
        if (cell != nil)
        {
            
            if getMemberDetailsbyId?[indexPath.row].dailyTimeInMinutes != nil
            {
                cell?.displayTimeLabel.text = seconds2Timestamp(intSeconds:(getMemberDetailsbyId?[indexPath.row].dailyTimeInMinutes)!)
            }
            
           
            print(cell?.displayTimeLabel.text)
            
            if self.getMemberDetailsbyId?[indexPath.row].day == "Monday"
            {
                cell!.monday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.monday.setTitleColor(UIColor.white, for: .normal)
                cell!.monday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.monday.borderWidth = 0.5
            }else
            {
                cell!.monday.backgroundColor = UIColor.white
                cell!.monday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
                cell!.monday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.monday.borderWidth = 0.5
            }
            if self.getMemberDetailsbyId?[indexPath.row].day == "Tuesday"
            {
                cell!.thuesday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.thuesday.setTitleColor(UIColor.white, for: .normal)
                cell!.thuesday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.thuesday.borderWidth = 0.5
            }else
            {
                cell!.thuesday.backgroundColor = UIColor.white
                cell!.thuesday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
                cell!.thuesday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.thuesday.borderWidth = 0.5
            }
            if self.getMemberDetailsbyId?[indexPath.row].day == "Wednesday"
            {
                cell!.wednesday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.wednesday.setTitleColor(UIColor.white, for: .normal)
                cell!.wednesday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.wednesday.borderWidth = 0.5
            }else
            {
                cell!.wednesday.backgroundColor = UIColor.white
                cell!.wednesday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
                cell!.wednesday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.wednesday.borderWidth = 0.5
            }
            if self.getMemberDetailsbyId?[indexPath.row].day == "Thursday"
            {
                cell!.thursday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.thursday.setTitleColor(UIColor.white, for: .normal)
                cell!.thursday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.thursday.borderWidth = 0.5
            }else
            {
                cell!.thursday.backgroundColor = UIColor.white
                cell!.thursday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
                cell!.thursday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.thursday.borderWidth = 0.5
            }
            if self.getMemberDetailsbyId?[indexPath.row].day == "Friday"
            {
                cell!.friday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.friday.setTitleColor(UIColor.white, for: .normal)
                cell!.friday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.friday.borderWidth = 0.5
            }else
            {
                cell!.friday.backgroundColor = UIColor.white
                cell!.friday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
                cell!.friday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.friday.borderWidth = 0.5
            }
            if self.getMemberDetailsbyId?[indexPath.row].day == "Saturday"
            {
                cell!.saturday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.saturday.setTitleColor(UIColor.white, for: .normal)
                cell!.saturday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.saturday.borderWidth = 0.5
            }else
            {
                cell!.saturday.backgroundColor = UIColor.white
                cell!.saturday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
                cell!.saturday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.saturday.borderWidth = 0.5
            }
            if self.getMemberDetailsbyId?[indexPath.row].day == "Sunday"
            {
                cell!.sunday.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.sunday.setTitleColor(UIColor.white, for: .normal)
                cell!.sunday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.sunday.borderWidth = 0.5
            }else
            {
                cell!.sunday.backgroundColor = UIColor.white
                cell!.sunday.setTitleColor(#colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1), for: .normal)
                cell!.sunday.borderColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
                cell!.sunday.borderWidth = 0.5
            }
            
            cell?.deleteButton.tag = indexPath.row
            cell?.deleteButton.addTarget(self, action: #selector(deleteDailyTimeLimits), for: .touchUpInside)
            return cell!
            
            
            //Functionality goes here when it not needed to happen every time.
        }
        
        
        return UITableViewCell()
    }
    func seconds2Timestamp(intSeconds:Int)->String {
        let mins:Int = intSeconds/60
        let hours:Int = mins/60
        let secs:Int = intSeconds%60
        
        let strTimestamp:String = ((mins<10) ? "0" : "") + String(mins) + "Hrs" + " " + ((secs<10) ? "0" : "") + String(secs) + "Mins"
        return strTimestamp
    }
    
    func callApidailyTimeLimit()
    {
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        // http://35.165.108.250:8080/CloudPeppers-V2_dev/getAllDailyTimeLimits?userId=2&memberId=1
        let addMemberUrl = API.getAllDailyTimeLimits + "?userId=\(String(describing: profileData.userId!))" + "&memberId=\(String(describing: memberId!))"
        print(addMemberUrl)
        HttpWrapper.gets(with: addMemberUrl, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatusgetDailyTimeLimit.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    
                    
                    
                    
                    self.getMemberDetailsbyId  = loginRespone.response
                    
                    //                    if self.getMemberDetailsbyId?.count == 0
                    //                    {
                    //                        self.dailyTimeLimitsTV.isHidden = true
                    //                        return
                    //                    }else
                    //                    {
                    //                        self.dailyTimeLimitsTV.isHidden = false
                    //                    }
                    //                    if self.getMemberDetailsbyId != nil
                    //                    {
                    //
                    
                    
                    self.dailyTimeLimitsTV.reloadData()
                    
                    
                    
                    //                        self.alarmReminderTableview.dataSource = self
                    //                        self.alarmReminderTableview.delegate = self
                    //
                    //                        self.alarmReminderTableview.reloadData()
                    // self.updateUi(sender: self.getMemberDetailsbyId)
                    //  }                    
                }
                else
                    
                {
                    //   SharedData.data.getMemberDetails  = nil
                }
                
            }catch let jsonErr {
                
                self.dailyTimeLimitsTV.isHidden = true
                //  SharedData.data.getMemberDetails=nil
                
                print("Error serializing json:", jsonErr)
                
            }
        }){ (error) in
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
            
            //  SharedData.data.getMemberDetails=nil
            
            
            print(error)
        }
    }
    @objc func deleteDailyTimeLimits(sender: UIButton!) {
        Analytics.logEvent("DailyTimeLimits_Screen_DeleteRestricedTime_Clicked", parameters:nil)
        
        let alertController = UIAlertController(title: "", message: "Do you want to remove?", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "NO", style: .cancel, handler: { (action : UIAlertAction!) -> Void in })
        
        let saveAction = UIAlertAction(title: "YES", style: .default, handler: { alert -> Void in
            
            // http://35.165.108.250:8080/CloudPeppers-V2_dev/deleteDailyTimeLimitsByMemberId? dailyTimeId=4&userId=4&memberId=1
            //http://35.165.108.250:8080/CloudPeppers-V2_dev/deleteDailyTimeLimitsByMemberId?dailyTimeId=60?userId=Optional(61)memberId=Optional(62)
            
            //   ttp://35.165.108.250:8080/CloudPeppers-V2_dev/deleteDailyTimeLimitsByMemberId?userId=4&memberId=1
            
            let deleteAlarm = API.deleteDailyTimeLimitsByMemberId + "?dailyTimeId=\(String(describing: (self.getMemberDetailsbyId?[sender.tag].dailyTimeId!)!))" + "&userId=\(self.userId!)" + "&memberId=\(self.memberId!)"
            print(deleteAlarm)
            
            print(self.getMemberDetailsbyId?.count)
            HttpWrapper.gets(with: deleteAlarm, parameters: nil, headers: nil, completionHandler: { (response, error)  in
                
                guard let data = response else { return }
                do {
                    
                    let loginRespone = try JSONDecoder().decode(ApiStatus1.self, from: data)
                    let loginStatus = loginRespone.status
                    if  loginStatus  ==  true
                    {
                        self.showAlert(title: "", msg: "Deleted Successfully")
                        self.callApidailyTimeLimit()
                        //   self.dailyTimeLimitsTV.reloadData()
                    }
                    
                }catch let jsonErr {
                    
                    print("Error serializing json:", jsonErr)
                }
            }){ (error) in
                
                print(error)
                if Reachability.isConnectedToNetwork(){
                    print("Internet Connection Available!")
                }else{
                    self.showAlert(title: "", msg: "No network connection")
                }
            }
            
        })
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    
    @IBAction func timePickerTapped(_ sender: UIDatePicker) {
        
        Analytics.logEvent("DailyTimeLimits_Screen_TimeChoosen_Clicked", parameters:nil)

        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "HH:mm"
        let timeData = dateFormatter.string(from: sender.date)
        timeString = timeData
        
        print(timeString!)
        
        
        let time = timeString!
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm"
        let value =  time.components(separatedBy: ":")
        print(Int(value[0])! * 60)
        print(Int(value[1])!)
        numberOfMinutes = (Int(value[0])! * 60) + Int(value[1])!
        print("numberOfMinutes :: ", numberOfMinutes)
    }
    
}



