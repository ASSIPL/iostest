//
//  ViewController.swift
//  ChatBubble
//
//  Created by Rahul kr on 06/06/18.
//

import UIKit
import AWSSNS
import IQKeyboardManagerSwift
import CoreData

protocol ChatViewControllerDelegate: class {
    func didNewChatMessagesRecieved() -> ()
}

class ChatViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var chatTXTBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var chatTable: UITableView!
    
    
    @IBOutlet var chatTXT: IQTextView!
    var getMemberDetails:responses?
    
    var name:String?
    var memberId:Int?
    var memberIds:String?
    var fromNotification:Bool?
    var plateform:String?
    var endpointArnfrom:String?
    
    
    var endARNfromAServer:String?
    var plateForm:String?
    
    
    var chatData:[ChatData] = []
    
    //MARK:- Properties
    var chatviewmodel: ChatViewModel?
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setUpInitializers()
        self.navigationController?.navigationBar.isHidden = false
        
        chatTXT.layer.borderWidth = 1.5
        chatTXT.layer.borderColor = UIColor(red: 53/255, green: 154/255, blue: 210/255, alpha: 1.0).cgColor
        chatTXT.layer.cornerRadius = 10
        chatTXT.layer.masksToBounds = true
        
        let imageDataDict:[String: Any] = ["isinChatView":"false"]
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "chatView"), object: nil,userInfo: imageDataDict)
        
        getPushNotificationData()
        navigationItem.title = name
        if getMemberDetails?.firstName != nil
        {
            
            navigationItem.title = (getMemberDetails?.firstName!)! + " " + (getMemberDetails?.lastName!)!
            
        }
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "messagenotification"), object: nil)
        //        chatviewmodel?.newchat = "jkdhgkjdahkjgd"
        //        chatviewmodel?.onItemAdded(name: "Me")
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        
        
        UserDefaults.standard.set("fromChatfalse", forKey: "fromChat")
        
        UserDefaults.standard.set(nil, forKey: "datafromNotification")
        let imageDataDict:[String: Any] = ["isinChatView":"true"]
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "chatView"), object: nil,userInfo: imageDataDict)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if fromNotification == true
        {
        }else
        {
            let Url =  API.getARNByMemberIdlan + "\((getMemberDetails?.userId)!)"
            
            HttpWrapper.gets(with: Url, parameters: nil, headers: nil, completionHandler: {(response, error)  in
                
                guard let data = response else { return }
                do {
                    
                    let loginRespone = try JSONDecoder().decode(ApiStatusARN.self, from: data)
                    let loginStatus = loginRespone.status
                    if  loginStatus  ==  true
                    {
                        self.endARNfromAServer = loginRespone.response?.snsARN
                        self.plateForm = loginRespone.response?.platform
                    }
                }catch let jsonErr {
                    
                    
                    print("Error serializing json:", jsonErr)
                    
                }
                
            }
                
                
            ){ (error) in
                
                
            }
        }
        
    }
    
    
    
    @objc func showSpinningWheel(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        
        
        getPushNotificationData()
        if let dict = notification.userInfo as NSDictionary? {
            if let id = dict["message"] as? String{
                chatviewmodel?.newchat = id
                chatviewmodel?.onItemAdded(name: "from")
                
                
                let memberid = dict["memberid"] as? String
                
                
                
                
                //
                //                if #available(iOS 10.0, *) {
                //                    let appDelegate = UIApplication.shared.delegate as? AppDelegate
                //                    let managedContext = appDelegate?.persistentContainer.viewContext
                //                    let entity: ChatData =  NSEntityDescription.insertNewObject(forEntityName: "ChatData", into: managedContext!) as! ChatData
                //
                //                    // badgeCount += 1
                //                    // UIApplication.sharedApplication().applicationIconBadgeNumber = ++badgeCount
                //                    //    UIApplication.shared.applicationIconBadgeNumber = badgeCount
                //                    print( UIApplication.shared.applicationIconBadgeNumber )
                //                    // UIApplication.shared.applicationIconBadgeNumber = 1
                //                    print(getMemberDetails?.userId!)
                //                    guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                //                        let profileData = try? JSONDecoder().decode(profile.self, from: data) else
                //                    {
                //
                //                        return
                //
                //                    }
                //
                //                    print(profileData.userId)
                //
                //
                //
                //
                //                    let memberid = String(describing: profileData.userId!)
                //
                //
                //                    entity.forme = "from"
                //                    entity.memberid = memberid
                //                    entity.message = id
                //
                //
                //                    // entity.timeStamp = String(describing: Int(NSDate().timeIntervalSince1970))
                //
                //
                //                    do
                //                    {
                //                        try managedContext?.save()
                //
                //
                //                        getPushNotificationData()
                //                    } catch let error as NSError
                //                    {
                //                        print("Could not save. \(error), \(error.userInfo)")
                //                    }
                //                }
                
            }
        }
    }
    deinit {
        print("ChatViewController de-initialized")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ChatViewController: KeyBoardNotificationDelegate,UITableViewDataSource,ChatViewControllerDelegate
{
    
    //MARK: - Custom Accessors
    
    fileprivate func setUpInitializers()
    {
        KeyboardNotificationController.sharedKC.registerforKeyBoardNotification(delegate: self)
        addOnTapDismissKeyboard()
        chatviewmodel = ChatViewModel(view: self)
    }
    
    fileprivate func scrollToBottom(){
        DispatchQueue.main.async {
            
            if self.chatData.count ?? 0 > 0
            {
                let indexPath = IndexPath(row: (self.chatData.count)-1, section: 0)
                self.chatTable.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
            
            
            
            
            //            if self.chatviewmodel?.chats.count ?? 0 > 0
            //            {
            //                let indexPath = IndexPath(row: (self.chatviewmodel?.chats.count)!-1, section: 0)
            //                self.chatTable.scrollToRow(at: indexPath, at: .bottom, animated: true)
            //            }
            
        }
    }
    
    fileprivate func addOnTapDismissKeyboard() {
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        print("**************Table view didSelect maynot work!!!!!!!!!!!!!!!")
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    
    
    func getPushNotificationData()
    {
        
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ChatData")
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        print(profileData.userId)
        print(getMemberDetails?.userId)
        
        print(memberIds)
        
        if fromNotification == true
        {
            fetchRequest.predicate = NSPredicate(format: "memberid == %@",memberIds!)
        }else
        {
            fetchRequest.predicate = NSPredicate(format: "memberid == %@",String(describing: (getMemberDetails?.userId)!))
        }
        
        
        
        
        
        let sectionSortDescriptor = NSSortDescriptor(key: "memberid", ascending: true)
        let sortDescriptors = [sectionSortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        
        //        let descriptor: NSSortDescriptor = NSSortDescriptor(key: "lastMessageDate", ascending: false)
        //        let sortedResults = arrChatDialogs?.sortedArray(using: [descriptor])
        //
        do {
            
            chatData = try context.fetch(fetchRequest) as! [ChatData]
            
            print(chatData.count)
            
            if chatData.count == 0
            {
                
            }else
            {
                
                for each in chatData
                {
                    var model1 = allNotifications()
                    
                    print(each.memberid)
                    print(each.message)
                    
                    print(each.forme)
                    
                    chatviewmodel?.newchat = each.message
                    chatviewmodel?.onItemAdded(name: each.forme!)
                    
                    
                    //
                    //                    model1.notificationText = each.body
                    //
                    //                    print(each.timeStamp)
                    //
                    //                    // print(Int(each.timeStamp!))
                    //
                    //
                    //                    model1.createdDate = Int(each.timeStamp ?? "")
                    // model1.createdDate = NSDate().timeIntervalSince1970
                    
                    
                    //allNotificationsrr.append(model1)
                    
                }
                
            }
            
        }catch
        {
            
        }
        
    }
    
    
    
    fileprivate func addChat()
    {
        chatviewmodel?.newchat = chatTXT.text
        chatviewmodel?.onItemAdded(name: "Me")
        
        if #available(iOS 10.0, *) {
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            let managedContext = appDelegate?.persistentContainer.viewContext
            let entity: ChatData =  NSEntityDescription.insertNewObject(forEntityName: "ChatData", into: managedContext!) as! ChatData
            
            // badgeCount += 1
            // UIApplication.sharedApplication().applicationIconBadgeNumber = ++badgeCount
            //    UIApplication.shared.applicationIconBadgeNumber = badgeCount
            print( UIApplication.shared.applicationIconBadgeNumber )
            // UIApplication.shared.applicationIconBadgeNumber = 1
            
            guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                let profileData = try? JSONDecoder().decode(profile.self, from: data) else
            {
                
                return
                
            }
            
            print(profileData.userId)
            
            let formatter = DateFormatter()
            // initially set the format based on your datepicker date / server String
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let myString = formatter.string(from: Date()) // string purpose I add here
            // convert your string to date
            let yourDate = formatter.date(from: myString)
            //then again set the date format whhich type of output you need
            formatter.dateFormat = "HH:mm"
            // again convert your date to string
            let myStringafd = formatter.string(from: yourDate!)
            
            print(myStringafd)
            
            
            
            entity.forme = "Me"
            
            if fromNotification == true
            {
                entity.memberid = memberIds
            }else
            {
                entity.memberid = String(describing: (getMemberDetails?.userId)!)
            }
            
            entity.message = chatTXT.text
            entity.time = myStringafd
            
            // entity.timeStamp = String(describing: Int(NSDate().timeIntervalSince1970))
            
            
            do
            {
                try managedContext?.save()
                
                
                getPushNotificationData()
            } catch let error as NSError
            {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
        
        
        
        
        let sns = AWSSNS.default()
        
        let request = AWSSNSPublishInput()
        //  let request = AWSSNSPlatformApplication()
        
        
        request?.messageStructure = "json"
        
        
        print(getMemberDetails?.userId)
        
        
        
        
        print()
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        print(profileData.userId)
        
        let userId:String?
        
        if fromNotification == true
        {
            userId = memberIds
        }else
        {
            userId = String(describing: (getMemberDetails?.userId)!)
        }
        
        // request?.platformApplicationArn = SNSPlatformApplicationArn
        
        let chattext = String(describing: (chatTXT.text)!)
        let memberid = String(describing: (profileData.userId)!)
        
        
        let name = (profileData.firstName)! + " " + (profileData.lastName!)
        
        
        let phonenumber = profileData.countryCode ?? "" + profileData.mobileNo!
        
        //    let dict = ["default": "The default message", "APNS_SANDBOX": "{\"aps\":{\"alert\": \"\(chattext)\",\"sound\":\"default\",\"content_available\": \"1\",\"priority\": \"normal\", \"badge\":\"1\",\"memberId\":\"\(memberid)\",\"userId\":\"\(userId!)\"} }"]
        
        print(UserDefaults.standard.string(forKey: "endpointArnForSNS")!)
        
        let endpointArn = UserDefaults.standard.string(forKey: "endpointArnForSNS")!
        print(endpointArn)
        var dict:[String:Any]?
        
        
        
        if plateForm != nil
        {
            getMemberDetails?.platform = plateForm
        }
        
        if getMemberDetails?.platform == "Android"
        {
           // \"notification\":{\"body\":\"\(chattext)\",\"title\":\"Coolpad FamilyLabs\"},
            
            dict = ["default":"Naresh","GCM":"{\"data\":{\"body\":\"great match!\",\"title\":\"Coolpad FamilyLabs\",\"msg\":\"\(chattext)\",\"membername\":\"\(String(describing: (profileData.firstName)!))\",\"type\":\"awssnschat\",\"memberid\":\"\(memberid)\",\"plateform\":\"iOS\",\"loginendpointarn\":\"\(endpointArn)\",\"phonenumber\":\"\(phonenumber)\"}}"]
            
            
            
        }else
        {
            
            //*********Production**********
           dict = ["default": "The default message", "APNS": "{\"aps\":{\"alert\": \"\(chattext)\",\"sound\":\"default\",\"content_available\": \"1\",\"priority\": \"normal\", \"badge\":\"1\",\"memberid\":\"\(memberid)\",\"loginendpointarn\":\"\(endpointArn)\",\"membername\":\"\(name)\",\"plateform\":\"iOS\"} }"]
            
            
            
            //*********Staging**********
         //    dict = ["default": "The default message", "APNS_SANDBOX": "{\"aps\":{\"alert\": \"\(chattext)\",\"sound\":\"default\",\"content_available\": \"1\",\"priority\": \"normal\", \"badge\":\"1\",\"memberid\":\"\(memberid)\",\"loginendpointarn\":\"\(endpointArn)\",\"membername\":\"\(name)\",\"plateform\":\"iOS\"} }"]
            
            
            
        //     dict = ["default": "The default message", "APNS_SANDBOX": "{\"aps\":{\"alert\": \"\(chattext)\",\"sound\":\"default\",\"content_available\": \"1\",\"priority\": \"normal\", \"badge\":\"1\",\"memberid\":\"\(memberid)\",\"loginendpointarn\":\"\(endpointArn)\",\"membername\":\"\(name)\",\"plateform\":\"iOS\"} }"]
            
        }
        
        
        if fromNotification == true
        {
            if plateform == "android"
            {
                dict = ["default":"Naresh","GCM":"{\"data\":{\"body\":\"great match!\",\"title\":\"Coolpad FamilyLabs\",\"msg\":\"\(chattext)\",\"membername\":\"\(String(describing: (profileData.firstName)!))\",\"type\":\"awssnschat\",\"memberid\":\"\(memberid)\",\"plateform\":\"iOS\",\"loginendpointarn\":\"\(endpointArn)\",\"phonenumber\":\"\(phonenumber)\"}}"]
                

            }else
            {
                //*********Production**********
                    dict = ["default": "The default message", "APNS": "{\"aps\":{\"alert\": \"\(chattext)\",\"sound\":\"default\",\"content_available\": \"1\",\"priority\": \"normal\", \"badge\":\"1\",\"memberid\":\"\(memberid)\",\"loginendpointarn\":\"\(endpointArn)\",\"membername\":\"\(name)\",\"plateform\":\"iOS\"} }"]
                
                
                
                //*********Staging**********
            //    dict = ["default": "The default message", "APNS_SANDBOX": "{\"aps\":{\"alert\": \"\(chattext)\",\"sound\":\"default\",\"content_available\": \"1\",\"priority\": \"normal\", \"badge\":\"1\",\"memberid\":\"\(memberid)\",\"loginendpointarn\":\"\(endpointArn)\",\"membername\":\"\(name)\",\"plateform\":\"iOS\"} }"]
                
                
                
           //     dict = ["default": "The default message", "APNS": "{\"aps\":{\"alert\": \"\(chattext)\",\"sound\":\"default\",\"content_available\": \"1\",\"priority\": \"normal\", \"badge\":\"1\",\"memberid\":\"\(memberid)\",\"loginendpointarn\":\"\(endpointArn)\",\"membername\":\"\(name)\",\"plateform\":\"iOS\"} }"]
                
        
            }
            
            
        }
        
        
        
        
        
        
        print(dict)
        
        
        //   var dict = ["default": "The default message", "APNS_SANDBOX": "{\"aps\":{\"alert\": \"YOUR_MESSAGE\",\"sound\":\"default\", \"badge\":\"1\"} }"]
        //  var dict = ["default": "The default message","GCM": "{ \"notification\": { \"text\": \"test message\" } }"]
        
        
        
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: dict)
            
            //  request?.message = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            //   request?.targetArn = "arn:aws:sns:us-east-1:167315797857:endpoint/APNS_SANDBOX/CpiOSTest/7ab4990a-fa68-3ad3-93a3-8150e301eb91"
            
            //    request?.targetArn = "arn:aws:sns:us-west-2:167315797857:app/GCM/coolpad1234/7ab4990a-fa68-3ad3-93a3-8150e301eb91"
            request?.message = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String?
            chatTXT.text = ""
            
            //NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String?
            
            
            //"test"
            //  endpointArn: arn:aws:sns:us-east-1:167315797857:endpoint/APNS_SANDBOX/CpiOSTest/03838671-edc0-32f6-9835-471cc73bd38d
            
            // request?.
            // request?.targetArn = "arn:aws:sns:us-east-1:167315797857:endpoint/APNS_SANDBOX/CpiOSTest/03838671-edc0-32f6-9835-471cc73bd38d"
            
            
            if fromNotification == true
            {
                print(endpointArnfrom)
                
                request?.targetArn = endpointArnfrom
                
            }else
            {
                print(getMemberDetails?.snsARN)
                if getMemberDetails?.snsARN != nil
                {
                    
                    if endARNfromAServer != nil
                    {
                        request?.targetArn = endARNfromAServer
                    }else
                    {
                        request?.targetArn = getMemberDetails?.snsARN!
                    }
                    
                }
                
            }
            
            
            
            
            //"arn:aws:sns:us-east-1:167315797857:endpoint/APNS_SANDBOX/CpiOSTest/7ab4990a-fa68-3ad3-93a3-8150e301eb91"
            
            
            //     request?.targetArn = "arn:aws:sns:us-west-2:167315797857:endpoint/GCM/coolpad1234/4eca7a81-1608-3ad7-a90b-237bf71d71a2"
            
            
            
            
            
            sns.publish(request!).continueWith { (task) -> AnyObject? in
                print("error \(task.error), result:; \(task.result)")
                return nil
            }
        }catch
        {
            
        }
        //
    }
    
    //MARK: - ChatViewControllerDelegate
    
    func didNewChatMessagesRecieved() {
        chatTable.reloadData()
        scrollToBottom()
    }
    
    //MARK: - KeyBoardNotificationDelegate
    
    func didKeyBoardAppeared(keyboardHeight: CGFloat) {
        
        self.chatTXTBottomConstraint.constant = (keyboardHeight == 0) ? 0 : -keyboardHeight
        chatTable.reloadData()
        scrollToBottom()
    }
    
    
    //MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatData.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        
        let msg = chatData[indexPath.row]
        
        print(msg.forme)
        
        if msg.forme == "Me"
        {
            
            //   lccell.chatLBL.text = "hadkjghkadg kadhsgkhadkjg adsgkjhadgkj asdgkjhadkjgh akjhdgkjhakjdgh adhgkjhadgkjdakhjgkdhgkjhdfgj"
            
            
            cell = tableView.dequeueReusableCell(withIdentifier: "rightbubblecell")!
            let rccell = cell as! RightChatCell
            
            
            rccell.chatLBL.text = msg.message
            rccell.rightchatTime.text = msg.time
            // rccell.configureRightChatCell(chat: (chatviewmodel?.chats[indexPath.row])!)
        }else
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "leftbubblecell")!
            let lccell = cell as! LeftChatCell
            
            
            
            lccell.chatLBL.text = msg.message
            lccell.rightchatTime.text = msg.time
            
            
            // lccell.configureLeftChatCell(chat: (chatviewmodel?.chats[indexPath.row])!)
        }
        
        //        switch indexPath.row % 2
        //        {
        //
        //        case 0:
        //
        //
        //        default:
        //            cell = tableView.dequeueReusableCell(withIdentifier: "rightbubblecell")!
        //            let rccell = cell as! RightChatCell
        //           rccell.chatLBL.text = "iyeirytieyrity yetyeiurwytiueyrwt eytiuyerwiutyerwiiueytiueryt ertiuyerwiutyerwiuty eiuyrtiuewrytewrity erityerityeritw"
        //        }
        //
        return cell
    }
    
    //MARK: - IBAction
    
    @IBAction func sendChat(_ sender: UIButton)
    {
        (chatTXT.text?.isEmpty)! ? nil : addChat()
    }
}

class RightChatCell: UITableViewCell {
    
    //MARK: - IBOutlets
    
    @IBOutlet var chatBubbleView: UIView!
    
    @IBOutlet var rightchatTime: UILabel!
    
    @IBOutlet weak var chatLBL: UILabel!
    
    //MARK: - View life cycle
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // showOutgoingMessage(width: chatBubbleView.frame.width, height:  chatBubbleView.frame.height)
        
        //        let backgroundImage = UIImageView(frame: chatBubbleView.bounds)
        //        backgroundImage.image = UIImage(named: "message_blue")
        //        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
        //        self.chatBubbleView.insertSubview(backgroundImage, at: 0)
    }
    
    func configureRightChatCell(chat: Chat)
    {
        chatLBL.text = chat.chatMessage
        
    }
    
    func showOutgoingMessage(width: CGFloat, height: CGFloat) {
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: width - 22, y: height))
        bezierPath.addLine(to: CGPoint(x: 17, y: height))
        bezierPath.addCurve(to: CGPoint(x: 0, y: height - 17), controlPoint1: CGPoint(x: 7.61, y: height), controlPoint2: CGPoint(x: 0, y: height - 7.61))
        bezierPath.addLine(to: CGPoint(x: 0, y: 17))
        bezierPath.addCurve(to: CGPoint(x: 17, y: 0), controlPoint1: CGPoint(x: 0, y: 7.61), controlPoint2: CGPoint(x: 7.61, y: 0))
        bezierPath.addLine(to: CGPoint(x: width - 21, y: 0))
        bezierPath.addCurve(to: CGPoint(x: width - 4, y: 17), controlPoint1: CGPoint(x: width - 11.61, y: 0), controlPoint2: CGPoint(x: width - 4, y: 7.61))
        bezierPath.addLine(to: CGPoint(x: width - 4, y: height - 11))
        bezierPath.addCurve(to: CGPoint(x: width, y: height), controlPoint1: CGPoint(x: width - 4, y: height - 1), controlPoint2: CGPoint(x: width, y: height))
        bezierPath.addLine(to: CGPoint(x: width + 0.05, y: height - 0.01))
        bezierPath.addCurve(to: CGPoint(x: width - 11.04, y: height - 4.04), controlPoint1: CGPoint(x: width - 4.07, y: height + 0.43), controlPoint2: CGPoint(x: width - 8.16, y: height - 1.06))
        bezierPath.addCurve(to: CGPoint(x: width - 22, y: height), controlPoint1: CGPoint(x: width - 16, y: height), controlPoint2: CGPoint(x: width - 19, y: height))
        bezierPath.close()
        
        let outgoingMessageLayer = CAShapeLayer()
        outgoingMessageLayer.path = bezierPath.cgPath
        outgoingMessageLayer.frame = CGRect(x: chatBubbleView.frame.width/2 - width/2,
                                            y: chatBubbleView.frame.height/2 - height/2,
                                            width: width,
                                            height: height)
        outgoingMessageLayer.fillColor = UIColor(red: 0.09, green: 0.54, blue: 1, alpha: 1).cgColor
        
        chatBubbleView.layer.addSublayer(outgoingMessageLayer)
        
        chatBubbleView.addSubview(chatLBL)
    }
}

class LeftChatCell: UITableViewCell {
    
    //MARK: - IBOutlets
    
    
    @IBOutlet var rightchatTime: UILabel!
    @IBOutlet weak var chatLBL: UILabel!
    
    //MARK: - View life cycle
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func configureLeftChatCell(chat: Chat)
    {
        chatLBL.text = chat.chatMessage
    }
}

