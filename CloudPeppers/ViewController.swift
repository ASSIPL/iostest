//
//  ViewController.swift
//  CloudPeppers
//
//  Created by Allvy on 14/03/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var btnMenuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btnMenuButton.target = revealViewController()
        
        btnMenuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
}


