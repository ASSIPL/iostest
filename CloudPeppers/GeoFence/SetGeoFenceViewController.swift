//
//  SetGeoFenceViewController.swift
//  CloudPeppers
//
//  Created by Ashok Reddy G on 25/04/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import GoogleMaps
import SVProgressHUD
import Firebase

class SetGeoFenceViewController: UIViewController,CLLocationManagerDelegate {
    
    @IBOutlet weak var deleteFenceButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var geoFenceMapView: GMSMapView!
    @IBOutlet var addFenceButton: UIButton!
    
    var titleName:String?
    var membersGeoList:MembersGeoList?
    @IBOutlet weak var fenceCountLabel: UILabel!
    let locationManager : CLLocationManager = CLLocationManager()
    var firstLocValue: CLLocationCoordinate2D? = nil
    var IndividualMemberFenceresponse: [[IndividualMemberFence]] = [[]]
    var fromcreated:Bool? = false
    
    var FenceIndex = 0
    
    var rightButtonItem:UIBarButtonItem?
    
    var memberId:Int?
    var userId:Int?
    
    var notificationTap: Bool?
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.logEvent("SetGeoFenceViewenter", parameters:nil)
        
        if SharedData.data.membersGeoList?.count ?? 0 > 0
        {
            for each in SharedData.data.membersGeoList!
            {
                if each.userId == memberId
                {
                    let lat = each.latitude ?? ""
                    let Latitude = Double(lat)
                    let lng =  each.longitude ?? ""
                    let longitude = Double(lng)
                    
                    firstLocValue = CLLocationCoordinate2D(latitude: Latitude!, longitude:  longitude!)
                    
                    
                    
                }
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification2(notification:)), name: Notification.Name("createdFence"), object: nil)
        
        
      //  let imageDataDict:[String: Any] = ["memberid": memberId as Any]
        
      //   NotificationCenter.default.post(name: NSNotification.Name(rawValue: "geo"), object: nil,userInfo: imageDataDict)

        
        
        
        rightButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "whitePlus"), style: .done, target: self, action: #selector(rightButtonAction(sender:)))
        
        //        let rightButtonItem = UIBarButtonItem.init(
        //            title: "Add",
        //            style: .done,
        //            target: self,
        //            action:#selector(rightButtonAction(sender:))
        //        )
        self.navigationItem.rightBarButtonItem = rightButtonItem
        
        if titleName != nil
        {
            self.title = titleName
        }
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                
            case  .restricted, .denied:
                print("No access")
                showLocationSettingAlert()
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            case .notDetermined:
                break
            }
        } else {
            print("Location services are not enabled")
            showLocationSettingAlert()
        }
        geoFenceMapView.isMyLocationEnabled = true
        geoFenceMapView.settings.myLocationButton = true
        // geoFenceMapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: self.view.frame.height - 200, right: 5)
        
        getFencesFromServer()
        
        
        
    }
    
    @objc func methodOfReceivedNotification2(notification: Notification) {
        if let dict = notification.userInfo as NSDictionary? {
            if let id = dict["fromcreated"] as? Bool{
                fromcreated = id
                print(fromcreated)
            }}
        getFencesFromServer()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Analytics.logEvent("SetGeoFenceViewexit", parameters:nil)
        
        
    }
    
    @IBAction func showAnotherfence(_ sender: UIButton) {
        Analytics.logEvent("SetGeofence_Screen_PagenationFenceShow_Clicked", parameters:nil)
        
        
        if sender.tag == 0{
            if FenceIndex > 0
            {
                FenceIndex = FenceIndex - 1
            }
        }
        else{
            if FenceIndex < IndividualMemberFenceresponse.count - 1
            {
                FenceIndex = FenceIndex + 1
            }
        }
        drawFenceof(index: FenceIndex)
    }
    
    func getFencesFromServer(){
        
        let getFenceByIndividualMemberUrl = API.getFenceByIndividualMember + "?userId=\(String(describing:userId!))" + "&memberId=\(String(describing: memberId!))"
        
        print(getFenceByIndividualMemberUrl)
        
        HttpWrapper.gets(with: getFenceByIndividualMemberUrl, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            //  print(response)
            
            SVProgressHUD.dismiss()
            guard let data = response else { return }
            do {
                
                let loginRespone = try JSONDecoder().decode(ApiStatus1.self, from: data)
                let loginStatus = loginRespone.status
                if  loginStatus  ==  true
                {
                    self.FenceIndex = 0
                    let geoFenceResponse = try JSONDecoder().decode(ApiStatusgetIndividualMember.self, from: data)
                    if geoFenceResponse.response == nil{
                        self.IndividualMemberFenceresponse  = [[]]
                        self.rightButtonItem = UIBarButtonItem.init(image:UIImage(named: ""), style: .done, target: self, action: #selector(self.rightButtonAction(sender:)))
                        self.rightButtonItem?.isEnabled = false
                        self.navigationItem.rightBarButtonItem = self.rightButtonItem
                        
                    }else{
                        self.IndividualMemberFenceresponse  = geoFenceResponse.response!
                        
                        
                        if self.fromcreated == true
                        {
                            self.IndividualMemberFenceresponse.reverse()
                            print("response reversed")
                        }
                        self.rightButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "whitePlus"), style: .done, target: self, action: #selector(self.rightButtonAction(sender:)))
                        
                        self.rightButtonItem?.isEnabled = true
                        self.navigationItem.rightBarButtonItem = self.rightButtonItem
                    }
                    
                    //  self.drawFenceof(index: self.FenceIndex)
                
                    
                    self.notificationTap =  self.defaults.bool(forKey:"notificationTap")
                    
                    if self.notificationTap == true
                    {
                    let notificatificationFenceId = self.defaults.object(forKey: "notificationFenceID") as! String
                        var i = 0
                        for item in self.IndividualMemberFenceresponse
                            
                        {
                            var itemDetails: [IndividualMemberFence] = item
                            let geoFenceId = itemDetails[0].geoId
                            print("geoFence ID's \(String(describing: geoFenceId))")
                            
                            let notId = Int(notificatificationFenceId)
                            
                            if geoFenceId == notId
                                
                            {
                                print("get the i value \(i)")
                                self.FenceIndex = i
                                
                                self.drawFenceof(index: self.FenceIndex)
                            }
                            
                            i += 1
                            
                        }
                        
                    }
                    else{
                        
                        self.drawFenceof(index: self.FenceIndex)
                    }
                    
                }
                //                else{
                //                    self.drawFenceof(index: 0)
                //
                //                }
            }catch let jsonErr {
                
                
                print("Error serializing json:", jsonErr)
            }
        }){ (error) in
            
            print(error)
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
    }
    
    @IBAction func handleDeleteFence(_ sender: UIButton) {
        
        Analytics.logEvent("SetGeofence_Screen_DeleteFence_Clicked", parameters:nil)
        
          defaults.set(false, forKey: "notificationTap")
        
        if IndividualMemberFenceresponse[0].count == 0
        {
            return
        }
        
        //    let alertController = UIAlertController(title: Constants.alertTitle, message: "Are you sure you want to delete the fence?", preferredStyle: .alert)
        
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        
        let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
        let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
        
        let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
        let msgAttrString = NSMutableAttributedString(string: "Are you sure you want to delete the fence?", attributes: msgFont)
        
        alert.setValue(titAttrString, forKey: "attributedTitle")
        alert.setValue(msgAttrString, forKey: "attributedMessage")
        
        
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("NO", comment: ""), style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: NSLocalizedString("YES", comment: ""), style: .default) { (UIAlertAction) in
            
            let deleteFenceURL = API.deleteFence + "?serialKey=\(self.IndividualMemberFenceresponse[self.FenceIndex][0].serialKey ?? "")&memberId=\(self.IndividualMemberFenceresponse[self.FenceIndex][0].memberId ?? 0)"
            
            let fencename = self.IndividualMemberFenceresponse[self.FenceIndex][0].name
            
            APIServices.getDataFromServer(url:deleteFenceURL, parameters:nil, controller : self, headers1: nil,completionHandler: { (status,response) in
                print(response)
                if status == true{
                    print(response)
                    
                    self.getFencesFromServer()
                    
                    //    let alertController = UIAlertController(title: Constants.alertTitle, message: "\(String(describing: fencename!)) Fence deleted successfully", preferredStyle: .alert)
                    
                    let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                    
                    let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
                    let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
                    
                    let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
                    let msgAttrString = NSMutableAttributedString(string: "\(String(describing: fencename!)) Fence deleted successfully", attributes: msgFont)
                    
                    alert.setValue(titAttrString, forKey: "attributedTitle")
                    alert.setValue(msgAttrString, forKey: "attributedMessage")
                    
                    
                    let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                        
                    }
                    
                    alert.addAction(settingsAction)
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
            })
        }
        
        alert.addAction(cancelAction)
        alert.addAction(settingsAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func addFenceAction(_ sender: Any) {
        
        Analytics.logEvent("SetGeofence_Screen_AddFenceButton_Clicked", parameters:nil)
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddGeoFenceViewController") as! AddGeoFenceViewController
        vc.memberId = self.memberId
        vc.firstLocValue = firstLocValue
        vc.userId = self.userId
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func drawFenceof(index:NSInteger)
    {
        geoFenceMapView.clear()
        
        
        if self.notificationTap == true
        {
            
            var notifiFenceLocation: [IndividualMemberFence] = IndividualMemberFenceresponse[index]
            
            let notiLat = (notifiFenceLocation[0].latitude! as NSString).doubleValue
            let notiLong = (notifiFenceLocation[0].longitude! as NSString).doubleValue
            
            let notifiCoordinate = CLLocationCoordinate2D(latitude: notiLat, longitude: notiLong)
            
            
            if (firstLocValue?.latitude == notifiCoordinate.latitude && firstLocValue?.longitude == notifiCoordinate.longitude)
            {
                print("Equal coordinate")
                
                
                if IndividualMemberFenceresponse[0].count == 0
                {
                    if firstLocValue != nil{
                        geoFenceMapView.camera = GMSCameraPosition.camera(withTarget: firstLocValue!, zoom: 18.0)
                        let marker = GMSMarker()
                        let icon = UIImage(named: "mapLocation")
                        marker.position   = firstLocValue!
                        marker.icon                     = icon
                        marker.map = self.geoFenceMapView
                        
                        // show your device location on map
                        
                    }
                    
                    deleteFenceButton.isHidden = true
                    leftButton.isHidden = true
                    rightButton.isHidden = true
                    fenceCountLabel.isHidden = true
                    addFenceButton.isHidden = false
                    
                    return
                }else{
                    deleteFenceButton.isHidden = false
                    rightButton.isHidden = false
                    leftButton.isHidden = false
                    fenceCountLabel.isHidden = false
                    addFenceButton.isHidden = true
                    
                    if index + 1 >= IndividualMemberFenceresponse.count
                    {
                        rightButton.isHidden = true
                    }
                    if index <= 0 {
                        leftButton.isHidden = true
                    }
                }
                
                var fenceLocations: [IndividualMemberFence] = IndividualMemberFenceresponse[index]
                if  fenceLocations[0].name != nil
                {
                    fenceCountLabel.numberOfLines = 0
                    fenceCountLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
                    fenceCountLabel.text = "Fence Name : \(String(describing: (fenceLocations[0].name!)))\n" + "Showing \(index+1) fence out of \(IndividualMemberFenceresponse.count)"
                    // fenceCountLabel.sizeToFit()
                }else
                {
                    fenceCountLabel.numberOfLines = 0
                    fenceCountLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
                    fenceCountLabel.text = "Showing \(index+1) fence out of \(IndividualMemberFenceresponse.count)"
                    //  fenceCountLabel.sizeToFit()
                }
                //"Showing \(index+1) fence out of \(IndividualMemberFenceresponse.count)"
                if fenceLocations[0].isRadiusOrNot == "true"{
                    
                    Analytics.logEvent("SetGeofence_Screen_CircleFence_Created", parameters:nil)
                    
                    let lat = (fenceLocations[0].latitude! as NSString).doubleValue
                    let lon = (fenceLocations[0].longitude! as NSString).doubleValue
                    //let userId = each.key as! String
                    
                    //let complainLoc = CLLocationCoordinate2DMake(lat, lon)
                    
                    let Coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                    
                    let position = Coordinates
                    //            let marker = GMSMarker(position: position)
                    //            marker.map = geoFenceMapView
                    
                    let circle = GMSCircle()
                    circle.radius = CLLocationDistance((fenceLocations[0].radius ?? 0) * (Constants.FeetsForMeters)) // miles to meters
                    circle.fillColor = UIColor.blue.withAlphaComponent(0.1)
                    circle.position = Coordinates// Your CLLocationCoordinate2D  position
                    circle.strokeWidth = 1;
                    circle.strokeColor = UIColor.black
                    circle.map = geoFenceMapView; // Add it to the map
                    
                    
                    
                    if firstLocValue != nil {
                        let marker = GMSMarker()
                        
                        let icon = UIImage(named: "mapLocation")
                        
                        marker.position   = firstLocValue!
                        
                        marker.icon                     = icon
                        
                        marker.map = self.geoFenceMapView
                    }
                    
                    
                    
                    let update = GMSCameraUpdate.fit(circle.bounds())
                    geoFenceMapView.animate(with: update)
                    
                }
                else{
                    Analytics.logEvent("SetGeofence_Screen_PolygoneFence_Created", parameters:nil)
                    let rect = GMSMutablePath()
                    let polygon = GMSPolygon()
                    
                    var bounds: GMSCoordinateBounds = GMSCoordinateBounds()
                    for each in fenceLocations{
                        let lat = (each.latitude! as NSString).doubleValue
                        let lon = (each.longitude! as NSString).doubleValue
                        let Coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                        
                        bounds = bounds.includingCoordinate(Coordinates)
                        rect.add(Coordinates)
                    }
                    
                    polygon.path = rect
                    polygon.fillColor = UIColor.blue.withAlphaComponent(0.1)
                    polygon.strokeColor = UIColor.black
                    polygon.strokeWidth = 1.5
                    polygon.map = geoFenceMapView
                    
                    
                    if firstLocValue != nil{
                        let marker = GMSMarker()
                        
                        let icon = UIImage(named: "mapLocation")
                        
                        marker.position   = firstLocValue!
                        
                        marker.icon                     = icon
                        
                        marker.map = self.geoFenceMapView
                        
                    }
                    
                    
                    geoFenceMapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 70))
                }
                
                
            }
            else
            {
                print("Not Equal coordinate")
                
                
                if IndividualMemberFenceresponse[0].count == 0
                {
                    if firstLocValue != nil{
                        geoFenceMapView.camera = GMSCameraPosition.camera(withTarget: firstLocValue!, zoom: 18.0)
                        let marker = GMSMarker()
                        let icon = UIImage(named: "mapLocation")
                        marker.position   = firstLocValue!
                        marker.icon                     = icon
                        marker.map = self.geoFenceMapView
                        
                        // show your device location on map
                        
                    }
                    
                    deleteFenceButton.isHidden = true
                    leftButton.isHidden = true
                    rightButton.isHidden = true
                    fenceCountLabel.isHidden = true
                    addFenceButton.isHidden = false
                    
                    return
                }else{
                    deleteFenceButton.isHidden = false
                    rightButton.isHidden = false
                    leftButton.isHidden = false
                    fenceCountLabel.isHidden = false
                    addFenceButton.isHidden = true
                    
                    if index + 1 >= IndividualMemberFenceresponse.count
                    {
                        rightButton.isHidden = true
                    }
                    if index <= 0 {
                        leftButton.isHidden = true
                    }
                }
                
                var fenceLocations: [IndividualMemberFence] = IndividualMemberFenceresponse[index]
                if  fenceLocations[0].name != nil
                {
                    fenceCountLabel.numberOfLines = 0
                    fenceCountLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
                    fenceCountLabel.text = "Fence Name : \(String(describing: (fenceLocations[0].name!)))\n" + "Showing \(index+1) fence out of \(IndividualMemberFenceresponse.count)"
                    // fenceCountLabel.sizeToFit()
                }else
                {
                    fenceCountLabel.numberOfLines = 0
                    fenceCountLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
                    fenceCountLabel.text = "Showing \(index+1) fence out of \(IndividualMemberFenceresponse.count)"
                    //  fenceCountLabel.sizeToFit()
                }
                //"Showing \(index+1) fence out of \(IndividualMemberFenceresponse.count)"
                if fenceLocations[0].isRadiusOrNot == "true"{
                    
                    Analytics.logEvent("SetGeofence_Screen_CircleFence_Created", parameters:nil)
                    
                    if firstLocValue != nil {
                        geoFenceMapView.camera = GMSCameraPosition.camera(withTarget: firstLocValue!, zoom: 18.0)
                        let marker = GMSMarker()
                        
                        let icon = UIImage(named: "mapLocation")
                        
                        marker.position   = firstLocValue!
                        
                        marker.icon                     = icon
                        
                        marker.map = self.geoFenceMapView
                    }
                    
                    let lat = (fenceLocations[0].latitude! as NSString).doubleValue
                    let lon = (fenceLocations[0].longitude! as NSString).doubleValue
                    //let userId = each.key as! String
                    
                    //let complainLoc = CLLocationCoordinate2DMake(lat, lon)
                    
                    let Coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                    
                    let position = Coordinates
                    //            let marker = GMSMarker(position: position)
                    //            marker.map = geoFenceMapView
                    
                    let circle = GMSCircle()
                    circle.radius = CLLocationDistance((fenceLocations[0].radius ?? 0) * (Constants.FeetsForMeters)) // miles to meters
                    circle.fillColor = UIColor.blue.withAlphaComponent(0.1)
                    circle.position = Coordinates// Your CLLocationCoordinate2D  position
                    circle.strokeWidth = 1;
                    circle.strokeColor = UIColor.black
                    circle.map = geoFenceMapView; // Add it to the map
                    
                    
                    
                    
                    //    let update = GMSCameraUpdate.fit(circle.bounds())
                    // geoFenceMapView.animate(with: update)
                    
                }
                else{
                    Analytics.logEvent("SetGeofence_Screen_PolygoneFence_Created", parameters:nil)
                    
                    
                    if firstLocValue != nil{
                        geoFenceMapView.camera = GMSCameraPosition.camera(withTarget: firstLocValue!, zoom: 18.0)
                        let marker = GMSMarker()
                        
                        let icon = UIImage(named: "mapLocation")
                        
                        marker.position   = firstLocValue!
                        
                        marker.icon                     = icon
                        
                        marker.map = self.geoFenceMapView
                        
                    }
                    
                    let rect = GMSMutablePath()
                    let polygon = GMSPolygon()
                    
                    var bounds: GMSCoordinateBounds = GMSCoordinateBounds()
                    for each in fenceLocations{
                        let lat = (each.latitude! as NSString).doubleValue
                        let lon = (each.longitude! as NSString).doubleValue
                        let Coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                        
                        bounds = bounds.includingCoordinate(Coordinates)
                        rect.add(Coordinates)
                    }
                    
                    polygon.path = rect
                    polygon.fillColor = UIColor.blue.withAlphaComponent(0.1)
                    polygon.strokeColor = UIColor.black
                    polygon.strokeWidth = 1.5
                    polygon.map = geoFenceMapView
                    
                    
                    
                    //  geoFenceMapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 70))
                }
                
                
                
            }
            
            
        }
        else
        {
            
            if IndividualMemberFenceresponse[0].count == 0
            {
                if firstLocValue != nil{
                    geoFenceMapView.camera = GMSCameraPosition.camera(withTarget: firstLocValue!, zoom: 18.0)
                    let marker = GMSMarker()
                    let icon = UIImage(named: "mapLocation")
                    marker.position   = firstLocValue!
                    marker.icon                     = icon
                    marker.map = self.geoFenceMapView
                    
                    // show your device location on map
                    
                }
                
                deleteFenceButton.isHidden = true
                leftButton.isHidden = true
                rightButton.isHidden = true
                fenceCountLabel.isHidden = true
                addFenceButton.isHidden = false
                
                return
            }else{
                deleteFenceButton.isHidden = false
                rightButton.isHidden = false
                leftButton.isHidden = false
                fenceCountLabel.isHidden = false
                addFenceButton.isHidden = true
                
                if index + 1 >= IndividualMemberFenceresponse.count
                {
                    rightButton.isHidden = true
                }
                if index <= 0 {
                    leftButton.isHidden = true
                }
            }
            
            var fenceLocations: [IndividualMemberFence] = IndividualMemberFenceresponse[index]
            if  fenceLocations[0].name != nil
            {
                fenceCountLabel.numberOfLines = 0
                fenceCountLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
                fenceCountLabel.text = "Fence Name : \(String(describing: (fenceLocations[0].name!)))\n" + "Showing \(index+1) fence out of \(IndividualMemberFenceresponse.count)"
                // fenceCountLabel.sizeToFit()
            }else
            {
                fenceCountLabel.numberOfLines = 0
                fenceCountLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
                fenceCountLabel.text = "Showing \(index+1) fence out of \(IndividualMemberFenceresponse.count)"
                //  fenceCountLabel.sizeToFit()
            }
            //"Showing \(index+1) fence out of \(IndividualMemberFenceresponse.count)"
            if fenceLocations[0].isRadiusOrNot == "true"{
                
                Analytics.logEvent("SetGeofence_Screen_CircleFence_Created", parameters:nil)
                
                let lat = (fenceLocations[0].latitude! as NSString).doubleValue
                let lon = (fenceLocations[0].longitude! as NSString).doubleValue
                //let userId = each.key as! String
                
                //let complainLoc = CLLocationCoordinate2DMake(lat, lon)
                
                let Coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                
                let position = Coordinates
                //            let marker = GMSMarker(position: position)
                //            marker.map = geoFenceMapView
                
                let circle = GMSCircle()
                circle.radius = CLLocationDistance((fenceLocations[0].radius ?? 0) * (Constants.FeetsForMeters)) // miles to meters
                circle.fillColor = UIColor.blue.withAlphaComponent(0.1)
                circle.position = Coordinates// Your CLLocationCoordinate2D  position
                circle.strokeWidth = 1;
                circle.strokeColor = UIColor.black
                circle.map = geoFenceMapView; // Add it to the map
                
                
                
                if firstLocValue != nil {
                    let marker = GMSMarker()
                    
                    let icon = UIImage(named: "mapLocation")
                    
                    marker.position   = firstLocValue!
                    
                    marker.icon                     = icon
                    
                    marker.map = self.geoFenceMapView
                }
                
                
                
                let update = GMSCameraUpdate.fit(circle.bounds())
                geoFenceMapView.animate(with: update)
                
                
            }
            else{
                Analytics.logEvent("SetGeofence_Screen_PolygoneFence_Created", parameters:nil)
                let rect = GMSMutablePath()
                let polygon = GMSPolygon()
                
                var bounds: GMSCoordinateBounds = GMSCoordinateBounds()
                for each in fenceLocations{
                    let lat = (each.latitude! as NSString).doubleValue
                    let lon = (each.longitude! as NSString).doubleValue
                    let Coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                    
                    bounds = bounds.includingCoordinate(Coordinates)
                    rect.add(Coordinates)
                }
                
                polygon.path = rect
                polygon.fillColor = UIColor.blue.withAlphaComponent(0.1)
                polygon.strokeColor = UIColor.black
                polygon.strokeWidth = 1.5
                polygon.map = geoFenceMapView
                
                
                if firstLocValue != nil{
                    let marker = GMSMarker()
                    
                    let icon = UIImage(named: "mapLocation")
                    
                    marker.position   = firstLocValue!
                    
                    marker.icon                     = icon
                    
                    marker.map = self.geoFenceMapView
                    
                }
                
                
                
                geoFenceMapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 70))
            }
            
            
        }
        
        
        
    }
    
    @objc func rightButtonAction(sender: UIBarButtonItem)
    {
        defaults.set(false, forKey: "notificationTap")
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddGeoFenceViewController") as! AddGeoFenceViewController
        vc.memberId = self.memberId
        vc.userId = self.userId
        vc.IndividualMemberFenceresponse = IndividualMemberFenceresponse
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func showLocationSettingAlert(){
        let alertController = UIAlertController(title: "", message: "Location Access Disabled, please eneble them", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (UIAlertAction) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)! as URL, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if firstLocValue == nil{
            firstLocValue = manager.location!.coordinate
            if IndividualMemberFenceresponse[0].count == 0
            {
                geoFenceMapView.camera = GMSCameraPosition.camera(withTarget: firstLocValue!, zoom: 18.0) // show your device location on map
                
            }
        }
    }
}
extension GMSCircle {
    
    func bounds() -> GMSCoordinateBounds {
        
        func locationMinMax(positive : Bool) -> CLLocationCoordinate2D {
            
            let sign:Double = positive ? 1 : -1
            let dx  = sign * self.radius  / 6378000 * (180/Double.pi)
            let lat = position.latitude + dx
            let lon = position.longitude + dx / cos(position.latitude * .pi/180)
            
            return CLLocationCoordinate2D(latitude: lat, longitude: lon)
        }
        
        return GMSCoordinateBounds(coordinate: locationMinMax(positive: true),
                                   coordinate: locationMinMax(positive: false))
    }
    
}


