//
//  AddGeoFenceViewController.swift
//  CloudPeppers
//
//  Created by APPLE on 4/25/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Firebase


class AddGeoFenceViewController: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate,GMSAutocompleteViewControllerDelegate ,UISearchBarDelegate,UITextFieldDelegate{
    @IBOutlet weak var locationSearchFiels: UISearchBar!
    @IBOutlet weak var addFenceMapView: GMSMapView!
    @IBOutlet var addfenceButton: UIButton!
    
    
    @IBOutlet var fenceBgView: UIView!
    @IBOutlet weak var fenceAlertBgView: UIView!
    @IBOutlet weak var alertTxtFld: UITextField!
    @IBOutlet weak var alertCancelBtn: UIButton!
    @IBOutlet weak var alertOkayBtn: UIButton!
    
    var firstLocValue: CLLocationCoordinate2D?
    let locationManager : CLLocationManager = CLLocationManager()
    var placesClient: GMSPlacesClient!
    var geoFanceLocations : [CLLocationCoordinate2D] = []
    let circle = GMSCircle()
    var polygon = GMSPolygon()
    
    var memberId:Int?
    var userId:Int?
     var leftButtonItem:UIBarButtonItem?
    var IndividualMemberFenceresponse: [[IndividualMemberFence]] = [[]]
    
    var fenceradius :Float = 0.0;
    
    @IBOutlet weak var geoFenceSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fenceAlertBgView.dropShadow()
        alertTxtFld.useUnderline()
        if firstLocValue != nil{
            addFenceMapView.camera = GMSCameraPosition.camera(withTarget: firstLocValue!, zoom: 18.0)
            let marker = GMSMarker()
            let icon = UIImage(named: "mapLocation")
            marker.position   = firstLocValue!
            marker.icon                     = icon
            marker.map = self.addFenceMapView
            
            // show your device location on map
            
        }
        
        
        leftButtonItem = UIBarButtonItem.init(title: "Clear", style: .done, target: self, action: #selector(rightButtonAction(sender:)))
            
            
            
           // UIBarButtonItem.init(image: #imageLiteral(resourceName: "whitePlus"), style: .done, target: self, action: #selector(rightButtonAction(sender:)))
        
        //        let rightButtonItem = UIBarButtonItem.init(
        //            title: "Add",
        //            style: .done,
        //            target: self,
        //            action:#selector(rightButtonAction(sender:))
        //        )
        self.navigationItem.rightBarButtonItem = leftButtonItem
//        if SharedData.data.membersGeoList?.count ?? 0 > 0
//        {
//            for each in SharedData.data.membersGeoList!
//            {
//                if each.userId == memberId
//                {
//                    let lat = each.latitude ?? ""
//                    let Latitude = Double(lat)
//                    let lng =  each.longitude ?? ""
//                    let longitude = Double(lng)
//                    
//                    firstLocValue = CLLocationCoordinate2D(latitude: Latitude!, longitude:  longitude!)
//                    
//                    
//                    
//                }
//            }
//        }

        
        addfenceButton.changeButtonCornerRadius()
        addFenceMapView.isMyLocationEnabled = true
        addFenceMapView.settings.myLocationButton = true
        addFenceMapView.delegate = self
        
        placesClient = GMSPlacesClient.shared()
        self.locationSearchFiels.delegate     = self
        
        
        
        self.fenceBgView.center.x = self.view.center.x
        self.fenceBgView.center.y = self.view.center.y
        
        fenceBgView.frame = self.view.frame
        fenceBgView.isHidden = true
        fenceBgView.layer.cornerRadius = 20
        fenceBgView.clipsToBounds = true
        fenceAlertBgView.dropShadow()
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @objc func rightButtonAction(sender: UIBarButtonItem)
    {
        self.geoFenceSlider.value = 0
         self.geoFanceLocations.removeAll()
         fenceBgView.isHidden = true
          addFenceMapView.clear()
    }
    
    func showPlacesSDK() {
        
        Analytics.logEvent("SetGeofence_Screen_SearchLocation_Clicked", parameters:nil)

        
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        //        UINavigationBar.appearance().barTintColor = UIColor.white
        //
        //        UINavigationBar.appearance().tintColor = UIColor.white
        //        UISearchBar.appearance().tintColor = UIColor.white
        
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func HandleAddFence(_ sender: UIButton) {
        
        if geoFanceLocations.count == 0
        {
            if self.geoFenceSlider.value == 0
            {
                self.showAlert(title: "", msg: "Please create the fence")
                return
                
            }
            
        }
        
        fenceBgView.isHidden = false
        self.view.addSubview(fenceBgView)
        CustomDesignButtonsMethod()
        
        /*
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Name"
            textField.delegate = self
            
        }
        let saveAction = UIAlertAction(title: "Ok", style: .default, handler: { alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            
            if firstTextField.text == nil || (firstTextField.text?.isEmpty)!
            {
                
                self.showAlert(title: "", msg: "Fence name should not empty")
                return
            }
            
            for each in self.IndividualMemberFenceresponse
            {
                for eachs in each
                {
                    
                    if firstTextField.text == eachs.name
                    {
                        
                        firstTextField.text = nil
                        let alertController = UIAlertController(title: "", message: "This fence name already exists", preferredStyle: .alert)
                        
                        let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .cancel, handler: nil)
                        alertController.addAction(cancelAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        return
                        
                    }
                }
                
                
            } 
            
            let rect = GMSMutablePath()
            
            for each in self.geoFanceLocations{
                rect.add(each)
            }
            
            self.polygon.path = rect
            self.polygon.fillColor = UIColor.blue.withAlphaComponent(0.1)
            self.polygon.strokeColor = UIColor.black
            self.polygon.strokeWidth = 1
            self.polygon.map = self.addFenceMapView
            
            if self.geoFenceSlider.value == 0{
                self.sendFenceDetailsToServer(withCircular: false,Value: self.fenceradius,name:firstTextField.text!)
            }
            else{
                
                self.sendFenceDetailsToServer(withCircular: true,Value: self.fenceradius,name: firstTextField.text!)
            }
            self.geoFanceLocations.removeAll()
            
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        
        self.present(alertController, animated: true, completion: nil)
        */
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 10
    }
    func sendFenceDetailsToServer(withCircular:Bool,Value:Float,name:String){
        
        let parameter:NSMutableDictionary = NSMutableDictionary()
        
        parameter.setValue(memberId, forKey: "memberId")
        parameter.setValue(userId, forKey: "userId")
        parameter.setValue(withCircular, forKey: "isRadiusOrNot")
        parameter.setValue(name, forKey: "name")
        
        var locationsArray : [NSMutableDictionary] = []
        
        if withCircular == true{
            parameter.setValue(Value , forKey: "redius")//MilestoFeetsTosendserver
            let location:NSMutableDictionary = NSMutableDictionary()
            location.setValue("\(firstLocValue?.latitude ?? 0)", forKey: "latitude")
            location.setValue("\(firstLocValue?.longitude ?? 0)", forKey: "longitude")
            locationsArray.append(location)
            
        }
        else{
            
            for each in geoFanceLocations{ 
                let location:NSMutableDictionary = NSMutableDictionary()
                location.setValue("\(each.latitude)", forKey: "latitude")
                location.setValue("\(each.longitude)", forKey: "longitude")
                locationsArray.append(location)
                
            }
        }
        
        parameter.setObject(locationsArray, forKey: "latLog" as NSCopying)
        
        print(parameter)
        
        
        APIServices.postDataToServer(url: API.createFence, parameters: parameter, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
            print(response)
            if status == true{
                
                //     let alertController = UIAlertController(title: Constants.alertTitle, message: "Fence has been created successfully", preferredStyle: .alert)
                
                let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                
                let titFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
                let msgFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 15.0)!]
                
                let titAttrString = NSMutableAttributedString(string: "", attributes: titFont)
                let msgAttrString = NSMutableAttributedString(string: "Fence has been created successfully", attributes: msgFont)
                
                alert.setValue(titAttrString, forKey: "attributedTitle")
                alert.setValue(msgAttrString, forKey: "attributedMessage")
                
                
                let settingsAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                    if #available(iOS 10.0, *) {
                        
                        let imageDataDict:[String: Bool] = ["fromcreated": true]
                        
            NotificationCenter.default.post(name: Notification.Name("createdFence"), object: nil,userInfo: imageDataDict)
                        
                        
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        // Fallback on earlier versions
                    }
                }
                
                
                alert.addAction(settingsAction)
                self.present(alert, animated: true, completion: nil)
                
            }
        })
        
    }
    
    
    
    @IBAction func handleCircularFence(_ sender: UISlider) {
        addFenceMapView.clear()
        
        
        if firstLocValue == nil{
            showAlert(title: "", msg: "Please long tap on map from where you want to draw fence ")
            return
        }
        
        let currentValue = Float(sender.value)
        fenceradius = currentValue
        
                let position = firstLocValue
                let marker = GMSMarker(position: position!)
                marker.map = addFenceMapView
        
//        let markers = GMSMarker()
//     //   let icon = UIImage(named: "mapLocation")
//        markers.position   = firstLocValue!
//    //    markers.icon                     = icon
//        markers.map = self.addFenceMapView
//        
        
        print("Slider changing to \(currentValue) ?")
        //circle.radius = CLLocationDistance(currentValue * 1000) // Km to Meters
        circle.radius = CLLocationDistance(currentValue * Constants.FeetsForMeters) // Feets to meters
        
        circle.fillColor = UIColor.blue.withAlphaComponent(0.1)
        circle.position = firstLocValue!// Your CLLocationCoordinate2D  position
        circle.strokeWidth = 1;
        circle.strokeColor = UIColor.black
        circle.map = addFenceMapView; // Add it to the map
    }
    
    
    func CustomDesignButtonsMethod() {
        
        alertCancelBtn.layer.cornerRadius = 25
        alertCancelBtn.layer.borderWidth = 0.1
        alertCancelBtn.layer.masksToBounds = true
        
        alertOkayBtn.layer.cornerRadius = 25
        alertOkayBtn.layer.borderWidth = 0.1
        alertOkayBtn.layer.masksToBounds = true
    }
    // MARK: - Button Events
    @IBAction func alertCancelBtnActn(_ sender: Any) {
        fenceBgView.isHidden = true
        alertTxtFld.text = nil
    }
    @IBAction func alertOkayBtnActn(_ sender: Any) {
        
        if alertTxtFld.text == nil || (alertTxtFld.text?.isEmpty)!
        {
            self.showAlert(title: "", msg: "Fence name should not empty")
            return
        }
        
        for each in self.IndividualMemberFenceresponse
        {
            for eachs in each
            {
                
                if alertTxtFld.text?.lowercased() == eachs.name?.lowercased()
                {
                    
                    alertTxtFld.text = nil
                    let alertController = UIAlertController(title: "", message: "This fence name already exists", preferredStyle: .alert)
                    
                    let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .cancel, handler: nil)
                    alertController.addAction(cancelAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    return
                    
                }
            }
            
        }
        
        let rect = GMSMutablePath()
        
        for each in self.geoFanceLocations{
            rect.add(each)
        }
        
        self.polygon.path = rect
        self.polygon.fillColor = UIColor.blue.withAlphaComponent(0.1)
        self.polygon.strokeColor = UIColor.black
        self.polygon.strokeWidth = 1
        self.polygon.map = self.addFenceMapView
        
        if self.geoFenceSlider.value == 0{
            self.sendFenceDetailsToServer(withCircular: false,Value: self.fenceradius,name:alertTxtFld.text!)
            fenceBgView.isHidden = true
            alertTxtFld.text = nil
        }
        else{
            
            self.sendFenceDetailsToServer(withCircular: true,Value: self.fenceradius,name: alertTxtFld.text!)
            fenceBgView.isHidden = true
            alertTxtFld.text = nil
        }
        self.geoFanceLocations.removeAll()
        
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        geoFenceSlider.value = 0
        circle.radius = 0
        firstLocValue = coordinate
        
        if geoFanceLocations.count == 0{
            addFenceMapView.clear()
        }
       
        let position = coordinate
        let marker = GMSMarker(position: position)
        marker.icon = #imageLiteral(resourceName: "pin")
        marker.map = addFenceMapView
        geoFanceLocations.append(coordinate)
        
        
        let rect = GMSMutablePath()

        for each in self.geoFanceLocations{
            
            rect.add(each)
       
            print(geoFanceLocations.count)
            
        }

        if geoFanceLocations.count == 2
            
        {
            
            let rectangle = GMSPolyline(path: rect)
            
            rectangle.strokeColor = UIColor.black
            
            rectangle.strokeWidth = 2
            
            rectangle.map = addFenceMapView
            
            
            
            return
            
        }
        
        
        
        
        
        
        
        self.polygon.path = rect
        
        self.polygon.fillColor = UIColor.clear
        
        self.polygon.strokeColor = UIColor.black
        
        self.polygon.strokeWidth = 2
        
        self.polygon.map = self.addFenceMapView
        
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        // let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        if firstLocValue == nil{
            firstLocValue = manager.location!.coordinate
            addFenceMapView.camera = GMSCameraPosition.camera(withTarget: firstLocValue!, zoom: 18.0) // show your device location on map
        }
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        
        print(place.name)
        print(place.formattedAddress)
        print(place.coordinate)
        locationSearchFiels.text = place.name
        firstLocValue = place.coordinate
        
        let position = firstLocValue
        let marker = GMSMarker(position: position!)
        marker.map = addFenceMapView
        addFenceMapView.camera = GMSCameraPosition.camera(withTarget: firstLocValue!, zoom: 18.0) // show your device location on map
        
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        showPlacesSDK()
        return false
    }
    
    
}

