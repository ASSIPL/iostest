//
//  ConfirmPermissionsVC.swift
//  CloudPeppers
//
//  Created by Allvy on 03/05/19.
//  Copyright © 2019 Allvy. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import Firebase
class ConfirmPermissionsVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIPopoverPresentationControllerDelegate {
    
    
    
    @IBOutlet var alertView: UIView!
    
    
    @IBOutlet var alertViewLabel: UILabel!
    
    
    
    
    @IBOutlet weak var featureBackHeightVi: NSLayoutConstraint!
    @IBOutlet weak var infoViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var confirmPerTV: UITableView!
    @IBOutlet weak var displayView: UIView!
    
    var selectButton: UIButton? = nil
     var PermissionsDataArrayNormal:Array = [String]()
    var confirmTextArray:Array = [String]()
    var menuNameArray:Array = [String]()
    var iconsArray:Array = [UIImage]()
    
    var menuNameArray1:Array = [String]()
    var iconsArray1:Array = [UIImage]()
    
    
    @IBOutlet var featureInfoView: UIView!
    
    @IBOutlet var featureInfoTableView: UITableView!
    
    
    @IBOutlet weak var noItemLabel: UILabel!
    
    @IBOutlet var popUpBgViewObj: UIView!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var cancelPopUpBtn: UIButton!
    @IBOutlet weak var yesPopUpBtn: UIButton!
    @IBOutlet weak var confirmPopUpTblView: UITableView!
    @IBOutlet weak var popUpDescLbl: UILabel!
    
    var fromNotification:String?
    var notificationId:Int?
    var fromNotificationtap:String?
    
    
    var membername:String?
    
    var selectedArray:Array = [String]()
    
    var PermissionsDispArray:Array = [String]()
    var PermissionsDataArray:Array = [String]()
    
    var approve:String?
    var fromOrTo:String?
    var memberId:Int?
    
    var reponsedata:responses?
    var allNotifications:allNotifications?
    
    var permissionlist:Permissionlist?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        self.alertView.center.x = self.view.center.x
        self.alertView.center.y = self.view.center.y
        
        
        self.featureInfoTableView.estimatedRowHeight = 100.0; // put max you expect here.
        self.featureInfoTableView.rowHeight = UITableView.automaticDimension;
        self.popUpBgViewObj.center.x = self.view.center.x
        self.popUpBgViewObj.center.y = self.view.center.y
        
        self.featureInfoView.center.x = self.view.center.x
        self.featureInfoView.center.y = self.view.center.y
        
        featureInfoTableView.delegate = self
        featureInfoTableView.dataSource = self
        
        featureInfoView.frame = self.view.frame
        alertView.frame = self.view.frame
        
        alertView.isHidden = true
        featureInfoView.isHidden = true
        featureInfoView.layer.cornerRadius = 20
        featureInfoView.clipsToBounds = true
        featureInfoView.layer.masksToBounds = true
        featureInfoView.dropShadow()
        
        
        popUpBgViewObj.frame = self.view.frame
        
        popUpBgViewObj.isHidden = true
        popUpBgViewObj.layer.cornerRadius = 20
        popUpBgViewObj.clipsToBounds = true
        popUpBgViewObj.layer.masksToBounds = true
        popUpView.dropShadow()

       
        
        noItemLabel.isHidden = true
        GoogleAnalytics.screen(screen: AnalyticsEventCategory.HomeScreen, event: .PermissionScreenEntered)
        //        vc.approve = "approve"
        //        vc.fromOrTo = "fromyou"
        Analytics.logEvent("PermissionScreenEntered", parameters: nil)
        
        navigationController?.navigationBar.barTintColor = Constants.colour
        self.navigationItem.title = "Confirm Permissions "
        self.changeFontForViewController()
        
        if  fromOrTo == "toyou" && approve == "pending"
        {
            // self.navigationItem.title = "Confirm Permissions "
            
            confirmButton.isHidden = true
        }
        if  fromOrTo == "fromyou" && approve == "pending"
        {
            self.navigationItem.title = "Confirm Permissions "
        }
        
        
        if   fromOrTo == "fromyou" && approve == "approve"
        {
            
            
            
            if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type" || UserDefaults.standard.string(forKey: "roleKey") == "thing.type"
            {
                confirmButton.isHidden = true
                
                confirmButton.setTitle("Confirm", for: .normal)
            }
            else{
                confirmButton.isHidden = false
                confirmButton.setTitle("Update", for: .normal)
            }
            
            
            // confirmButton.isHidden = false
            
        }
        
        
        
        guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
            let profileData = try? JSONDecoder().decode(profile.self, from: data) else
        {
            
            return
            
        }
        
        let url:String?
        
        if fromNotification == "true"
        {
            url = API.getRequestPermissions + "?notificationId=\(String(describing: (notificationId!)))"
            
            if  UserDefaults.standard.string(forKey: "roleKey") == "parent.type"
            {
                
                if allNotifications?.memberName != nil ||  allNotifications?.memberName == ""
                {
                    confirmTextArray = ["\(String(describing: (allNotifications?.memberName!)!)) has requested below permissions, would you like to confirm?"]
                }else
                {
                    
                    if fromNotificationtap == "true"
                    {
                        confirmTextArray = ["\(String(describing: membername!)) has requested below permissions, would you like to confirm?"]
                    }else
                    {
                        confirmTextArray = ["\(String(describing: (allNotifications?.userName!)!)) has requested below permissions, would you like to confirm?"]
                    }
                    
                    
                    //   confirmTextArray = ["Member has requested below permissions,would you like to confirm?"]
                }
                
            }else
            {
                if allNotifications?.userName != nil ||  allNotifications?.userName == ""
                {
                    confirmTextArray = ["\(String(describing: (allNotifications?.userName!)!)) has requested below permissions, would you like to confirm?"]
                    
                    
                    
                    
                }else
                {
                    if fromNotificationtap == "true"
                    {
                        confirmTextArray = ["\(String(describing: membername!)) has requested below permissions, would you like to confirm?"]
                    }else
                    {
                        confirmTextArray = ["\(String(describing: (allNotifications?.userName!)!)) has requested below permissions, would you like to confirm?"]
                    }
                }
                
                
                
                
            }
            
            
            
        }else
        {
            
            //  self.navigationItem.title = (reponsedata?.firstName!)! + " " + reponsedata!.lastName!
            confirmTextArray = ["\(String(describing: (reponsedata?.firstName!)!)) has requested below permissions,would you like to confirm?"]
            
            if  fromOrTo == "fromyou" && approve == "pending"
            {
                //  confirmTextArray = [""]
                confirmTextArray = ["\(String(describing: (reponsedata?.firstName!)!)) has requested below permissions,would you like to confirm?"]
                
            }
            
            print(fromOrTo)
            print(approve)
            
            url = API.getMemberPermissionToYouFromYou + "?userId=\(String(describing: profileData.userId!))" + "&memberId=\(String(describing: memberId!))" + "&fromOrTo=\(fromOrTo!)" + "&approveOrReject=\(approve!)"
        }
        
        //    print(url)
        
        
        SVProgressHUD.show()
        
        
        HttpWrapper.posts(with: url!, parameters: nil, headers: nil, completionHandler: { (response, error)  in
            
            SVProgressHUD.dismiss()
            //       print(response)
            guard let data = response else { return }
            do {
                let loginRespone = try JSONDecoder().decode(ApiStatuspermission.self, from: data)
                let loginStatus = loginRespone.status
                
                if loginStatus!{
                    
                    
                    
                    self.permissionlist = loginRespone.response
                    
                    //                    for each in (self.permissionlist?.ask)!
                    //                    {
                    //
                    //                        self.selectedArray.append((each.roleType)!)
                    //
                    //
                    //
                    //                    }
                    //                    if self.fromNotification == "true"
                    //                    {
                    //
                    //
                    //
                    //                        for each in (self.permissionlist?.ask)!
                    //                        {
                    //                            self.selectedArray.append(each.roleType!)
                    //                        }
                    //
                    //                    }
                    //                    if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type"
                    //                    {
                    for each in (self.permissionlist?.ask)!
                    {
                        
                        self.selectedArray.append((each.roleType)!)
                        
                        
                        
                    }
                    
                    
                    if  self.permissionlist?.ask?.count ?? 0 == 1 {
                        self.featureBackHeightVi.constant = 220
                        self.infoViewHeightConstant.constant = 180
                    }else if  self.permissionlist?.ask?.count ?? 0 == 2 {
                        self.featureBackHeightVi.constant = 300
                        self.infoViewHeightConstant.constant = 250
                    }else if  self.permissionlist?.ask?.count ?? 0 == 3 {
                        self.featureBackHeightVi.constant = 400
                        self.infoViewHeightConstant.constant = 350
                    }else {
                        self.featureBackHeightVi.constant = 580
                        self.infoViewHeightConstant.constant = 500
                    }
                    
                    //       }
                    
                    
                    if   self.fromOrTo == "fromyou" && self.approve == "approve"
                    {
                        
                        self.selectedArray.removeAll()
                        for each in (self.permissionlist?.ask)!
                        {
                            
                            self.selectedArray.append((each.roleType)!)
                            //                            if self.selectedArray.contains((each.roleType)!) {
                            //                                self.selectedArray.remove(at: self.selectedArray.index(of: (each.roleType)!)!)
                            //                            } else {
                            //                                self.selectedArray.append((each.roleType)!)
                            //                            }
                        }
                        
                        
                        
                        
                    }
                    
                    
                    
                    
                    self.confirmPerTV.reloadData()
                    
                    print(self.permissionlist)
                    
                    
                }
            }
            catch let jsonErr {
                
                
                SVProgressHUD.dismiss()
                print(jsonErr)
                
            }
            
        }){ (error) in
            
            SVProgressHUD.dismiss()
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
            }else{
                self.showAlert(title: "", msg: "No network connection")
            }
        }
        
        
        
        
        displayView.roundedTop()
        
        
        menuNameArray = ["Set Geofence","Lock Device"]
        //   iconsArray = [#imageLiteral(resourceName: "Set Geofence"),#imageLiteral(resourceName: "Lock Device")]
        
        menuNameArray1 = ["Daily Time Limits","Calls & SMS","Alarms & Reminders","Restricted Times","Web Filtering","Panic Button"]
        //   iconsArray1 = [#imageLiteral(resourceName: "Daily Time Limits"),#imageLiteral(resourceName: "Calls & SMS"),#imageLiteral(resourceName: "Alarms & Reminders"),#imageLiteral(resourceName: "Restricted Times"),#imageLiteral(resourceName: "Web Filtering"),#imageLiteral(resourceName: "Panic Button")]
        
        confirmButton.changeButtonCornerRadius()
        
        
    }
    
    @IBAction func removeButtonActionSmall(_ sender: Any) {
        
        
         alertView.removeFromSuperview()
    }
    
    
    
    @IBAction func removeButtonAction(_ sender: Any) {
        
        featureInfoView.removeFromSuperview()
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        //  GoogleAnalytics.screen(screen: AnalyticsEventCategory.HomeScreen, event: .PermissionScreenExited)
        Analytics.logEvent("PermissionScreenExited", parameters: nil)
        
        UserDefaults.standard.set(nil, forKey: "ConfirmNotificationtap")
        UserDefaults.standard.set(nil, forKey: "dataConfirmNotificationtap")
        
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    // MARK: - Table view Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == featureInfoTableView
        {
            
            return 1
        }
        
        if tableView == confirmPopUpTblView
        {
            return 1
        }
        else
        {
            if permissionlist?.give?.count ?? 0 > 0
            {
                  return 3
            }else
            {
                 return 2
            }
          
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == featureInfoTableView
        {
            
            return permissionlist?.ask?.count ?? 0
        }
        
        if tableView == confirmPopUpTblView {
            return self.PermissionsDataArray.count
        }
        else
        {
            
            
            if section == 0
            {
                return confirmTextArray.count
                
            }
            else if section == 1
            {
                
                if permissionlist?.ask?.count ?? 0 == 0
                {
                    confirmButton.isHidden = true
                    noItemLabel.isHidden = false
                }else
                {
                    
                    if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type" || UserDefaults.standard.string(forKey: "roleKey") == "thing.type"
                    {
                        if fromNotification == "true" ||  fromOrTo == "fromyou" && approve == "pending"
                        {
                            confirmButton.isHidden = false
                        }
                        else{
                            confirmButton.isHidden = true
                        }
                        
                    }else
                    {
                        confirmButton.isHidden = false
                    }
                    
                    noItemLabel.isHidden =  true
                }
                
                
                return permissionlist?.ask?.count ?? 0
            }
            else
            {
                
                if fromNotification == "true"
                {
                    return permissionlist?.give?.count ?? 0
                }
                
                if  fromOrTo == "toyou" && approve == "pending"
                {
                    return 0
                }
                if   fromOrTo == "fromyou" && approve == "approve"
                {
                    return 0
                }
                
                return permissionlist?.give?.count ?? 0
            }
            
            
            
            
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == featureInfoTableView
        {
            
            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeatureInfoTableViewCell") as! FeatureInfoTableViewCell
            
            // cell.imageFeature.image = U
            
            if permissionlist?.ask?[indexPath.row].roleType == "Lock Device" || permissionlist?.ask?[indexPath.row].roleType == "UnLock Device"
            {
                cell.imageFeature.image = #imageLiteral(resourceName: "key.lockAndUnlock")
            }
            else{
                
                
                
                cell.imageFeature.image = UIImage(named: (permissionlist?.ask?[indexPath.row].roleType)!)
                
            }
            
            
            
            
            //            Daily Time Limits: [User] will be able to set the time limit that your device will be locked for usage.
            //                Restricted Times: [User] can set time range for your device in which the the device would be locked.
            //            Apps & Games: [User] will be able to allow or block your apps.
            //            Contacts: [User] will have the ability to allow or block your Contacts.
            //            Alarms & Reminders: [User] will have the provision to add alarms & reminders for you.
            //                Set Geofence: [User] will get timely notifications from you if you are inside/outside the fence.
            //            Lock/Unlock Device: [User] will be able to lock/unlock your device.
            //            Battery: [User] will get to know your device battery percentage.
            //            Web Filtering: [User] will have the ability to add, allow and block Web URLs in your device.
            //
            
            var userName:String?
            
            if fromNotification == "true"
            {
                if  UserDefaults.standard.string(forKey: "roleKey") == "parent.type"
                {
                    
                    if allNotifications?.memberName != nil ||  allNotifications?.memberName == ""
                    {
                        
                        
                        userName = allNotifications?.memberName!
                    
                    }else
                    {
                        
                        if fromNotificationtap == "true"
                        {
                            
                            
                                userName = membername!
                      
                        }else
                        {
                            
                             userName = allNotifications?.userName!
                         
                        }
                        
                        
                     
                    }
                    
                }else
                {
                    if allNotifications?.userName != nil ||  allNotifications?.userName == ""
                    {
                     
                        userName = allNotifications?.userName!
                        
                        
                    }else
                    {
                        if fromNotificationtap == "true"
                        {
                            
                                userName = membername!
                       
                        }else
                        {
                            
                              userName = allNotifications?.userName!
                        
                        }
                    }
                    
                    
                    
                    
                }
                
            }else
            {
                
                
                userName = ((reponsedata?.firstName!)!) + " " + ((reponsedata?.lastName!)!)
                
              
            }
            
            if permissionlist?.ask?[indexPath.row].roleType == "Lock Device" || permissionlist?.ask?[indexPath.row].roleType == "UnLock Device"
            {
                 cell.detailsFeature.text = "\(String(describing: (userName)!)) will be able to lock/unlock your device."
            }
            
            if permissionlist?.ask?[indexPath.row].roleType == "key.dailyTimeLimits"
            {
                cell.detailsFeature.text = "\(String(describing: (userName)!)) will be able to set the time limit that your device will be locked for usage."
            }
            if permissionlist?.ask?[indexPath.row].roleType == "key.restrictedTimes"
            {
                cell.detailsFeature.text = "\(String(describing: (userName)!)) can set time range for your device in which the the device would be locked."
            }
            if permissionlist?.ask?[indexPath.row].roleType == "key.appsAndGames"
            {
                cell.detailsFeature.text = "\(String(describing: (userName)!)) will be able to allow or block your apps."
            }
            if permissionlist?.ask?[indexPath.row].roleType == "key.callAndSms"
            {
                cell.detailsFeature.text =  "\(String(describing: (userName)!)) will have the ability to allow or block your Contacts."
            }
            if permissionlist?.ask?[indexPath.row].roleType == "key.alarmAndReminders"
            {
                cell.detailsFeature.text = "\(String(describing: (userName)!)) will have the provision to add alarms & reminders for you."
            }
            if permissionlist?.ask?[indexPath.row].roleType == "key.web.filter"
            {
                cell.detailsFeature.text = "\(String(describing: (userName)!)) will have the ability to add, allow and block Web URLs in your device."
            }
            if permissionlist?.ask?[indexPath.row].roleType == "key.battery"
            {
                cell.detailsFeature.text = "\(String(describing: (userName)!)) will get to know your device battery percentage."
            }
            if permissionlist?.ask?[indexPath.row].roleType == "key.lockAndUnlock"
            {
                cell.detailsFeature.text = "\(String(describing: (userName)!)) will be able to lock/unlock your device."
            }
            if permissionlist?.ask?[indexPath.row].roleType == "key.geo"
            {
                cell.detailsFeature.text = "\(String(describing: (userName)!)) will get timely notifications from you if you are inside/outside the fence."
            }
            
            print(permissionlist?.ask?[indexPath.row].roleType)
            
            
            
            
            for each in   (organizationdetails?.rulez)!
            {
                if each.rulesKey ==  permissionlist?.ask?[indexPath.row].roleType
                {
                    cell.namefature.text  = each.name
                    
                }
                if self.permissionlist?.ask?[indexPath.row].roleType == "Lock Device" || self.permissionlist?.ask?[indexPath.row].roleType == "Unlock Device"
                {
                    
                    cell.namefature.text  = self.permissionlist?.ask?[indexPath.row].roleType
                    
                }
            }
            
            return cell
        }
        
        
        
        
        
        if tableView == confirmPopUpTblView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmPopUpTableViewCell") as! ConfirmPopUpTableViewCell
            
            cell.popUpTitleLbl.text =  self.PermissionsDataArray[indexPath.row]
            
            
            return cell
            
        }else
        {
            
            
            let data = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
            let organizationdetails = try? JSONDecoder().decode(response.self, from: data!)
            if indexPath.section == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmPermissionTextTVCell") as! ConfirmPermissionTextTVCell
                cell.upperTextLabel.text = confirmTextArray[indexPath.row]
                
                if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type" || UserDefaults.standard.string(forKey: "roleKey") == "thing.type"
                {
                    cell.lowerTextLabel.text = ""
                }
                
                //   cell.lowerTextLabel.text = confirmTextArray[indexPath.row]
                
                return cell
            }
            else if indexPath.section == 1
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ApprovePermissionsTVCell") as! ApprovePermissionsTVCell
                
                if permissionlist?.ask?[indexPath.row].roleType == "Lock Device" || permissionlist?.ask?[indexPath.row].roleType == "UnLock Device"
                {
                    cell.approveImageView.image = #imageLiteral(resourceName: "key.lockAndUnlock")
                }
                else{
                    
                    
                    
                    cell.approveImageView.image = UIImage(named: (permissionlist?.ask?[indexPath.row].roleType)!)
                }
                
                if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type" || UserDefaults.standard.string(forKey: "roleKey") == "thing.type"
                {
                    cell.checkButton.isHidden = true
                    
                    
                    
                }else
                {
                    cell.checkButton.isHidden = false
                }
                
                //            if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type"
                //            {
                //                 cell.checkButton.isHidden = true
                //            }
                
                cell.checkButton.setImage(UIImage(named: self.selectedArray.contains((permissionlist?.ask?[indexPath.row].roleType)!) ?  "checknew" : "UnCheck"), for: .normal)
                //            UnCheck
                //            CheckBox
                //
                //            if self.selectedArray.contains((permissionlist?.ask?[indexPath.row].roleType)!)
                //            {
                //
                //                cell.checkButton.setImage(UIImage(named: "CheckBox"), for: .normal)
                //
                //            }else
                //            {
                //                cell.checkButton.setImage(UIImage(named: "UnCheck"), for: .normal)
                //
                //            }
                
                //            if permissionlist?.ask?[indexPath.row].roleType == "Apps & Games"
                //            {
                //             //   cell.checkButton.isEnabled = false
                //                cell.checkButton.setImage(UIImage(named: "UnCheck"), for: .normal)
                //            }else
                //            {
                //                cell.checkButton.isEnabled = true
                //
                //            }
                //
                
                
                
                
                //   cell.approveImageView.image = iconsArray[indexPath.row]
                
                
                for each in   (organizationdetails?.rulez)!
                {
                    if each.rulesKey ==  permissionlist?.ask?[indexPath.row].roleType
                    {
                        cell.approveTextLabel.text  = each.name
                        
                    }
                    if self.permissionlist?.ask?[indexPath.row].roleType == "Lock Device" || self.permissionlist?.ask?[indexPath.row].roleType == "Unlock Device"
                    {
                        cell.approveImageView.image  = #imageLiteral(resourceName: "key.lockAndUnlock")
                        cell.approveTextLabel.text  = self.permissionlist?.ask?[indexPath.row].roleType
                        
                    }
                }
                //     cell.approveTextLabel.text =  permissionlist?.ask?[indexPath.row].roleType
                cell.checkButton.tag = indexPath.row
                if   fromOrTo == "fromyou"  && approve == "approve"
                {
                    //     cell.checkButton.isHidden = true
                    
                    //  confirmButton.isHidden = true
                    print(selectedArray)
                    cell.checkButton.setImage(UIImage(named: self.selectedArray.contains((permissionlist?.ask?[indexPath.row].roleType)!) ?  "checknew" : "UnCheck"), for: .normal)
                    
                    
                    
                    
                }
                
                
                
                if  fromOrTo == "toyou" && approve == "pending"
                {
                    cell.checkButton.isHidden = true
                    confirmButton.isHidden = true
                    
                }
                cell.checkButton.addTarget(self, action: #selector(checkAction), for: .touchUpInside)
                
                
                return cell
            }else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PermissionsGiveToYouCell") as! PermissionsGiveToYouCell
                
                if permissionlist?.give?[indexPath.row].roleType == "Lock Device" ||  permissionlist?.give?[indexPath.row].roleType == "key.lockAndUnlock"
                {
                    cell.PermissionsImageView.image = #imageLiteral(resourceName: "Lock Device")
                }
                else{
                    cell.PermissionsImageView.image = UIImage(named: (permissionlist?.give?[indexPath.row].roleType)!)
                }
                
                //   cell.PermissionsImageView.image = iconsArray1[indexPath.row]
                cell.permissionsTextLabel.text = permissionlist?.give?[indexPath.row].roleType
                return cell
            }
            
            
            
            
            
            
        }
        
        
        
    }
    
    @objc func checkAction(sender: UIButton!) {
        
        
        if self.selectedArray.contains((permissionlist?.ask?[sender.tag].roleType)!) {
            self.selectedArray.remove(at: self.selectedArray.index(of: (permissionlist?.ask?[sender.tag].roleType)!)!)
        } else {
            self.selectedArray.append((permissionlist?.ask?[sender.tag].roleType)!)
        }
        for each in selectedArray
        {
            if each == "key.dailyTimeLimits" || each == "key.restrictedTimes"
            {
                if self.selectedArray.contains("key.lockAndUnlock") || self.selectedArray.contains("Lock Device") {
                    
                } else {
                    
                    for each in (permissionlist?.ask)!
                    {
                        if each.roleType == "key.lockAndUnlock"
                        {
                            self.selectedArray.append("key.lockAndUnlock")
                        }
                        
                        if  each.roleType == "Lock Device"
                        {
                            self.selectedArray.append("Lock Device")
                        }
                        
                        
                        
                    }
                    
                }
                
                
                
            }
            
        }
        //        for each in selectedArray
        //        {
        //            if each == "key.dailyTimeLimits" || each == "key.restrictedTimes"
        //            {
        //                if self.selectedArray.contains("key.lockAndUnlock") {
        //
        //                } else {
        //                    self.selectedArray.append("key.lockAndUnlock")
        //                }
        //            }else
        //            {
        //                if self.selectedArray.contains("Lock Device")
        //                {
        //                    self.selectedArray.remove(at: self.selectedArray.index(of: "Lock Device")!)
        //                }
        //
        //                if self.selectedArray.contains("Unlock Device")
        //                {
        //                    self.selectedArray.remove(at: self.selectedArray.index(of: "Unlock Device")!)
        //                }
        //            }
        //        }
        confirmPerTV.reloadData()
        
        print(selectedArray)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if tableView == confirmPopUpTblView
        {
            return 50
            
        }else
        {
            
            if tableView == featureInfoTableView
            {
                
                return 110
            }
            
            if indexPath.section == 0
            {
                
                
                
                if fromOrTo == "toyou"  || fromOrTo == "fromyou" && approve == "approve"
                {
                    return 0
                }else
                {
                    
                    if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type" || UserDefaults.standard.string(forKey: "roleKey") == "thing.type"
                    {
                        return  50
                        
                    }
                    return 70
                }
                
                
            }
            else
            {
                if fromNotification == "true"
                {
                    return 80
                }
                
                return 60
            }
            
        }
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        
        if tableView == featureInfoTableView
        {
            
            return ""
        }
        
        
        if section == 0
        {
            return ""
        }
        else if section == 1
        {
            
            if fromNotification == "true"
            {
                //                if  UserDefaults.standard.string(forKey: "roleKey") == "parent.type"
                //                {
                //                    return "Approved Permissions to \(String(describing: (allNotifications?.userName!)!))"
                //                }else
                //                {
                //
                //
                //
                //                    return "Approved Permissions to \(String(describing: (allNotifications?.userName)!))"
                //
                //                }
                
                return "Pending Permissions from me"
                
            }else
            {
                
                
                if  fromOrTo == "toyou" && approve == "pending"
                    
                {
                    
                    if fromNotification == "true"
                    {
                        
                        return "Pending Permissions from me"
                    }
                    else
                    {
                        self.navigationItem.title = (reponsedata?.firstName!)! + " " + reponsedata!.lastName!
                        
                        
                        if  fromOrTo == "fromyou" || approve == "pending"
                            
                        {
                            return "Pending Permissions from \((reponsedata?.firstName!)! + " " + reponsedata!.lastName!)"
                        }
                        
                        return "Pending Permissions from \(String(describing: (reponsedata?.firstName!)!))  \(String(describing: (reponsedata?.lastName!)!))"
                    }
                    
                }
                
                if   fromOrTo == "fromyou" && approve == "approve"
                {
                    selectButton?.isHidden = false
                    self.navigationItem.title = (reponsedata?.firstName!)! + " " + reponsedata!.lastName!
                    return "Permissions given to \(String(describing: (reponsedata?.firstName!)!))  \(String(describing: (reponsedata?.lastName!)!))"
                }
                else
                {
                    selectButton?.isHidden = false
                }
                if   fromOrTo == "fromyou" && approve == "pending"
                {
                    return "Pending Permissions from me"
                }
                
                return "Approved Permissions to \(String(describing: (reponsedata?.firstName!)!))"
            }
            
        }
        else if section == 2
        {
            
            if fromNotification == "true"
            {
                
                
                if  permissionlist?.give?.count ?? 0 > 0
                {
                    return "Permissions given to \(String(describing: (allNotifications?.userName)!))"
                }else
                {
                    return  ""
                }
                return "Permissions given to \(String(describing: (allNotifications?.userName)!))"
            }
            
            
            if  fromOrTo == "toyou" || approve == "pending"
            {
                return  ""
            }
            
            if   fromOrTo == "fromyou" && approve == "approve"
            {
                return  ""
            }
            
            
            if  permissionlist?.give?.count ?? 0 > 0
            {
                return "Request Permissions from you"
            }else
            {
                return  ""
            }
            
        }
        
        return  ""
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        
        if section == 1
        {
            let header = view as? UITableViewHeaderFooterView
            header?.textLabel?.font = UIFont(name: "Montserrat", size: 12)
            //    noDataLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
            header?.textLabel?.textColor = UIColor.white
            header?.textLabel?.textAlignment = .center
            header?.backgroundView?.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            header?.layer.cornerRadius = 10
            header?.clipsToBounds = true
            
       
            for i in 0..<view.subviews.count {
                if view.subviews[i] is UIButton {
                    selectButton = view.subviews[i] as? UIButton
                }
            }
            if selectButton == nil {
                selectButton = UIButton(type: .system)
                header?.addSubview(selectButton!)
                
                
            }
            // Configure button
          //  selectButton?.frame = CGRect(x: view.frame.size.width - 85, y: view.frame.size.height - 28, width: 77, height: 26)
        
             selectButton?.frame = CGRect(x: view.frame.size.width - 35, y: view.frame.size.height - 28, width: 26, height: 26)
            selectButton?.tag = section
          //  selectButton?.setTitle("SELECT ALL", for: .normal)
            
             selectButton?.setImage(#imageLiteral(resourceName: "askper"), for: .normal)
            
            selectButton?.contentHorizontalAlignment = .right;
           // selectButton?.setTitleColor(self.view.tintColor, for: .normal)
            selectButton?.addTarget(self, action: #selector(selectAllInSection), for: .touchUpInside)
            
            
            
            
        }
        else
        {
            let header = view as? UITableViewHeaderFooterView
            header?.textLabel?.font = UIFont(name: "Montserrat", size: 12)
            header?.textLabel?.textColor = UIColor.white
            header?.textLabel?.textAlignment = .center
            header?.layer.cornerRadius = 10
            header?.clipsToBounds = true
            
            
            
            
            
            if  permissionlist?.give?.count ?? 0 > 0
            {
                header?.backgroundView?.backgroundColor = #colorLiteral(red: 0, green: 0.662745098, blue: 0.7176470588, alpha: 1)
            }else
            {
                header?.backgroundView?.backgroundColor = UIColor(displayP3Red: 239.0/255.0, green: 239.0/255.0, blue: 244.0/255.0, alpha: 1)
            }
            
            if  fromOrTo == "toyou" || approve == "pending"
            {
                header?.backgroundView?.backgroundColor = UIColor(displayP3Red: 239.0/255.0, green: 239.0/255.0, blue: 244.0/255.0, alpha: 1)
            }
            if   fromOrTo == "fromyou" && approve == "approve"
            {
                header?.backgroundView?.backgroundColor = UIColor(displayP3Red: 239.0/255.0, green: 239.0/255.0, blue: 244.0/255.0, alpha: 1)
            }
            
            if fromNotification == "true"
            {
                if  permissionlist?.give?.count ?? 0 > 0
                {
                    header?.backgroundView?.backgroundColor = Constants.colour
                }else
                {
                    header?.backgroundView?.backgroundColor = UIColor(displayP3Red: 239.0/255.0, green: 239.0/255.0, blue: 244.0/255.0, alpha: 1)
                }
                
            }
            
            
            
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
    }
    
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        
        return UIModalPresentationStyle.none
        
    }
    
    @objc  func selectAllInSection() {
        
        
        
        
        
        if  fromOrTo == "toyou" && approve == "pending"
        {
//            alertView.isHidden = false
//            alertViewLabel.text = "You should be able to access this permissions once they are approved by \((reponsedata?.firstName!)! + " " + reponsedata!.lastName!)"
//            self.view.addSubview(self.alertView)
            
            
//            let popController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "info") as! ConfirmPopInfoVC
//
//            popController.info = "pending"
//            popController.responseName =  "You should be able to access this permissions once they are approved by \((reponsedata?.firstName!)! + " " + reponsedata!.lastName!)"
//
//            // set the presentation style
//            popController.modalPresentationStyle = UIModalPresentationStyle.popover
//
//            // set up the popover presentation controller
//            popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
//            popController.popoverPresentationController?.delegate = self
//            popController.popoverPresentationController?.sourceView = selectButton // button
//            popController.popoverPresentationController?.sourceRect = selectButton!.bounds
//            popController.preferredContentSize = CGSize(width: self.view.frame.width - 50, height: 70)
//
//            // present the popover
//            self.present(popController, animated: true, completion: nil)
            
            
            
            var popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "info") as! ConfirmPopInfoVC
            
            var nav = UINavigationController(rootViewController: popoverContent)
            
            nav.modalPresentationStyle = UIModalPresentationStyle.popover
            
            let popover = nav.popoverPresentationController
            
            popoverContent.preferredContentSize = CGSize(width:  self.view.frame.width - 150,height: 70)
            popoverContent.info = "pending"
            popoverContent.responseName =  "You should be able to access this permissions once they are approved by \((reponsedata?.firstName!)! + " " + reponsedata!.lastName!)"
            popover?.delegate = self
            
            popover?.sourceView = selectButton
            
            popover?.sourceRect = selectButton!.bounds
            self.present(nav, animated: true, completion: nil)
            
        
            return
        }
        func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
            return .none
        }
        
        if fromOrTo == "fromyou" && approve == "approve"
        {
            
//            alertView.isHidden = false
//            alertViewLabel.text = "\((reponsedata?.firstName!)! + " " + reponsedata!.lastName!) will be able to access the listed permissions."
//            self.view.addSubview(self.alertView)
            
//            let popController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "info") as! ConfirmPopInfoVC
//
//            popController.info = "approve"
//            popController.responseName =  "\((reponsedata?.firstName!)! + " " + reponsedata!.lastName!) will be able to access the listed permissions."
//
//            // set the presentation style
//            popController.modalPresentationStyle = UIModalPresentationStyle.popover
//
//            // set up the popover presentation controller
//            popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
//            popController.popoverPresentationController?.delegate = self
//            popController.popoverPresentationController?.sourceView = selectButton // button
//            popController.popoverPresentationController?.sourceRect = CGRect(x: 0, y:0, width: (selectButton?.frame.size.width)!, height: (selectButton?.frame.size.height)!)
//            popController.preferredContentSize = CGSize(width: 250, height: 70)
//
////            popController.modalTransitionStyle = .crossDissolve
////            popController.modalPresentationStyle = .overCurrentContext
//
//            // present the popover
//            self.present(popController, animated: true, completion: nil)
//

            var popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "info") as! ConfirmPopInfoVC

            var nav = UINavigationController(rootViewController: popoverContent)
            
            nav.modalPresentationStyle = UIModalPresentationStyle.popover
            
            let popover = nav.popoverPresentationController
            
            popoverContent.preferredContentSize = CGSize(width:  self.view.frame.width - 150,height: 70)
            popoverContent.info = "approve"
            popoverContent.responseName =  "\((reponsedata?.firstName!)! + " " + reponsedata!.lastName!) will be able to access the listed permissions."
            popover?.delegate = self
            
            popover?.sourceView = selectButton
            
            popover?.sourceRect = selectButton!.bounds
            self.present(nav, animated: true, completion: nil)
            

            
            return
        }
        
        
        
       
            self.featureInfoView.isHidden = false
            featureInfoTableView.reloadData()
            self.view.addSubview(self.featureInfoView)
            
            featureInfoView.layer.cornerRadius = 20
            featureInfoView.clipsToBounds = true
            featureInfoView.layer.masksToBounds = true
            featureInfoView.dropShadow()
            
        
       
        
    }
    
   
    
    @IBAction func confirmButtonTapped(_ sender: UIButton) {
        
        Analytics.logEvent("Conf_Perm_Confirm_Btn_Clicked", parameters:nil)
        
        
        //        if selectedArray.isEmpty || selectedArray.count == 0
        //        {
        //            showAlert(title: "", msg: "Please select permissions")
        //            return
        //        }
        
        if   fromOrTo == "fromyou" && approve == "approve"
        {
            
            
            //                    if selectedArray.isEmpty || selectedArray.count == 0
            //                    {
            //                        showAlert(title: "", msg: "Please select atleast one permission")
            //                        return
            //                    }
            //
            
            guard let data = UserDefaults.standard.value(forKey: "profile_details") as? Data,
                let profileData = try? JSONDecoder().decode(profile.self, from: data) else
            {
                
                return
                
            }
            let RequestList:NSMutableDictionary = NSMutableDictionary()
            
            RequestList.setValue(profileData.userId, forKey: "userId")
            RequestList.setValue(memberId, forKey: "memberId")
            
            
            let permissionRequestList:NSMutableArray = NSMutableArray()
            
            for each in (permissionlist?.ask)!
            {
                
                let innerRequestList:NSMutableDictionary = NSMutableDictionary()
                
                print(each.roleType)
                print(each.roleId)
                print(each.permissionId)
                print(selectedArray)
                
                if self.selectedArray.contains(each.roleType!)
                {
                    
                    //                    innerRequestList.setValue(each.permissionId, forKey: "permissionId")
                    //                    innerRequestList.setValue(each.roleId, forKey: "roleId")
                    //                    innerRequestList.setValue("approve", forKey: "approveOrReject")
                    
                }else
                {
                    
                    
                    //                    print(each.roleId)
                    //                    print(each.permissionId)
                    
                    
                    innerRequestList.setValue(each.permissionId, forKey: "permissionId")
                    innerRequestList.setValue(each.roleId, forKey: "roleId")
                    innerRequestList.setValue("reject", forKey: "approveOrReject")
                    innerRequestList.setValue("false", forKey: "ask")
                    innerRequestList.setValue("false", forKey: "give")
                    
                    permissionRequestList.add(innerRequestList)
                    
                    print()
                }
                
                
                
                
                
            }
            
            
            
            RequestList.setValue(permissionRequestList, forKey: "permissionRequestList")
            
            print(RequestList)
            
            APIServices.postDataToServer(url: API.editPermission, parameters: RequestList, controller : self, headers1: nil,completionHandler: { (status,response) -> Void in
                if status == true{
                    
                    
                    let respons = response as? [String:Any]
                    
                    //   let statusCode = respons?["statusCode"] as?  Int
                    
                    let alertController = UIAlertController(title: "", message: respons?["message"] as? String, preferredStyle: .alert)
                    
                    
                    let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                        
                        let revealViewController:SWRevealViewController = self.revealViewController()
                        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let desController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        let newFrontViewController = UINavigationController.init(rootViewController:desController)
                        revealViewController.pushFrontViewController(newFrontViewController, animated: true)
                        
                        
                    }
                    
                    alertController.addAction(settingsAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                    
                }else
                {
                    
                    
                    
                }
            })
            
            
            
            
            
            
            
            return
        }
        
        
        
        
        
        callpermisionapi()
        
        
        
        
        //            print(permissionRequestList)
        //        guard let data = try? JSONSerialization.data(withJSONObject: permissionRequestList, options: []) else {
        //            return
        //        }
        //        let paramString = String(data: data, encoding: String.Encoding.)
        //
        //
        //      //  request.httpBody = paramString?.data(using: .utf8)
        //
        //        request.httpBody = try! JSONSerialization.data(withJSONObject: permissionRequestList)
        //
        //        Alamofire.request(request).responseJSON { (response) in
        //
        //
        //
        //            print(response)
        //
        //        }
        //
        //
        //        APIServices.postDataToServerp(url: API.grantPermission, parameters: permissionRequestList as! [[String : Any]], controller : self,completionHandler: { (status,response) -> Void in
        //
        //
        //                if status == true{
        //
        //                    let alertController = UIAlertController(title: Constants.alertTitle, message: "Updated Successfully", preferredStyle: .alert)
        //
        //
        //                    let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
        //
        //                        if let viewControllers = self.navigationController?.viewControllers {
        //                            if viewControllers.count > 1 {
        //                                self.navigationController?.viewControllers.remove(at: viewControllers.count - 2)
        //
        //
        //
        //                            } else {
        //                                // fail
        //                            }
        //                        }
        //
        //                        self.navigationController?.popViewController(animated: true)
        //
        //
        //
        //
        //                    }
        //
        //                    alertController.addAction(settingsAction)
        //                    self.present(alertController, animated: true, completion: nil)
        //
        //                    print(response)
        //
        //                }
        //            })
        //
        
        
    }
    
    func  callpermisionapi()
    {
        
        let permissionRequestList:NSMutableArray = NSMutableArray()
        //  let RequestList:NSMutableDictionary = NSMutableDictionary()
        for each in (permissionlist?.ask)!
        {
            let innerRequestList:NSMutableDictionary = NSMutableDictionary()
            print(selectedArray)
            
            if self.selectedArray.contains(each.roleType!)
            {
                
                innerRequestList.setValue(each.permissionId, forKey: "permissionId")
                //      innerRequestList.setValue(each.r, forKey: "permissionId")
                innerRequestList.setValue("approve", forKey: "approvedOrRejected")
                
            }else
            {
                innerRequestList.setValue(each.permissionId, forKey: "permissionId")
                innerRequestList.setValue("reject", forKey: "approvedOrRejected")
            }
            
            
            
            permissionRequestList.add(innerRequestList)
            
        }
        
        
        print(permissionRequestList)
        
        
        let parameters: [[String: Any]] = permissionRequestList as! [[String : Any]]
        var request = URLRequest(url: URL(string: API.grantPermission)!)
        request.httpMethod = "POST"
        let id = UserDefaults.standard.string(forKey: "jwtToken")
        
        if id != nil && id != ""
        {
            let token = "Bearer" + " "  + "\(UserDefaults.standard.string(forKey: "jwtToken")!)"
            
            print(token)
            request.setValue(token, forHTTPHeaderField: "Authorization")
        }
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let dataToSync = parameters
        request.httpBody = try! JSONSerialization.data(withJSONObject: dataToSync)
        
        let data2 = UserDefaults.standard.value(forKey: "organizationdetails") as? Data
        let organizationdetails = try? JSONDecoder().decode(response.self, from: data2!)
        Alamofire.request(request).responseJSON { (response) in
            
            //   print("Success: \(response.data)")
            
            
            switch response.result{
            case .success:
                let statusCode: Int = (response.response?.statusCode)!
                switch statusCode{
                case 200:
                    
                    guard let data = response.data else { return }
                    do {
                        let loginRespone =  try JSONDecoder().decode(ApiStatus12.self, from: data)
                        
                        let loginStatus = loginRespone.status
                        
                        print(loginRespone.status)
                        if loginStatus!
                        {
                            
                            
                            let alertController = UIAlertController(title: "", message: loginRespone.message!, preferredStyle: .alert)
                            
                            
                            let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                                
                                //                                if let viewControllers = self.navigationController?.viewControllers {
                                //                                    if viewControllers.count > 1 {
                                //                                        self.navigationController?.viewControllers.remove(at: viewControllers.count - 1)
                                //
                                //
                                //
                                //                                    } else {
                                //                                        // fail
                                //                                    }
                                //                                }
                                //
                                //                                self.navigationController?.popViewController(animated: true)
                                //
                                let revealViewController:SWRevealViewController = self.revealViewController()
                                let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let desController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                let newFrontViewController = UINavigationController.init(rootViewController:desController)
                                revealViewController.pushFrontViewController(newFrontViewController, animated: true)
                                
                                
                                
                            }
                            
                            alertController.addAction(settingsAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                        }
                        else
                        {
                            var string = String()
                             self.PermissionsDataArrayNormal = (loginRespone.response?.invalidpermissions!)!
                            
                            for each in (loginRespone.response?.invalidpermissions!)!
                            {
                                string.append("\n")
                                
                                
                                for each1 in (organizationdetails?.rulez)!
                                {
                                    
                                    if each1.rulesKey == each
                                    {
                                        string.append(each1.name!)
                                    }
                                }
                                
                                
                                
                            }
                            
                            
                            if  UserDefaults.standard.string(forKey: "roleKey") == "kid.type" || UserDefaults.standard.string(forKey: "roleKey") == "thing.type"
                            {
                                
                                self.popUpBgViewObj.isHidden = false
                                self.view.addSubview(self.popUpBgViewObj)
                                self.CustomPopUpDesignViews()
                                
                                self.popUpDescLbl.text = loginRespone.message!
                                
                                self.PermissionsDispArray = (loginRespone.response?.invalidpermissions!)!
                                
                                
                                for each in (loginRespone.response?.invalidpermissions!)!
                                {
                                    
                                    for each1 in (organizationdetails?.rulez)!
                                    {
                                        
                                        if each1.rulesKey == each
                                        {
                                            self.PermissionsDataArray.append(each1.name!)
                                        }
                                    }
                                }
                                
                                
                                self.confirmPopUpTblView.delegate = self
                                self.confirmPopUpTblView.dataSource = self
                                self.confirmPopUpTblView.reloadData()
                                
                                
                                /*
                                 let alertController = UIAlertController(title: "", message: loginRespone.message! + "\n"  + string, preferredStyle: .alert)
                                 
                                 let yesAction = UIKit.UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .cancel) { (UIAlertAction) in
                                 print(self.selectedArray)
                                 for each in loginRespone.response.invalidpermissions!
                                 {
                                 
                                 if self.selectedArray.contains(each)
                                 {
                                 
                                 self.selectedArray.remove(at: self.selectedArray.index(of: each)!)
                                 
                                 }
                                 
                                 
                                 
                                 }
                                 self.callpermisionapi()
                                 print(self.selectedArray)
                                 
                                 
                                 }
                                 
                                 
                                 let noAction = UIKit.UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default) { (UIAlertAction) in
                                 let revealViewController:SWRevealViewController = self.revealViewController()
                                 let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                 let desController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                 let newFrontViewController = UINavigationController.init(rootViewController:desController)
                                 revealViewController.pushFrontViewController(newFrontViewController, animated: true)
                                 
                                 
                                 }
                                 alertController.addAction(yesAction)
                                 
                                 alertController.addAction(noAction)
                                 self.present(alertController, animated: true, completion: nil) */
                                
                                
                                return
                                
                            }
                            
                            
                            
                            
                            let alertController = UIAlertController(title: "", message: loginRespone.message! + "\n"  + string, preferredStyle: .alert)
                            
                            let settingsAction = UIKit.UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { (UIAlertAction) in
                            }
                            let settingsAction1 = UIKit.UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                                
                                
                                
                                for each in self.PermissionsDataArrayNormal
                                    
                                {
                                    
                                    
                                    
                                    if self.selectedArray.contains(each)
                                        
                                    {
                                        
                                        
                                        
                                        self.selectedArray.remove(at: self.selectedArray.index(of: each)!)
                                        
                                        
                                        
                                    }
                                    
                                    
                                    
                                }
                                
                                
                                
                                self.confirmPerTV.reloadData()
                                
                            }
                            
                            alertController.addAction(settingsAction1)
                            alertController.addAction(settingsAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                        
                    }catch
                    {
                        print(error)
                        
                        
                    }
                    
                    
                    //
                    //
                    
                    break
                default:
                    
                    break
                }
                break
            case .failure:
                
                break
            }
        }
        
    }
    
    // MARK: - Custom Methods
    func CustomPopUpDesignViews()  {
        
        cancelPopUpBtn.layer.cornerRadius = 20
        cancelPopUpBtn.layer.borderWidth = 0.1
        cancelPopUpBtn.layer.masksToBounds = true
        
        yesPopUpBtn.layer.cornerRadius = 20
        yesPopUpBtn.layer.borderWidth = 0.1
        yesPopUpBtn.layer.masksToBounds = true
        
    }
    // MARK: - Button Events
    @IBAction func cancelPopUpBtnActn(_ sender: Any) {
        //    self.popUpBgViewObj.isHidden = true
        Analytics.logEvent("Conf_Perm_Popup_No_Clicked", parameters:nil)
        
        let revealViewController:SWRevealViewController = self.revealViewController()
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let newFrontViewController = UINavigationController.init(rootViewController:desController)
        revealViewController.pushFrontViewController(newFrontViewController, animated: true)
    }
    @IBAction func yesPopUpBtnActn(_ sender: Any) {
        
        Analytics.logEvent("Conf_Perm_Popup_Yes_Clicked", parameters:nil)
        
        for each in self.PermissionsDispArray
        {
            
            if self.selectedArray.contains(each)
            {
                
                self.selectedArray.remove(at: self.selectedArray.index(of: each)!)
                
            }
            
        }
        
        print("get final values : \(self.selectedArray)")
        
        self.callpermisionapi()
        
    }
    
}

