//
//  Analytics+API.swift
//  unfoldprints
//
//  Created by Abhishek on 31/07/17.
//  Copyright © 2017 unfoldlabs. All rights reserved.
//

import UIKit
import MapKit



class Analytics1: NSObject {
    
    
    class func getParamData() -> [String: Any] {
        //
        var params: [String: Any] = [:]
        //
        params["imei"] = UIHelper.getUUID()
        params["token"]  = ""
        
        return params
    }
    class func getParamDevice() -> [String: Any] {
        //
        var params: [String: Any] = [:]
        
        params["imei"] = UIHelper.getUUID()
        params["deviceName"] = UIDevice.current.name
        params["deviceManufacturer"] = "Apple"
        params["deviceInternalStorage"] = DiskStatus.totalDiskSpace
        params["deviceRam"] =  DiskStatus.physicalStorage
        params["basebandVersion"] =  ProcessInfo().operatingSystemVersionString
        params["serialNumber"] = ""
        params["macAddress"] = ""
        params["blutoothAddress"] = ""
        params["deviceId"] = UIDevice.current.model
        params["cpuMake"] = "32"
        params["cpuModel"] = ""
        params["screenSize"] = ""
        params["screenDisplay"] = ""
        params["deviceType"] =  UIDevice.current.model
        params["deviceLanguage"] = NSLocale.current.identifier
        params["typeOfDevice"] = "iOS"
        
        return params
    }
    
    
    class func getMoreParamDevice() -> [String:Any]
    {
        var params: [String: Any] = [:]
        params["imei"] = UIHelper.getUUID()
        params["osVersion"] = ProcessInfo().operatingSystemVersionString
        params["deviceName"] =  UIDevice.current.name
        params["kernelVersion"] =  Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        params["deviceHardware"] =  ""
        params["deviceSoftwareVersion"] = ProcessInfo().operatingSystemVersionString
        params["osUpdateDate"] = ""
        params["sdk"] = ""
        params["typeOfDevice"] = "iOS"   //UIDevice.current.model  //
        
        
        return params
        
    }
    
    //
    class func sendAnalytics() {
        //        APIServices.registerUser(completion: { (response) in
        //        }) { (error) in
        //        }
        let params = self.getParamData()
        let getParamDevice = self.getParamDevice()
        let getMoreParamDevice = self.getMoreParamDevice()
        
        print(getParamDevice)
        print(getMoreParamDevice)
        
        
        
        
        if !UserDefaults.standard.bool(forKey: "launchedBefore")
        {
            
            
            HttpWrapper.post(with: API.devicedetails, parameters: getParamDevice, headers: nil, completionHandler: { (response) in
                print(response)
                
            }) { (error) in
                //sendAnalytics()
                let alert = UIAlertController(title: "Alert", message: "Unable to connect, Please try Again", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertAction.Style.default, handler: { action in
                    sendAnalytics()
                }))
                UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                
                
                
            }
            HttpWrapper.post(with: API.deviceosdetails, parameters: getMoreParamDevice, headers: nil, completionHandler: { (response) in
                print(response)
                
                
            }) { (error) in
                
                
            }
            
            
        }
        
        
        if !UserDefaults.standard.bool(forKey: "launchedBefore")
        {
            HttpWrapper.post(with: API.token, parameters: params, headers: nil, completionHandler: { (response) in
                print(response)
                
                
                //                if response["imei"] != nil{
                //                    UserDefaults.standard.set(true, forKey: "launchedBefore")
                //
                //                    UserDefaults.standard.set(response["imei"], forKey: "imei")
                //                    UserDefaults.standard.set(response["token"], forKey: "token")
                //
                //                }
                //                else{
                //                    let alert = UIAlertController(title: "Alert", message: "Unable to connect, Please try Again", preferredStyle: UIAlertController.Style.alert)
                //                    alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertAction.Style.default, handler: { action in
                //                        sendAnalytics()
                //                    }))
                //                    UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                //                }
                // UserDefaultsHelper.SetObject(for: "imei", value: response["imei"] as Any)
                // UserDefaultsHelper.SetObject(for: "token", value: response["token"] as Any)
                HttpWrapper.post(with: API.devicedetails, parameters: getParamDevice, headers: nil, completionHandler: { (response) in
                    print(response)
                    
                }) { (error) in
                    //sendAnalytics()
                    
                    
                    
                }
                HttpWrapper.post(with: API.deviceosdetails, parameters: getMoreParamDevice, headers: nil, completionHandler: { (response) in
                    print(response)
                    
                    
                }) { (error) in
                    
                    
                    
                }
                
                
                
                
                
                //  UserDefaults.standard.set(true, forKey: "launchedBefore")
                
                
            }) { (error) in
                //                let alert = UIAlertController(title: "Alert", message: "Unable to connect, Please try Again", preferredStyle: UIAlertController.Style.alert)
                //                alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertAction.Style.default, handler: { action in
                //                    sendAnalytics()
                //                }))
                //                UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)        }
                
            }
            
        }
        
        
        
    }
    
}


class DiskStatus {
    //MARK: Formatter MB only
    class func MBFormatter(_ bytes: Int64) -> String {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = ByteCountFormatter.Units.useMB
        formatter.countStyle = ByteCountFormatter.CountStyle.decimal
        formatter.includesUnit = false
        return formatter.string(fromByteCount: bytes) as String
    }
    
    //MARK: Get String Value
    class var totalDiskSpace:String {
        get {
            return ByteCountFormatter.string(fromByteCount: totalDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.binary)
        }
    }
    
    class var freeDiskSpace:String {
        get {
            return ByteCountFormatter.string(fromByteCount: freeDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.binary)
        }
    }
    
    class var usedDiskSpace:String {
        get {
            return ByteCountFormatter.string(fromByteCount: usedDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.binary)
        }
    }
    
    //MARK: Get raw value
    class var totalDiskSpaceInBytes:Int64 {
        get {
            do {
                let systemAttributes = try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String)
                let space = (systemAttributes[FileAttributeKey.systemSize] as? NSNumber)?.int64Value
                return space!
            } catch {
                return 0
            }
        }
    }
    
    class var freeDiskSpaceInBytes:Int64 {
        get {
            do {
                let systemAttributes = try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String)
                let freeSpace = (systemAttributes[FileAttributeKey.systemFreeSize] as? NSNumber)?.int64Value
                return freeSpace!
            } catch {
                return 0
            }
        }
    }
    
    class var usedDiskSpaceInBytes:Int64 {
        get {
            let usedSpace = totalDiskSpaceInBytes - freeDiskSpaceInBytes
            return usedSpace
        }
    }
    
    class var physicalStorage: String {
        get {
            let memory = ProcessInfo.processInfo.physicalMemory/(1024 * 1024 * 1024)
            print("MEMORY \(memory)")
            return "\(memory) GB"
        }
    }
}

