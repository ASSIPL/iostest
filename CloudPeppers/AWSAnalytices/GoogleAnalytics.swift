//
//  GoogleAnalytics.swift
//  2020Wallet
//
//  Created by Ashok Reddy G on 20/02/18.
//  Copyright © 2018 Ashok Reddy G. All rights reserved.
//

import UIKit

class GoogleAnalytics: NSObject {
    class func initiate() {
        
        
        guard let gai = GAI.sharedInstance() else {
            assert(false, "Google Analytics not configured correctly")
            return
        }
        
        
        gai.tracker(withTrackingId: "")
        
        
        gai.trackUncaughtExceptions = true
        
    }
    public static let shared = GoogleAnalytics()
    class func track(category: String, event: AnalyticsEvents) {
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: event.description)
        
        tracker.send(GAIDictionaryBuilder.createEvent(withCategory: category,
                                                      action: event.description,
                                                      label: event.description,
                                                      value: nil).build() as! [AnyHashable : Any]!)
    }
    
    class func screen(screen: String, event: AnalyticsEvents) {
        
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: event.description)
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
}
public struct AnalyticsEventCategory {
    static let HomeScreen               =   "Home Screen"
    static let PermissionScreen                   =   "Permission Screen"
    
    
    
}

public enum AnalyticsEvents: CustomStringConvertible {
    
    case HomeScreenEntered
    case HomeScreenExited
    
    
    case PermissionScreenEntered
    case PermissionScreenExited
    
    
    
    
    
    public var description: String {
        
        switch self {
        case .HomeScreenEntered:
            return "HomeScreen - Entered"
        case .HomeScreenExited:
            return "HomeScreen - Exited"
            
        case .PermissionScreenEntered:
            return "PermissionScreen - Entered"
        case .PermissionScreenExited:
            return "PermissionScreen - Exited"
            
            
        default:
            return ""
        }
    }
    
    public var parameters: [String: Any]? {
        switch self {
        default:
            return [:]
        }
    }
}
